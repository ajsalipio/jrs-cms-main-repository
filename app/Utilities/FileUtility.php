<?php

namespace App\Utilities;

class FileUtility
{
    
    /**
     * Utility constructor
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Store uploaded file to /storage/tmp/ 
     * 
     * @author Sally Jean Sumeguin < sally_sumeguin@commude.ph
     * @since  2018/05/28 Louie: Refractored to be generic
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return String
     **/
    public function storeTempFile($requestFile)
    {
        $fileOriginalName = time() . $requestFile->getClientOriginalName();
        $fileExtension = $requestFile->getClientOriginalExtension();
        $filename = md5($fileOriginalName) . "." . $fileExtension;
        $requestFile->storeAs('tmp', $filename, 'public');
        
        return [
            'path' => "/storage/tmp/$filename",
            'original' => $requestFile->getClientOriginalName()
        ];
    }

    /**
     * Convert csv to array helper
     *
     * @param  array  $object
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @since  2018/07/19
     * 
     * @param  string $fileName
     * @param  string $delimiter (optional)
     * 
     * @return Array
     */
    public function getCsVAsArray($filename = '', $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename))
                return false;

        $header = null;
        $data = array();

        if (($handle = fopen($filename, 'r')) !== false) {

                while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {                   
                    if (!$header){
                        $header = $row;
                    }
                    else{
                        $data[] = array_combine($header, $row);
                    }                  
                }
                fclose($handle);
        }

        return $data;
    }

}