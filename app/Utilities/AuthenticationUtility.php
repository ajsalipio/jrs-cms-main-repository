<?php

namespace App\Utilities;

use App\User;

use Illuminate\Support\Facades\Hash;

use Cookie;

class AuthenticationUtility
{
    
    /**
     * Utility constructor
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Remember me functionality
     * 
     * Gets user from a cookie
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @since  2018/06/14
     * 
     * @param  string $cookie
     * @return User
     **/
    public function getUserFromCookie($cookie = null)
    {
        $user = null;

        $cookie = ($cookie)
                ? $cookie
                : Cookie::get(config('constants.LOGIN_COOKIE_ID'));
        
        if($cookie){
            $user = User::where('id', $cookie)
                        ->select('id', 'email')
                        ->first()
                        ->toArray();

            // Use id as hash key and password
            $user['id'] = Hash::make($user['id']); 

        }

        return $user;
    }

    /**
     * Cross check password hashed if cookie exist. 
     * 
     * This method utilizes cookie hash check from user id. 
     * Hash check true will use cookie.
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @since  2018/06/14
     * 
     * @param  string $cookie
     * @return User
     **/
    public function getUserFromEncryption(string $userId)
    {
        $user    = null;
        $cookie = Cookie::get(config('constants.LOGIN_COOKIE_ID'));

        try {
            if(Hash::check($cookie, $userId)){
                
                $userId = $cookie;
                $user   = User::find($userId);
            }
                
        } catch (DecryptException $e) {
            
        }

        return $user;

    }

}