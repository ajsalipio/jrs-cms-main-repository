<?php

namespace App\Utilities;

use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;

use Route;

/**
 * Pagination utility for joined tables
 *
 * LengthawarePaginator is utilized to enable pagination
 * when multiple objects/entiries are joined from a DB transaction
 *
 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @copyright Commude Philippines Inc. 2018
 * 
 */
class PaginationUtility{

	/**
     * Custom Pagination for any object
     *
     *
     * @param  array  $object
     * @param  string $routePath (url_tail)
     * @param  array  $queryParams
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @since  2018/03/14
     * 
     * @return LengthAwarePaginator instance of the custom object
     */
    public static function getObjectPagination($objects, $queryParams=null,  $currentPage = null,$routePath = null)
    {
        $path = ($routePath == null)
              ? Route::getCurrentRequest()->getPathInfo() // raw path without query params
              : $routePath;

        $currentPage = ($currentPage)
                     ? $currentPage
                     : LengthAwarePaginator::resolveCurrentPage();// Get current page form url e.g. &page=6

        $collection  = new Collection($objects); // Create a new Laravel collection from the array data

        $perPage     =  session('limit') ?? config('constants.PAGINATION.front_pc'); // Define how many items we want to be visible in each page

        // Slice the collection to get the items to display in current page
        $currentPageSearchResults = $collection->slice(($currentPage - 1) * $perPage, $perPage)->all();
       
        // Create the paginator
        $paginator    = new LengthAwarePaginator($currentPageSearchResults, count($collection) 
                                        ,$perPage, $currentPage, ['path' => url($path)]);
      

        if(!is_null($queryParams))
        {
              $paginator->appends($queryParams);
        }

        return $paginator;

	}

}