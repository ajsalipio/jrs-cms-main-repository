<?php

namespace App\Utilities;

class DateUtility
{
    
    /**
     * Utility constructor
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Revised http://forums.devshed.com/php-development-5/check-current-time-times-973364.html.
     * Checks a given time between a start time and and end time in ('H:i') format
     * Cross checks with current time by default if called.
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since  2018/05/28
     * 
     * @return void
     */
    public function isBetweenTime($time = null, $start = null, $end = null ) {
        $timeToCheck = ($time)
                     ? $time
                     : date( 'H:i' );
        
        if ( $start == null ) {
            $start = config('constants.PUBLISH_TIME_OUT_OF_SERVICE')['START']; 
        }
        if ( $end == null ){
            $end = config('constants.PUBLISH_TIME_OUT_OF_SERVICE')['END']; 
        }

        return ( $start <= $timeToCheck && $timeToCheck <= $end );
    }

}