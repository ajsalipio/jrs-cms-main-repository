<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\Situation;
use App\Events\SituationSavedEvent;
use App\Utilities\DateUtility;

use Pusher;
Use Log;

class SituationScheduleFrontUpdateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    public $situation;
    public $dateUtility;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Situation $situation)
    {
        $this->situation   = $situation;
        $this->dateUtility = new DateUtility();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $situation   = $this->situation;
        $currentTime = date('H:i');
        $isOffHours  = $this->dateUtility->isBetweenTime($currentTime);

        // PUSHER SET SETTINGS
        $channel     = config('constants.CHANNELS.situation');
        $event       = config('constants.EVENTS.situation_save');

        $options = array(
            'cluster' => 'ap1',
            'encrypted' => true
        );

        $pusher = new Pusher\Pusher(
            env('PUSHER_APP_KEY'),
            env('PUSHER_APP_SECRET'),
            env('PUSHER_APP_ID'),
            $options
        );

        $viewData    = view()->make('partials.top_situation')->with('situation', $situation)
                                                             ->with('isOffHours', $isOffHours);
        
        $res = $pusher->trigger($channel, $event, $viewData->render());

        // Log::info($res);
    }
}
