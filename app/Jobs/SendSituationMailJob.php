<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

use App\Models\Mail as MailModel;
use App\Models\Situation;
use App\Mail\SituationPublishMail;

use Carbon\Carbon;

/**
 * Job version of Situation Mail sending
 * 
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @copyright 2018 Commude Philippines, Inc.
 *  
 */

class SendSituationMailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     1*
     * @return void
     */
    public function __construct()
    {
         
    }

    /**
     * Execute the job.
     * 
     * Create a new job instance.
     *
     * Use job as single point of entry. Delaying used in Mail::later
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since  2018/05/31
     * 
     * @return void
     */
    public function handle()
    {
       \Artisan::call('command:send_situation_mail'); // Just use the command for the job
    }
}
