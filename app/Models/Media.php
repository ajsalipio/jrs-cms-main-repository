<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    //set the table name
    protected $table = 'media';

    //set guarded columns
    protected $guarded = [];

    //turn off the default model timestamps
    public $timestamps = false;

    //set custom Primary Key
    protected $primaryKey = 'media_id';


}
