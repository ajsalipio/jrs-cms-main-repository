<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';
    protected $primary_key = 'id';
    protected $fillable = [
	'news_title',
	'news_content',
	'file_pdf',
    'is_link',
    'is_sticky',
	'validity',
	'status',
	'category'
    ];


    /**
     * Definition of relationship of news model to media model
     *
     * @author Sally Jean Sumeguin <sally_sumeguin@commude.ph>
     **/
    public function media()
    {
	return $this->belongsTo('App\Models\Media', 'file_pdf', 'media_id');
    }

    /**
     * Returns a concatenated list (by ',') of categories retrieved from constant
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since  2018/06/22
     * 
     * @return string
     */
    public function getCategoryUnionAttribute()
    {
        $categories    = array();
        $categoriesStr = "";
        $categoryIds           = explode(',' , $this->category);

        // Prepare news categories as array
        foreach($categoryIds as $categoryId){
            if(array_key_exists($categoryId, config('constants.NEWS_CATEGORIES')))
            {
                $categories[] = config('constants.NEWS_CATEGORIES')[$categoryId];
            }
        }

        // Build final string
        foreach($categories as $category){
            $categoriesStr .= ($categoriesStr)
                            ? ', ' . $category
                            : $category; 
        }
        
        return $categoriesStr;
    }

    /**
     * Returns a concatenated list (by ',') of categories retrieved from constant
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since  2018/06/22
     * 
     * @return string
     */
    public function getRegionalCategoryUnionAttribute()
    {
        $regionalCategories    = array();
        $regionalCategoriesStr = "";
        $categoryIds           = explode(',' , $this->regional_category);

        // Prepare news categories as array
        foreach($categoryIds as $categoryId){

            $categoryId = trim($categoryId);
            if(array_key_exists($categoryId, config('constants.REGIONAL_NEWS_SUB_CAT')))
            {
                $regionalCategories[] = config('constants.REGIONAL_NEWS_SUB_CAT')[$categoryId];
            }
        }

        // Build final string
        foreach($regionalCategories as $regionalCategory){
            $regionalCategoriesStr .= ($regionalCategoriesStr)
                                   ? ', ' . $regionalCategory
                                   : $regionalCategory; 
        }
        
        return $regionalCategoriesStr;
    }

    /**
     * Accessor for title trimmed length
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since  2018/06/26
     * 
     * @return string
     */
    public function getNewsLabelAttribute()
    {
	 // Doesn't return the expected lenght for unknow reason 
      //  return  mb_strimwidth($this->news_title, 0, 37,  "...",'UTF-8');
	     return(mb_strlen($this->news_title) > 37)
                           ? mb_substr($this->news_title, 0, 37) .  "..."
                           : $this->news_title;
    }
    
    /**
     * Accessor for title trimmed length
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since  2018/06/26
     * 
     * @return string
     */
    public function getRegionalNewsLabelAttribute()
    {
        $newsLabel = '[' . $this->regionalCategoryUnion . '] ';

        $newsLabel .= $this->news_title;
        
        // 2 is for '[' and ']'
        $regionalCategoryCount = floor(strlen($this->regionalCategoryUnion)/2) + 2;
        $newsTitleCount        =  strlen($this->news_title);
        $fullTitleCount        = $newsTitleCount + $regionalCategoryCount;
        $line1Count            = 20 - $regionalCategoryCount;
        
        $regionalNewsLabel = (mb_strlen($newsLabel) > 37)
                           ? mb_substr($newsLabel, 0, 37) .  "..."
                           : $newsLabel;

        // $newsLabel = explode( "\n", wordwrap($this->news_title, $line1Count))[0];
        // $newsLabel = mb_strimwidth($this->news_title, 0, (47 - 2 - (strlen($this->regionalCategoryUnion)/2)),  "...");

        return $regionalNewsLabel;
    }

    /**
     * Get news list that are activated by category
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since  2018/06/27
     * 
     * @param  int $categoryId
     * 
     * @return Collection<News>
     */
    public static function getTopNews($categoryId = null)
    {
        $toppageMax = 6;
        $topNews    = collect();

        $stickyNews = News::where([
			['status', '>=', 1],
            ['category', 'like', '%' . $categoryId . '%'],
            ['is_sticky', 1]
        ])
        ->orderBy('created_at', 'desc')
        ->take($toppageMax)->get();
       
        if(count($stickyNews) > 0){
            foreach($stickyNews as $item){
                $topNews->push($item);
            }
        }
        

        if(count($stickyNews) < 6){
            $news = News::where([
                    ['status', '>=', 1],
                    ['category', 'like', '%' . $categoryId . '%']
                ])
                ->whereNotIn('id', $stickyNews->pluck('id')->toArray())
                ->orderBy('created_at', 'desc')
                ->take($toppageMax - count($stickyNews))->get();
            
            foreach($news as $item){
                $topNews->push($item);
            }
        }

        return  $topNews;
    }

    /**
     * Accessor for title trimmed length
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since  2018/07/02
     * 
     * @return string
     */
    public function getCategoriesAttribute()
    {
        $categories    = array();
        $categoriesStr = "";
        $categoryIds   = explode(',' , $this->category);
        
        $categoryIds   = array_map('trim', $categoryIds);
        
        // Prepare news categories as array
        foreach($categoryIds as $categoryId){
            if(array_key_exists($categoryId, config('constants.NEWS_CATEGORIES')))
            {
                $categories[] = config('constants.NEWS_CATEGORIES')[$categoryId];
            }
        }

        return $categories;
    }

}
