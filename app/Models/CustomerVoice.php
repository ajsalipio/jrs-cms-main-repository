<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerVoice extends Model
{
    protected $table = 'customervoices';
    protected $primary_key = 'id';
    protected $fillable = [
	'cv_title',
	'cv_content',
	'cv_company',
	'cv_picture',
	'cv_status',
    ];

    /**
     * Definition of relationship of customer voice model to media model
     *
     * @author Sally Jean Sumeguin <sally_sumeguin@commude.ph>
     **/
    public function media()
    {
	return $this->belongsTo('App\Models\Media', 'cv_picture', 'media_id');
    }

    /**
     * 
     * Accessor for title trimmed length
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since  2018/06/26
     * 
     */
    public function getCvTitleLabelAttribute()
    {
        if((mb_strlen($this->cv_title) > 14)){
            $cvTitleLabel = (mb_strlen($this->cv_title) > 15)
                          ? mb_substr($this->cv_title, 0, 14) .  "..."
                          : $this->cv_title;
        }else{
            $cvTitleLabel = $this->cv_title;
        }
        
      

        return  $cvTitleLabel;
    }
}
