<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Situation extends Model
{
    protected $table = 'situations';
    protected $primaryKey = 'id';
    protected $fillable = [
        'situation_title',
        'situation_status',
        'situation_content',
        'situation_details',
        'file_xls',
        'file_doc',
        'file_pic',
        'have_time_publishing',
        'publish_from',
        'publish_end',
        'publishing_status',
    ];

    /**
     * Situation->file_xls belongs to one Media
     *
     * @return Media
     */
    public function fileXls()
    {
	return $this->belongsTo('App\Models\Media', 'file_xls', 'media_id');
    }
    
    /**
     * Situation->file_doc belongs to one Media
     * 
     * @return Media
     */
    public function fileDoc()
    {
	return $this->belongsTo('App\Models\Media', 'file_doc', 'media_id');
    }
    
    /**
     * Situation->file_pic belongs to one Media
     * 
     * @return Media
     */
    public function filePics()
    {
        return $this->hasManyThrough('App\Models\Media', 
            'App\Models\SituationImage', 
            'situation_id', 
            'media_id', 
            'id', 
            'media_id'
        )->get();
    }
}
