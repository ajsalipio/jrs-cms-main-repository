<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SuccessJob extends Model
{
    protected $table = 'success_jobs';
    protected $primaryKey = 'id';
    protected $fillable = [
        'job_name',
        'connection',
        'queue',
        'payload'
    ];

    public $timestamps = false;
    
}