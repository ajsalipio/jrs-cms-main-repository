<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mail extends Model
{
    protected $table = 'mails';
    protected $primaryKey = 'mail_id';
    protected $fillable = [
        'mail_address',
        'options'
    ];
    
}
