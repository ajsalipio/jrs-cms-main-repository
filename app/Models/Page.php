<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    //set the table name
		protected $table = 'pages';

		//set guarded columns
		protected $guarded = [];

		//turn off the default model timestamps
		public $timestamps = false;

		//set custom Primary Key
		protected $primaryKey = 'id';

}
