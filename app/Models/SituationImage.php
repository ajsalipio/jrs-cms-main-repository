<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SituationImage extends Model
{
    protected $table = 'situation_images';
    protected $primary_key = 'id';
    protected $fillable = [
        'situation_id',
        'media_id'
    ];
    
    public $timestamps = false;

    /**
     * Situation->file_doc belongs to one Media
     * 
     * @return Media
     */
    public function fileMedia()
    {
	    return $this->belongsTo('App\Models\Media', 'media_id', 'media_id');
    }
    
}
