<?php

namespace App\Services;

use App\Utilities\FileUtility;
use Carbon\Carbon;

class CalculatorService
{
	protected $fileUtility;
    protected $constants;

	 /**
     * Inject dependencies
     *
     * @return void
     */
    public function __construct()
    {
       $this->fileUtility = new FileUtility();
       $this->constants = config('calculator');
	}
    
    
     /**
     * Compute train via prefecture : start station -> end station
     * 
     * Note: This is a simplified station to station computation via reading the
     * csv via getStationToStationDistance
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @since  2018/07/19
     * 
     * @param float $weight
     * @param array $collectionData
     * 
     * @return array
     */    
    public function calculateTrain($weight, $collectionData)
    {
        $train = array();
        
        $train['transport'] = $collectionData['start_station_name'] . ' => ' . $collectionData['dest_station_name'];
        $train['distance']  = $this->getStationToStationDistance($collectionData['start_station'], $collectionData['dest_station']);
        $train['energy']    = $weight * $train['distance'] * $this->constants['ENERGY_CONSTANTS']['train_energy_per_unit'];
        $train['co2']       = $weight * $train['distance'] * $this->constants['ENERGY_CONSTANTS']['train_co2_emission_per_unit']  / 1000000.0;
        $train['equivalent_oil']  = $train['energy'] * $this->constants['ENERGY_CONSTANTS']['equiv_oil_energy'];
        
        $this->roundTrainResults($train);

        return $train;
    }

    /**
     * Compute truck/delivery
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @since  2018/07/19
     * 
     * @param float $weight
     * @param array $collectionData
     * 
     * @return array
     */
	public function calculateTruck($weight, $collectionData)
    {
        $truck   = array();
        $maxLoad = null;
        $truck['weight']   = $weight;
        $truck['distance'] = $collectionData['distance'];

        switch($collectionData['type']){
            case 1:
                $truck['loading_ratio'] = $this->constants['TRUCK_LOADING_RATES'][$collectionData['loading_ratio']];
                $truck['truck']         = "普通トラック普通トラック " . $collectionData["max_load"] . "トン車"; // For labels 
                $maxLoad                = $collectionData["max_load"];
                
                break;
            case 2:
                $truck['loading_ratio'] = '';
                $truck['truck']         = $this->constants['RAIL_TYPE_LABELS'][$collectionData['container_max_load']];
                $maxLoad                = $collectionData["container_max_load"];
                break;
            default:
                $maxLoad = 0;
                break;
        }

        $fuelConsumptionFactor   = $this->getFuelConsumptionFactor($maxLoad, $collectionData['loading_ratio']);        
        $fuelConsumption         = $weight * $collectionData['distance'] * $fuelConsumptionFactor;
        
        $truck['energy']         = $fuelConsumption * $this->constants['ENERGY_CONSTANTS']['truck_energy_per_unit'];
        $truck['co2']            = $truck['energy'] * $this->constants['ENERGY_CONSTANTS']['equiv_co2_energy'] / 1000.0;
        $truck['equivalent_oil'] = $truck['energy'] * $this->constants['ENERGY_CONSTANTS']['equiv_oil_energy'];
        
        $this->roundTruckResults($truck);
        
		return $truck;
	}

    /**
     * Legacy: adjustValues (name)
     * 
     * Rounding to last digit non zero based on analysis
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @since  2018/07/19
     */
    private function roundTruckResults(&$truck)
    {
        $truck['energy']         = $this->round_by_effective($truck['energy'], 3);
        $truck['co2']            = $this->round_by_effective($truck['co2'], 3);
        $truck['equivalent_oil'] = $this->round_by_effective($truck['equivalent_oil'], 3);
    }

      /**
     * Legacy: adjustValues (name)
     * 
     * Rounding to last digit non zero based on analysis
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @since  2018/07/19
     */
    private function roundTrainResults(&$train)
    {
        $train['energy']         = $this->round_by_effective($train['energy'], 3);
        $train['co2']            = $this->round_by_effective($train['co2'], 3);
        $train['equivalent_oil'] = $this->round_by_effective($train['equivalent_oil'], 3);
        
    }
    
    /**
     * Get fuel consumption factor given the max load and loading rate 
     * in between the constant range.
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @since  2018/07/19
     * 
     * @param float  $maxLoad 
     * @param int    $loadingRate
     * 
     * @return float
     */
    private function getFuelConsumptionFactor($maxLoad, $loadingRate = 0)
    {
        $fuelConsumption       = 0;
        $fuelConsumptionRanges = config('calculator.FUEL_CONSUMPTION_RANGES');
        
        $maxLoad    *= 1000; // use multiplier        
        $loadingRate = ($loadingRate)
                     ? (int) $loadingRate
                     : 0;
        
        $maxRange = $this->getFuelConsumptionMaxRange($fuelConsumptionRanges);
        
        // If max range, use it's key value pair immediately and no need to search the ranges
        if($maxLoad >= $maxRange['value']){
            $fuelConsumption =  $fuelConsumptionRanges[$maxRange['key']][$loadingRate];
        }else{
            foreach($fuelConsumptionRanges as $key => $value){
                $ranges = explode("-", $key);
                $min    = (int) $ranges[0];
                $max    = (int) $ranges[1];
                
                if($this->in_range($maxLoad, $min, $max)){
                    $fuelConsumption = $value[$loadingRate]; // If no loading rate specified, use 0 from key as generic
                }
            }
        }
       
        return $fuelConsumption;
    }
    
    /**
     * Get max value from constant range (n-n)
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @since  2018/07/20
     * 
     * @param  array $fuelConsumptionRanges
     * 
     * @return array
     */
    private function getFuelConsumptionMaxRange($fuelConsumptionRanges)
    {
        $maxRange = array();

        end($fuelConsumptionRanges);       
        $key = key($fuelConsumptionRanges); 
        reset($fuelConsumptionRanges);

        $maxRange['key']   = $key;
        $maxRange['value'] = (int) explode("-", $key)[1];
        return $maxRange;
    }
    /**
     * Loads csv and get value via logic:
     * 
     * 1. 2nd row is the row to determine the starting station - get the column here
     * 2. From row 8 onwards, check colmn 1 as the destination start - get the row here
     * 3. From step 2, same row, get the value using the value in step 1
     * 
     * Pre-reqs:
     * 
     * 1. Make sure you save station_matrix as csv first
     * 2. Row to use for 'column' index to search is 1
     * 3. Row to use for 'column' starts at 11 since the csv parses the new line in 1 cell as a new line itself
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @since  2018/07/19
     * 
     * @param int $startStation
     * @param int $destinationStation
     * 
     * @return float
     */
    private function getStationToStationDistance($startStation, $destinationStation)
    {
        $filename = database_path('/files/station_matrix.csv');

        $csvLines = array_map('str_getcsv', file($filename));

        // $startStation       = 1012;
        // $destinationStation = 2034;

        $startStationColumn    = null;
        $destinationStationRow = null;

        for($i=0; $i<count($csvLines); $i++){
          
            // Step 1
            if($i==1){
                $startStationColumn = array_search($startStation, $csvLines[$i]);
            }
            
            // Step 2
            if($i>=8){
                $csvLines[$i] = array_map('trim', $csvLines[$i]); // Trim all items in array first
                $destinationStationRow = $destinationStation == $csvLines[$i][0]
                                       ? $i
                                       : null;

                if($destinationStationRow){
                    break; // match found. exit loop
                }
            }
        }
        // dd($startStationColumn, $destinationStationRow);
        $distance = ($destinationStationRow && $startStationColumn)
                  ? $csvLines[$destinationStationRow][$startStationColumn] // Step 3
                  : 0;

        return $distance;
    }

    /**
     * Add all data params results
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @since  2018/07/20
     * 
     * @param array $delivery
     * @param array $collection
     * @param array $train 
     * @return array
     */
    public function calculateTotal($collection, $delivery, $train)
    {
        $total = array();

        $total['distance']       = $collection['distance'] + $train['distance'] + $delivery['distance'];
        $total['energy']         = $collection['energy'] + $train['energy'] + $delivery['energy'];
        $total['co2']            = $collection['co2'] + $train['co2'] + $delivery['co2'];
        $total['equivalent_oil'] = $collection['equivalent_oil'] + $train['equivalent_oil'] + $delivery['equivalent_oil'];

        $total['distance']       = $this->round_by_effective_fp($total['distance'], 3);
        $total['energy']         = $this->round_by_effective_fp($total['energy'], 3);
        $total['co2']            = $this->round_by_effective_fp($total['co2'], 3);
        $total['equivalent_oil'] = $this->round_by_effective_fp($total['equivalent_oil'], 3);

        return $total;
    }

	/**
	 * Determines if $number is between $min and $max
	 *
	 * @param  integer  $number     The number to test
	 * @param  integer  $min        The minimum value in the range
	 * @param  integer  $max        The maximum value in the range
	 * @param  boolean  $inclusive  Whether the range should be inclusive or not
     * 
	 * @return boolean              Whether the number was in the range
	 */
	private function in_range($number, $min, $max, $inclusive = TRUE)
	{
		if (is_int($number) && is_int($min) && is_int($max))
		{
			return $inclusive
				? ($number >= $min && $number <= $max)
				: ($number > $min && $number < $max) ;
		}
		
		return FALSE;
    }
    
    /**
     * Legacy Rounding System
     * 
     * @since 2018/07/19 Louie: Adapted legacy system rounding method - unthouched
     * 
	 * 有効数字を指定して四捨五入.
     * 
     * @return float
	 */
	private function round_by_effective($value, $effective_digit){
	    $sign = 1;
	    // 符号を分離
	    if($value == 0.0){
	        return 0.0;
	    }
	    if($value < 0.0){
	        $sign = -1;
	        $value = - $value;
	    }
	    if($value >= 10.0){
	        // 値が10以上の場合、最上位の桁を1の桁にあわせてroundをとる
	        $order = 0;
	        while($value >= 10.0){
	            $value /= 10.0;
	            $order++;
	        }
	        $value = round($value, $effective_digit-1);
	        for($i=0;$i<$order;$i++){
	            $value *= 10.0;
	        }
	    }else if($value < 1.0){
	        // 値が1.0より小さい場合、最上位の桁を1の桁にあわせてroundをとる
	        $order = 0;
	        while($value < 1.0){
	            $value *= 10.0;
	            $order++;
	        }
	        $value = round($value, $effective_digit-1);
	        for($i=0;$i<$order;$i++){
	            $value /= 10.0;
	        }
	    }else{
	        $value = round($value, $effective_digit-1);
	    }
	    return $sign * $value;
    }
    
    /**
     * Legacy Rounding System
     * 
     * @since 2018/07/20 Louie: Adapted legacy system rounding method - unthouched
     * 
	 * 四捨五入が小数の桁で行なわれる場合にのみ、四捨五入を行なう
	 */
	function round_by_effective_fp($value, $effective_digit){
	    $tmp = $value;
	    if($tmp < 0){
	        $tmp = - $tmp;
	    }
	    for($i=0;$i<$effective_digit;$i++){
	        $tmp /= 10.0;
	    }
	    if($tmp < 1.0){
	        return $this->round_by_effective($value, $effective_digit);
	    }else{
	        return round($value);
	    }
	}
}
