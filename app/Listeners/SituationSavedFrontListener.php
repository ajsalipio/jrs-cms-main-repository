<?php

namespace App\Listeners;

use App\Events\SituationSavedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Utilities\DateUtility;

use Pusher;
Use Log;

class SituationSavedFrontListener
{

    protected $dateUtility;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(DateUtility $dateUtility)
    {
        $this->dateUtility = $dateUtility;
    }

    /**
     * Handle the event.
     *
     * @param  SituationSavedEvent  $event
     * @return void
     */
    public function handle(SituationSavedEvent $event)
    {
        // SET VIEW DATA
        $situation   = $event->situation;
        $currentTime = date('H:i');
        $isOffHours  = $this->dateUtility->isBetweenTime($currentTime);

        // PUSHER SET SETTINGS
        $channel     = $event->broadcastOn();
        $event       = config('constants.EVENTS.situation_save');

        $options = array(
            'cluster' => 'ap1',
            'encrypted' => true
        );

        $pusher = new Pusher\Pusher(
            env('PUSHER_APP_KEY'),
            env('PUSHER_APP_SECRET'),
            env('PUSHER_APP_ID'),
            $options
        );

        $viewData    = view()->make('partials.top_situation')->with('situation', $situation)
                                                             ->with('isOffHours', $isOffHours);
      
        $pusher->trigger($channel, $event, $viewData->render());
    }
}
