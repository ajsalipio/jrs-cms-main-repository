<?php

namespace App\Observers;

use App\Models\Situation;
use App\Events\SituationSavedEvent;
use App\Jobs\SituationScheduleFrontUpdateJob;

use Carbon\Carbon;
use Log;

/**
 * Observes the Users model
 */
class SituationObserver 
{

    /**
     * Function will be triggerd when a user is created
     * 
     * @param Situation $model
     */
    public function created(Situation $situation)
    {
        $isScheduled = ($situation->have_time_publising == 1);

        if($isScheduled){
            // Log::info('Job will be scheduled in 1 minute');
            // SituationScheduleFrontUpdateJob::dispatch($situation)
            //                                ->delay(now()->addMinutes(3));
            
        }else{
           event(new SituationSavedEvent($situation));
        }
      
    }

    /**
     * Function will be triggerd when a user is updated
     * 
     * @param Situation $model
     */
    public function updated(Situation $situation)
    {
        $isScheduled = ($situation->have_time_publising == 1);
        
        if($isScheduled){
            // Log::info('Job updated will be scheduled in 1 minute');
            // SituationScheduleFrontUpdateJob::dispatch($situation);
           
        }else{
            event(new SituationSavedEvent($situation));
        }
    }

    /**
     * Function will be triggerd when a user is deleted
     * 
     * @param Situation $model
     */
    public function deleted(Situation $situation)
    {
        //  Get the last recent situation from DB
        $situation =  Situation::where(function($query) {
                                    $query->where('publishing_status','>' , 0);
                                    $query->where('have_time_publising', 0);
                                })
                                ->orWhere(function($query){
                                    $query->where('have_time_publising', 1);
                                    $query->where('publish_from', '<=', Carbon::now()->toDateTimeString());
                                    $query->where('publish_end', '>=', Carbon::now()->toDateTimeString());
                                })
                                ->orderBy('created_at', 'desc')->first();
            
       event(new SituationSavedEvent($situation));
    }
}