<?php

namespace App\Http\Middleware;

use App\Events\SituationCreated;

use Closure;

class SituationSaveMiddleware
{
    /**
     * AFTER MIDDLEWARE
     * 
     * Handle an incoming request. 
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        
        return $response;
    }
}
