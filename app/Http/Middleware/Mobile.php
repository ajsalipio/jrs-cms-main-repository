<?php

namespace App\Http\Middleware;

use Closure;
use Mobile_Detect;
use Redirect;
use Session;
class Mobile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		$response = $next($request);
		$detect = new Mobile_Detect;

		$detect->isMobile() ? Session::put('limit', config('constants.PAGINATION.front_mobile'))
							: Session::put('limit', config('constants.PAGINATION.front_pc'));
			
		return $response;
	}
}
