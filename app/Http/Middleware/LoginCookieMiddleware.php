<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;

use Closure;

class LoginCookieMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $response = $next($request);

        // After Middleware
        // If successful login, check if remember checkbox is checked
        if(Auth::check()){
            
            $expiry = config('auth.reminder.expire');

            if($request->has('remember_me')){
                $cookie = cookie(config('constants.LOGIN_COOKIE_ID'), Auth::user()->id, $expiry);
                
                return $response->cookie($cookie);
            }
        }

        return $response;
    }
}
