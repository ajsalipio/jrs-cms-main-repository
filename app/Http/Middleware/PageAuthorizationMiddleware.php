<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class PageAuthorizationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       
        $fullRouteName     = $request->route()->getName();
        $currentRoupGroup  = substr($fullRouteName, 0, strpos($fullRouteName, '.'));
        $currentRouteName  = substr($fullRouteName, strpos($fullRouteName, '.') + 1, strlen($fullRouteName));
        $userGroup         = Auth::user()['group'];
        $pagesAllowed      = config('constants.USER_GROUP_PAGES')[$userGroup];
        
        $exemptedRoutes    = ['getEditProfileView','changePassword'];
        
        if(!in_array($currentRouteName, $exemptedRoutes))
        {
            // RESTRICT ACCESS for pages not maintained in constants.USER_GROUP_PAGES based on logged in user.group
            if(!in_array($currentRoupGroup, $pagesAllowed)){
                abort(404);
            }
        }

        return $next($request);
    }
}
