<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreMail extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $requestData = $this->request->all();
        
        $id = $requestData['mail_id'] ?? null;

        return [
            'mail_address' => 'required|email|max:255|unique:mails,mail_address,' . $id . ',mail_id',
            'options'     => 'required|int|max:3',
        ];
    }
    
    /**
     * custom error messages
     *
     * 2018/0/04 Louie: See messages.php for custom error messages
     */
    public function messages()
    {
        return [
            // 'mail_address.required'         => __('messages.MAIL_ADDRESS_REQUIRED'),
            // 'mail_address.email'            => __('messages.EMAIL'),,
            // 'mail_address.unique'           => __('messages.UNIQUE'),,
            // 'mail_address.max'              => __('messages.MAX'),,
            
            // 'options.required'              =>__('messages.MAIL_ADDRESS_REQUIRED'),
            // 'options.max'                   => __('messages.MAIL_ADDRESS_REQUIRED'),
        ];
    }
}
