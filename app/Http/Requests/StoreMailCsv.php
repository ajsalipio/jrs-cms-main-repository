<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use App\Models\Mail;

class StoreMailCsv extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file_csv' => [
                            'required' 
                            ,'file'
                            ,'mimes:csv,txt'
                            ,'max:10000' // 10MB
                          ] 
        ];
    }

    /**
     * custom error messages
     *
     * 2018/0/04 Louie: See messages.php for custom error messages
     */
    public function messages()
    {
        return [
            // 'file_csv.required'       => __('messages.REQUIRED'),
            // 'file_csv.max'            => __('messages.REQUIRED'),
            // 'file_csv.file'           => __('messages.FILE'),
            // 'file_csv.unique'         => __('messages.MAIL_UNIQUE'),

            // 'options.required'        => __('messages.REQUIRED'),
            // 'options.max'             => __('messages.MAIL_ADDRESS_REQUIRED'),
            // 'file_csv.mimes'            => 'The file must be of type csv',
        ];
            
    }

    /**
     * Custom validations that doesn't need "Rule" creation may be compiled here.
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since  2018/05/31
     * 
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
          // Do not proceed with custom validations if base rules has error
        if($validator->fails()){
            return;
        }

        // Add DB validations only after base rules are clear
        $validator->after(function ($validator)
        {
            $request = $validator->getData();
            $results = array();
            $file    = $request['file_csv'];
            
            //the requierments said:csv with 2 columns: mail_data , list_no
            $currentCustom = 2;//
            
            $fileName = $file->getClientOriginalName();
            //is CSV ?
            $isCSV = (strtolower(substr($fileName, -4)) === '.csv') ? true : false;
            $results['isCSV'] =$isCSV;
            
            // CSV type check
            if(!$isCSV ){
                $validator->errors()->add('file_csv', 'Uploaded file must be csv');
                return;
            }

            $source = fopen($file, "r");
            //most of the time customer CSV have columns header on the first line
            $firstLine = fgets($source);
            // For this project we can assume if there's no @ in the first line, it a columns headers
            $header = (substr_count($firstLine, '@'))? false: true;
            // need to found what is the separator in use in CSV
            $separator = ',';
            $csvLine = fgetcsv($source, 1000, $separator);
           
            if(count($csvLine) === 1){
                $separator = ';';
                // $csvLine = fgetcsv($source, 1000, $separator);
                $csvLine   = explode($separator, $csvLine[0]); // Louie: changed to expldoe since fgetcsv returns false
            }
            
            $colFound = count($csvLine);
            
            
            // Columns count check 
            if( $currentCustom != $colFound ){
                //This should be adapted according your need
                // Display an error about columns count error
                $difference = $colFound - $currentCustom ;
                $result = '
                    カラムの回数エラー：インポート出来ませんでした。
                    <br/> CSVファイルに'.$colFound.' カラムがりました
                    <br/>'.$difference.
                    ( ($difference >0 )? ' カラムを追加してください。' :'　カラムを削除してください。').'
                ';

                 $validator->errors()->add('file_csv', $result);
            }
            
            // Assume header checks and file validity are done
            // Proceed with ROW per ROW checks

            $rowNo = 1; // Start of row
            
            // Re-open the file
            $sourceCSV = fopen($file, "r");
            $customerColumns = array();
            
            // As of now, no strict checks were imposed to have a header
            // Accept if it has no headers
            // Accept if has headers (exported csv used as import) 

            // If header exists, skip the first line
            if($header){
                fgetcsv($sourceCSV, 1000,$separator);
                $rowNo = 2;
            }
            
            // Access constants for check
            $allowedOptions = config('constants.MAIL_STATUS');

            $mailAddreses = array(); // temporary holder for unique check

            // Read rows
            while (($row = fgetcsv($sourceCSV, 1000,$separator)) !== FALSE) {
                
                $mailAddress = $row[0];
                $option      = ($row[1]);
                
                $mailSpaceCount   = substr_count($mailAddress, ' ');
                $optionSpaceCount = substr_count($option, ' ');
                
                if(!$mailAddress){
                    $validator->errors()->add('file_csv', "エラー： $rowNo 行目メールアドレスは必須です");
                }else if($mailSpaceCount > 0){
                    $validator->errors()->add('file_csv', "エラー： $rowNo 入力した電子メールの形式が正しくありません");
                }else if(filter_var($mailAddress, FILTER_VALIDATE_EMAIL) == false){
                    $validator->errors()->add('file_csv', "エラー： $rowNo 入力した電子メールの形式が正しくありません");
                }
                
                $mailExists = Mail::where('mail_address', $mailAddress)->exists();

                if($mailExists){
                    $validator->errors()->add('file_csv', "エラー： $rowNo 行目 複数メールアドレス ");
                }
                
                if(!in_array($option, array_keys($allowedOptions))){
                    $validator->errors()->add('file_csv', "エラー： $rowNo 行目設定エラー");
                }

                // Check null state
                if($optionSpaceCount > 0){
                    $validator->errors()->add('file_csv', "エラー： $rowNo 行目設定エラー");
                }
                
                if(is_null($option) || strlen($option) === 0){
                    $validator->errors()->add('file_csv', "エラー： $rowNo 行目設定エラー");
                }

                if(!in_array($mailAddress, $mailAddreses)){
                    $mailAddreses[] = $mailAddress;
                }else{
                     $validator->errors()->add('file_csv', "エラー： $rowNo already exists in the same file");
                }
                
                $rowNo++;
            }
        });
        
    }
}
