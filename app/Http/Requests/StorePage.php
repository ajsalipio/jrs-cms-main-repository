<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePage extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'page_title'    => 'required|string|max:255',
      //      'ogp_title'     => 'required|string|max:255',
       //     'page_content'  => 'required'
        ];
    }

    /**
    * custom error messages
    *
    *
    */
    public function messages()
    {
        return [
            //required error messages  
            // 'page_title.required'       => 'Please enter a page title.',
     //       'ogp_title.required'        => 'Please enter an OGP title.',
            // 'page_content.required'     => 'Please enter a page content.',

            //max error messages
            // 'page_title.max'            => 'Maximum number of characters allowed for Page Title is :max.',
        //    'ogp_title.max'             => 'Maximum number of characters allowed for OGP Title is :max.',
            
        ];
    }
}
