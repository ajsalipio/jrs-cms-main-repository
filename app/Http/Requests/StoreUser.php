<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $requestData = $this->request->all();
        
        $id = $requestData['id'] ?? null;

        return [
            'email'        => 'required|string|max:190|unique:users,email,' . $id,
            'password'     => 'required|string|max:190',
            'group'        => 'required|string|max:190'
        ];
    }
}
