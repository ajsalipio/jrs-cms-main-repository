<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreNews extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        	'news_title'	=> 'required|string|max:255',
			'file_pdf'		=> 'mimes:pdf'
		];
    }

	public function messages()
	{
		return [
			// 'news_title.required'	=>	__('messages.REQUIRED'),
			// 'news_title.string'		=>	__('messages.STRING'),
			// 'news_title.max'		=>	__('messages.MAX'),
			// 'file_pdf.mimes'		=>	__('messages.MIMES'),	
			// 'file_pdf.max'			=>	__('messages.FILE_SIZE'),	
		];
	}
}
