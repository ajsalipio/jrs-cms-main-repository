<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCustomerVoice extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        	'cv_title'		=> 'required|string|max:255',
        	'cv_company'	=> 'required|string|max:255',
			'cv_picture'	=> 'mimes:jpeg,png,gif'
		];
    }
	
	public function messages()
	{
		return [
			// 'cv_title.required'		=>	__('messages.REQUIRED'),
			// 'cv_title.string'		=>	__('messages.STRING'),
			// 'cv_title.max'			=>	__('messages.MAX'),
			
			// 'cv_company.required'	=>	__('messages.REQUIRED'),
			// 'cv_company.string'		=>	__('messages.STRING'),
			// 'cv_company.max'		=>	__('messages.MAX'),
			
			
			// 'cv_picture.mimes'		=>	__('messages.MIMES'),	
			// 'cv_picture.max'		=>	__('messages.FILE_SIZE'),	
		];
	}
}
