<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreSituation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'situation_status'    => 'required|int|max:3',
            'situation_title'     => 'required_if:situation_status,3',
            'publishing_status'   => 'required|int|max:3',
            'publish_from'        => 'nullable|date|before:publish_end|after:today',
            'publish_end'         => 'nullable|date',
            'have_time_publising' => 'required_with:publish_from,publish_end',
            'file_xls'            => 'file|max:10000|mimes:xlsx',
            'file_doc'            => 'file|max:10000|mimes:xlsx',
            'file_pic.*'          => 'file|max:10000|mimes:jpeg,jpg,png',
        ];
    }

    /**
     * custom error messages
     *
     * 2018/0/04 Louie: See messages.php for custom error messages
     * 
     */
    public function messages()
    {
        return [

            // 'mail_address.required' => __('messages.REQUIRED'),
            // 'mail_address.email'    => __('messages.EMAIL'),

            // 'options.required'     => __('messages.REQUIRED'),
            // 'options.max'          => __('messages.REQUIRED'),

            // 'situation_status.required'   => __('messages.REQUIRED'),
            // 'situation_status.int'        => __('messages.INT'),
            // 'situation_status.max'        => __('messages.REQUIRED'),
           
            // 'publishing_status.reqired' => __('messages.REQUIRED'),
            // 'publishing_status.int' => __('messages.INT'),
            // 'publishing_status.max' => __('messages.MAX'),
            
            // 'have_time_publising.required_with' => '予約投稿 must be checked if 開始時間 and 終了時間 is present',
            
            // 'publish_from.date'        => __('messages.DATE'),
            // 'publish_from.required_if' => '開始時間 is required when 予約投稿 status is selected',
            // 'publish_from.before'      => '開始時間 must be before 終了時間',
            // 'publish_from.after'       => __('messages.AFTER'),
            // 'publish_end.date'         => __('messages.DATE'),
            // 'publish_end.required_if' => '終了時間 is required when 予約投稿 status is selected',
            // 'publish_end.after'       => '終了時間 must be after 開始時間',

            // 'situation_title.required_if' => '表題 is required when 作成した記事を表示 status is selected',            
            // 'file_xls.mimes'              => 'The file must be of type (.xlsx)',
            // 'file_xls.file'        => __('messages.REQUIRED'),
            // 'file_xls.max'        => 'The maximum file upload size is 100MB',
            
            // 'file_doc.file'        => __('messages.REQUIRED'),
            // 'file_doc.max'        => 'The maximum file upload size is 100MB',
            // 'file_doc.mimes'      => 'The file must be of type (.xlsx)',

            // 'file_pic.file'        => __('messages.REQUIRED'),
            // 'file_pic.max'        => 'The maximum file upload size is 100MB',
            // 'file_pic.mimes'      => 'The file must be of type (.jpg/.png/.jpeg)',        
        ];
    }

    /**
     * Custom validations that doesn't need "Rule" creation may be compiled here.
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since  2018/06/05
     * 
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $request = $validator->getData();
     
        // Do not proceed with custom validations if base rules has error
        if($validator->fails()){
            return;
        }
        
        $request           = $validator->getData();
        $hasTimePublishing = $request['have_time_publising'] ?? 0;
        
        // Assume triggered = 0
        if($hasTimePublishing == 1){
            if(empty($request['publish_from'])){
                $validator->addRules(['publish_from'   => 'required_if:have_time_publising, 1']);
            }

            if(empty($request['publish_end'])){
                $validator->addRules(['publish_end' => 'required_if:have_time_publising, 1']);
            }
        }

        // 2018/07/04 Louie: assume same validator instance to run nullable_if
        // $validator->after(function ($validator)
        // {
            // $request           = $validator->getData();
            // $hasTimePublishing = $request['have_time_publising'] ?? 0;
            
            // // Assume triggered = 0
            // if($hasTimePublishing == 1){
            //     if(empty($request['publish_from'])){
            //         $validator->errors()->add('publish_from', '開始時間 is required when 予約投稿 status is selected');    
            //     }

            //     if(empty($request['publish_end'])){
            //         $validator->errors()->add('publish_end', '表題 is required when 作成した記事を表示 status is selected');    
            //     }
            // }
        // });
        
    }
}
