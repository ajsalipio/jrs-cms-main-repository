<?php

namespace App\Http\Helper;

class CategoryHelper
{

    /**
     * Helper constructor
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Prepare regional category data 
     * 
     * @author Sally Jean Sumeguin < sally_sumeguin@commude.ph
     * @since  2018/06/14 
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return String
     **/
	public static function prepareRegionalCategories($request)
	{
		$subCategories = array();
		
		if ($request->regional_category) {
			foreach(config('constants.REGIONAL_NEWS_SUB_CAT') as $key => $value) {
				if (in_array($key, $request->regional_category)) {
					array_push($subCategories, $value);	
				}
			}	
		}
		return implode(',', $subCategories);
	}	
}
