<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Helper\CategoryHelper;
use App\Http\Requests\StoreNews;
use App\Models\News;
use App\Models\Media;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;


class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @author Sally Jean Sumeguin < sally_sumeguin@commude.ph
     * @return View home/news/list 
     */
    public function index()
    {
		$news = News::orderBy('created_at', 'desc')
		    		->paginate(config('constants.PAGINATION.limit'));
		
		return view('admin.news.list')
		    		->with('headAdminTitle', '最新情報　一覧') 
		    		->with('data', $news) ;
    }

    /**
     * Show form for adding new resource
     *
     * @author Sally Jean Sumeguin < sally_sumeguin@commude.ph
     * @return View home/news/create
     */
    public function create()
    {
		return view('admin.news.create')
		    	->with('headAdminTitle', '最新情報　作成');
    }
     
 
    /**
     * Store a newly created resource in storage.
     *
     * @author Sally Jean Sumeguin < sally_sumeguin@commude.ph
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreNews $request)
    {
		$news = new News();
		
		if ($request->hasfile('file_pdf')) {
	    	$news->file_pdf = MediaController::storeCustom($request->file('file_pdf'));
		}
		
		$news->validity				= $request->validity;
		$news->news_title			= $request->news_title;
		$news->news_content			= $request->news_content;
		$news->is_link				= $request->is_link ? $request->is_link : 0;
		$news->status				= $request->status;
		$news->category				= $request->category 
									? implode(', ',$request->category)
									: null;	
		$news->regional_category    = $request->regional_category
									? implode(', ',$request->regional_category)
									: null;
		$news->save();
		return redirect('/dashboard/news/list');
    }
     
    /**
     * Show the form for editing the specified resource.
     *
     * @author Sally Jean Sumeguin < sally_sumeguin@commude.ph
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$news = News::find($id);
		if (empty($news)) {
			return abort(404);
		}

		$categories = $news->category !== null 
		    		? explode(', ', $news->category)
		    		: [];
		
		$regional_categories = $news->regional_category !== null 
							 ? explode(', ', $news->regional_category)
							 : [];
		return view('admin.news.edit')
		    	->with('headAdminTitle', '最新情報　作成')
		    	->with('categories' , $categories)
		    	->with('regional_categories' , $regional_categories)
		    	->with('data' , $news);
    }

    /**
     * Update the specified resource in storage.
     *
     * @author Sally Jean Sumeguin < sally_sumeguin@commude.ph
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreNews $request, $id)
    {
		$news = News::find($id);
		
		if (empty($news)) {
			return abort(404);
		}
		
		$news->news_title			= $request->news_title;
		$news->news_content			= $request->news_content;
		$news->status				= $request->status;
		$news->is_link				= $request->is_link ? $request->is_link : 0;
		$news->validity				= $request->validity;
		$news->category				= $request->category  
									? implode(', ', $request->category)
									: null;

		$news->regional_category	= $request->regional_category
									? implode(', ', $request->regional_category)
									: null;
		if ($request->hasfile('file_pdf')) {
	    	if ($news->file_pdf) {
				MediaController::updateCustom($request->file('file_pdf'), $news->file_pdf);
			} else {
				$news->file_pdf = MediaController::storeCustom($request->file('file_pdf'));
			}
		}
		$news->save();
		return back();	
    }

    /**
     * Remove the specified resource from storage.
     *
     * @author Sally Jean Sumeguin < sally_sumeguin@commude.ph
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {	
		$news = News::find($request->id);
		
		if (empty($news)) {
			return abort(404);
		}
		
		foreach($news as $info) {
	   		MediaController::deleteCustom($info->file_pdf);
	    	$info->delete();
		}
		return redirect('/dashboard/news/list');
    }

    
    /**
     * Show specified resource in preview page dashboard/news/preview 
     *
     * @author Sally Jean Sumeguin < sally_sumeguin@commude.ph
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function preview(Request $request)
    {	
	$news = $request->all();	
		$news['regional_category']	= CategoryHelper::prepareRegionalCategories($request); 
		$news['category']	= ($news['category']) ?? [];
	
	$file = $this->storeTempFile($request);
		
		$catClasses = array('btn-blue',
			'btn-cyan',
			'btn-orange',
			'btn-purple',
			'btn-pink');
		
		if(!empty($news['is_link'])) {
			return Redirect::to($file['path']);
		} 

	return view('admin.news.preview')
		->with('headAdminTitle', 'ニュースをプレビュー')
		->with('file', $file)
				->with('catClasses', $catClasses)
				->with('data', $news);	

	}	
	
    /**
     * POST: /news/sticky/edit
     *
     *  Show the form for editing a situtation.
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @since  2018/05/21
     *
     * @param  int $id
     * @return  \Illuminate\Http\Response situation/edit.blade.php
     */
	public function editSticky(Request $request)
	{
		$data    = $request->all();
		
		$stickyItems = $data['sticky_items'] ?? null; // ids
		$archivedStickyItems = $data['archived_sticky'] ?? null; // for update to null
	
		if($stickyItems){
			News::whereIn('id', $stickyItems)->update(['is_sticky' => 1]);
		}

		if($archivedStickyItems){			
			$nonStickyItems = array_filter(explode(',', $archivedStickyItems));
			News::whereIn('id', $nonStickyItems)->update(['is_sticky' => null]);
		}
        // $archivedItems = $data['archived_items'] ?? null;
        
        // if($archivedItems){
        //     foreach($archivedItems as $id){
        //         $situation = Situation::find($id);
        //         $mediaId   = $situation->fileXls['media_id'];
        //         $results  += $situation->delete();

        //         // Delete media files if exist
        //         if($mediaId){
        //             MediaController::deleteCustom($mediaId);
        //         }
        //     }
        // }

		return redirect('dashboard/news/list');
	}
	
	/**
     * Store uploaded file to /storage/tmp/ 
     * 
     * @author Sally Jean Sumeguin < sally_sumeguin@commude.ph
     * @param  \Illuminate\Http\Request  $request
     * @return String
     **/
    private function storeTempFile($request)
    {
		if ($request->hasfile('file_pdf')) {
	    	$file = $request->file('file_pdf');
	    	$fileOriginalName = time() . $file->getClientOriginalName();
	    	$fileExtension = $file->getClientOriginalExtension();
	    	$filename = md5($fileOriginalName) . "." . $fileExtension; 
	    	$file->storeAs('tmp', $filename, 'public');
	    	return [
				'path' => "/storage/tmp/$filename",
				'original_name' => $file->getClientOriginalName()
	    	];  	
		} else {
	    	$news = News::find($request['id']);

			if ($news) {
				if ($news->file_pdf) {
		    		return [ 
						'path' => $news->media->path, 
						'original_name' => $news->media->original
					];	
				} 
	    	}
		}

		return [ 
	    	'path' => '', 
	    	'original_name' => ''
		];
    }
}
