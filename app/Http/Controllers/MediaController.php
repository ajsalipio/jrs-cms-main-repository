<?php

namespace App\Http\Controllers;

use App\Models\Media;

//Used in delete function
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use File;

class MediaController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$media = Media::all();
		return view('admin.media')->with('data', $media);
    }

    /**
     * Store a newly created resource in storage.
     * shoudl be used for main form
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    static function store($image )
    {
		$media = new Media;		
		if(!empty($image)){
			$filename = 'image_'.time().'_'.$image->hashName();
			
			$media->name = $filename;
			$media->path = 'upload'.'/'. $filename;
			$media->original =$image->getClientOriginalName();
			$media->save();
			
			$image = $image->move(public_path('upload'), $filename);

			return   '/upload/'.$filename;
		}
		return false;
    }
    /**
     * Store a newly created resource in storage.
     * return a json array with file location or ereur 500
     *
     */
    public function tinyMCE(Request $request )
    {
		$media = new Media;
		
        $image = $request->file('image');
        $filename = 'image_'.time().'_'.$image->hashName();

		$media->name = $filename;
		$media->path = 'user_img'.'/'. $filename;
		$media->original =$image->getClientOriginalName();
		$media->save();

        $image = $image->move(public_path('user_img'), $filename);
		//2018-06-09: changed the approach
	/*
      return  ("
            <script>
            top.$('.mce-btn.mce-open').parent().find('.mce-textbox').val('". url('/user_img/'.$filename)."').closest('.mce-window').find('.mce-primary').click();
            </script>
        ");*/
		//return  json with location instead
		if($image){
			echo json_encode(array('location' =>  url('/user_img/'.$filename)));
		} else {
		 header("HTTP/1.1 500 Server Error");
		}
    }
	/*Upload 1 file for campaign*/
	/*
	file should be uploaded first, then when form is confirmed deleted and registered in db.
	*/
	public function fileUpload(Request $request )
    {
		$media = new Media;
		//Need to find the media key
		$data = $request->all();
		$destinationFolder = 'user_img';
		//print_r($data);
		unset($data['_token']);
		$imgKey = array_keys($data)[0];

        $image = $request->file($imgKey);
        $filename = 'image_'.time().'_'.$image->hashName();
		$originalName = $image->getClientOriginalName();
		$media->name = $filename;
		$media->path =$destinationFolder.'/'. $filename;
		$media->original =$originalName;
//		$media->save();

        $image = $image->move(public_path($destinationFolder), $filename);
		
		$json = json_encode(array(
			'url' => url("/$destinationFolder/$filename"),
			'path' => "/$destinationFolder/$filename",
			'name' => $originalName ,
			'upload' => $filename,
			'input' => $imgKey,
		));
      return $json;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * --- 2018-02-08: greg
	* Display a page according the content found in DB
     * --- 2018-01-15: greg
     *
     *
     */
    public function show($id)
    {
		return view('notfound');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$pages = Page::find($id);
		return view('admin/page_edit')->with('page', $pages);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StorePage $request, $id)
    {
		$data = $request->all();
		return redirect('admin/page_edit/'.$id );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
		$data = $request->all();

		foreach($data['media_id'] as $id){

			$target = Media::select('path')->where('media_id',$id)->get();
			$target = $target->toArray()[0]['path'];

			$targetPath = public_path( $target);
			if(file_exists($targetPath)){
				unlink($targetPath);
			}
			Media::find($id)->delete();
		}
		
		$pages = Media::all();
		return view('admin.media')->with('data', $pages);
    }

    /**
     * Store file upload with custom prefix. 
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @since  2018/05/22
     * 
     * @param  \Illuminate\Http\Request  $requestFile
     * 
     * @return media->id
     */
    static function storeCustom($requestFile)
    {
        $media = new Media;
        
        if(!empty($requestFile)){
            $filename = self::getFilename($requestFile);

            $mediaId  = self::saveMedia($media, $filename, $requestFile->getClientOriginalName());
            $requestFile->storeAs('upload',  $filename, 'public');
            return $mediaId;
        }

        return false;
    }
    
    /**
     * Store file upload with custom prefix. 
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	   * @since  2018/05/22
     * @author Sally Jean Sumeguin <sally_sumeguin@commude.ph>
	* @since 2018/05/28
     * 
     * @param  \Illuminate\Http\Request  $requestFile
     * @param  int $mediaId
     * @param  string $filePrefix
     * 
     * @return bool
     */
    static function updateCustom($requestFile, $mediaId)
    {
        $media = Media::find($mediaId);

        if(!empty($media)){
            // Delete first the old file
            //unlink('storage/upload/' . $media->name);
			unlink($media->path);

            $filename = self::getFilename($requestFile);

            $mediaId  = self::saveMedia($media, $filename, $requestFile->getClientOriginalName());
            $requestFile->storeAs('upload', $filename, 'public');
            return true;
        } 

        return false;
    }

    /**
     * Store file upload with custom prefix. 
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	   * @since  2018/05/22
     * 
     * @param  \Illuminate\Http\Request $requestFile
     * @param  int $mediaId
     * 
     * @return media->id
     */
    static function deleteCustom($mediaId)
    {
        $media = Media::find($mediaId);

        if($mediaId){
           // unlink('storage/upload/' . $media->name);
            unlink($media->path);

	    $media->delete();
	    return true; 
	}
        return false;
    }

    /**
     * Store file upload with custom filename. 
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since  2018/05/23
     * @author Sally Jean Sumeguin <sally_sumeguin@commude.ph>
	* @since 2018/05/28
     * @param  Media $media
     * @param  string $filename
     * @param  string $filenameOrigina
     * 
     * @return media->id
     */
    static function saveMedia($media, $filename, $filenameOriginal)
    {         
        $media->name     = $filename;
        $media->path     = 'storage/upload' . '/' . $filename;
        $media->original = $filenameOriginal;
        $media->save();

        return  $media->media_id;
    }

    /**
     * Get file name with correct prefix based from the file extension
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since  2018/05/23
     * @author Sally Jean Sumeguin <sally_sumeguin@commude.ph>
	* @since 2018/05/28
     * 
     * @param  \Illuminate\Http\Request  $requestFile
     * 
     * @return string $filename
     */
    static function getFilename($requestFile)
    {
	   $filename = "\xEF\xBB\xBF"; // SET UTF-8
	   $originalName = time() . $requestFile->getClientOriginalName();
       $fileExtension = $requestFile->getClientOriginalExtension();
	   $filename =  md5($originalName) . "." . $fileExtension;
       
	   return $filename;
    } 
}
