<?php

namespace App\Http\Controllers;

//for the front page
//reference to the model
use App\Models\CustomerVoice as Feedback;
use App\Models\News;
use App\Models\Page;
use App\Models\Situation;

//reference to regional sub categories helper
use App\Http\Helper\CategoryHelper;
use App\Utilities\DateUtility;
use App\Utilities\PaginationUtility;
use App\Services\CalculatorService;

//reference the validator
use App\Http\Requests\StorePage;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

use Carbon\Carbon;
use DB;
use Session;

class FrontController extends Controller
{
	protected $dateUtility;
	protected $calculatorService;

	 /**
     * Inject dependencies
     *
     * @return void
     */
    public function __construct(DateUtility $dateUtility)
    {
	   $this->dateUtility = $dateUtility;
	   $this->calculatorService = new CalculatorService();
	}
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * 2018-05-21: Greg Not required, but keep till further directives 
     */
    public function index()
    {
		$headAdminTitle = 'JR貨物 日本貨物鉄道株式会社';
		//get the latest situation information
		$situation =  Situation::where(function($query) {
				$query->where('publishing_status','>' , 0);
				$query->where('have_time_publising', 0);
			})
			->orWhere(function($query){
				$query->where('have_time_publising', 1);
				$query->where('publish_from', '<=', Carbon::now()->toDateTimeString());
				$query->where('publish_end', '>=', Carbon::now()->toDateTimeString());
			})
			->orderBy('created_at', 'desc')->first();
		//get the latest news:
		//Each catgeories have his own dedicated var
		$toppageMax = 6;
		// 最新情報全て
		$all   = News::getTopNews();
		// ニュースリリース
		$news  =  News::getTopNews(0);
		//ダイヤ改正
		$daiya =  News::getTopNews(1);
		//決算・中間決算
		$settlements = News::getTopNews(2);
		//地域ニュース
		$locals = News::getTopNews(3);
		//イベント
		$events = News::getTopNews(4);

		$customerVoices =  Feedback::where([
			['cv_status', '>=', 1]
			])->orderBy('created_at', 'desc')->take($toppageMax)->get();
		
		$currentTime = date('H:i');
		$isOffHours  = $this->dateUtility->isBetweenTime($currentTime);
		
		$allData = $this->formatAllNewsData($all);
		$allData = $allData->sortByDesc('created_at')->take(6);
		
		// get page
		//$pages = Page::all();
		return view('front.front')
		//	->with('headAdminTitle', $headAdminTitle)
			->with('situation', $situation)
			->with('news', $news)
			->with('daiya', $daiya)
			->with('settlements', $settlements)
			->with('locals', $locals)
			->with('events', $events)
			->with('customerVoices', $customerVoices)
			->with('isOffHours', $isOffHours)
			->with('all', $allData)
		//	->with('data', $pages)
			;
    }
	
  
	/**
	 *
	 * Display the default page for the front office
	 * News List
	 *
	 *2018-06-29: By default select only news for curent years
	 * 
	 * @since 2018/07/02 Louie: Pagination not converted to each tab
	 */
	protected function infoList(Request $request)
	{
		$headAdminTitle = '最新情報';
		$year = date("Y");
		//$years = News::select(['created_at'])->where([
		//	['status', '>=', '1'],
		//	['created_at', 'like', '%'.date("Y").'%']
		//])->distinct()->get();
		
		//$years = $years? $years->toArray(): array();
		//2018-06-15: greg: added created_at to select close for MySQL 5.7 compatibility.
		$yearsList = News::selectRaw('DISTINCT (DATE_FORMAT(created_at, "%Y")) as created_at_unique')
				->where([['status', '>=', '1']])
				->orderBy('created_at_unique', 'desc')
				->get();
				
		//		print_r($yearsList[0]->created_at);
		$yearsList = $yearsList? $yearsList->toArray(): array();
		
		// $all = News::where([
		// 	['status', '>=', '1']
		// 	])->orderBy('created_at', 'desc')
		// 	  ->paginate(session('limit'));		
		// //Greg: 2018-06-06: I'm using "like" on purpose: Mutiple categories can be saved.

			
		// $news =  News::where([
		// 	['status', '>=', 1],
		// 	['category', 'like', '%0%']
		// 	])->orderBy('created_at', 'desc')
		// 	->paginate(session('limit'));

		// //ダイヤ改正
		// $daiya =  News::where([
		// 	['status', '>=', 1],
		// 	['category', 'like', '%1%']
		// 	])->orderBy('created_at', 'desc')
		// 	->paginate(session('limit'));

		// //決算・中間決算
		// $settlements =  News::where([
		// 	['status', '>=', 1],
		// 	['category', 'like', '%2%']
		// 	])->orderBy('created_at', 'desc')
		// 	->paginate(session('limit'));

		// //地域ニュース
		// $locals =  News::where([
		// 	['status', '>=', 1],
		// 	['category', 'like', '%3%']
		// 	])->orderBy('created_at', 'desc')
		// 	->paginate(session('limit'));

		// //イベント
		// $events =  News::where([
		// 	['status', '>=', 1],
		// 	['category', 'like', '%4%']
		// 	])->orderBy('created_at', 'desc')
		// 	->paginate(session('limit'));
	
		// 2018/07/02 Louie: use new logic above
		// Use a single pagination
		// Using this, no longer need to foreach this part since the all tab contains a collapsed view for all tabs
		// See my comment in the infoListblade line 54 - 71
		// $all = News::where([
		// 	['status', '>=', '1']
		// 	])->orderBy('created_at', 'desc')
		// 	  ->paginate(session('limit'));
		
		// The paginated items can now itself be searched via like
		// $news = collect($all->items())->reject(function($element) {
		// 	return mb_strpos($element->category, 0) === false;
		// });
		
		// $daiya = collect($all->items())->reject(function($element) {
		// 	return mb_strpos($element->category, 1) === false;

		// });
		
		// $settlements = collect($all->items())->reject(function($element) {
		// 	return mb_strpos($element->category, 2) === false;
		// });
		
		// $locals = collect($all->items())->reject(function($element) {
		// 	return mb_strpos($element->category, 3) === false;
		// });
		
		// $events = collect($all->items())->reject(function($element) {
		// 	return mb_strpos($element->category, 4) === false;
		// });
		// END 2018/07/02 Louie
		
		$all = News::where([
		['status', '>=', '1']
		])->orderBy('created_at', 'desc')
			->get();
		
		$allData    = $this->formatAllNewsData($all);
		
		// dd($allData);

		$currentTab = $request->has('tab')
					? $request->input('tab')
					: null;

		$allData = PaginationUtility::getObjectPagination($allData, $request->only('tab'), ($currentTab == "最新情報全て" ) ? $request->input('page') : 1);

		//Greg: 2018-06-06: I'm using "like" on purpose: Mutiple categories can be saved.
		
		$news =  News::where([
			['status', '>=', 1],
			['category', 'like', '%0%']
			])->orderBy('created_at', 'desc')
			  ->get();

		$newsData = PaginationUtility::getObjectPagination($news, $request->only('tab'), ($currentTab == "ニュースリリース" ) ? $request->input('page') : 1);
		
		//ダイヤ改正
		$daiya =  News::where([
			['status', '>=', 1],
			['category', 'like', '%1%']
			])->orderBy('created_at', 'desc')
			->get();
		
		$daiyaData = PaginationUtility::getObjectPagination($daiya, $request->only('tab'), ($currentTab == "ダイヤ改正" ) ? $request->input('page') : 1);
		

		//決算・中間決算
		$settlements =  News::where([
			['status', '>=', 1],
			['category', 'like', '%2%']
			])->orderBy('created_at', 'desc')
			->get();

		$settlementData = PaginationUtility::getObjectPagination($settlements, $request->only('tab'), ($currentTab == "決算・中間決算" ) ? $request->input('page') : 1);
		
		//地域ニュース
		$locals =  News::where([
			['status', '>=', 1],
			['category', 'like', '%3%']
			])->orderBy('created_at', 'desc')
			->get();
		
		$localData = PaginationUtility::getObjectPagination($locals, $request->only('tab'), ($currentTab == "地域ニュース" ) ? $request->input('page') : 1);
		
		//イベント
		$events =  News::where([
			['status', '>=', 1],
			['category', 'like', '%4%']
			])->orderBy('created_at', 'desc')
			->get();

		$eventData = PaginationUtility::getObjectPagination($events, $request->only('tab'), ($currentTab == "イベント" ) ? $request->input('page') : 1);
		
		$pageDefaults        = $request->all();
		$pageDefaults['tab'] = (!empty($pageDefaults['tab']))
							 ? $pageDefaults['tab']
							 : '最新情報全て';

		$currentPaginatedData = null;

		if($request->input('page') >= 2){
			$currentPaginatedData = $this->getPaginatedNewsData($currentTab, $request->input('page'));
		}
		
		// END
			return view('front.infoList')
			->with('headAdminTitle', $headAdminTitle)
			->with('all', $allData)
			->with('news', $newsData)
			->with('daiya', $daiyaData)
			->with('settlements', $settlementData)
			->with('locals', $localData)
			->with('yearsList', $yearsList)
			->with('events', $eventData)
			->with('pageDefaults', $pageDefaults)
			->with('currentPaginatedData', $currentPaginatedData);
			;
	}
	
	
	private function getPaginatedNewsData($currentTab, $currentPage, $year = null)
	{
		$paginatedNewsData = null;
		
		switch($currentTab){
			case '最新情報全て':
				$all = News::where([
				['status', '>=', '1'],
				['created_at', 'like', "%$year%"]
				])->orderBy('created_at', 'desc')
					->get();
				$allData		   = $this->formatAllNewsData($all);
				$paginatedNewsData = PaginationUtility::getObjectPagination($allData, ['tab' => $currentTab], 1);
				
				break;
			case 'ニュースリリース':
				$news =  News::where([
					['status', '>=', 1],
					['category', 'like', '%0%'],
					['created_at', 'like', "%$year%"]
					])->orderBy('created_at', 'desc')
					->get();

				$paginatedNewsData = PaginationUtility::getObjectPagination($news, $currentTab, 1);
				break;
			case 'ダイヤ改正':
				$daiya =  News::where([
					['status', '>=', 1],
					['category', 'like', '%1%'],
					['created_at', 'like', "%$year%"]
					])->orderBy('created_at', 'desc')
					->get();

				$paginatedNewsData = PaginationUtility::getObjectPagination($daiya, $currentTab, 1);
				break;
			case '決算・中間決算':
				$settlements =  News::where([
					['status', '>=', 1],
					['category', 'like', '%2%'],
					['created_at', 'like', "%$year%"]
					])->orderBy('created_at', 'desc')
					->get();

				$paginatedNewsData = PaginationUtility::getObjectPagination($settlements, $currentTab, 1);
				break;
			case '地域ニュース':
				$locals =  News::where([
					['status', '>=', 1],
					['category', 'like', '%3%'],
					['created_at', 'like', "%$year%"]
					])->orderBy('created_at', 'desc')
					->get();

				$paginatedNewsData = PaginationUtility::getObjectPagination($locals, $currentTab, 1);
				break;
			case 'イベント':
				//イベント
				$events =  News::where([
					['status', '>=', 1],
					['category', 'like', '%4%'],
					['created_at', 'like', "%$year%"]
					])->orderBy('created_at', 'desc')
					->get();

				$paginatedNewsData = PaginationUtility::getObjectPagination($events, $currentTab, 1);
				break;
			
			default:
				$paginatedNewsData = null;
		}

		return $paginatedNewsData;
	}

	/**
	 * 
	 * @since 2018/07/02 Louie: Pagination not converted to each tab
	 */
	public function getInfoList(Request $request, $year)
	{
		$headAdminTitle = '最新情報';
		
		$yearsList = News::selectRaw('DISTINCT (DATE_FORMAT(created_at, "%Y")) as created_at_unique')
				->where([['status', '>=', '1']])
				->orderBy('created_at_unique', 'desc')
				->get();
				
		$yearsList = $yearsList? $yearsList->toArray(): array();
			
		// $all = News::where([
		// 	['status', '>=', '1'],
		// 	['created_at', 'like', "%$year%"]
		// ])->orderBy('created_at', 'desc')
		//   ->paginate(session('limit'));

		// // The paginated items can now itself be searched via like
		// $news = collect($all->items())->reject(function($element) {
		// 	return mb_strpos($element->category, 0) === false;
		// });
		
		// $daiya = collect($all->items())->reject(function($element) {
		// 	return mb_strpos($element->category, 1) === false;
		// });
		
		// $settlements = collect($all->items())->reject(function($element) {
		// 	return mb_strpos($element->category, 2) === false;
		// });
		
		// $locals = collect($all->items())->reject(function($element) {
		// 	return mb_strpos($element->category, 3) === false;
		// });
		
		// $events = collect($all->items())->reject(function($element) {
		// 	return mb_strpos($element->category, 4) === false;
		// });
		
		// 2018/07/02 Louie: comment. Use logic above
		// 2018/06/08 Louie: Just reuse a single query and search the paginated limit below

		$all = News::where([
		['status', '>=', '1'],
		['created_at', 'like', "%$year%"]
		])->orderBy('created_at', 'desc')
			->get();
		
		$allData    = $this->formatAllNewsData($all);
		
		$currentTab = $request->has('tab')
					? $request->input('tab')
					: null;

		$allData = PaginationUtility::getObjectPagination($allData, $request->only('tab'),  ($currentTab == "最新情報全て" ) ? $request->input('page') : 1);

		//Greg: 2018-06-06: I'm using "like" on purpose: Mutiple categories can be saved.

		$news =  News::where([
			['status', '>=', 1],
			['created_at', 'like', "%$year%"],
			['category', 'like', '%0%']
			])->orderBy('created_at', 'desc')
			  ->get();

		$newsData = PaginationUtility::getObjectPagination($news, $request->only('tab'), ($currentTab == "ニュースリリース" ) ? $request->input('page') : 1);
		
		//ダイヤ改正
		$daiya =  News::where([
			['status', '>=', 1],
			['created_at', 'like', "%$year%"],
			['category', 'like', '%1%']
			])->orderBy('created_at', 'desc')
			->get();
		
		$daiyaData = PaginationUtility::getObjectPagination($daiya, $request->only('tab'), ($currentTab == "ダイヤ改正" ) ? $request->input('page') : 1);
		
		//決算・中間決算
		$settlements =  News::where([
			['status', '>=', 1],
			['created_at', 'like', "%$year%"],
			['category', 'like', '%2%']
			])->orderBy('created_at', 'desc')
			->get();

		$settlementData = PaginationUtility::getObjectPagination($settlements, $request->only('tab'), ($currentTab == "決算・中間決算" ) ? $request->input('page') : 1);
		
		//地域ニュース
		$locals =  News::where([
			['status', '>=', 1],
			['created_at', 'like', "%$year%"],
			['category', 'like', '%3%']
			])->orderBy('created_at', 'desc')
			->get();
		
		$localData = PaginationUtility::getObjectPagination($locals, $request->only('tab'), ($currentTab == "地域ニュース" ) ? $request->input('page') : 1);
		
		//イベント
		$events =  News::where([
			['status', '>=', 1],
			['created_at', 'like', "%$year%"],
			['category', 'like', '%4%']
			])->orderBy('created_at', 'desc')
			->get();

		$eventData = PaginationUtility::getObjectPagination($events, $request->only('tab'), ($currentTab == "イベント" ) ? $request->input('page') : 1);
		
		$pageDefaults        = $request->all();
		$pageDefaults['tab'] = (!empty($pageDefaults['tab']))
							 ? $pageDefaults['tab']
							 : '最新情報全て';
		$currentPaginatedData = null;

		if($request->input('page') >= 2){
			$currentPaginatedData = $this->getPaginatedNewsData($currentTab, $request->input('page'), $year);
		}

		return view('front.infoList')
			->with('headAdminTitle', $headAdminTitle)
			->with('all', $allData)
			->with('news', $newsData)
			->with('daiya', $daiyaData)
			->with('settlements', $settlementData)
			->with('locals', $localData)
			->with('yearsList', $yearsList)
			->with('yearSearched', $year)
			->with('events', $eventData)
			->with('pageDefaults', $pageDefaults)
			->with('currentPaginatedData', $currentPaginatedData)
			;
	}

	protected function showInfo($id)
	{
		$headAdminTitle = '最新情報';
		$category = array();
		$catClasses = array('btn-blue',
			'btn-cyan',
			'btn-orange',
			'btn-purple',
			'btn-pink');
			
		
		$news =  News::find($id);
		$news->regional_category =	$news->regional_category !== null 
								 ?	explode(',', $news->regional_category)
								 : [];

		$news->regional_category = CategoryHelper::prepareRegionalCategories($news);
		
		$category =	$news->category !== null 
				  ?	explode(',', $news->category)
				  : [];
		
		return view('front.info')
				->with('data', $news)
				->with('catClasses', $catClasses)
				->with('categories', $category)
				->with('headAdminTitle', $headAdminTitle)
				;
	}
	/**
	 *
	 * Display the default page for the front office
	 * Customer voice List
	 *
	 */
	protected function voiceList()
	{
		$headAdminTitle = 'お客様の声';
		$customerVoices =  Feedback::where('cv_status', '>=', 1)
									->paginate(config('constants.PAGINATION.front_pc'));
		return view('front.voiceList')
			->with('headAdminTitle', $headAdminTitle)
			->with('customerVoices', $customerVoices)
		;
	}


	protected function showVoice($id)
	{
		$voice =  Feedback::find($id);
		$category = array();
		
		$headAdminTitle = 'お客様の声';
		
		return view('front.voice')
				->with('voice', $voice)
				->with('headAdminTitle', $headAdminTitle)
		;
	}
	protected function showSituation($id)
	{
		$situation =  Situation::find($id);
		$category = array();
		$filePics =$situation->filePics();

		$currentTime = date('H:i');
        $isOffHours  = $this->dateUtility->isBetweenTime($currentTime);

		$headAdminTitle = 'お客様の声';
		
		return view('front.situationDetails')
				->with('situation', $situation)
				->with('filePics', $filePics)
				->with('isOffHours', $isOffHours)
				->with('headAdminTitle', $headAdminTitle)
		;
	}
	protected function showLastSituation()
	{
		$currentTime = date('H:i');
		$isOffHours  = $this->dateUtility->isBetweenTime($currentTime);
		
		// 2017/06/18 Louie: Show latest situation based on front logic 
		// $situation =  Situation::latest()->first();
		$situation =   Situation::where(function($query) {
				$query->where('publishing_status','>' , 0);
				$query->where('have_time_publising', 0);
			})
			->orWhere([
					['have_time_publising', '=', 1],
					['publish_from', '<=', Carbon::now()],
					['publish_end', '>=', Carbon::now()],
			])
			->orderBy('created_at', 'desc')->first();
		// end
			
		$category = array();
		
		if( $situation){
			$filePics = $situation->filePics();
		}else {
			$filePics = array();
			$situation = array('situation_status'=>0);
		}



		$headAdminTitle = 'お客様の声';
		
		return view('front.situationDetails')
				->with('situation', $situation)
				->with('filePics', $filePics)
				->with('isOffHours', $isOffHours)
				->with('headAdminTitle', $headAdminTitle)
		;
	}
	
	public function getCalculateModalShiftView()
    {
		return view('front.modalshift.calculate');
	}

	public function calculateModalShift(Request $request)
	{
		$data = $request->all();

		$collection = array();
		$train      = array();
		$delivery   = array();
		$collectionResults = null;
		$deliveryResults   = null;
		$trainResults      = null;
		$totalResults      = null;
		
		$collectionType = config('calculator.TRUCK_TYPES');
		
		$weight     = $data['weight'] ?? 0;

		$collection['distance'] 	 = $data['collection_distance'];
		$collection['type']     	 = array_search($data['collection_type'], $collectionType);
		$collection['max_load']      = $data['collection_max_load'] ?? 0;
		$collection['loading_ratio'] = $data['collection_loading_ratio'] ?? 0;
		$collection['container_max_load'] = $data['collection_container_max_load'] ?? 0;
		
		if($request->has('start_prefecture') && $request->has('dest_prefecture')){
			$train['start_prefecture']    = $data['start_prefecture'];
			$train['start_station']       = $data['start_station'];
			$train['dest_prefecture']     = $data['dest_prefecture'];
			$train['dest_station']        = $data['dest_station'];
			
			$train['start_station_name']  = $data['start_station_name'];
			$train['dest_station_name']   = $data['dest_station_name'];
		}

		if($request->has('delivery_type')){
			$delivery['distance']           = $data['delivery_distance'];
			$delivery['type']               = array_search($data['delivery_type'], $collectionType);
			$delivery['max_load']           = $data['delivery_max_load'] ?? 0;
			$delivery['loading_ratio']      = $data['delivery_loading_ratio'] ?? 0;
			$delivery['container_max_load'] = $data['delivery_container_max_load'] ?? 0;
		}
		
		$collectionResults   = $this->calculatorService->calculateTruck($weight, $collection);

		if($delivery){
			$deliveryResults = $this->calculatorService->calculateTruck($weight, $delivery);
		}

		if($train){
			$trainResults    = $this->calculatorService->calculateTrain($weight, $train);
		}

		if($delivery || $train){
			$totalResults    = $this->calculatorService->calculateTotal($collectionResults, 
																 $deliveryResults, 
																 $trainResults);
		}
		
		return view('front.modalshift.finish')->with('weight', $weight)
											  ->with('collectionResults', $collectionResults)
											  ->with('deliveryResults', $deliveryResults)
											  ->with('trainResults', $trainResults)
											  ->with('totalResults', $totalResults);
	}
	
	
	/**
	 * GET (AJAX): /info/{tab}
	 * 
	 * Get news tab page data
	 * 
	 * UNUSED
	 * 
	 * @param  \Illuminate\Http\Request  $request
	 * @param string $tab
	 * 
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since 2018/07/06
	 */
	public function getNewsTabData(Request $request, $tab)
	{
		$items = null;

		switch($tab){
	
			default:
				$items = null;
		}

		$all = News::where([
		['status', '>=', '1']
		])->orderBy('created_at', 'desc')
			->get();
		
		$allData   = $this->formatAllNewsData($all);
		
		$currentTab = $request->has('tab')
					? $request->input('tab')
					: null;

		$allData = PaginationUtility::getObjectPagination($allData, $request->only('tab'));

		//Greg: 2018-06-06: I'm using "like" on purpose: Mutiple categories can be saved.
		$news =  News::where([
			['status', '>=', 1],
			['category', 'like', '%0%']
			])->orderBy('created_at', 'desc')
			  ->get();

		$newsData = PaginationUtility::getObjectPagination($news, $request->only('tab'), ($currentTab == "ニュースリリース" ) ? $request->input('page') : 1);
		
		//ダイヤ改正
		$daiya =  News::where([
			['status', '>=', 1],
			['category', 'like', '%1%']
			])->orderBy('created_at', 'desc')
			->get();
		
		$daiyaData = PaginationUtility::getObjectPagination($daiya, $request->only('tab'), ($currentTab == "ダイヤ改正" ) ? $request->input('page') : 1);
		

		//決算・中間決算
		$settlements =  News::where([
			['status', '>=', 1],
			['category', 'like', '%2%']
			])->orderBy('created_at', 'desc')
			->get();

		$settlementData = PaginationUtility::getObjectPagination($settlements, $request->only('tab'), ($currentTab == "決算・中間決算" ) ? $request->input('page') : 1);
		
		//地域ニュース
		$locals =  News::where([
			['status', '>=', 1],
			['category', 'like', '%3%']
			])->orderBy('created_at', 'desc')
			->get();
		
		$localData = PaginationUtility::getObjectPagination($locals, $request->only('tab'), ($currentTab == "地域ニュース" ) ? $request->input('page') : 1);
		
		//イベント
		$events =  News::where([
			['status', '>=', 1],
			['category', 'like', '%4%']
			])->orderBy('created_at', 'desc')
			->get();

		$eventData = PaginationUtility::getObjectPagination($events, $request->only('tab'), ($currentTab == "イベント" ) ? $request->input('page') : 1);
		
		return view()->make('partials.news_table')->with('items', $items)
												  ->with('currentTab', $currentTab);
	}

	/**
	 * Flattens all news into each item per category
	 * 
	 * @param Collection<News> $all
	 * 
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since 2018/07/06
	 * 
	 * @return Collection
	 */
	private function formatAllNewsData($all)
	{
		$allData = collect();

		foreach($all as $item){

			foreach($item->categories as $category){
				$itemPerCategory = array();
				$itemPerCategory['created_at']  = $item->created_at;
				$itemPerCategory['category']    = $category;
				$itemPerCategory['news_title']  = $item->news_title;
				$itemPerCategory['id']          = $item->id;
				
				if($item->is_link){
					$itemPerCategory['url']         = url('/') . '/' . $item->media->path;
					$itemPerCategory['is_link']    = true;
				}else{
					$itemPerCategory['url']         = url('/info') . '/detail' . $item->id;
					$itemPerCategory['is_link']    = false;
				}
				
				if($category == '地域ニュース' && $item->regional_category){								
					$itemPerCategory['news_title']  = '['. $item->regionalCategoryUnion . '] '.  $item->news_title;
				}elseif($category == 'イベント'){
					if( $item->validity && strtotime( $item->validity) < strtotime('NOW')){
						$itemPerCategory['invalid'] = true;
					}
				}

				// SET MAX for news title

				$itemPerCategory['news_title'] = (mb_strlen($itemPerCategory['news_title']) > 37)
										       ? mb_substr($itemPerCategory['news_title'], 0, 37) .  "..."
											   : $itemPerCategory['news_title'];

				$allData->push($itemPerCategory);
			}
		}

		return $allData;
	}
}
