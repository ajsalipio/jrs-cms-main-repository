<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\User;
use App\Http\Requests\StoreUser;

class UserController extends Controller
{
     /**
     * Inject dependencies
     *
     * @return void
     */
    public function __construct()
    {
    
    }
    
    /**
	 * GET: /user/list
	 *
	 * Pre load all user lists with pagination
	 * 
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since  2018/07/03
	 *
	 * @return  \Illuminate\Http\Response user/index.blade.php
	 */
    public function index()
    {
        $headAdminTitle = 'ユーザー 一覧・削除';
        $users          = User::orderBy('created_at', 'desc')
                              ->where('id', '!=', 1)
                              ->paginate(config('constants.PAGINATION.limit'));
        
        return view('admin.user.index')->with('headAdminTitle', $headAdminTitle)
                                       ->with('users', $users);
    }

    /**
	 * GET: /user/add
	 * 
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since  2018/07/03
	 *
	 * @return  \Illuminate\Http\Response user/create.blade.php
	 */
    public function create()
    {
        $headAdminTitle = 'ユーザー 作成';
        
        return view('admin.user.create')->with('headAdminTitle', $headAdminTitle);
    }

    /**
	 * POST: /user/add
	 *
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since  2018/07/04
	 *
     * @param  StoreUser  $request
	 * @return  \Illuminate\Http\Response user/index.blade.php
	 */
    public function store(StoreUser $request)
    {
        $data = $request->all();
        
        $user = new User();
        $user->email      = $data['email'];
        $user->password   = Hash::make($data['password']);
        $user->group      = $data['group'];
        
        $user->save();

        return redirect('dashboard/user/list');
    }

    /**
     * GET: /situation/edit
     *
     *  Show the form for editing a situtation.
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @since  2018/05/21
     *
     * @param  int $id
     * @return  \Illuminate\Http\Response user/edit.blade.php
     */
    public function edit($id)
    {
        $user = User::find($id);
        
        return view('admin.user.edit')->with('user', $user);
    }

    /**
	 * POST: /situation/add
	 *
	 *  Show the form for creating a situtation.
	 * 
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since  2018/05/21
	 *
     * @param  int $id
     * @param  StoreUser $request
	 * @return  \Illuminate\Http\Response user/edit.blade.php
	 */
    public function update(StoreUser $request, $id)
    {
        $data = $request->all();
        
        $user = User::find($id);
        $user->email      = $data['email'];
        $user->password   = Hash::make($data['password']);
        $user->group      = $data['group'];
        
        $user->save();
        
        return redirect('dashboard/user/edit/' . $id);
    }

    /**
	 * POST: /user/delete
	 *
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since  2018/07/04
	 *
     * @param  \Illuminate\Http\Request  $request
	 * @return  \Illuminate\Http\Response user/index.blade.php
	 */
    public function delete(Request $request)
    {
        $results = 0;
        $data    = $request->all();
        
        $archivedItems = $data['archived_items'] ?? null;
        
        if($archivedItems){
                foreach($archivedItems as $id){
                    $situation = User::find($id)->delete();
            }
        }

        return redirect('dashboard/user/list');
    }

    /**
	 * POST: /user/profile
	 *
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since  2018/07/05
	 *
     * @param  \Illuminate\Http\Request  $request
	 * @return  \Illuminate\Http\Response user/index.blade.php
	 */
    public function editProfileView(Request $request)
    {
        $user = \Auth::user();
        
        return view('admin.user.profile')->with('user', $user);
    }

    /**
	 * POST: /user/profile
	 *
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since  2018/07/05
	 *
     * @param  \Illuminate\Http\Request  $request
	 * @return  \Illuminate\Http\Response user/index.blade.php
	 */
    public function changePassword(Request $request, $id)
    {
        $data = $request->all();
        
        $user = User::find($id);
        $user->password   = Hash::make($data['password']);

        $user->save();

        return redirect('dashboard/user/profile');
    }
    

}