<?php

namespace App\Http\Controllers;

use App\Models\Situation;
use App\Models\SituationImage;
use App\Models\Mail as MailModel;
use App\Utilities\FileUtility;
use App\Utilities\DateUtility;
use App\Mail\SituationPublishMail;
use App\Http\Requests\StoreSituation;

use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;

use Carbon\Carbon;
use Log;

//for custom query
use Illuminate\Support\Facades\DB;

/**
 * Situation / Transportation Controller
 *
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @copyright 2018 Commude Philippines, Inc.
 * 
 */
class SituationController extends Controller
{
    protected $fileUtility;
    protected $dateUtility;
    
    /**
     * Create a new controller instance. Inject dependencies
     *
     * @return void
     */
    public function __construct(FileUtility $fileUtility, DateUtility $dateUtility)
    {
       $this->fileUtility = $fileUtility;
       $this->dateUtility = $dateUtility;
    }

    /**
	 * GET: /situation/list
	 *
	 * Pre load all spot lists with pagination
	 * 
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since  2018/05/21
	 *
	 * @return  \Illuminate\Http\Response situation/index.blade.php
	 */
    public function index()
    {
        $headAdminTitle = '現在の輸送状況　一覧';
        $situations     = Situation::orderBy('created_at', 'desc')
                                   ->paginate(config('constants.PAGINATION.limit'));
        
        return view('admin.situation.index')->with('headAdminTitle', $headAdminTitle)
                                            ->with('situations', $situations);
    }


    /**
	 * GET: /situation/add
	 *
	 *  Show the form for creating a situtation.
	 * 
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since  2018/05/21
	 * @since  2018/06/27 Louie: Remove 3rd status from list indefinitely
     * 
	 * @return  \Illuminate\Http\Response situation/create.blade.php
	 */
    public function create()
    {
        $headAdminTitle    = '現在の輸送状況　作成';
        $situationStatuses = config('constants.SITUATION_STATUS');
        $currentTime       = date('H:i');// '02:01'; // MANUAL FORCE time to 2AM for testing
        
        // If in between default time. Can be overriden, just add a parameter of current time in '(H:i)'
        // $timeVar = ($this->dateUtility->isBetweenTime($currentTime))
        //          ? config('constants.PUBLISH_TIME_OUT_OF_SERVICE.MESSAGE')
        //          : null;
        $isOffHours  = $this->dateUtility->isBetweenTime($currentTime);
        
        unset($situationStatuses[2]);
        
        $timeVar    = config('constants.PUBLISH_TIME_OUT_OF_SERVICE.MESSAGE');
        
        return view('admin.situation.create')->with('headAdminTitle', $headAdminTitle)
                                             ->with('timeVar', $timeVar)
                                             ->with('situationStatuses', $situationStatuses);
    }

    /**
     * GET: /situation/edit
     *
     *  Show the form for editing a situtation.
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @since  2018/05/21
     *
     * @param  int $id
     * @return  \Illuminate\Http\Response situation/edit.blade.php
     */
    public function edit($id)
    {
        $situation         = Situation::find($id);
        $situationStatuses = config('constants.SITUATION_STATUS');

        if(empty($situation)){
            return abort(404);
        }
        
        $currentTime    = date('H:i');// '02:01'; // MANUAL FORCE time to 2AM for testing
        
        // If in between default time. Can be overriden, just add a parameter of current time in '(H:i)'
        // $timeVar = ($this->dateUtility->isBetweenTime($currentTime))
        //          ? config('constants.PUBLISH_TIME_OUT_OF_SERVICE.MESSAGE')
        //          : null;
        $isOffHours  = $this->dateUtility->isBetweenTime($currentTime);
        
        unset($situationStatuses[2]);

        $timeVar     = config('constants.PUBLISH_TIME_OUT_OF_SERVICE.MESSAGE');

        $filePicCount = count($situation->filePics());
        $filePicExist = $filePicCount > 0;
        
        return view('admin.situation.edit')->with('situation', $situation)
                                           ->with('timeVar', $timeVar)
                                           ->with('filePicCount', $filePicCount)
                                           ->with('filePicExist', $filePicExist)
                                           ->with('situationStatuses', $situationStatuses);
    }

    /**
	 * GET: /situation/{id}
	 *
	 *  Show the form for editing a situtation.
	 * 
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since  2018/05/28
	 *
     * @param  int $id
	 * @return  \Illuminate\Http\Response front/situation.blade.php
	 */
    public function show($id)
    {
        $situation   = Situation::find($id);
        
        if(!$situation['have_time_publising']){
            return abort(404); // Override with page to redirect for non existing pages
        }
       
        return view('front.situation')->with('situation', $situation);
    }

    /**
	 * POST: /situation/add
	 *
	 *  Show the form for creating a situtation.
	 * 
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since  2018/05/21
	 *
     * @param  StoreSituation  $request
	 * @return  \Illuminate\Http\Response situation/index.blade.php
	 */
    public function store(StoreSituation $request)
    {
        $data = $request->all();
        
        // Create new instance of Situation Model
        $situation         = new Situation();
        $hasTimePublishing = $data['have_time_publising'] ?? 0;

        // Set data for each column
        $situation->situation_title     = $data['situation_title'];
        $situation->situation_content   = $data['situation_content'];
        $situation->situation_details   = $data['situation_details'];
        $situation->publish_from        = $data['publish_from'];
        $situation->publish_end         = $data['publish_end'];
        $situation->have_time_publising = $hasTimePublishing;
        $situation->publishing_status   = $data['publishing_status'];
        $situation->situation_status    = $data['situation_status'];

        // Manage files
        if($request->hasFile('file_xls')){
            $situation->file_xls  = MediaController::storeCustom($request->file('file_xls'));
        }
        
        if($request->hasFile('file_doc')){
            $situation->file_doc  = MediaController::storeCustom($request->file('file_doc'));
        }
        
		$situation->save();
       
        // Successful
        if($situation['id']){

            if($request->hasFile('file_pic')){
                foreach($request->file('file_pic') as $filePic){
                    $situationImage = new SituationImage();

                    $situationImage->situation_id = $situation['id'];
                    $situationImage->media_id     = MediaController::storeCustom($filePic);

                    $situationImage->save();
                }
            }
            
            $isScheduled = ($hasTimePublishing == 1);

            if($data['publishing_status'] == 2){
                $this->sendMail($situation, $isScheduled);
            }
        }
       

        return redirect('dashboard/situation/list');
    }

    /**
	 * POST: /situation/add
	 *
	 *  Show the form for creating a situtation.
	 * 
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since  2018/05/21
	 *
     * @param  int $id
     * @param  StoreSituation $request
	 * @return  \Illuminate\Http\Response situation/edit.blade.php
	 */
    public function update(StoreSituation $request, $id)
    {
        $data = $request->all();
        
        $arrs = 1;
        
        // get instance of Situation via id
        $situation           = Situation::find($id);
        $hasTimePublishing   = $data['have_time_publising'] ?? 0;
        $originalPublishDate = $situation->getOriginal()['publish_from'];
        
        // Set data for each column
        $situation->situation_title     = $data['situation_title'];
        $situation->situation_content   = $data['situation_content'];
        $situation->situation_details   = $data['situation_details'];
        $situation->publish_from        = $data['publish_from'];
        $situation->publish_end         = $data['publish_end'];
        $situation->have_time_publising = $hasTimePublishing;
        $situation->publishing_status   = $data['publishing_status'];
        $situation->situation_status    = $data['situation_status'];
        
        // Manage files
        $situation->file_xls = $this->manageFileUpdate(
                                    $situation, 
                                   'file_xls', 
                                    $request->file('file_xls'), 
                                    $data['file_xls_archived'],
                                    $situation->fileXls['media_id']);
        $situation->file_doc = $this->manageFileUpdate(
                                    $situation, 
                                    'file_doc', 
                                    $request->file('file_doc'), 
                                    $data['file_doc_archived'],
                                    $situation->fileDoc['media_id']);
       
        $situation->save();
        
        $situation->file_pic = $this->manageMultipleFileUpdate(
                                    $situation->id,
                                    $request->file('file_pic'), 
                                    $data['file_pic_archived']);

        $isScheduled = ($hasTimePublishing == 1 
                    && Carbon::parse($data['publish_from']) < Carbon::parse($originalPublishDate)); // If publish_from changed
    
        if($data['publishing_status'] == 2){
            $this->sendMail($situation, $isScheduled);
        }

        return redirect('dashboard/situation/edit/' . $id);
    }

    /**
	 * POST: /situation/delete
	 *
	 *  Show the form for creating a situtation.
	 * 
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since  2018/05/21
	 *
     * @param  \Illuminate\Http\Request  $request
	 * @return  \Illuminate\Http\Response situation/create.blade.php
	 */
    public function delete(Request $request)
    {
        $results = 0;
        $data    = $request->all();
        
        $archivedItems = $data['archived_items'] ?? null;
        
        if($archivedItems){
            foreach($archivedItems as $id){
                $situation = Situation::find($id);
                $mediaId   = $situation->fileXls['media_id'];
                $results  += $situation->delete();

                // Delete media files if exist
                if($mediaId){
                    MediaController::deleteCustom($mediaId);
                }
            }
        }
        
        return redirect('dashboard/situation/list');
    }

    /**
	 * POST: /situation/preview
	 *
	 *  Preview current input data form
	 * 
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since  2018/05/28
	 *
     * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response situation/edit.blade.php
	 */
    public function preview(Request $request)
    {
        $headAdminTitle = 'プレビュー';
        $situation      = $request->all();
       
        $situation['situation_status'] = ($request->has('situation_status'))
                                       ? $situation['situation_status'] 
                                       : null;
        
        $fileXls = ($request->hasfile('file_xls'))
                ? $this->fileUtility->storeTempFile($request->file('file_xls'))
                : json_decode($situation['file_xls_existing'] ?? null, true);
        $fileDoc = ($request->hasfile('file_doc'))
                ? $this->fileUtility->storeTempFile($request->file('file_doc'))
                :json_decode($situation['file_doc_existing'] ?? null, true);

        $filePics = array();

        // Append existing pics
        $existingPics   = json_decode($situation['file_pic_existing'] ?? null, true);
        $archivedPicIds = isset($situation['file_pic_archived']) ? array_filter(explode(',', $situation['file_pic_archived'])): array();
        if($existingPics){
            foreach($existingPics as $existingPic){
                // Add only unarchived pictures
                if(!in_array($existingPic['media_id'], $archivedPicIds)){
                    $filePics[] = $existingPic;
                }                
            }
        }
        
        if($request->hasfile('file_pic')){
            foreach($request->file('file_pic') as $filePic){
                $filePics[] = $this->fileUtility->storeTempFile($filePic);
            }
        }
        
        return view('admin.situation.preview')->with('situation', $situation)
                                              ->with('fileXls', $fileXls)
                                              ->with('fileDoc', $fileDoc)
                                              ->with('filePics', $filePics);
    }

    /**
	 * Manage file update of model file column (add,update or delete)
	 * 
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since  2018/05/23
	 *
     * @param  App\Model\Situation $situation
     * @param  string $fileColumnName
     * @param  \Illuminate\Http\Request  $requestFile
     * @param  bool $isArchived
     * @param  int $mediaId
     * 
	 * @return  \Illuminate\Http\Response situation/create.blade.php
	 */
    private function manageFileUpdate($model, $fileColumnName, $requestFile, $isArchived, $mediaId)
    {    
        if($requestFile){ // File uploaded exist, proceed with check if new or change            
            if($mediaId){ // Change
                MediaController::updateCustom($requestFile, $mediaId);
            }else{ // New
                $mediaId = MediaController::storeCustom($requestFile);
            }
        }else if($isArchived){ // Just delete it
            // Delete link to parent first
            $model[$fileColumnName] = null;
            $model->save();
            
            MediaController::deleteCustom($mediaId);
            $mediaId = null;
        }

        return $mediaId;
    }

    /**
	 * Manage file update of model child bag
     * 
     * Update same as change
     * Delete = (Changed or flagged for removal)
	 * 
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since  2018/06/06
	 *
     * @param  int $situationId
     * @param  Array<\Illuminate\Http\Request>  $requestFiles
     * @param  string $archivedMediaIds
     * 
	 * @return  \Illuminate\Http\Response situation/create.blade.php
	 */
    public function manageMultipleFileUpdate($situationId, $requestFiles, $archivedMediaIds)
    {    
        $archivedMediaIds = array_filter(explode(',', $archivedMediaIds));
        $result = null;

        if($requestFiles){ // File uploaded exist, proceed with creation
            
            foreach($requestFiles as $requestFile){
                $situationImage = new SituationImage();

                $situationImage->situation_id = $situationId;
                $situationImage->media_id     = MediaController::storeCustom($requestFile);

                $result += $situationImage->save();
            }
        }

        // Delete all archived or changed files
        foreach($archivedMediaIds as $archivedMediaId){                
            // Let the on delete cascade handle the deletion of the situation images
            // $result += SituationImage::where('media_id', $archivedMediaId)
            //               ->where('situation_id', $situation->id)
            //               ->delete();
            $result += MediaController::deleteCustom($archivedMediaId);
        }

        return $result;
    }
    
    
     /**
	 * Send mail to all users given a situation object
	 * 
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since  2018/06/04
	 *
     * @param  App\Model\Situation $situation
     * @param  bool $isScheduled
     * 
	 */
    private function sendMail($situation, $isScheduled)
    {
        $recipients = MailModel::all();
            
        foreach($recipients as $recipient)
        {
            $isFileAttached = null;
            
                // Check if files are to be attached
            switch($recipient['options']) {
                case 0:
                    $isFileAttached = true;
                    break;
                default: // case 1 and 2
                    $isFileAttached = false;
            }
                
            // Schedule sending via job = entry in jobs
            if($isScheduled){
                    Mail::to($recipient['mail_address'])->later(
                    // Carbon::now()->addMinute(2), // TESTER
                    // $situation['publish_from'], // Schedule a time // Old
                    Carbon::parse($situation['publish_from']),
                    new SituationPublishMail($situation, $isFileAttached, $recipient['options'])
                );
            }else{
                // Send immediately
                Mail::to($recipient['mail_address'])->send(
                    new SituationPublishMail($situation, $isFileAttached, $recipient['options'])
                );
            }

            if(count(Mail::failures()) > 0 ) {
                Log::error(Mail:: failures());
            }
        }
    }
}
