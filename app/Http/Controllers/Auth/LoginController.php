<?php

namespace App\Http\Controllers\Auth;

use App\User;

use App\Utilities\AuthenticationUtility;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;


use Cookie;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    protected $authenticationUtility;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(AuthenticationUtility $authenticationUtility)
    {
        $this->middleware(['guest', 'login_cookie'])->except('logout');
        $this->authenticationUtility = $authenticationUtility;
    }

     /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response 
     */
    public function showLoginForm()
    {
        $user = $this->authenticationUtility->getUserFromCookie();

        return view('auth.login')->with('user', $user);
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        // If checked and exist -> login via cookie
        if(Cookie::get(config('constants.LOGIN_COOKIE_ID'))){
            $user = $this->authenticationUtility->getUserFromEncryption($request->password);
            
            if($user){
                Auth::login($user, true);

                if(!$request->has('remember_me')){            
                    Cookie::queue(
                        Cookie::forget(config('constants.LOGIN_COOKIE_ID'))
                    );
                }
            
                return $this->sendLoginResponse($request);
            }
        }

        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {

            if(!$request->has('remember_me')){            
                Cookie::queue(
                    Cookie::forget(config('constants.LOGIN_COOKIE_ID'))
                );
            }

            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect('/login');
    }
}
