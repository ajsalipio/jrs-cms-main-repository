<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//for custom query
use Illuminate\Support\Facades\DB;
use App\Models\CustomerVoice as Feedback;
use App\Models\News;
use App\Models\Situation;
use App\Models\Mail;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

	public function getApacheVersion()
	{
		$version = isset($_SERVER['SERVER_SOFTWARE']) ? explode( ' ', $_SERVER['SERVER_SOFTWARE'])[0] : '---';
		return $version;
	}

	public function getSQLVersion()
	{
		$results = DB::select( DB::raw("select version()") );
		$mysql_version =  $results[0]->{'version()'};
		
		return (strpos($mysql_version, 'Maria') !== false
			? 'mariaDB:'
			: 'MySQL:'
		).$mysql_version;
	}

    /**
     * Show the application dashboard.
     *
	 * @since 2018/06/27 Louie: adjusted customer voices count logic
	 * 
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$headAdminTitle = 'ダッシュボード';
		$appver= config('app.name', 'Laravel');
		$apache = function_exists( 'apache_get_version')
			?apache_get_version()
			: $this->getApacheVersion();
		$phpver ='PHP: '.phpversion();
		$mysqlver =$this->getSQLVersion();

		//Situation:
		//retrieve last entry details
		$theSituation = Situation::latest()->first();
		$situationInfo = $theSituation? $theSituation->toArray(): array(
			'publishing_status' => 0,
			'situation_title' => '',
			'created_at' => '',
			);

		//Mails: display count by options
		$theMailsAttachementPC = Mail::select(DB::raw('count(`mail_id`) AS total, MAX(created_at) AS day'))
			->where('options', 0)
			->get();
		$totalMailsAttachementPC = isset($theMailsAttachementPC->toArray()[0]['total']) ?$theMailsAttachementPC->toArray()[0]['total'] : 0;
		
		$theMailsPC = Mail::select(DB::raw('count(`mail_id`) AS total, MAX(created_at) AS day'))
			->where('options', 1)
			->get();
		$totalMailsPC = isset($theMailsPC->toArray()[0]['total']) ?$theMailsPC->toArray()[0]['total'] : 0;
		
		$theMailsPhone = Mail::select(DB::raw('count(`mail_id`) AS total, MAX(created_at) AS day'))
			->where('options', 2)
			->get();
		$totalMailsPhone = isset($theMailsPhone->toArray()[0]['total']) ?$theMailsPhone->toArray()[0]['total'] : 0;
		
		
		//$totalMails=0;
		//display total of  "news"
		$theNews = News::select(DB::raw('count(`id`) AS total'))
			->get();
		$totalNews = isset($theNews->toArray()[0]['total'])? $theNews->toArray()[0]['total']: 0;
		
		//retrieve total of  "customer voice":
		$customerVoice = Feedback::selectRaw('count(`id`) AS total')
								  ->get();
		$totalCustomerVoice = ($customerVoice) ? $customerVoice[0]['total'] : 0;
			
		// $totalCustomerVoice = isset($customerVoice->toArray()[0]['total'])? $customerVoice->toArray()[0]['total'] :0;
		
        return view('admin.dashboard')
		->with('situationInfo', $situationInfo)
		->with('totalMailsAttachementPC', $totalMailsAttachementPC)
		->with('totalMailsPC', $totalMailsPC)
		->with('totalMailsPhone', $totalMailsPhone)
		->with('totalNews', $totalNews)
		->with('totalCustomerVoice', $totalCustomerVoice)
		->with('apache', $apache)
		->with('appver', $appver)
		->with('phpver' , $phpver)
		->with('mysqlver' , $mysqlver)
		->with('headAdminTitle', $headAdminTitle)
			;
    }
}
