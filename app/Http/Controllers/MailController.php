<?php

namespace App\Http\Controllers;

use App\Models\Mail;
use App\Http\Requests\StoreMail;
use App\Http\Requests\StoreMailCsv;

use Illuminate\Http\Request;
//for custom query
use Illuminate\Support\Facades\DB;

use App\Jobs\SendSituationMailJob;

use Log;
use Response;


/**
 * Mail Controller
 *
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @copyright 2018 Commude Philippines, Inc.
 * 
 */
class MailController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // Init
    }

    /**
	 * GET: /mail/list
	 *
	 * Pre load all spot lists with pagination
	 * 
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since  2018/05/23
	 *
	 * @return  \Illuminate\Http\Response mail/index.blade.php
	 */
    public function index()
    {
		$headAdminTitle = 'メールの配信先　一覧';
		$mails          = Mail::paginate(config('constants.PAGINATION.limit'));
		
		return view('admin.mail.index')->with('headAdminTitle', $headAdminTitle)
									   ->with('mails', $mails);
	}

	/**
	 * GET: /mail/add
	 *
	 * Pre load all spot lists with pagination
	 * 
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since  2018/05/24
	 *
	 * @return  \Illuminate\Http\Response mail/index.blade.php
	 */
    public function csvImportView()
    {
		$headAdminTitle = 'メールリストのCSVインポート';
        $mails          = Mail::all();

		// Load statistics
		// $pcMailCount    = $mails->where('options', 0)
		// 						->where('options', 1)->count();

		$withAttachmentCount     = $mails->where('options', 0)->count();
		$noPcAttachmentCount     = $mails->where('options', 1)->count();
		$noMobileAttachmentCount = $mails->where('options', 2)->count();
		
		return view('admin.mail.csv_import')->with('headAdminTitle', $headAdminTitle)
											->with('mails', $mails)
										    ->with('withAttachmentCount', $withAttachmentCount)
											->with('noPcAttachmentCount', $noPcAttachmentCount)
											->with('noMobileAttachmentCount', $noMobileAttachmentCount);										   
	}
	
	/**
	 * POST: /mail/add
	 *
	 * Create and update mail 
	 * 
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since  2018/05/25
	 *
     * @param  StoreMail  $request
	 * @return \Illuminate\Http\Response mail/csv_import.blade.php
	 */
    public function create(StoreMail $request)
    {
        $data = $request->all();
		
        // Create new instance of Mail 
		$mail = new Mail();

        // Set data for each column
        $mail->mail_address = $data['mail_address'];
		$mail->options      = $data['options'];
       
        $mail->save();
		
		return back();
	}

	/**
	 * [AJAX] POST: /mail/save
	 *
	 * Create and update mail 
	 * 
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since  2018/05/24
	 *
	 * @param  StoreMail  $request
	 * @return  JSON true if success
	 */
    public function store(StoreMail $request)
    {
        $data = $request->all();
		$result = null;
		
        // Create new instance of Mail or find existing
		$mail = !empty($data['mail_id'])
			  ? Mail::find($data['mail_id'])
			  : new Mail();

        // Set data for each column
        $mail->mail_address = $data['mail_address'];
		$mail->options      = $data['options'];
       
        $result = $mail->save();
		
		if($result)
			return $this->reloadPartialIndex();
		
		// Assume this return is an error
        return response()->json($result);
	}
	
	/**
	 * POST: /situation/delete
	 *
	 * Delete selected mail entries
	 * 
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since  2018/05/21
	 *
     * @param  \Illuminate\Http\Request  $request
	 * @return  \Illuminate\Http\Response mail/index.blade.php
	 */
    public function delete(Request $request)
    {
        $results = 0;
        $data    = $request->all();
		
		$archivedItems = $data['archived_items'] ?? null;
        
        if($archivedItems){
            foreach($archivedItems as $id){
                $results  += Mail::find($id)->delete();
            }
        }
        
        return redirect('dashboard/mail/list');
	}
	
    /**
	 * CSV export function
	 *
	 * @author Greg
	 * @since  2018/05/28 Louie: added file name from constants
	 * 
	 * @return \Illuminate\Http\Response
	 */
	public function export()
	{
		$OUT_ENC="SJIS";
		$fileName = config('constants.MAIL_FILE_EXPORT_NAME');

		//File headers
		$headers = array(
			"Content-encoding" => $OUT_ENC,
			"Content-type" => "text/csv",
			"Content-Disposition" => "attachment; filename=". $fileName . ";charset:".$OUT_ENC,
			"Pragma" => "no-cache",
			"Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
			"Expires" => "0"
		);

		//CSV columns header: 
		$header = array(
			mb_convert_encoding( '配信先メールアドレス', $OUT_ENC, mb_detect_encoding( '配信先メールアドレス')),
			mb_convert_encoding( '配信種別', $OUT_ENC, mb_detect_encoding( '配信種別')),
			// mb_convert_encoding( '作成日', $OUT_ENC, mb_detect_encoding( '作成日')),
		);

		//retireve all the mail in the DB
		// the number of entry is expected to be arround 1000.

		//data formating
		$data =  Mail::select('mail_address', 'options')->get(); // Recessary row
		$raw = ($data) ? $data->toArray() : null;
		$list= array();
		//need to convert the list for CSV exportation
		foreach($raw as $row){
			$tmp = array();
			//buil the CSV row
			foreach($row as $col){
				$tmp[] =  mb_convert_encoding($col , $OUT_ENC, mb_detect_encoding($col));
			}
			//Add the required rows to the whole CSV//Add the row to the whole CSV
			$list[] = $tmp;
		}
		
		# add headers for each column in the CSV download
		array_unshift($list,$header);

	    $callback = function() use ($list) 
		{
			$FH = fopen('php://output', 'w');
			$osSeparator =  substr_count($_SERVER[ 'HTTP_USER_AGENT'], 'Mac') > 0 
				? ',' //use the , for mac only
				:';';//use the ; by default
			foreach ($list as $row) { 
				fputcsv($FH, $row, $osSeparator,'"' );
			}
			fclose($FH);
		};

		return Response::stream($callback, 200, $headers);
    }
    
    /**
	 * Import a CSV file
	 *
	 * expect a file from 'csvImport'
	 *
	 * This function return a json object with the import status.
	 * 
	 * Be sure to set the form to  enctype="multipart/form-data" in order to send the file.
	 * 
	 * @author Greg
	 * @since  2018/05/28 Louie: updated Mail fields to reflect download
	 * 
	 * @param StoreMailCsv $request
	 * 
	 * @return \Illuminate\Http\Response
	 */
	public function importCSV(StoreMailCsv $request)
	{
		// Original
		// $results = array();
		// $file = $request->file('file_csv');
		// //the requierments said:csv with 2 columns: mail_data , list_no
		// $currentCustom = 2;//
		// $fileName = $file->getClientOriginalName();
		// //is CSV ?
		// $isCSV = (strtolower(substr($fileName, -4)) === '.csv') ? true : false;
		// $results['isCSV'] =$isCSV;
		
		// // do th CSV check stuff
		// if($isCSV ){
		// 	$source = fopen($file, "r");
		// 	//most of the time customer CSV have columns header on the first line
		// 	$firstLine = fgets($source);
		// 	// For this project we can assume if there's no @ in the first line, it a columns headers
		// 	$header = (substr_count($firstLine, '@'))? false: true;
		// 	// need to found what is the separator in use in CSV
		// 	$separator = ',';
		// 	$csvLine = fgetcsv($source, 1000, $separator);
		// 	if(count($csvLine) === 1){
		// 		$separator = ';';
		// 		$csvLine = fgetcsv($source, 1000, $separator);
		// 	}
			
		// 	$colFound = count($csvLine);
		// 	/*Close the file for reset the pointer*/
		// 	if( $currentCustom == $colFound ){
		// 		// $result = '***'; // Louie: Remove success message for now
		// 		$result = '';
				
		// 		// Re-open the file
		// 		$sourceCSV = fopen($file, "r");
		// 		$customerColumns = array();
				
		// 		//If header exists, skip the first line
		// 		if($header){
		// 			fgetcsv($sourceCSV, 1000,$separator);
		// 		}

		// 		// Read rows
		// 		while (($row = fgetcsv($sourceCSV, 1000,$separator)) !== FALSE) {
					
		// 			$email = new Mail;
		// 			$email->mail_address = $row[0] ;
		// 			$email->options = $row[1] ;
		// 			$email->save() ;
		// 		}

		// 		// Success statement
		// 		// $result = '<div><p>データ登録完了しました</p></div>';
		// 		$results['match'] = $result;
		// 		$results['col'] = $colFound;
		// 		$results['filename'] = $fileName;
		// 	} else {
		// 		//This should be adapted according your need
		// 		// Display an error about columns count error
		// 		$difference = $colFound - $currentCustom ;
		// 		  $result = '
		// 		   カラムの回数エラー：インポート出来ませんでした。
		// 		 <br/> CSVファイルに'.$colFound.' カラムがりました
		// 		 <br/>'.$difference.
		// 		 ( ($difference >0 )? ' カラムを追加してください。' :'　カラムを削除してください。').'
		// 		';
		// 	}
		// } else {
		// 	//This should be adapted according your need
		// 	  $result = '
		// 	   Upload file must be CSV
		// 	  ';
		// }
		// //For debug purpose, can be deleted
		// $results['DBG'] = $result;
		
		// We can move this into a utility class
		// json_decode($results)

		// Validations moved to StoreMailCsv
		$results = array();
		$file = $request->file('file_csv');
		//the requierments said:csv with 2 columns: mail_data , list_no
		$currentCustom = 2;//
		$fileName = $file->getClientOriginalName();
		//is CSV ?
		$isCSV = (strtolower(substr($fileName, -4)) === '.csv') ? true : false;
		$results['isCSV'] = $isCSV;
		$result = '';
		
		// do th CSV check stuff
		if($isCSV ){
			$source = fopen($file, "r");
			//most of the time customer CSV have columns header on the first line
			$firstLine = fgets($source);
			// For this project we can assume if there's no @ in the first line, it a columns headers
			$header = (substr_count($firstLine, '@'))? false: true;
			// need to found what is the separator in use in CSV
			$separator = ',';
			$csvLine = fgetcsv($source, 1000, $separator);
			if(count($csvLine) === 1){
				$separator = ';';
				// $csvLine = fgetcsv($source, 1000, $separator);
                $csvLine   = explode($separator, $csvLine[0]); // Louie: changed to expldoe since fgetcsv returns false
			}
			
			$colFound = count($csvLine);
			/*Close the file for reset the pointer*/
			if( $currentCustom == $colFound ){
				// $result = '***'; // Louie: Remove success message for now
				
				// Re-open the file
				$sourceCSV = fopen($file, "r");
				$customerColumns = array();
				
				//If header exists, skip the first line
				if($header){
					fgetcsv($sourceCSV, 1000,$separator);
				}

				// Read rows
				while (($row = fgetcsv($sourceCSV, 1000,$separator)) !== FALSE) {
					
					$email = new Mail;
					$email->mail_address = $row[0] ;
					$email->options = $row[1] ;
					$email->save() ;
				}

				// Success statement
				// $result = '<div><p>データ登録完了しました</p></div>';
				$results['match'] = $result;
				$results['col'] = $colFound;
				$results['filename'] = $fileName;
			}
		}

		//For debug purpose, can be deleted
		$results['DBG'] = $result;

		$request->session()->flash('result', $results['DBG']);
		
		return back();
	}

	/**
	 * Reload paginated table list of Mails
	 * 
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since  2018/05/28
	 *
	 * @return  \Illuminate\Http\Response mail/index.blade.php
	 */
    private function reloadPartialIndex()
    {
		$headAdminTitle = 'メールの配信先　一覧';
		$query          = parse_url(url()->previous(), PHP_URL_QUERY);
		parse_str($query, $params);
		$currentPage    = ($params) 
						? $params['page'] 
						: 1 ;
		$maxPages       = config('constants.PAGINATION.limit');
		$mails          = Mail::paginate($maxPages, ['*'], 'page', $currentPage);
		$mails->withPath('/dashboard/mail/list');
		
		return view()->make('partials.mail_row')->with('mails', $mails);
	}
	

}
