<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
//reference the Model
use App\Models\Page;

//for the front page
use App\Models\CustomerVoice as Feedback;
use App\Models\News;
use App\Models\Situation;
//reference the validator
use App\Http\Requests\StorePage;

class PageController extends Controller
{
    /**
     * Show the form for editing the specified resource.
     *
     * This is the main function for this controller
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$pages = $this->getPage($id);
       $headAdminTitle =$pages->page_title;
	   $bodyClass = $pages->id == 1 ?'container': 'parking';
		return view('admin/page_edit')
			->with('headAdminTitle', $headAdminTitle)
			->with('bodyClass', $bodyClass)
			->with('page', $pages);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StorePage $request, $id)
    {
		$data = $request->all();
        //get the instance of the id
        $pages = Page::find($id);
        //update each column
        $pages->page_title              = $data['page_title'];
        $pages->page_status             =  isset($data['page_status'])? 1 :0;
        $pages->page_content            = $data['page_content'];
        //save the updated version of the row
        $pages->save();

		return redirect('dashboard/page/edit/'.$id );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * 2018-05-21: Greg This may need refactoring and adjustement
     * 2018-06-07: Greg: I'm using the same view as the front for the preview
     *
     */
    public function show($id)
    {
		//check if site is open
		$now =  strtotime('now');
		if (is_numeric($id)){
			$pageView = $id %2 ? 'parking':'container';
		} else{
			$pageView = $id =='parking' ? 'parking':'container';
		}
		// if status is open
		
		$redirect = 'home';

		//If the id is a kind of permalink setting I.E a key word, we need to set it in database
		$pages = $this->getPage($id);
		$headTitle =$pages->page_title;
	//	$allowed =(isset($pages) && $pages->page_status )?1: 0;
		$mainClass = 'child about relation';
	//	if($allowed){
			//echo "**allowed**";
			if (isset($pages) && $pages->page_status ){
				$rendered = $this->renderPage($pages->page_content, $pages->page_id);
				$pages->page_content = $rendered;

				//return view('admin.page_preview')
				return view('front.'.$pageView)
					->with('page', $pages)
					->with('mainClass', $mainClass)
					->with('headTitle', $headTitle)
					;
			}
	//	}
		//echo "**Not Allowed**";
		//return not found page OR redirect to the specified page.
	/*	return (substr_count($redirect,'http')>0)
			?Redirect::to($redirect)
			:view($redirect);*/
    }

	/**
	 * Sub function.
	 * return the formated html
	 */
	protected function renderPage($html, $pageID)
	{

		// remove the /admin/ parts of URL saved by TinyMCE
		$html = str_replace('public/admin/page_edit','public',$html);
		$html = str_replace('public/admin','public',$html);
		return $html;
	}

	/**	 * return all the data of the requested page	 *	 * @var $id : mixed	 * if $id is an int, research the page id parameter
	 * if $id is a string,research the page_permalink parameter	 */
	protected function getPage($id)
	{
		return (is_numeric($id))
			? Page::find($id)
			: Page::where('page_permalink',$id)->first();
	}


    /**
     * Store data in order to make a preview.
     * The controller return a json object, with the 'url' parameter.
     * it's a preview url forget with the preview id.
     */
    public function store(Request $request, $id)
    {
		//Is there any  preview for this id?
		$has_preview = Page::where('page_permalink',$id)
			->where('page_status', -1)
			->get();
		
		$preview = isset($has_preview[0]->id)? $has_preview[0]->id:0;
		
		$results = array();
		$data = $request->all();
		$results['data'] = $data;
		//print_r($data);
		
		if($preview){
			$pages = Page::find($preview);
			$pages->page_title              = $data['page_title'];
			$pages->page_content            = $data['page_content'];
			$pages->save();
		} else {
			//create a new instance of the Page Model
			$pages = new Page;
			//set data for each column
			$pages->page_title              = $data['page_title'];
			$pages->page_status             =  -1;
			$pages->page_content            = $data['page_content'];
			$pages->page_permalink            = $id;
			//save the entry in the database
			$pages->save();
			//set the preview id
			$preview = $pages->id;
		}
		
		$results['url'] =  url('preview/page/'.$preview) ;

		return json_encode($results);
		//return view('admin.page_edit')->with('page', $pages);
    }

	/** To keep or not to keep **/

	/**	 *	 * 2018-05-23: Greg : regorganized the "Not required, but keep till further directives" 	 *	 *	 */

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     * 2018-05-21: Greg Not required, but keep till further directives 
     */
    public function create()
    {
		$headAdminTitle = '新規作成';
       return view('admin.page_new')
		->with('headAdminTitle', $headAdminTitle)
		;
    }

	/**	 *	 * Display the default page for the front office	 *	 *	 */
	protected function defaultPage()
	{
			return view('index');
	}



    /**
     * 2018-05-23: GReg the "page are not supposed to be deleted"
     * Remove the specified resource from storage.
     *
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function delete(Request $request)
    {
		$data = isset($_POST['page_id'])? $_POST['page_id'] : 0;
		$results = 0;
		if($data){
			foreach($data as $id){
				$results + Page::find($id)->delete();
			}
		}
		$pages = Page::all();
		return view('admin.page')->with('data', $pages);
    }
}
