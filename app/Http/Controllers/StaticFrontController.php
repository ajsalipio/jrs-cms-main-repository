<?php

namespace App\Http\Controllers;

use Log;
use Session;
use Carbon\Carbon;

use App\Mail\InquirySentMail;
use App\Mail\ComplianceSentMail;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;

class StaticFrontController extends Controller
{

    /**
	 * GEt: /inquiry/
	 *
	 * Pre load all spot lists with pagination
	 * 
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since  2018/06/14
	 *
	 * @return  \Illuminate\Http\Response situation/index.blade.php
	 */
    public function getInquiryView()
    {
		return view('front.inquiry.index');
    }
    
    /**
	 * POST: /inquiry/confirm
	 *
	 * Pre load all spot lists with pagination
	 * 
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since  2018/06/14
	 *
	 * @return  \Illuminate\Http\Response situation/index.blade.php
	 */
    public function confirmInquiry(Request $request)
    {
        $inquiry = $request->except('_token');

        
        return view('front.inquiry.confirm')->with('inquiry', $inquiry);
    }

	public function confirmCompliance(Request $request)
	{
		$compliance = $request->except('_token');

		return view('front.compliance.confirm')
					->with('compliance', $compliance)
					;
	}

    /**
	 * POST: /inquiry/send
	 *
	 * Pre load all spot lists with pagination
	 * 
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since  2018/06/14
	 *
	 * @return  \Illuminate\Http\Response situation/index.blade.php
	 */
    public function sendInquiry(Request $request)
    {
        $inquiry    = $request->except('_token');
		// $recipient = config('constants.INQUIRY_MAIL_TO')[$inquiry['ご意見']];
		$recipients = config('mail.inquiry.to_test');
		
		foreach($recipients as $recipient){
			Mail::to($recipient)
				->send(
					new InquirySentMail($inquiry)
				);
		}

		if(count(Mail::failures()) > 0 ) {
			Log::error(Mail:: failures());
		}
		
        return redirect('inquiry/complete');
	}
	
    /**
	 * POST: /compliance/send
	 *
	 * Pre load all spot lists with pagination
	 *
	 * @author Sally Jean Sumeguin <sally_Sumeguin@commude.ph>
	 * @since  2018/06/14
	 *
	 */
    public function sendCompliance(Request $request)
    {
        $compliance = $request->except('_token');        
		$recipients = config('mail.compliance.to_test');
		
		foreach($recipients as $recipient){
			Mail::to($recipient)
				->send(
					new ComplianceSentMail($compliance)
				);
		}

		if(count(Mail::failures()) > 0 ) {
			Log::error(Mail:: failures());
		}

		return redirect('compliance/complete');
	}
	
	/**
	 * GEt: /inquiry/success
	 *
	 * Pre load all spot lists with pagination
	 * 
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since  2018/06/14
	 *
	 * @return  \Illuminate\Http\Response situation/index.blade.php
	 */
    public function getSuccessMailView()
    {
		return view('front.inquiry.complete');
    }
		
}
