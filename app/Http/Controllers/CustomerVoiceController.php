<?php

namespace App\Http\Controllers;

use App\Models\CustomerVoice as Feedback;
use App\Http\Requests\StoreCustomerVoice as VoiceRequest;
use Carbon\Carbon;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;


class CustomerVoicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @author Sally Jean Sumeguin < sally_sumeguin@commude.ph
     * @return View home/customer-voices/list 
     */
    public function index()
    {
		$feedback = Feedback::orderBy('created_at', 'desc')
			         ->paginate(config('constants.PAGINATION.limit'));
		
		return view('admin.customer-voices.list')
                ->with('headAdminTitle', 'お客様の音声 一覧')
                ->with('data', $feedback);
    }

    /**
     * Show form for adding new resource
     *
     * @author Sally Jean Sumeguin < sally_sumeguin@commude.ph
     * @return View home/customer-voices/create
     */
    static function create()
    {
		$headAdminTitle = 'カスタマーボイスを作成する';

	   	return view('admin.customer-voices.create')
		    		->with('headAdminTitle', $headAdminTitle);
    }
     
    
    /**
     * Store a newly created resource in storage.
     *
     * @author Sally Jean Sumeguin < sally_sumeguin@commude.ph
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    static function store(VoiceRequest $request)
    {	
		$feedback = new Feedback();
		$feedback->cv_title = $request->cv_title;
		$feedback->cv_content   = $request->cv_content;
		$feedback->cv_company   = $request->cv_company;
		$feedback->cv_status    = $request->cv_status;  
		if ($request->hasfile('cv_picture')) {
			$feedback->cv_picture = MediaController::storeCustom($request->file('cv_picture'));
		}
		$feedback->save();
		return redirect('/dashboard/customer-voices/list');
    }
     
    /**
     * Show the form for editing the specified resource.
     *
     * @author Sally Jean Sumeguin < sally_sumeguin@commude.ph
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$headAdminTitle = 'お客様の声を編集する';
		$feedback = Feedback::find($id);
		
		if(empty($feedback)) {
			return abort(404);
		}
		
		return view('admin.customer-voices.edit')
				  ->with('headAdminTitle', $headAdminTitle)
				  ->with('data', $feedback);
    }

    /**
     * Update the specified resource in storage.
     *
     * @author Sally Jean Sumeguin < sally_sumeguin@commude.ph
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(VoiceRequest $request, $id)
    {
		$feedback = Feedback::find($id);
		
		if (empty($feedback)) {
			return abort(404);
		}

		if ($request->hasfile('cv_picture')) {
		   if ($feedback->cv_picture) { 
			   MediaController::updateCustom($request->file('cv_picture'), $feedback->cv_picture); 
			} else {
				$feedback->cv_picture = MediaController::storeCustom($request->file('cv_picture'));
			}
		}

		$feedback->cv_title = $request->cv_title;
		$feedback->cv_content   = $request->cv_content;
		$feedback->cv_company   = $request->cv_company;
		$feedback->cv_status    = $request->cv_status;  
		$feedback->save();
		return back();	
    }

    /**
     * Remove the specified resource from storage.
     *
     * @author Sally Jean Sumeguin < sally_sumeguin@commude.ph
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {	
		$feedbacks = Feedback::find($request->id);
		if (empty($feedbacks)) {
			return abort(404);
		}

		foreach($feedbacks as $feedback) {
		   MediaController::deleteCustom($feedback->cv_picture);
		   $feedback->delete();
		}
		return redirect('/dashboard/customer-voices/list');
    }

    /**
     * Show specified resource in preview page 
     *
     * @author Sally Jean Sumeguin < sally_sumeguin@commude.ph
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     **/
    public function preview(Request $request)
    {
		$feedback= $request->all();
		$imageUrl = $this->storeTempImage($request);	
		return view('admin.customer-voices.preview')
					->with('headAdminTitle', 'お客様の声をプレビュー')
		 			->with('imageUrl', $imageUrl)
		  			->with('data', $feedback);
    
    }
    
    /**
     * Store upload image image to /storage/tmp/ 
     * 
     * @author Sally Jean Sumeguin < sally_sumeguin@commude.ph
     * @param  \Illuminate\Http\Request  $request
     * @return String
     **/
    private function storeTempImage($request)
    {
		if ($request->hasfile('cv_picture')) {
		   $image = $request->file('cv_picture');
		   $filename = time() . $image->getClientOriginalName();
		   $fileExtension = $image->getClientOriginalExtension();
		   $imageName = md5($filename) . "." . $fileExtension; 
		   $image->storeAs('tmp', $imageName, 'public');
		   return "/storage/tmp/$imageName";  	
		} else {  
		   $feedback = Feedback::find($request['id']);
		   if ($feedback) {
				if ($feedback->cv_picture) {
					  return $feedback->media->path;
				}
		   } 
		   return "";
		}
    }
}
