<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ComplianceSentMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($compliance)
    {
        $this->compliance = $compliance;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $compliance = $this->compliance;
        $subject = config('mail.compliance.subject');

        return $this->from(config('mail.compliance.from_test'))
                    ->subject($subject)
                    ->view('vendor.mail.complaince_mail')
                    ->with('compliance', $compliance);
    }
}
