<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class InquirySentMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $inquiry;
    
    /**
     * Create a new message instance.
     *
     * @param string $inquiry
     * 
     * @return void
     */
    public function __construct($inquiry)
    {
        $this->inquiry = $inquiry;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $inquiry = $this->inquiry;
        $subject = config('mail.inquiry.subject');

        return $this->from(config('mail.inquiry.from_test'))
                    ->subject($subject)
                    ->view('vendor.mail.inquiry_mail')
                    ->with('inquiry', $inquiry);
    }
}
