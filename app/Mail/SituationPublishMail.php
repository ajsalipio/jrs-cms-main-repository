<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\Situation;

use Log;

class SituationPublishMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    protected $situation;
    protected $isFileAttached;
    protected $mailType;
    
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 5;

    /**
     * Create a new message instance.
     *
     * $isFileAttached can be moved to a $settings variable
     * if a lot of checks to be considered
     * 
     * @return void
     */
    public function __construct($situation, $isFileAttached, $mailType)
    {
        $this->situation      = $situation;
        $this->isFileAttached = $isFileAttached;
        $this->mailType       = $mailType;
    }

    /**
     * Build the message.
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since  2018/05/30
     * 
     * @return $this
     */
    public function build()
    {   
        $situation      = $this->situation; // Temp var to shorten calls
        $isFileAttached = $this->isFileAttached;  // Temp var to shorten calls
        $mailType       = $this->mailType;
        
        $situationTitle = ($situation['situation_title']) 
                        ? $situation['situation_title'] 
                        : config('constants.SITUATION_STATUS')[$situation['situation_status']];
        
        $mailComplete = $this->from(config('mail.situation.from_test'))
                    ->subject(config('mail.situation.subject'))
                    ->with('situation', $situation)
                    ->with('situationTitle', $situationTitle);

        // Use mail optio nas target mail design
        switch($mailType){
            case 0:
            case 1:
                $mailComplete = $mailComplete->view('vendor.mail.situation_mail')
                                    ->withSwiftMessage(function ($message) {
                                        // Remove default utf-8 encoding by SwiftMailer
                                        $message->getHeaders()->get('Content-Type')->setValue('text/html');
                                        $message->getHeaders()->get('Content-Type')->setParameters(['charset' => 'ISO-2022-JP']);
                                        $message->getHeaders()->addTextHeader('Content-Type', 'text/html; charset=ISO-2022-JP');
                                    });
                break;
            default;
                $mailComplete = $mailComplete->view('vendor.mail.situation_mail-text')
                                     ->withSwiftMessage(function ($message) {
                                        // Remove default utf-8 encoding by SwiftMailer
                                        $message->getHeaders()->get('Content-Type')->setValue('text/plain');
                                        $message->getHeaders()->get('Content-Type')->setParameters(['charset' => 'ISO-2022-JP']);
                                        $message->getHeaders()->addTextHeader('Content-Type', 'text/plain; charset=ISO-2022-JP');
                                    });
                break;
        }
        
       
        
        // Attach files if required
        if($isFileAttached){
            if($situation->fileXls){
                $mailComplete->attach(public_path($situation->fileXls['path']),
                    [
                        'as' => $situation->fileXls['original']
                    ]
                );
            }
            if($situation->fileDoc){
                $mailComplete->attach(public_path($situation->fileDoc['path']),
                    [
                        'as' => $situation->fileDoc['original']
                    ]
                );
            }
            if($situation->filePics()){

                foreach($situation->filePics() as $filePic){
                    $mailComplete->attach(public_path($filePic['path']),
                        [
                            'as' => $situation->filePic['original']
                        ]
                    );
                }
            }
            
        }

        return $mailComplete;
    }
}
