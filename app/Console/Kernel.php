<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

use App\Console\Commands\SendSituationMailCommand;
use App\Jobs\SendSituationMailJob;

use Log;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // Execute in server/console php artisan queue:listen database
        // Log::info('SCHEDULER RUNNING AT ' . now());
        // $schedule->command(SendSituationMailCommand::class, ['--force'])->everyMinute();
        
         // Execute in server/console php artisan queue:listen database
        // $schedule->job(new SendSituationMailJob, 'situation_mails')->daily();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
