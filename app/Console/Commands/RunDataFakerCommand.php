<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Carbon\Carbon;
use Log;

class RunDataFakerCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:run_data_faker';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates test data using the faker factory';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

   /**
     * Execute the faker factory makers
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since  2018/06/08
     * 
     * @return mixed
     */
    public function handle()
    {
        $this->info('Running ' . get_class($this) . ' at ' . now());

        $customerVoicesCount = factory(\App\Models\CustomerVoice::class, 'customerVoiceToday', 25)->create();
        $this->info('CustomerVoice [today]: ' . count($customerVoicesCount) . ' successfully created');

        $customerVoicesCount = factory(\App\Models\CustomerVoice::class, 'customerVoicPast', 25)->create();
        $this->info('CustomerVoice [past]: ' . count($customerVoicesCount) . ' successfully created');
        
        $newsCount = factory(\App\Models\News::class, 'newsToday', 25)->create();
        $this->info('News: [today]: ' . count($newsCount) . ' successfully created');
        
        $newsCount = factory(\App\Models\News::class, 'newsPast', 25)->create();
        $this->info('News: [past]: ' . count($newsCount) . ' successfully created');
                
        $this->info('Test data generation done at '  . now());
    }
}
