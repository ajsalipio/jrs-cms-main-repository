<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class RunSetupBasic extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:setup_basic';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'view, route and cache clear';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$this->info('Running view:clear');
		\Artisan::call('view:clear');
		$this->info('Running cache:config');
		\Artisan::call('config:cache');
		$this->info('Running route:clear');
		\Artisan::call('route:clear');
		$this->info('Done');
	}
}
