<?php

namespace App\Console\Commands;

use App\Mail\SituationPublishMail;
use App\Models\Mail as MailModel;
use App\Models\Situation;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;

use Log;

/**
 * Command version of Situation Mail sending
 * 
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @copyright 2018 Commude Philippines, Inc.
 *  
 */
class SendSituationMailCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:send_situation_mail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Simulate sending of all scheduled mails of (have_time_publishing = 1) to all emails in mails table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * 
     * Email notification for situations scheduled via publish_from
     *
     * @todo Consider timezone? 
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since  2018/05/30
     * 
     * @return mixed
     */
    public function handle()
    { 
        // Log::info('SendSituationMailCommand RUNNING AT ' . now());
        $now = Carbon::now()->toDateString();
        
        // Get all situations to be published for today
        $situations = Situation::where('have_time_publising', 1)
                               ->whereDate('publish_from', $now)->get();
    
        // Log::info('SendSituationMailCommand RUN AT ' . now());
        return;
        if(empty($situations)){
            // Log::info('No situations/tranposrations to mail');
            return;
        }

        foreach($situations as $situation)
        {
            $recipients = MailModel::all();
            
            foreach($recipients as $recipient)
            {
                $isFileAttached = null;
                
                 // Check if files are to be attached
                switch($recipient['options']) {
                    case 0:
                        $isFileAttached = true;
                        break;
                    default: // case 1 and 2
                        $isFileAttached = false;
                }
                
                Mail::to($recipient['mail_address'])->later(
                    // Carbon::now()->addMinute(5), // TESTER
                    $situation['publish_from'], // Schedule a time
                    new SituationPublishMail($situation, $isFileAttached)
                );
                
                if(count(Mail::failures()) > 0 ) {
                    Log::error(Mail:: failures());
                }else{
                    // Log::info('Situation Mail with id ' . $situation['id'] . ' '
                    //         . 'SCHEDULED for recipient: ' . $recipient['mail_address']
                    // );
                }
            }
        }
 
        // TESTER for a specific situation
        // $situation = Situation::find(1);
        // $isFileAttached = true;
        // Mail::to('martin_delaserna@commude.ph')->send(
        //             new SituationPublishMail($situation, $isFileAttached)
        //         );
        
        $this->info('SendSituationMailCommand run and done at ' . now());
    }
}
