<?php

namespace App\Providers;

use Illuminate\Support\Facades\Queue;
use Illuminate\Support\ServiceProvider;
use Illuminate\Queue\Events\JobProcessed;
use Illuminate\Queue\Events\JobFailed;
use Illuminate\Queue\Events\JobProcessing;

use Illuminate\Support\Facades\Schema;

use App\Models\SuccessJob;
use App\Models\FailedJob;
use App\Models\Situation;
use App\Observers\SituationObserver;

use Log;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // https://laravel-news.com/laravel-5-4-key-too-long-error
        Schema::defaultStringLength(191);

        /**************** JOG LOBGS  ****************/
        Queue::after(function (JobProcessed $event) {
            // Log::info('JOB LOG after at  ' . now());
            // Log::info($event->job->resolveName());
            
            $successJob = new SuccessJob();

            $successJob->job_name   = $event->job->resolveName();
            $successJob->connection = $event->job->getConnectionName();
            $successJob->queue      = json_encode($event->job->getQueue());
            $successJob->payload    = json_encode($event->job->payload());

            $successJob->save();
        });

        Queue::failing(function (JobFailed $event) {
            // Log::info('JOB FAILURE at  ' . now());
            // Log::info($event->job->resolveName());

            $failedJob = new FailedJob();
            
            $failedJob->connection = $event->job->getConnectionName();
            $failedJob->queue      = json_encode($event->job->getQueue());
            $failedJob->payload    = json_encode($event->job->payload());
            $failedJob->exception  = json_encode($event->exception);

            $failedJob->save();
        });

        /** When a job starts, do some check
        Used by the ff Jobs:
        
        1. SituationScheduleFrontUpdateJob
        - Determine whether there was a change in latest situation, if there is, disregard job
        
        **/
        
         Queue::before(function (JobProcessing $event) {
            // $event->connectionName
            // $event->job
            // $event->job->payload()
            // Log::info('Job processing: ' . $event->job->resolveName());
        });


         /******************************** OBSERVERS  ********************************/
        Situation::observe(SituationObserver::class);

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
