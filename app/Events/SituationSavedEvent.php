<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SituationSavedEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $situation;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($situation)
    {
        $this->situation = $situation;
    }

    /**
     * Get the channels the event should broadcast on.
     * 
     * Note: let the listener get the event
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return config('constants.CHANNELS.situation');
    }
}
