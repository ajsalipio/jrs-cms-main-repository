<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
$factory->defineAs(App\Models\CustomerVoice::class, 'customerVoiceToday', function (Faker $faker) {
    return [
		
	'cv_title'	    => $faker->realText($maxNbChars = 40, $indexSize = 2),
	'cv_content'	=> $faker->paragraph(6),
	'cv_company'	=> $faker->company,
	'cv_picture'	=> factory(\App\Models\Media::class, 'mediaImage')->create()->media_id,
	'cv_status'	    => 1, //rand(0,1),
	'created_at'    => $faker->dateTimeThisYear($max = 'now', $timezone = null),
	'updated_at'    => now()
    ];
});


$factory->defineAs(App\Models\CustomerVoice::class, 'customerVoicPast', function (Faker $faker) {
    return [
		
	'cv_title'	    => $faker->realText($maxNbChars = 40, $indexSize = 2),
	'cv_content'	=> $faker->paragraph(6),
	'cv_company'	=> $faker->company,
	'cv_picture'	=> factory(\App\Models\Media::class, 'mediaImage')->create()->media_id,
	'cv_status'	    => 1, //rand(0,1),
	'created_at'    => $faker->dateTimeBetween($startDate = '-5 years', $endDate = '-1 years', $timezone = null),
	'updated_at'    => now()
    ];
});
