<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->defineAs(App\Models\Media::class, 'mediaImage', function (Faker $faker) {
	
    return [
	'recorded'  => $faker->dateTime,
	'name'	    => 'img_' .$faker->word,
	'original'  => $faker->word,
	'path'	    => 'storage/upload/' . $faker->image($dir = 'public/storage/upload/', $width = 640, $height = 480, 'business', false)
    ];
});

$factory->defineAs(App\Models\Media::class, 'mediaPdf', function (Faker $faker) {
	
    return [
	'recorded'  => $faker->dateTime,
	'name'	    => 'pdf_' . $faker->word,
	'original'  => $faker->word,
	'path'	    => 'storage/upload/' . $faker->file($sourceDir = config('faker.PDF_PATH'), 
													$targetDir = 'public/storage/upload/',
													false)
    ];
});
