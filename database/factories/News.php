<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->defineAs(App\Models\News::class, 'newsToday', function (Faker $faker) {

	$categories         = null;
	$regionalCategories = null;

	for ($i=0; $i < 3; $i++) {
		if($categories){
			$categories .= ',' . $faker->unique()->numberBetween($min=0, $max=4);
		}else{
			$categories = $faker->unique()->numberBetween($min=0, $max=4);
		}
	}
	
	// Reset each time
	$faker->unique($reset = true);

	for ($i=0; $i < 3; $i++) {
		if($categories){
			$regionalCategories .= ',' . $faker->unique()->numberBetween($min=0, $max=4);
		}else{
			$regionalCategories = $faker->unique()->numberBetween($min=0, $max=4);
		}
	}
	
	// Reset each time
	$faker->unique($reset = true);

    return [
	'news_title'		=> $faker->realText($maxNbChars = 40, $indexSize = 2),
	'news_content'		=> $faker->paragraph(10),
	'file_pdf'	        => factory(\App\Models\Media::class, 'mediaPdf')->create()->media_id,
	'is_link'	        => rand(1,0),
	'validity'			=> $faker->dateTimeBetween($startDate = '+2 days', $endDate = '+1 months', $timezone = null),
	'status'			=> 1, // rand(0,1),
	'category'	        => $categories,
	'regional_category'	=> $regionalCategories,
	'created_at'        => $faker->dateTimeThisYear($max = 'now', $timezone = null),
	'updated_at'        => now()
    ];
});

//

$factory->defineAs(App\Models\News::class, 'newsPast', function (Faker $faker) {

	$categories         = null;
	$regionalCategories = null;

	for ($i=0; $i < 3; $i++) {
		if($categories){
			$categories .= ',' . $faker->unique()->numberBetween($min=0, $max=4);
		}else{
			$categories = $faker->unique()->numberBetween($min=0, $max=4);
		}
	}
	
	// Reset each time
	$faker->unique($reset = true);

	for ($i=0; $i < 3; $i++) {
		if($categories){
			$regionalCategories .= ',' . $faker->unique()->numberBetween($min=0, $max=4);
		}else{
			$regionalCategories = $faker->unique()->numberBetween($min=0, $max=4);
		}
	}
	
	// Reset each time
	$faker->unique($reset = true);

    return [
	'news_title'		=> $faker->realText($maxNbChars = 40, $indexSize = 2),
	'news_content'		=> $faker->paragraph(10),
	'file_pdf'	        => factory(\App\Models\Media::class, 'mediaPdf')->create()->media_id,
	'is_link'	        => rand(1,0),
	'validity'			=> $faker->dateTimeBetween($startDate = '+2 days', $endDate = '+1 months', $timezone = null),
	'status'			=> 1, // rand(0,1),
	'category'	        => $categories,
	'regional_category'	=> $regionalCategories,
	'created_at'        => $faker->dateTimeBetween($startDate = '-5 years', $endDate = 'now', $timezone = null),
	'updated_at'        => now()
    ];
});
