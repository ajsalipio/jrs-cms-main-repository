<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Situation::class, function (Faker $faker) {
    return [
        'situation_title' => $faker->word,
        'situation_status' => rand(0,1),
        'situation_content' => $faker->sentence,
        'situation_details' => $faker->sentence,
        'file_xls' => factory(App\Models\Media::class)->create()->media_id,
        'file_doc' => factory(App\Models\Media::class)->create()->media_id,
        'have_time_publising' => rand(0,1),
        'publish_from' => $faker->dateTime($max = 'now', $timezone = null), 
        'publish_end' => $faker->dateTime($max = 'now', $timezone = null),
        'publishing_status' => rand(0,1),
    
    ];
});
