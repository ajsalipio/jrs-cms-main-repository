<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSituationImagesForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('situation_images', function (Blueprint $table) {
            $table->foreign('situation_id')->references('id')->on('situations')->onDelete('cascade');
            $table->foreign('media_id')->references('media_id')->on('media')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $table->dropForeign('situation_images_situation_id');
        $table->dropForeign('situation_images_media_id');
    }
}
