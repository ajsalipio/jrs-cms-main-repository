<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropTableNews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	Schema::dropIfExists('news');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('news', function (Blueprint $table) {
	    Schema::create('news', function (Blueprint $table) {
		$table->increments('id');
		$table->string('news_title', 255);
		$table->text('news_content')->nullable();				
		$table->unsignedInteger('file_pdf')->nullable();
		$table->integer('is_link')->nullable();
		$table->timestamp('validity')->nullable();
		$table->integer('status')->nullable();
		$table->string('category', 255)->nullable();
		$table->timestamps();
	    });
        });
    }
}
