<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropTableCustomervoices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	Schema::dropIfExists('customervoices');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('customervoices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cv_title', 45);
            $table->text('cv_content')->nullable();
            $table->string('cv_company', 45);
            $table->unsignedInteger('cv_picture')->nullable();
            $table->integer('cv_status')->nullable();
	    $table->timestamps();
	});
    }
}
