<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSituationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('situations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('situation_title', 255)->nullable();
            $table->integer('situation_status');
            $table->text('situation_content')->nullable();			
            $table->text('situation_details')->nullable();			
			//files
			$table->unsignedInteger('file_xls')->nullable();
			$table->unsignedInteger('file_doc')->nullable();
			// $table->unsignedInteger('file_pic')->nullable();
			// settings
			$table->integer('have_time_publising')->nullable();
            $table->timestamp('publish_from')->useCurrent()->nullable();
            $table->timestamp('publish_end')->useCurrent()->nullable();
			$table->integer('publishing_status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('situations');
    }
}
