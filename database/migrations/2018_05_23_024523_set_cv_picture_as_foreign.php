<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetCvPictureAsForeign extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customervoices', function (Blueprint $table) {
	    $table->foreign('cv_picture')->references('media_id')->on('media')->onDelete('cascade');
	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customervoices', function (Blueprint $table) {
	    $table->dropForeign('customervoices_cv_picture_foreign');
	});
    }
}
