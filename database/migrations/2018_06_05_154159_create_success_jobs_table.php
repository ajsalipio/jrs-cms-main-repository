<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuccessJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('success_jobs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('job_name');
            $table->text('connection');
            $table->text('queue');
            $table->longText('payload');

            $table->timestamp('success_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('success_jobs');
    }
}
