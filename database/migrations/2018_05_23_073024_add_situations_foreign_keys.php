<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSituationsForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('situations', function (Blueprint $table) {
            $table->foreign('file_xls')->references('media_id')->on('media')->onDelete('set null');
            $table->foreign('file_doc')->references('media_id')->on('media')->onDelete('set null');
            // $table->foreign('file_pic')->references('media_id')->on('media')->onDelete('set null');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::table('situations', function (Blueprint $table) {
            $table->dropForeign('situations_file_xls_foreign');
            $table->dropForeign('situations_file_doc_foreign');
            // $table->dropForeign('situations_file_pic_foreign');            
        });
    }
}
