<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomervoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customervoices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cv_title', 45);
            $table->text('cv_content')->nullable();
            $table->string('cv_company', 45);
            $table->string('cv_picture', 255)->nullable();
            $table->integer('cv_status')->nullable();
            $table->timestamp('cv_created')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customervocies');
    }
}
