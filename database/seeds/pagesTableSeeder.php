<?php

use Illuminate\Database\Seeder;
use App\Models\Page;

class pagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	// Contains the default HTML for "used container" and "Parking"
        Page::insert([
			[
				'id' => 1,
				'page_title' => '中古コンテナ',
				'page_content' => '<div class="child about relation">
<section class="template__content child__btnlist">
<div class="container">
<p>JR貨物では、中古コンテナの販売を行なっております。</p>
<br />
<div class="template__btnlist_container">
<div class="template__btnlist"><a class="btn-primary-fill" href="#jrcontainer_sales">ＪＲコンテナの販売 </a> <a class="btn-primary-fill" href="#sales_offshore">海上コンテナの販売</a></div>
</div>
</div>
</section>
<!-- child__btnlist -->
<section id="jrcontainer_sales" class="template__content about__panel relation__section">
<div class="container">
<div class="header__title">ＪＲコンテナの販売</div>
<div class="content__container">
<p>中古のJRコンテナの販売を行なっております。ご入用の方は下記の「お問合せ先」にご連絡下さい。</p>
<div class="inner__container">
<div class="linkblock__container">
<div class="linkblock__item"><img src="'. url('front_images/images/child/about/used_pic_01.jpg').'">&nbsp;</div>
<div class="linkblock__item"><img scr="'. url('front_images/images/child/about/used_pic_02.jpg').'">&nbsp;</div>
</div>
</div>
<div class="table__container">
<h3>● JRコンテナのサイズ（例）</h3>
<br />
<table class="material_procurement_info">
<tbody>
<tr>
<th rowspan="2">形式</th>
<th colspan="3" width="200px">外法寸法<br /> (mm)</th>
<th colspan="3" width="200px">内法寸法<br /> (mm)</th>
<th colspan="2">側入口寸法<br /> (mm)</th>
<th rowspan="2">面積<br /> (㎡)</th>
<th rowspan="2">容積<br /> (㎥)</th>
<th rowspan="2">重量<br /> (ｔ)</th>
</tr>
<tr>
<th>高さ</th>
<th>幅</th>
<th>長さ</th>
<th>高さ</th>
<th>幅</th>
<th>長さ</th>
<th>高さ</th>
<th>幅</th>
</tr>
<tr>
<td>19D</td>
<td>2,500</td>
<td>2,450</td>
<td>3,715</td>
<td>2,252</td>
<td>2,275</td>
<td>3,647</td>
<td>2,187</td>
<td>3,635</td>
<td>8.29</td>
<td>18.7</td>
<td>約2.0</td>
</tr>
<tr>
<td>19G</td>
<td>2,500</td>
<td>2,450</td>
<td>3,715</td>
<td>2,232</td>
<td>2,325</td>
<td>3,587</td>
<td>2,187</td>
<td>3,525</td>
<td>8.33</td>
<td>18.6</td>
<td>約2.0</td>
</tr>
</tbody>
</table>
</div>
<br />
<ul class="about__list">
<li>※コンテナのサイズは形式によって異なります。</li>
<li>※鉄道輸送には使用できません。</li>
</ul>
</div>
<div class="content__container">
<h3>◼ JRコンテナ販売のお問合せ</h3>
<div class="table__container">
<table>
<tbody>
<tr>
<th>エリア</th>
<th>所在地</th>
<th>在庫<br /> （個）</th>
<th>お問合せ先</th>
<th>電話番号</th>
</tr>
<tr>
<td rowspan="1">北海道</td>
<td>ジェイアール貨物・北海道物流(株)</td>
<td>-</td>
<td>札幌市白石区流通センター5丁目5-18</td>
<td>011-846-1295</td>
</tr>
<tr>
<td rowspan="1">東北</td>
<td>(株)ジェイアール貨物・東北ロジスティクス</td>
<td>-</td>
<td>宮城県仙台市宮城野区宮城野3丁目2番1号</td>
<td>022-291-0591</td>
</tr>
<tr>
<td rowspan="17">関東</td>
<td>酒田港駅</td>
<td>0</td>
<td>山形県酒田市南新町2丁目7番37号</td>
<td>0234-22-7244</td>
</tr>
<tr>
<td>新潟貨物ターミナル駅</td>
<td>0</td>
<td>新潟県新潟市東区中島401番地1号</td>
<td>025-270-5440</td>
</tr>
<tr>
<td>南長岡駅</td>
<td>0</td>
<td>新潟県長岡市宮内八丁目11番1号</td>
<td>0258-32-2704</td>
</tr>
<tr>
<td>黒井駅</td>
<td>0</td>
<td>新潟県上越市大字黒井字西原2692番地1号</td>
<td>025-543-2237</td>
</tr>
<tr>
<td>土浦駅</td>
<td>0</td>
<td>茨城県土浦市有明町2丁目41番</td>
<td>029-821-3014</td>
</tr>
<tr>
<td>宇都宮貨物ターミナル駅</td>
<td>0</td>
<td>栃木県河内郡上三川町大字多功上の原2970</td>
<td>0285-53-2011</td>
</tr>
<tr>
<td>(株)ジェイアール貨物・北関東ロジスティクス　エフ・ショップ高崎（倉賀野・熊谷(タ)）</td>
<td>0</td>
<td>群馬県高崎市倉賀野1757番地</td>
<td>027-347-5215</td>
</tr>
<tr>
<td>越谷貨物ターミナル駅</td>
<td>0</td>
<td>埼玉県越谷市南越谷2-10</td>
<td>048-986-3321</td>
</tr>
<tr>
<td>新座貨物ターミナル駅</td>
<td>0</td>
<td>埼玉県新座市大和田2丁目1番9号</td>
<td>048-479-4060</td>
</tr>
<tr>
<td>東京貨物ターミナル駅</td>
<td>0</td>
<td>東京都品川区八潮三丁目3番22号</td>
<td>03-3790-0620</td>
</tr>
<tr>
<td>隅田川駅</td>
<td>0</td>
<td>東京都荒川区南千住4丁目1番1号</td>
<td>03-3807-6548</td>
</tr>
<tr>
<td>八王子駅</td>
<td>0</td>
<td>東京都八王子市旭町1番1号</td>
<td>042-645-0190</td>
</tr>
<tr>
<td>横浜羽沢駅</td>
<td>0</td>
<td>神奈川県横浜市神奈川区羽沢町83番1号</td>
<td>045-381-7558</td>
</tr>
<tr>
<td>梶ヶ谷貨物ターミナル駅</td>
<td>0</td>
<td>神奈川県川崎市宮前区野川140番地</td>
<td>044-855-5421</td>
</tr>
<tr>
<td>川崎貨物駅</td>
<td>0</td>
<td>神奈川県川崎市川崎区塩浜4-1-1</td>
<td>044-266-5187</td>
</tr>
<tr>
<td>相模貨物駅</td>
<td>0</td>
<td>神奈川県中郡大磯町高麗3丁目4番地16号</td>
<td>0463-32-0586</td>
</tr>
<tr>
<td>(株)ジェイアール貨物・信州ロジスティクス</td>
<td>-</td>
<td>長野県長野市桐原2-2-1</td>
<td>026-243-7715</td>
</tr>
<tr>
<td rowspan="2">東海</td>
<td>(株)ジェイアール貨物・東海ロジスティクス</td>
<td>-</td>
<td>愛知県名古屋市中村区椿町21-2 第二太閤ビル</td>
<td>052-451-9381</td>
</tr>
<tr>
<td>(株)ジェイアール貨物・東海ロジスティクス 静岡支店</td>
<td>-</td>
<td>静岡県静岡市駿河区池田字大黒坪346</td>
<td>054-261-7833</td>
</tr>
<tr>
<td rowspan="3">関西</td>
<td>(株)ジェイアール貨物・北陸ロジスティクス</td>
<td>-</td>
<td>石川県金沢市高柳町1-1</td>
<td>076-253-4660</td>
</tr>
<tr>
<td>(株)ジェイアール貨物・西日本ロジスティクス</td>
<td>-</td>
<td>大阪府福島区福島6-13-7　森山ビル2F</td>
<td>06-6345-5600</td>
</tr>
<tr>
<td>(株)ジェイアール貨物・中国ロジスティクス</td>
<td>-</td>
<td>広島県広島市東区矢賀5-1-1</td>
<td>082-286-7629</td>
</tr>
<tr>
<td rowspan="1">九州</td>
<td>(株)ジェイアール貨物・九州ロジスティクス</td>
<td>-</td>
<td>福岡県北九州市小倉北区大門2丁目1番8号</td>
<td>093-581-3221</td>
</tr>
</tbody>
</table>
</div>
<br />
<ul class="about__list">
<li>※在庫数が未記載の箇所およびコンテナの形式などは「お問合せ先」でご確認下さい。</li>
<li>※販売価格はコンテナの状態によって異なります。「お問合せ先」でご確認下さい。</li>
<li>※お客様のご希望により運送事業者をご紹介いたします。</li>
<li>※お問合せ時間は、土日祝日を除く10：30～12：00、13：00～17：00となります。</li>
</ul>
</div>
</div>
</section>
<!-- jrcontainer_sales -->
<section id="sales_offshore" class="template__content about__panel relation__section">
<div class="container">
<div class="header__title">海上コンテナの販売</div>
<div class="content__container">
<p>２０ｆｔ海上中古コンテナの販売を行っております。荷物の保管、自治会の防災用具の収納、倉庫などにも幅広く活用できます。耐震・防犯に優れ耐久性は抜群です。ご入用の方は下記の「お問合せ先」にご連絡下さい。<span class="orange">なお、販売は東北６県のみとなります。</span></p>
<div class="inner__container">
<div class="linkblock__container">
<div class="linkblock__item"><img src="'. url('front_images/images/child/about/used_pic_03.jpg').'">&nbsp;</div>
<div class="linkblock__item"><img src="'. url('front_images/images/child/about/used_pic_04.jpg').'">&nbsp;</div>
</div>
</div>
</div>
<div class="content__container">
<h3>◼ 海上コンテナのサイズ・販売価格</h3>
<div class="table__container">
<table class="marine_containers__table">
<tbody>
<tr>
<th rowspan="2">種類</th>
<th colspan="3">外法寸法<br /> (mm)</th>
<th colspan="3">内法寸法<br /> (mm)</th>
<th rowspan="2">重量<br /> (ｔ)</th>
<th rowspan="2">販売価格<br />(税別)</th>
</tr>
<tr>
<th>高さ</th>
<th>幅</th>
<th>長さ</th>
<th>高さ</th>
<th>幅</th>
<th>長さ</th>
</tr>
<tr>
<td>20ft</td>
<td>2,591</td>
<td>2,438</td>
<td>6,058</td>
<td>2,416</td>
<td>2,351</td>
<td>5,949</td>
<td>約2.3</td>
<td>290,000円～</td>
</tr>
</tbody>
</table>
</div>
<br />
<ul class="about__list">
<li>※販売価格に運送費は含まれておりません。</li>
<li>※コンテナに塗装もできます。（費用別途）</li>
<li>※40ft、40ft（背高）の取り扱いは現在行っておりません。</li>
</ul>
</div>
<div class="content__container">
<h3>◼ 海上コンテナ販売のお問合せ</h3>
<ul class="about__list">
<li>〒980－0022宮城県仙台市青葉区五橋一丁目1番1号　JR貨物 東北事業開発支店</li>
<li>TEL 022-213-3891（平日9：00～17：00）</li>
</ul>
</div>
</div>
</section>
<!-- sales_offshore -->
</div>',
				'page_status' =>1,
				'page_permalink' => 'used_container',
			],
			[
				'id' => 2,
				'page_title' => '駐車場のご案内',
				'page_content' => '<div class="child about relation">
<section class="template__content child__btnlist">
<div class="container">
<p>JR貨物では、全国各地で駐車場事業を展開しています。お客様のニーズに応じて、月極駐車場や時間貸駐車場をご用意しております。</p>
<br />
<div class="template__btnlist_container">
<div class="template__btnlist"><a class="btn-primary-fill" href="#monthly_parking">月極駐車場 </a> <a class="btn-primary-fill" href="#hourly_parking">時間貸駐車場</a> <a class="btn-primary-fill" href="#group_parking">グループ会社駐車場</a></div>
</div>
</div>
</section>
<!-- child__btnlist -->
<section id="monthly_parking" class="template__content about__panel relation__section">
<div class="container">
<div class="header__title">月極駐車場</div>
<div class="content__container">
<p>【空き状況】 ○：3台以上空きあり、△：3台未満空きあり、&times;：空きなし</p>
<div class="table__container">
<table>
<tbody>
<tr>
<th>エリア</th>
<th>月極駐車場名</th>
<th>空き<br />状況</th>
<th>所在地</th>
<th>収容<br />台数</th>
<th>月額料金<br />(税込)</th>
<th>敷金</th>
<th>現地<br />お問合せ先</th>
<th>お申込み<br />お問合せ先</th>
</tr>
<tr>
<td rowspan="1">北海道</td>
<td>苗穂駅前駐車場</td>
<td>&times;</td>
<td>北海道札幌市中央区北3条東14-284-4<br />（ＪＲ苗穂駅徒歩1分）</td>
<td>55台</td>
<td>10,800円</td>
<td>1ヶ月分</td>
<td>北海道事業開発支店<br />（平日9:00～17:50）<br />011-737-2720</td>
<td>&nbsp;</td>
</tr>
<tr>
<td rowspan="5">東北</td>
<td>横手駅駐車場</td>
<td>△</td>
<td>秋田県横手市駅前町5-1<br />（ＪＲ横手駅隣接）</td>
<td>4台</td>
<td>7,560円</td>
<td>不要</td>
<td>東北支社<br />秋田総合鉄道部<br />（平日9:00～17:00）<br />018-823-6703</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>本塩釜駅付近仙石線<br />高架下駐車場</td>
<td>△</td>
<td>宮城県塩釜市海岸通り16-4<br />（ＪＲ本塩釜駅徒歩5分）</td>
<td>39台</td>
<td>9,720円</td>
<td>不要</td>
<td>東北事業開発支店<br />（平日9:00～17:00）<br />022-213-3891<br />022-213-5155</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>五輪社宅フォレスト<br />みやぎの駐車場</td>
<td>△</td>
<td>宮城県仙台市宮城野区五輪1-7-13<br />（ＪＲ宮城野原駅徒歩10分）</td>
<td>13台</td>
<td>10,800円</td>
<td>不要</td>
<td>東北事業開発支店<br />（平日9:00～17:00）<br />022-213-3891<br />022-213-5155</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>蔵王駅駐車場</td>
<td>〇</td>
<td>山形県山形市大字松原字東谷地15-9<br />（ＪＲ蔵王駅徒歩3分）</td>
<td>30台</td>
<td>3,240円</td>
<td>不要</td>
<td>東北事業開発支店<br />（平日9:00～17:00）<br />022-213-3891<br />022-213-5155</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>会津若松駅駐車場</td>
<td>&times;</td>
<td>福島県会津若松市駅前1-1<br />（ＪＲ会津若松駅徒歩5分）</td>
<td>6台</td>
<td>7,560円</td>
<td>不要</td>
<td>東北事業開発支店<br />（平日9:00～17:00）<br />022-213-3891<br />022-213-5155</td>
<td>&nbsp;</td>
</tr>
<tr>
<td rowspan="3">東海</td>
<td>稲沢駅東駐車場</td>
<td>&times;</td>
<td>愛知県稲沢市下津町東森9-8<br />（ＪＲ稲沢駅東口徒歩3分）</td>
<td>65台</td>
<td>8,640円</td>
<td>不要</td>
<td>東海事業開発支店<br />（平日9:00～17:00）<br />0587-24-3625</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>四日市駅駐車場</td>
<td>&times;</td>
<td>三重県四日市市本町108-16<br />（ＪＲ四日市駅徒歩3分）</td>
<td>67台</td>
<td>8,640円</td>
<td>不要</td>
<td>東海事業開発支店<br />（平日9:00～17:00）<br />0587-24-3625</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>近江長岡駐車場</td>
<td>〇</td>
<td>滋賀県米原市山東町万願寺130-12<br />（ＪＲ近江長岡駅徒歩3分）</td>
<td>27台</td>
<td>3,600円</td>
<td>不要</td>
<td>東海事業開発支店<br />（平日9:00～17:00）<br />0587-24-3625</td>
<td>&nbsp;</td>
</tr>
<tr>
<td rowspan="2">九州</td>
<td>西小倉駐車場</td>
<td>&times;</td>
<td>福岡県北九州市小倉北区大門2-200-21<br />（ＪＲ西小倉駅南口徒歩10分）</td>
<td>15台</td>
<td>9,257円</td>
<td>1ヶ月分</td>
<td>九州事業開発支店<br />（平日9:00～17:50）<br />093-583-6254</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>エフパーキング北天神<br />※立体・屋根有り</td>
<td>〇</td>
<td>福岡県福岡市中央区長浜1-4-50<br />（地下鉄天神駅１番出口より徒歩10分）</td>
<td>777台</td>
<td>〔場所指定〕<br />22,680円<br />〔指定なし〕<br />19,440円</td>
<td>1ヶ月分</td>
<td>エフパーキング北天神<br />（トラストパーク）<br />（平日9:00～18:00）<br />092-473-8308</td>
<td>&nbsp;</td>
</tr>
</tbody>
</table>
</div>
<br />
<ul class="about__list">
<li>※「エフパーキング北天神」のみ立体・屋根つき、他の駐車場は全て平面・屋根なし</li>
</ul>
</div>
</div>
</section>
<!-- monthly_parking -->
<section id="hourly_parking" class="template__content about__panel relation__section">
<div class="container">
<div class="header__title">時間貸駐車場</div>
<div class="content__container">
<p>お探しの駐車場名をクリックしてください。</p>
<div class="table__container">
<table>
<tbody>
<tr>
<td>エリア</td>
<td>時間貸駐車場名</td>
</tr>
<tr>
<td rowspan="4">東北</td>
<td><a href="http://npc-npc.co.jp/parking/prefecture-2/city-192/parking-201/" target="_blank" rel="noopener">NPC24H八戸駅前パーキング</a></td>
</tr>
<tr>
<td><a href="http://www.repark.jp/parking_user/time/result/detail/?park=REP0013397&amp;p=1&amp;st=4&amp;lat=39.2867646&amp;lon=141.1131909&amp;word=%E5%B2%A9%E6%89%8B%E7%9C%8C%E5%8C%97%E4%B8%8A%E5%B8%82&amp;plc=%E5%B2%A9%E6%89%8B%E7%9C%8C%E5%8C%97%E4%B8%8A%E5%B8%82&amp;pref=3&amp;city=206&amp;" target="_blank" rel="noopener">リパーク北上駅前</a></td>
</tr>
<tr>
<td><a href="http://times-info.net/map/parkdetails/BUK0018610.html" target="_blank" rel="noopener">タイムズ米沢駅前</a></td>
</tr>
<tr>
<td><a href="http://npc-npc.co.jp/parking/prefecture-7/city-364/parking-232/" target="_blank" rel="noopener">NPC24H会津若松駅前駅前パーキング</a></td>
</tr>
<tr>
<td rowspan="5">関西</td>
<td><a href="http://npc-npc.co.jp/parking/prefecture-25/city-1167/parking-561/" target="_blank" rel="noopener">NPC24H守山駅東口パーキング</a></td>
</tr>
<tr>
<td><a href="http://npc-npc.co.jp/parking/prefecture-25/city-1167/parking-562/" target="_blank" rel="noopener">NPC24H守山駅西口パーキング</a></td>
</tr>
<tr>
<td><a href="http://times-info.net/map/parkdetails/BUK0018546.html" target="_blank" rel="noopener">タイムズＪＲ膳所駅前</a></td>
</tr>
<tr>
<td><a href="http://times-info.net/map/parkdetails/BUK0027509.html" target="_blank" rel="noopener">タイムズ安治川口駅南第２</a></td>
</tr>
<tr>
<td><a href="http://npc-npc.co.jp/parking/prefecture-33/city-1450/parking-317/" target="_blank" rel="noopener">NPC24H岡山西島田町パーキング</a></td>
</tr>
<tr>
<td rowspan="4">九州</td>
<td><a href="http://tpnavi.com/P719.html" target="_blank" rel="noopener">エフパーキング北天神</a></td>
</tr>
<tr>
<td><a href="http://times-info.net/map/parkdetails/BUK0025085.html" target="_blank" rel="noopener">タイムズ羽犬塚駅前第２</a></td>
</tr>
</tbody>
</table>
</div>
</div>
</div>
</section>
<!-- hourly_parking -->
<section id="group_parking" class="template__content about__panel relation__section">
<div class="container">
<div class="header__title">グループ会社駐車場</div>
<div class="content__container">
<p>ご希望の駐車場を管理している会社にお問い合わせください。<br />会社名をクリックで別ウィンドウで各会社のホームページが開きます。</p>
<div class="table__container">
<table>
<tbody>
<tr>
<td colspan="4"><a href="http://jrf-tlogi.com/jrftl[parking].html" target="_blank" rel="noopener"><strong>○(株)ジェイアール貨物・東北ロジスティクス</strong></a></td>
</tr>
<tr>
<th>エリア</th>
<th>種別</th>
<th>駐車場名</th>
<th>住所</th>
</tr>
<tr>
<td>東北</td>
<td>月極</td>
<td>盛岡盛南大橋下駐車場</td>
<td>岩手県盛岡市盛岡駅西通二丁目108-8地内</td>
</tr>
</tbody>
</table>
</div>
<div class="table__container">
<table>
<tbody>
<tr>
<td colspan="4"><a href="http://www.jrf-fudosan.co.jp/work/05-estate.html" target="_blank" rel="noopener"><strong>○(株)ジェイアール貨物・不動産開発</strong></a></td>
</tr>
<tr>
<th>エリア</th>
<th>種別</th>
<th>駐車場名</th>
<th>住所</th>
</tr>
<tr>
<td rowspan="16">関東 甲信越</td>
<td>月極・時間貸</td>
<td>アイガーデンパーキング</td>
<td>東京都千代田区飯田橋３-１０-９ガーデンエアタワー地下1階</td>
</tr>
<tr>
<td>月極・時間貸</td>
<td>Ｆ開発 田端操駅駐車場</td>
<td>東京都北区東田端 １－１６</td>
</tr>
<tr>
<td>月極</td>
<td>小名木川駐車場</td>
<td>東京都江東区北砂２－２０</td>
</tr>
<tr>
<td>月極・時間貸</td>
<td>ＪＲ貨物八王子駅南口立体駐車場</td>
<td>東京都八王子市旭町１－１１</td>
</tr>
<tr>
<td>時間貸</td>
<td>ＪＲ貨物八王子駅北口駐車場</td>
<td>東京都八王子市旭町１－１</td>
</tr>
<tr>
<td>月極</td>
<td>Ｆ開発 東高島駅駐車場</td>
<td>神奈川県横浜市神奈川区星野町４</td>
</tr>
<tr>
<td>月極</td>
<td>Ｆ開発 東高島駅東駐車場<br />（ｺｯﾄﾝﾊｰﾊﾞｰ地区）</td>
<td>神奈川県横浜市神奈川区星野町４</td>
</tr>
<tr>
<td>月極</td>
<td>安善駅駐車場</td>
<td>神奈川県横浜市鶴見区安善町１－４</td>
</tr>
<tr>
<td>月極</td>
<td>扇町駅駐車場</td>
<td>神奈川県川崎市川崎区扇町４－５</td>
</tr>
<tr>
<td>月極</td>
<td>下館駅前駐車場</td>
<td>茨城県筑西市字下田中丙１６４－２２</td>
</tr>
<tr>
<td>月極</td>
<td>土浦駅駐車場（桜川エリア）</td>
<td>茨城県土浦市有明町２９７９－２</td>
</tr>
<tr>
<td>時間貸</td>
<td>Ｆ開発 土浦駅</td>
<td>茨城県土浦市有明町２９７９－２</td>
</tr>
<tr>
<td>月極・時間貸</td>
<td>ＪＲ小山駅駐車場</td>
<td>栃木県小山市駅東通２－１８９２</td>
</tr>
<tr>
<td>月極</td>
<td>Ｆ開発 善道町駐車場</td>
<td>新潟県新潟市秋葉区善道町１ JR新津駅東口近く</td>
</tr>
<tr>
<td>月極</td>
<td>Ｆ開発 村上駅駐車場</td>
<td>新潟県村上市田端町１１</td>
</tr>
<tr>
<td>時間貸</td>
<td>Ｆ開発　長野駅西口駐車場</td>
<td>長野県長野市大字鶴賀７６０－９</td>
</tr>
<tr>
<td>東海</td>
<td>月極・時間貸</td>
<td>ＪＲＦ豊橋駅前駐車場</td>
<td>愛知県豊橋市駅前大通１－１３５</td>
</tr>
</tbody>
</table>
</div>
</div>
</div>
</section>
<!-- group_parking -->
</div>',
				'page_status' => 1,
				'page_permalink' => 'parking',
			],
		
		]);
    }
}
