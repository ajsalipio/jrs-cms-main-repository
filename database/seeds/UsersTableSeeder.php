<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
    		$newUser = new User;


            $newUser->fill([
                'name' => 'root',
                'password' => bcrypt('Commude1958'),
                'email' => 'dev@commude.co.jp',
            ]);

    		$newUser->save();

    }
}
