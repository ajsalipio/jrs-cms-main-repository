<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();
//previously using page controller for that part: controller should be cleaned
Route::get('/', 'FrontController@index');
Route::get('/dashboard', 'AdminController@index');
/**
 * Front Office routing:
 *
 * 
 *
 * The following route are for front page and preview
 *
 */
/** Front pages routing
  */
//for parking and second hand container
Route::get('/about/relation/{id}', 'PageController@show');
// For situations
Route::get('/situation/detail{id}',    'FrontController@showSituation');
Route::get('/i_daiya',    'FrontController@showLastSituation');
Route::view('/i_daiya/service','front.i_daiya.service');

//for info: public list
Route::get('/info',    'FrontController@infoList')->middleware('mobile');
Route::get('/info/{tab}',    'FrontController@infoList')->middleware('mobile');
Route::get('/info/detail{id}',    'FrontController@showInfo');
//public voice
Route::get('/voice',    'FrontController@voiceList');
Route::get('/voice/detail{id}',    'FrontController@showVoice');
Route::get('/info/list/{year}', 'FrontController@getInfoList');
//Preview routing
Route::get('/preview/page/{id}', 'PageController@show');
Route::post('/preview/page/{id}', 'PageController@store');

// Cargo calculator
Route::view('modalshift',                    'front.modalshift.index');
Route::get('modalshift/calculate',           'FrontController@getCalculateModalShiftView');
Route::post('modalshift/calculate/finish',   'FrontController@calculateModalShift');


// Static Pages
Route::get('inquiry',            'StaticFrontController@getInquiryView');
Route::post('inquiry/confirm',   'StaticFrontController@confirmInquiry');
Route::post('inquiry/send',      'StaticFrontController@sendInquiry');
Route::get('inquiry/complete',   'StaticFrontController@getSuccessMailView');

Route::view('compliance', 'front.compliance.index');
Route::view('compliance/form', 'front.compliance.form');
Route::post('finish', 'StaticFrontController@confirmCompliance');
Route::post('compliance/send', 'StaticFrontController@sendCompliance');
Route::view('compliance/complete','front.compliance.complete');

// RE REOUTE LINKS (LEGACY -> NEW)
Route::get('/index.html', function() { return redirect('/'); });
Route::get('/i_daiya/index.html', function() { return redirect('/i_daiya'); });
Route::get('/info/index.html', function() { return redirect('/info'); });
Route::get('/transport/merit/index.html', function() { return redirect('/service'); });
Route::get('/transport/price/index.html', function() { return redirect('/service/price'); });
Route::get('/transport/container/index.html', function() { return redirect('/service/container'); });
Route::get('/transport/service/index.html', function() { return redirect('/service/transport'); });
Route::get('/transport/service/move.html', function() { return redirect('/service/transport/move'); });
Route::get('/transport/service/lng.html', function() { return redirect('/service/transport/lng'); });
Route::get('/transport/service/international/index.html', function() { return redirect('/service/transport/international'); });
Route::get('/transport/service/vein.html', function() { return redirect('/service/transport/vein'); });
Route::get('/transport/service/31feet.html', function() { return redirect('/service/transport/31feet'); });
Route::get('/transport/service/large.html', function() { return redirect('/service/transport/large'); });
Route::get('/transport/service/petroleum.html', function() { return redirect('/service/transport/petroleum'); });
Route::get('/transport/improvement/solution.html', function() { return redirect('/service/transport/solution'); });
Route::get('/transport/area/index.html', function() { return redirect('/service/area'); });
Route::get('/transport/improvement/index.html', function() { return redirect('/service/improvement'); });
Route::get('/transport/improvement/development.html', function() { return redirect('/service/improvement/development'); });
Route::get('/transport/improvement/control.html', function() { return redirect('/service/improvement/control'); });
Route::get('/transport/improvement/keep.html', function() { return redirect('/service/improvement/keep'); });
Route::get('/transport/improvement/es.html', function() { return redirect('/service/improvement/es'); });
Route::get('/transport/improvement/it.html', function() { return redirect('/service/improvement/it'); });
Route::get('/transport/improvement/infra.html', function() { return redirect('/service/improvement/infra'); });
Route::get('/environment/modalshift.html', function() { return redirect('/modalshift'); });
Route::get('/environment/calculate/index.php', function() { return redirect('/modalshift/calculate'); });
Route::get('/recruit/index.html', function() { return redirect('/recruit'); });
Route::get('/about/outline/company.html', function() { return redirect('/about'); });
Route::get('/relation/railgate.html', function() { return redirect('/about/railgate'); });
Route::get('/relation/index.html', function() { return redirect('/about/relation'); });
Route::get('/relation/f_plaza.html', function() { return redirect('/about/relation/f_plaza'); });
Route::get('/relation/various.html', function() { return redirect('/about/relation/various'); });
Route::get('/used_container/index.html', function() { return redirect('/about/relation/used_container'); });
Route::get('/parking/index.html', function() { return redirect('/about/relation/parking'); });
Route::get('/about/group.html', function() { return redirect('/about/group'); });
Route::get('/about/society.html', function() { return redirect('/about/csr'); });
Route::get('/about/plan.html', function() { return redirect('/about/csr/plan'); });
Route::get('/about/financial.html', function() { return redirect('/about/csr/financial'); });
Route::get('/about/safety.html', function() { return redirect('/about/csr/safety'); });
Route::get('/about/other/index.html', function() { return redirect('/about/other/'); });
Route::get('/about/other/construction.html', function() { return redirect('/about/other/construction'); });
Route::get('/about/other/use.html', function() { return redirect('/about/other/use'); });
Route::get('/about/other/nation.html', function() { return redirect('/about/other/nation'); });
Route::get('/about/other/disasterprevention.html', function() { return redirect('/about/other/disasterprevention'); });
Route::get('/about/other/infectioncontrol.html', function() { return redirect('/about/other/infectioncontrol'); });
Route::get('/about/other/woman.html', function() { return redirect('/about/other/woman'); });
Route::get('/about/other/procurement.html', function() { return redirect('/about/other/procurement'); });
Route::get('/inquiry/index.html', function() { return redirect('/inquiry/'); });
Route::get('/compliance/compliance.html', function() { return redirect('/compliance/'); });
Route::get('/individual.html', function() { return redirect('/individual'); });
Route::get('/sitepolicy.html', function() { return redirect('/sitepolicy'); });
Route::get('/sitemap.html', function() { return redirect('/sitemap'); });
Route::get('/link.html', function() { return redirect('/link'); });


/**
 * English site routing
 *
 *
 *
 */
Route::group(['prefix' => 'en'], function() {

    Route::view('/', 'front.en.index');
    Route::view('/corporate-overview', 'front.en.corporate-overview.index');
    Route::view('/ecology', 'front.en.ecology.index');
    Route::view('/financials', 'front.en.financials.index');
    Route::view('/innovation', 'front.en.innovation.index');
    Route::view('/lifeline', 'front.en.lifeline.index');
    Route::view('/logistics', 'front.en.logistics.index');
    Route::view('/main-data', 'front.en.main-data.index');
    Route::view('/material-procurement-info', 'front.en.material-procurement-info.index');
    Route::view('/new-ventures', 'front.en.new-ventures.index');
    Route::view('/on-time', 'front.en.on-time.index');
    Route::view('/philosophy', 'front.en.philosophy.index');
    Route::view('/service', 'front.en.service.index');


});


/**
 * Dashboard (admin) pages routing
 *
 *
 *
 */
Route::group(['middleware' => ['auth', 'page_auth'], 'prefix' => 'dashboard'], function() {
    
     //News    
    Route::group(['prefix' => 'news', 'as' => 'NEWS_ROUTES.'], function() {
		Route::get('list', 'NewsController@index');
		Route::get('create', 'NewsController@create');
		Route::get('edit/{id}', 'NewsController@edit');
		Route::post('save', 'NewsController@store');
		Route::post('update/{id}', 'NewsController@update');
		Route::post('delete', 'NewsController@destroy');
        Route::post('preview', 'NewsController@preview');
        Route::post('sticky/edit', 'NewsController@editSticky')->name('editStickyNews');
    });
    
     // CUSTOMER VOICES ROUTES 
    Route::group(['prefix' => 'customer-voices', 'as' => 'CUSTOMER_VOICES_ROUTES.'], function() {
		Route::get('list', 'CustomerVoicesController@index');
		Route::get('create', 'CustomerVoicesController@create');
		Route::get('edit/{id}', 'CustomerVoicesController@edit');
		Route::post('save', 'CustomerVoicesController@store');
		Route::post('update/{id}', 'CustomerVoicesController@update');
		Route::post('delete', 'CustomerVoicesController@destroy');
		Route::post('preview', 'CustomerVoicesController@preview');
    });
     // SITUATION ROUTES
    Route::group(['prefix' => 'situation', 'as' => 'SITUATION_ROUTES.'], function() {
        Route::get('/list',         'SituationController@index')->name('displaySituations');        
        Route::get('/add',          'SituationController@create')->name('addSituation');
        Route::get('/edit/{id}',    'SituationController@edit')->name('editSituation');
        Route::post('/edit/{id}',   'SituationController@update')->name('updateSituation')->middleware('situation_save');
        Route::post('/add',         'SituationController@store')->name('createSituation')->middleware('situation_save');
        Route::post('/delete',      'SituationController@delete')->name('deleteSituations');
        Route::post('/preview',     'SituationController@preview')->name('previewSituation');
    });

    // MAIL ROUTES 
    Route::group(['prefix' => 'mail', 'as' => 'MAIL_ROUTES.'], function() {
        Route::get('/list',       'MailController@index')->name('displayMails');        
        Route::get('/add',        'MailController@csvImportView')->name('addMail');
        Route::post('/add',       'MailController@create')->name('createMail');
        Route::post('/import',    'MailController@importCSV')->name('importMailCsv');
        Route::post('/download',    'MailController@export')->name('exportMailCSV');
        Route::post('/save',       'MailController@store')->name('saveMail'); // Create and update
        Route::post('/delete',    'MailController@delete')->name('deleteMails');
    });

	//PAGES ROUTES
    Route::group(['prefix' => 'page', 'as' => 'PAGE_ROUTES.'], function() {
	//	Route::get('page', 'PageController@index');//2018-05-21: Greg May be not required.
	//	Route::get('page_new', 'PageController@create');//2018-05-21: Greg May be not required.
	//	Route::post('page_save', 'PageController@store');//2018-05-21: Greg May be not required.
		Route::get('edit/{id}', 'PageController@edit');
		Route::post('update/{id}', 'PageController@update');
	//	Route::post('page_delete', 'PageController@delete');//2018-05-21: Greg May be not required.
    });
////upload image to view
	Route::post('media/upload','MediaController@tinyMCE');
	Route::post('/media/delete','MediaController@delete');
    //Route::post('campaign/upload','MediaController@fileUpload');
    
    // USER ROURTES
    Route::group(['prefix' => 'user', 'as' => 'USER_ROUTES.'], function() {
        
        Route::get('/list',              'UserController@index')->name('displayUsers');
        Route::get('/add',               'UserController@create')->name('addUser');
        Route::get('/edit/{id}',         'UserController@edit')->name('editUser');
        Route::post('/edit/{id}',        'UserController@update')->name('updateUser');
        Route::post('/add',              'UserController@store')->name('createUser');
        Route::post('/delete',           'UserController@delete')->name('deleteUsers');
        Route::get('/profile',           'UserController@editProfileView')->name('getEditProfileView');
        Route::post('/profile/{id}',      'UserController@changePassword')->name('changePassword');
        
    });

  
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
