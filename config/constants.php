<?php 
return [

     /****************************************** APP DATA  ******************************************/

     'VERSION' => '1.0.1A',
     
    'SITUATION_STATUS' => [
        '0' => '只今輸送状況のお知らせはございません。', // (No new announcement on transportation status)
        '1' => '現在、平常通りに運行しております。', // (Operate normal)
        '2' => '只今の時間帯はサービス時間外となります。', // (Non-business hours)
        '3' => '作成した記事を表示 ' // (Display created article)
    ],
    
    'SITUATION_MAIL_STATUS' => [
        '0' => '無',
        '1' => '無',
        '2' => '有',
    ],
    
    'SITUATION_POST_STATUS' => [
        '0' => '下書き保存',
        '1' => 'ページ更新',
        '2' => 'ページ更新しメール配信する'
    ],
    
    'SITUATION_POST_PUBLISH_STATUS' => [
        '0' => '非公開', // Unpublished
        '1' => '公開中', // Published
        '2' => '公開中', // Published
    ], 
    
    'MAIL_STATUS' => [
		'0' => 'PC添付あり', // With attachment on pc
        '1' => 'PC添付なし', // Without attachment on pc
        '2' => '携帯添付なし' // No attachment on mobile
    ],

    'MAIL_FILE_EXPORT_NAME' =>  'mails.csv',

    'PUBLISH_TIME_OUT_OF_SERVICE' => [
        'START' => '02:00', 
        'END'   => '06:59',
        'MESSAGE' => '2:00-6:59までは「只今の時間帯はサービス時間外となります。」と表示されます。',
        'TEXT'    => '只今の時間帯はサービス時間外となります。'
    ],

    'PUBLISH_STATUS' => [
        '0' => '非公開', // Unpublished
        '1' => '公開中', // Published
    ],
	
    'NEWS_STATUS' => [
		'0' => '下書き保存', // Draft
		'1' => '公開'  // Public
    ],
	
    'NEWS_CATEGORIES' => [
	   '0' => 'ニュースリリース',
	   '1' => 'ダイヤ改正',
	   '2' => '決算・中間決算',
	   '3' => '地域ニュース',
	   '4' => 'イベント'
    ],

     'NEWS_CATEGORIES_CSS' => [
	   'ニュースリリース'   => 'btn-blue-fill',
	   'ダイヤ改正'        => 'btn-cyan-fill',
	   '決算・中間決算'    => 'btn-orange-fill',
	   '地域ニュース'      => 'btn-purple-fill',
	   'イベント'         => 'btn-pink-fill'
    ],
    
    'REGIONAL_NEWS_SUB_CAT' => [
	   '0' => '北海道',
	   '1' => '東北',
	   '2' => '関東',
	   '3' => '東海',
	   '4' => '関西',
	   '5' => '九州',

    ],
    'POST_STATUS' => [
	   '0' => '下書き保存', // Draft
	   '1' => '公開'// Public
    ],
    'PAGINATION' => [
	   'front_mobile' => '6',
	   'front_pc'	  => '20',
	   'limit'		  => '10',
    ],


    // Master Data

    'PREFECTURES' => [
        '北海道',
        '青森県',
        '岩手県',
        '宮城県',
        '秋田県',
        '山形県',
        '福島県',
        '茨城県',
        '栃木県',
        '群馬県',
        '埼玉県',
        '千葉県',
        '東京都',
        '神奈川県',
        '新潟県',
        '富山県',
        '石川県',
        '福井県',
        '山梨県',
        '長野県',
        '岐阜県',
        '静岡県',
        '愛知県',
        '三重県',
        '滋賀県',
        '京都府',
        '大阪府',
        '兵庫県',
        '奈良県',
        '和歌山県',
        '鳥取県',
        '島根県',
        '岡山県',
        '広島県',
        '山口県',
        '徳島県',
        '香川県',
        '愛媛県',
        '高知県',
        '福岡県',
        '佐賀県',
        '長崎県',
        '熊本県',
        '大分県',
        '宮崎県',
        '鹿児島県',
        '沖縄県'
    ],
    
    
    /****************************************** FILE CONFIG  ******************************************/
    
    'FILE_PREFIXES' => [
	'IMG' => 'image_', 
        'FILE' => 'file_', 
    ],

    // Cookies
    'LOGIN_COOKIE_ID' => 'jr_r',
    
     /****************************************** FILE CONFIG  ******************************************/

     'CHANNELS' => [
         'situation'           => 'front-situation_channel',
     ],

     'EVENTS' => [
         'situation_save'       => 'event-situation_save',
     ],

     /****************************************** USER  ******************************************/

     'USER_GROUPS' => [
        'ADMIN'          => '管理者',
        'SITUATION'      => '現在の輸送状況',
        'OTHERS'         => 'その他',
    ],

    'USER_GROUP_PAGES' => [
        'ADMIN'      => [ 
            'USER_ROUTES', // ユーザー
            'SITUATION_ROUTES', // 現在の輸送状況
            'MAIL_ROUTES', // 配信先一覧 (or メールの配信先)
            'NEWS_ROUTES', // 最新情報
            'CUSTOMER_VOICES_ROUTES', // お客様の声
            'USED_CONTAINER_ROUTES', // 中古コンテナ
            'PAGE_ROUTES', // 駐車場のご案内
        ],
        'SITUATION'  => [
            'SITUATION_ROUTES', // 現在の輸送状況
            'MAIL_ROUTES'
        ],
        'OTHERS'     =>  [
            'NEWS_ROUTES', // 最新情報
            'CUSTOMER_VOICES_ROUTES', // お客様の声
            'USED_CONTAINER_ROUTES', // 中古コンテナ
            'PAGE_ROUTES' // 駐車場のご案内
        ],
    ],
    
];
