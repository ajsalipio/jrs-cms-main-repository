<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Cargo constants
    |--------------------------------------------------------------------------
    |
    | Contains the constants from:
    | 1. constants.xslx
    | 2. fuel_consumption.xlsx
    | 3. substitute_station.xlsx
    |
    */

    /****************************************** CONSTANTS  ******************************************/
    
    'equiv_oil_diesel'                   => 0.99,
    'equiv_oil_electricity'              => 2.54,
    'truck_co2_emission_coefficient'     => 2.62,
    'truck_energy_per_unit'              => 37.7,
    'train_energy_per_unit'              => 0.491,
    'train_co2_emission_per_unit'        => 23,
    'train_co2_emission_coefficient'     => 0.368,
    'equiv_oil_energy_train'             => 2.58258,
    'equiv_co2_energy'                   => 0.0686,
    'equiv_oil_energy'                   => 0.0258,
    'train_truck_energy'                 => 1.36,

    /****************************************** FUEL CONSUMPTION ******************************************/
    
    'range_1' => [

    ],

];
