<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Mail Driver
    |--------------------------------------------------------------------------
    |
    | Laravel supports both SMTP and PHP's "mail" function as drivers for the
    | sending of e-mail. You may specify which one you're using throughout
    | your application here. By default, Laravel is setup for SMTP mail.
    |
    | Supported: "smtp", "sendmail", "mailgun", "mandrill", "ses",
    |            "sparkpost", "log", "array"
    |
    */

    'driver' => env('MAIL_DRIVER', 'smtp'),

    /*
    |--------------------------------------------------------------------------
    | SMTP Host Address
    |--------------------------------------------------------------------------
    |
    | Here you may provide the host address of the SMTP server used by your
    | applications. A default option is provided that is compatible with
    | the Mailgun mail service which will provide reliable deliveries.
    |
    */

    'host' => env('MAIL_HOST', 'smtp.mailgun.org'),

    /*
    |--------------------------------------------------------------------------
    | SMTP Host Port
    |--------------------------------------------------------------------------
    |
    | This is the SMTP port used by your application to deliver e-mails to
    | users of the application. Like the host we have set this value to
    | stay compatible with the Mailgun e-mail application by default.
    |
    */

    'port' => env('MAIL_PORT', 587),

    /*
    |--------------------------------------------------------------------------
    | Global "From" Address
    |--------------------------------------------------------------------------
    |
    | You may wish for all e-mails sent by your application to be sent from
    | the same address. Here, you may specify a name and address that is
    | used globally for all e-mails that are sent by your application.
    |
    */

    'from' => [
        'address' => env('MAIL_FROM_ADDRESS', 'info@jr-kamotsu.co.jp'),
        'name' => env('MAIL_FROM_NAME', 'JR Freight'),
    ],

    /*
    |--------------------------------------------------------------------------
    | E-Mail Encryption Protocol
    |--------------------------------------------------------------------------
    |
    | Here you may specify the encryption protocol that should be used when
    | the application send e-mail messages. A sensible default using the
    | transport layer security protocol should provide great security.
    |
    */

    'encryption' => env('MAIL_ENCRYPTION', 'tls'),

    /*
    |--------------------------------------------------------------------------
    | SMTP Server Username
    |--------------------------------------------------------------------------
    |
    | If your SMTP server requires a username for authentication, you should
    | set it here. This will get used to authenticate with your server on
    | connection. You may also set the "password" value below this one.
    |
    */

    'username' => env('MAIL_USERNAME'),

    'password' => env('MAIL_PASSWORD'),

    /*
    |--------------------------------------------------------------------------
    | Sendmail System Path
    |--------------------------------------------------------------------------
    |
    | When using the "sendmail" driver to send e-mails, we will need to know
    | the path to where Sendmail lives on this server. A default path has
    | been provided here, which will work well on most of your systems.
    |
    */

    'sendmail' => '/usr/sbin/sendmail -bs',

    /*
    |--------------------------------------------------------------------------
    | Markdown Mail Settings
    |--------------------------------------------------------------------------
    |
    | If you are using Markdown based email rendering, you may configure your
    | theme and component paths here, allowing you to customize the design
    | of the emails. Or, you may simply stick with the Laravel defaults!
    |
    */

    'markdown' => [
        'theme' => 'default',

        'paths' => [
            resource_path('views/vendor/mail'),
        ],
    ],
    
    /****************************************** CUSTOM  ******************************************/

    // Backup
    // 'INQUIRY_MAIL_TO' => [
    //     '鉄道貨物輸送のお問い合わせ・お見積り' => 'debug@commude.co.jp',
    //     '関連事業' => 'dev@commude.co.jp',
    //     '安全について' => 'ken_kasai@commude.co.jp',
    //     'その他' => 'web@commude.co.jp',
    // ],
    // 'COMPLIANCE_MAIL_TO' => 'dev@commude.co.jp',
    // TEST MAIL: info@jr-kamotsu.co.jp

    'inquiry' => [
        'from'       => 'webserver@jrfreight.co.jp',
        'from_test'  => 'eigyo_yuso@jrfreight.co.jp',
        'subject'    => '【お問い合わせフォーム】お問い合わせ',
        'to'         => [
	        'eigyo_web@jrfreight.co.jp',
            'kaihatsu@jrfreight.co.jp',
            'anzen@jrfreight.co.jp',
            'webmaster@jrfreight.co.jp',
        ],
        'to_test'    => [
            'wakana_saito@commude.co.jp',
            'commude.saito@gmail.com',
            'wildloveberryheart1987@yahoo.co.jp',
            'love_berry_latte.1987@docomo.ne.jp',	
            //'suzuki@jrfreight.co.jp',
            //'atsuhito-yoshioka@jrfreight.co.jp'
        ],
        
    ],
    
	'compliance' => [
        'from'       => 'webserver@jrfreight.co.jp',
        'from_test'  => 'eigyo_yuso@jrfreight.co.jp',
        'subject'    => '【外部通報専用フォーム】お問い合わせ',
        'to'         => [
            'suzuki@jrfreight.co.jp',
            'atsuhito-yoshioka@jrfreight.co.jp',
        ],
        'to_test'    => [
		    'wakana_saito@commude.co.jp'
		//'helpline@jrfreight.co.jp',
        ],
    ],

    'situation' => [
        'from'       => 'info@jr-kamotsu.co.jp',
        'from_test'  => 'eigyo_yuso@jrfreight.co.jp',
        'subject'    => '【JR貨物】輸送状況 ' . date("Y年m月d日 H時") . '現在',
        // 'TO' (recipient) variables are not required since recipients are set from the mails table in the database
        // Placing it here just in case
        'to'         => [
            // 'suzuki@jrfreight.co.jp',
            // 'atsuhito-yoshioka@jrfreight.co.jp',
        ],
        'to_test'    => [
            // 'helpline@jrfreight.co.jp',
        ],
    ]

];
