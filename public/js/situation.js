function checkAllItems(event) {
    var cbArchivedItems = document.getElementsByName('archived_items[]');
    for (var i = 0; i < cbArchivedItems.length; i++) {
        cbArchivedItems[i].checked = event.currentTarget.checked;
    }
}

function deleteSituationsEvent(){    
    var $cbCheckAll = $('#cb-check_all');
    if ($cbCheckAll.length > 0)
    {
        $cbCheckAll.change(checkAllItems);
    }
    $('#btn-delete').click(function () {
        if ($('.list_chk:checked').length <= 0) {
            alert(errorMessages['DELETE_CHECKBOX_EMPTY']);
            return false;
        }
    });
}

function deleteSituationFilesEvent(){
    var $btnRemoveFileXls = $('#btn-remove-file_xls')
    var $btnRemoveFileDoc = $('#btn-remove-file_doc')
    var $btnRemoveFilePic = $('#div-pic_container');

    if ($btnRemoveFileXls.length > 0) {
        $btnRemoveFileXls.click(function () {
            $('#file_xls_archived').val(1);
            $('#link-file_xls').html('');
            this.remove();
        });
    }
    

    if ($btnRemoveFileDoc.length > 0) {
        $btnRemoveFileDoc.click(function () {
            $('#file_doc_archived').val(1);
            $('#link-file_doc').html('');
            this.remove();
        });
    }

    if ($btnRemoveFilePic.length > 0) {
        $btnRemoveFilePic.on('click', '.js-btn-remove-file_pic', function () {
            var $this = $(this);
            var $filePicsArchived = $('#file_pic_archived');

            if ($('.js-div-pic').length == 1) {
                // $this.parent().find('.js-link-file_pic').remove();
                $this.parent().find('.pic__preview')[0].classList.add('hidden');
                this.classList.add('hidden');
            }

            // Edit mode exists
            if($filePicsArchived.length > 0){
                if ($this.data('id') !== undefined){
                    var ids = ($filePicsArchived.val() !== '')                
                        ? $filePicsArchived.val() + ',' + $this.data('id')
                        : $this.data('id')
                    $filePicsArchived.val(ids);
                }
                 
                // Only allow delete if more than 1
                if ($('.js-div-pic').length > 1) {
                    $this.parent().remove();
                }
            }else{
                // Add mode check
                if ($('.js-div-pic').length == 1) {
                    $('.js-div-pic').first().find('.js-btn-remove-file_pic')[0].classList.add('hidden');
                }
                $this.parent().remove();
            }
        });
    }
}

function saveSituationEvents()
{
    if ($('#form-save').length < 1){
        return;
    }

    var $rbSituationStatus = $('.js-rb-situation_status');

    $rbSituationStatus.change(function() {
        var $this = $(this);
        var $txtSituationTitle = $('#txt-situation_title');
        
        if($this.val() == 3){
            $txtSituationTitle.prop('required', true);
        }else{
            $txtSituationTitle.removeAttr('required');
        }
    });

    var $btnSave = $('#btn-save');

    $btnSave.click(function () {
        xlsExtensions = ['.xlsx'];
        docExtensions = ['.xlsx'];
        picExtensions = ['.jpg', '.png', '.jpeg'];

        var $fileXls = $('#file_xls');
        var $fileDoc = $('#file_doc');
        var $filePics = $('.js-file_pic');
        var errorMessage = null;

        if ($fileXls[0].files.length > 0 && !$fileXls.hasExtension(xlsExtensions)) {
            errorMessage = errorMessages['FILE_XLS_INVALID'];
        }

        if ($fileDoc[0].files.length > 0 && !$fileDoc.hasExtension(docExtensions)) {
            var docMessage = errorMessages['FILE_DOC_INVALID'];
            errorMessage  = (errorMessage != null)
                          ? errorMessage + "\n" + docMessage
                          : docMessage;
        }

        $filePics.each(function(index, element, test){
            if (element.files.length > 0 && !$(element).hasExtension(picExtensions)) {
                var picMessage = errorMessages['FILE_PIC_INVALID'];
                errorMessage = (errorMessage != null)
                              ? errorMessage + "\n" + picMessage
                              : picMessage;
            }
        });
        
        if (errorMessage) {
            alert(errorMessage);
            return false;
        }
    });

}

function addImagesEvent() {
    var $btnAddImage = $('#btn-add_image');

    $btnAddImage.click(function () {

        if ($('.js-file_pic').length < 3){
            var $this = $(this);
            var $divPic = $('.js-div-pic').first(); // use first instance as copy
            var $newDivPics = $divPic.clone();
            var divPicContainer = $('#div-pic_container');

            $newDivPics.find('.custom__upload input:file').val("").end()
            $newDivPics.find('img').remove();
            $newDivPics.find('.pic__preview')[0].classList.add('hidden');

            if ($newDivPics.find('a')[0].classList.contains('hidden')) {
                $newDivPics.find('a')[0].classList.remove('hidden');
            }

            delete $newDivPics.find('a')[0].dataset.id; // Clear reference ids
            delete $newDivPics.find('.custom__upload input:file')[0].dataset.id; // Clear reference ids

            // Finally duplicate it and append to current td
            $newDivPics.appendTo(divPicContainer);
        }
    });

    var $divPicContainer = $('#div-pic_container');

    $divPicContainer.on('change', '.js-file_pic', function(){
        
        var $filePicsArchived = $('#file_pic_archived');
        var $this = $(this);

        // Edit mode
        // if ($filePicsArchived.length > 0) {
        //     var ids = ($filePicsArchived.val() !== '')
        //         ? $filePicsArchived.val() + ',' + $this.data('id')
        //         : $this.data('id')
        //     $filePicsArchived.val(ids);
        // }

        $this.parent().parent().find('img').remove();
        
        $divPreview = $this.parent().parent().find('.pic__preview').first()[0];
    
        uploadMultiplePreview($this[0].files, $divPreview); 
        
        if ($this[0].files.length > 0) {
            $divPreview.classList.remove('hidden');
        }else{
            $divPreview.classList.add('hidden');
        }

        if ($('.js-div-pic').length == 1) {
            if ($this.parent().parent().find('a')[0]){
                $this.parent().parent().find('a')[0].classList.add('hidden');
            }
            
            return; // do not allow delete if on
        }
    });

    $('#file_doc').change(function () {
        var fileName = (this.files.length > 0)
                     ? this.files[0].name
                     : '';

        $('#lbl-file_doc').html(fileName);
    
    });

    $('#file_xls').change(function () {
        var fileName = (this.files.length > 0)
            ? this.files[0].name
            : '';

        $('#lbl-file_xls').html(fileName);

    });


}

function deleteMailEvent() {
    $('#btn-delete').click(function () {
        if ($('.list_chk:checked').length <= 0) {
            alert('No siutations selected to delete');
            return false;
        }
    });

}

function getImageRes() {
    if (($('.pic__preview').length)) {
        $('.pic__preview').each(function () {
            var width = $(this).find('img').width();
            height = $(this).find('img').height();

            if (width >= height) {
                $(this).addClass('pic__preview--horizontal');
            } else if (height >= width) {
                $(this).addClass('pic__preview--vertical');
            }
        })
    }
}

// Main loader
$(function () {
    getImageRes();

    deleteSituationsEvent();
    deleteSituationFilesEvent();

    saveSituationEvents();
    addImagesEvent();
    
});
