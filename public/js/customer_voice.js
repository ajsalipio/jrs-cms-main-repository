$(document).ready(function() {	
	$('#save').click(function() {
		var image = $('#cv_picture').val();
		if (image) {
			if (isImage(image)) {
				return;
			} else {
				alert(errorMessages['FILE_PIC_INVALID']);
				return false;
			}	
		}
		return;				
		
	})

	
	function strtolower(string){
			return string.toLowerCase();
	}
	/**
	 * Check image extension if valid
	 *
	 * @author Sally Jean Sumeguin <sally_sumeguin@commude.ph>
		* @since 2018-05-31
		* @since 2018-06-07
	 *
	 * @param string image
	 * @return boolean
	 **/
	function isImage(image) {
		var validExtensions = ["gif", "jpeg", "png", 'jpg'];
		
		var extension = ($('#cv_picture').val()).replace(/^.*\./, '');
		if (jQuery.inArray(strtolower(extension), validExtensions ) >= 0 ) {
			return true;
		} else {
			return false;
		}
	}
})
