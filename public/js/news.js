$(document).ready(function() {
	$('#nc_4').click(function() {
		$('.timer__container').toggle();
	})
		
	$('#save').click(function() {
		var file = $('#file_pdf').val();

		if (file) {
			if (isPdf(file)) {
				return;
			} else {
				alert(errorMessages['FILE_PDF_INVALID']);
				return false;
			}	
		}
		return;
	})

	$('.js-cb-regional_category').change(function () {		
		var checked = this.checked
		
		if (!checked){
			$('.js-cb-sub_category').each(function(i, element) {
				element.checked = false;
			});
		}else{
			// checked
			if ($('.js-cb-sub_category:checked').length == 0){
				this.checked = false;
			}
		}
	});

	$('.js-cb-sub_category').change(function () {		
		var checked = this.checked;
		var isLast = ($('.js-cb-sub_category:checked').length == 0);
		var $cbRegionalCategory = $('.js-cb-regional_category');

		if (checked){
			$cbRegionalCategory.first().prop('checked', true);
		}
	
		if (isLast && $cbRegionalCategory.first().prop('checked')){
			$cbRegionalCategory.first().prop('checked', false);
		}
		
	});

	$('#cb-sticky_all').change(function () {
		let status = $(this).is(":checked") ? true : false;
		$('input[class="cb-sticky_news"]').prop("checked", status);
	});

	// $('.cb-sticky_news').change(function() {
	// 	$this = $(this);

	// 	if($this.data('isSticky')){
			
	// 	}
	// });

	$('#form-list').submit(function() {

		var $archivedSticky = $('#archived_sticky');
		
		$('.cb-sticky_news').each(function(i, element) {
			
			if(this.checked == false && this.dataset.isSticky == "1"){
				$archivedSticky[0].value += ($archivedSticky.val())
					? "," + this.value
					: this.value;
			}
		})
		
		if ($('.cb-sticky_news:checked').length < 1 && !$archivedSticky.val()) {
			alert(errorMessages['DELETE_CHECKBOX_EMPTY']);
			return false;
		}
	});

	/**
	 * Check file extension if valid
	 *
	 * @author Sally Jean Sumeguin <sally_sumeguin@commude.ph>
		* @since 2018-06-01
	 *
	 * @param string file
	 * @return boolean
	 **/
	function isPdf(file) {
		var fileExtension = (file).replace(/^.*\./, '');
		if (fileExtension == 'pdf') {
			return true;
		}		
	}
})
