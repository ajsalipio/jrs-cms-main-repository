// scroll effect on anchor tag to id
function scroll() {
	$('a[href^="#"]').click(function(){
		var href= $(this).attr("href");
		var target = $(href == "#" || href == "" ? 'html' : href);
		var position = target.offset().top;
		$('html, body').stop().animate({ scrollTop: position }, 500, 'swing');
		return false;
	});
}

function toggleInnerArrow() {
	$('.arrow__navigator').on('click' , function () {
		$(this).closest('#inner__sidebar').toggleClass('js-inner__sidebar');
	});
}

function toggleRadioButton() {
	$('.custom__radiobutton .container').on( 'click' , function () {

		$('.custom__radiobutton .container').each( function () {
			$(this).removeClass('active');
		});

		$(this).addClass('active');
	});
}

function visibleError() {
	$('.login__submit, .forgotpw__submit').on('click', function () {
		$('.txt__error').css('visibility', 'visible');
	});
}

function toggleSubMenu() {
	$('.js--navmenu').on('click', function (){
		$(this).toggleClass('js--navmenuactive');
		$(this).next().slideToggle();
	});

	// TRANSPO SUB MENU
	if($('.transpo__create').length) {
		$('.transpo__container').addClass('js--navmenuactive');
		$('.transpo__container').next().slideToggle();
		$('.transpo__container').next().find('li:first-child').addClass('active');
	}else if($('.transpo__list').length) {
		$('.transpo__container').addClass('js--navmenuactive');
		$('.transpo__container').next().slideToggle();
		$('.transpo__container').next().find('li:nth-of-type(2)').addClass('active');
	}else if($('.transpo__mail').length) {
		$('.transpo__container').addClass('js--navmenuactive');
		$('.transpo__container').next().slideToggle();
		$('.transpo__container').next().find('li:nth-of-type(3)').addClass('active');
	}else if($('.transpo__csv').length) {
		$('.transpo__container').addClass('js--navmenuactive');
		$('.transpo__container').next().slideToggle();
		$('.transpo__container').next().find('li:last-child').addClass('active');
	}

	// INFO SUB MENU
	if($('.info__create').length) {
		$('.info__container').addClass('js--navmenuactive');
		$('.info__container').next().slideToggle();
		$('.info__container').next().find('li:first-child').addClass('active');
	}else if($('.info__list').length) {
		$('.info__container').addClass('js--navmenuactive');
		$('.info__container').next().slideToggle();
		$('.info__container').next().find('li:nth-of-type(2)').addClass('active');
	}

	// VOICE SUB MENU
	if($('.voice__create').length) {
		$('.voice__container').addClass('js--navmenuactive');
		$('.voice__container').next().slideToggle();
		$('.voice__container').next().find('li:first-child').addClass('active');
	}else if($('.voice__user').length) {
		$('.voice__container').addClass('js--navmenuactive');
		$('.voice__container').next().slideToggle();
		$('.voice__container').next().find('li:nth-of-type(2)').addClass('active');
	}

	// USER SUB MENU
	if ($('.user__create').length) {
		$('.user__container').addClass('js--navmenuactive');
		$('.user__container').next().slideToggle();
		$('.user__container').next().find('li:first-child').addClass('active');
	} else if ($('.user__list').length) {
		$('.user__container').addClass('js--navmenuactive');
		$('.user__container').next().slideToggle();
		$('.user__container').next().find('li:nth-of-type(2)').addClass('active');
	}

}

function datePicker() {
	$.datetimepicker.setLocale('en');
	jQuery('.datetimepicker').datetimepicker();
}

function sideBarToggle() {
	$('.sidebar__action').on('click', function () {
		$('#global__sidebar').toggleClass('js--sidebar');
	});


	$('.toggle_rsidebar').on('click', function() {
			$('.content__inner').toggleClass('js--rsidebarclosed');
	})

}

function actionMenu() {
	// MOVE TO MAIL.JS
	// $('.js--actionpopup').on('click', function () {
	// 	$('.popup__container').slideToggle(); 
	// });
}

function uploadBtnColorChange() {
	if ($('input[type="file"]').length) {

		$('input[type="file"]').change(function() {
			var fileName = $(this).val();
			if (fileName) {
				$(this).closest('.custom__upload').find('.btn__file').addClass('filled')
			} else {
				$(this).closest('.custom__upload').find('.btn__file').removeClass('filled')
			}
		})
	}
}

function checkPreviewState()
{
	var $previewFlag = $('#preview_flag');
	
	if ($previewFlag.length < 1){
		return;	
	}

	$('.js-btn-preview').click(function() {
		var isPublic = parseInt($('.js-dd-post_status')[0].value) > 0;

		$previewFlag.val(1);
		
		if (isPublic){
			$('.js-btn-save').first().removeClass('hidden');
		}
	});
	
	
	$('.js-dd-post_status').change(function() {
		// add display hide button on change
		var isPublic = parseInt($('.js-dd-post_status')[0].value) > 0;
		
		if (isPublic && !$previewFlag.val()){
			$('.js-btn-save').first().addClass('hidden');
		}else{
			$('.js-btn-save').first().removeClass('hidden');
		}

	});
	
	// Form submit handler for pages with preview
	 // Check for previw flag befure succes
	$('form').not('#logout-form').first().submit(function() {
		
		var isPublic = $('.js-dd-post_status')[0].value !== "0";

		if(!$previewFlag.val() && isPublic){
			// Let the hide unhide take care of this
			// alert("If the status is public, click the preview first")
			// return false;
		}
	});
	
	$('.js-btn-save, .js-btn-save_scheduled').click(function() {
		var isPublic = parseInt($('.js-dd-post_status')[0].value) > 0;
		
		$form = $('form').not('#logout-form').first();
		 
		if ($form[0].checkValidity()){
			if (isPublic) {
				displayModalDialog();
				return false;
			}
		}
		
	});

	// Modal confirm global button - just submit it's form
	$('#btn-modal-confirm').click(function() {
		
		$('form').not('#logout-form').first().submit();
	});

}

function displayModalDialog()
{
	$('#modalDialog').parent()[0].style.display = 'flex';
}

function modalDialog() {
	$('.close_modal').on('click', function() {
		// $('.modal_dialog').css({'display' : 'none'});
		$('.modal_dialog').fadeOut();
	})
}

// Hack to trigger web html native ui validation
function triggerFormValidation(form) {
	// Create the temporary button, click and remove it
	const tmpSubmit = document.createElement('button')
	form.appendChild(tmpSubmit)
	tmpSubmit.click()
	form.removeChild(tmpSubmit)
}


function initializeGlobalVariables()
{
	if ($('#messages_validations').length) {
		errorMessages = JSON.parse($('#messages_validations').val());
	}
}

$(function(){
	uploadBtnColorChange();
	datePicker();
	var dir = './'
	toggleInnerArrow();
	visibleError();

	setTimeout(function () {
		toggleSubMenu();
		sideBarToggle();
		actionMenu();
	} , 500);

	checkPreviewState();
	modalDialog();
	initializeGlobalVariables();

});


//2018-06-09: greg 
//display correct orientation for picture according EXIF 
 function exifTest(file, imgID, container) {
	var angle = 0;
	EXIF.getData(file, function() {
		orientation = file.exifdata.Orientation;

		if(orientation === undefined) {
			// if it returns undefined set 1(default)
			orientation = 1;
		}

		switch(orientation){
			case 1: default:
				angle =0;
				$('#'+container).css('height','inherit');
			break;
			case 8:  angle =-90;break;
			case 3:  angle =180;break;
			case 6:  angle =90;break;
		}
	
		if(angle){
			elemntWidth =  $('#'+container).css('width');
			elemntHeight =  $('#'+container).css('height');

			$('#'+container).css('height',elemntWidth);
			$('#'+imgID).css('transform-origin', 'top left');//translateY(-546px)
			transformation = 'rotate('+angle+'deg) translateY(-'+elemntHeight+')';

			$('#'+imgID).css('transform',transformation);
		}
			$('#'+container).show();
	});
}

function uploadPreview(source, target){
	var fileList = source.files ;
	//console.log(source);
	//console.log(fileList);
	$('#'+target).hide();
	for( var i=0,l=fileList.length; l>i; i++ ) {
		file = fileList[i];
		// Blob URLの作成
		var blobUrl = window.URL.createObjectURL(file) ;
		exifTest(file,target+'Img',target);
		// HTMLに書き出し (src属性にblob URLを指定)
		document.getElementById( target).innerHTML = '<img src="' + blobUrl + '" id="'+target+'Img">' ;
	}
}


function uploadMultiplePreview(fileList, target) {

	for (var i = 0, l = fileList.length; l > i; i++) {
		file = fileList[i];
		// Blob URLの作成
		var blobUrl = window.URL.createObjectURL(file);
		exifTest(file, target + 'Img', target);
		// HTMLに書き出し (src属性にblob URLを指定)

		var img = new Image();
		img.onload = function () {
			var width = this.width;
            var height = this.height;
			
			// $(target).hide();
			target.appendChild(img); // Add to div to attach hierarchy
			
            if (width >= height) {
				$(this).parent()[0].classList.add('pic__preview--horizontal');
				$(this).parent()[0].classList.remove('pic__preview--vertical');
            } else if (height >= width) {
				$(this).parent()[0].classList.add('pic__preview--vertical');
				$(this).parent()[0].classList.remove('pic__preview--horizontal');
			}

			// $(target).show();
		};

		img.src = blobUrl;
	}
}