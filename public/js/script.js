//Imported from Promon
//2018-05-22: Greg maybe not required for current project
//
//CSV upload script
 /**
 *
 * Added by Greg 2018-03-02
 * Used in admin screen for the CSV importation
 *
 */

function checkForCSVImport(){
	$('#csvImport').change(function(){
			
			var form_data = new FormData();
			var file_data =  $(this).prop('files')[0];
			var token = $('#CSV_import_form input[type= hidden]').val();
			var csvFields = $('#custom_csv_total input[type= hidden]').val();
			form_data.append(this.name, file_data);
			form_data.append('_token', token);

		   console.log('try to upload csv');
			$.ajax({
				url: $('#CSV_import_form').attr("action" ),
				dataType: "json",
				cache: false,
				contentType: false, // "multipart/form-data",
				data: form_data,// 'formData',//new FormData($('#uploadmedia')),// this.value,// new FormData($('#uploadmedia')),
				processData: false,
				type: 'post',				
				success: function (data, status)
				{
					console.log('success');
					console.log(data);
					console.log(status);
					var csvResult = '<p>インポート結果</p>'+data.match;
					$('#csvResult').html(csvResult);
				//	var preview = '<div style="width:120px"><img src="'+data.url+'" alt="'+data.name+'" style="width:100%"></div>';
				//	$("#check_"+data.input).html(preview);
					//$("#shadow_"+this.name).val(data.path);
				},
				error: function (xhr, desc, err)
				{
					console.log('error');
					console.log(xhr);
					console.log(desc);
					console.log(err);
					$('#csvResult').html(xhr.responseText);
				}
			}); 
		});
}


$(function(){
	//2018-05-22: tempory allow share the script for debuging purpose
	//Call shoulod be done in appropriate ection
	 checkForCSVImport();
}); // ready

