function checkAllItems(event) {
    var cbArchivedItems = document.getElementsByName('archived_items[]');
    for (var i = 0; i < cbArchivedItems.length; i++) {
        cbArchivedItems[i].checked = event.currentTarget.checked;
    }
}

function deleteUsersEvent() {
    var $cbCheckAll = $('#cb-check_all');
    if ($cbCheckAll.length > 0) {
        $cbCheckAll.change(checkAllItems);
    }
    $('#btn-delete').click(function () {
        if ($('.list_chk:checked').length <= 0) {
            alert(errorMessages['DELETE_CHECKBOX_EMPTY']);
            return false;
        }
    });
}

function isPasswordConfirmed()
{
    var passed = ($('#txt-password').val() == $('#txt-password_confirmation').val())
               ? true
               : false;
    
    if(!passed){
        $('#txt-password_confirmation').focus()
    }
    return passed;
}

function changePasswordEvent()
{
    if (!$('#txt-password_confirmation').length){
        return;
    }
    
    var delay = (function () {
        var timer = 0;
        return function (callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    })();

    
    $('#txt-password_confirmation').on('keyup', function () {
        var $this = $(this);

        delay(function () {
            if (isPasswordConfirmed()) {
                $this.next('.note--red').html('');
            } else{
                $this.next('.note--red').html(errorMessages['PASSWORD_CONFIRM_INVALID']);
            }
        }, 500);
        
    });

    $('#form-save').submit(function() {

        if (!isPasswordConfirmed()){
            return false;
        }
    });
}
// Main loader
$(function () {

    deleteUsersEvent();
    changePasswordEvent();

});
