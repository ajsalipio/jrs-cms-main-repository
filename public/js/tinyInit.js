//TinyMCE setting
// Imported from promon
/**
 *
 * Added by Greg 2018-03-16
 * for Tiny MCE code edition integration
 *
 *2018-05-24: Greg Update
 * Improved detection, starts the resize when the modal is matching the source code modal.
 *
 */
 function integrateTinyMCE()
  {
	  //Remove tinyMCE event lister for on resize: conflicting with our current resize
	  $(window).off( "resize");
	  var codeInputID ='' ;
	  var codeModalTMCEClasses = '.mce-textbox.mce-multiline.mce-abs-layout-item.mce-first.mce-last';
	  var codeWindow =   null;
	  //
	  // event observer
	  // resize and adjust position of the tiny mce popup
	  //	  
	  //mutation imply added codeModalTMCEs to the dom.
	  var observer = new MutationObserver(function(mutations) {

		if(typeof mutations[1] !== 'undefined'){
			var codeWindow = mutations[1].previousSibling.id;
			//console.log(mutations[1]);
		} else {
			var codeWindow =   null;
		}

		var codeModalTMCE = $('#'+codeWindow);
		var codeModalTMCEBody = $('#'+codeWindow+' .mce-container-body.mce-abs-layout');
		var codeModalTMCEFooter = $('#'+codeWindow+' .mce-container.mce-panel.mce-foot');
		//Assume this is the modal for  source code.
		var codeInputWrapper = $('#'+codeWindow+' .mce-container.mce-form.mce-abs-layout-item.mce-first.mce-last');
		var codeInput = $('#'+codeWindow+' textarea');
		codeInputID =codeInput.attr('id');

		//console.log(codeInputWrapper);
		//if(codeModalTMCEBody){
		if(codeInput.is(codeModalTMCEClasses)){
			$('#mce-modal-block').hide();
			$('.mce-window-head').hide();
			$('.mce-window').css('position','absolute');
			//console.log('edit code called');
			//main interface size and position:
			var tinyWidth = $('#visualContents_ifr').width();
			var tinyHeight = $('#visualContents_ifr').height();
			var tinyPosition =  $('.mce-container-body.mce-stack-layout').offset();
			var innerHeight = tinyHeight;// - 50;
			// remove tiny default class:
			console.log('remove class:');
			codeModalTMCE.removeClass('mce-in');		
			codeModalTMCE.removeClass('mce-window');

			//Code modal
			//relocate the code window:
			codeModalTMCE.offset(tinyPosition);
			codeModalTMCE.width(tinyWidth);
			codeModalTMCEBody.width('100%');
			codeModalTMCEFooter.width('100%');
			codeModalTMCE.height( innerHeight);
			codeModalTMCEBody.height(tinyHeight);			
			codeModalTMCE.css('border', 'none');
			
			codeInputWrapper.width('100%');
			codeInput.width('100%');

			//codeInput.css('99%');
			// HEIGHT
		
			$('.mce-reset').height(tinyHeight);
		
			// footerBodyID.height(50);
			codeInputWrapper.height(innerHeight);
			codeInput.height(innerHeight);
			$('#'+codeWindow+'-body').height( innerHeight);
			// TEXT AREA code input other spec
		    codeInput.css('top', -1);
		    codeInput.css('left', -1);

		}
		//Doesn't seems to be used
		//	$(codeModalTMCE).trigger('resize', function(){
			//	console.log('trigert windows resize!');
		//	});

		$(window).on('resize', function(){
			//resize the code windows
			codeModalTMCE = $('#'+codeWindow);
			//console.log('windows resize!');
			if(codeModalTMCE.offset()){
				//codeModalTMCE = $('#mceu_28');
				var tinyWidth = $('#visualContents_ifr').width();
				var tinyPosition =  $('.mce-container-body.mce-stack-layout').offset();
				codeModalTMCE.width(tinyWidth);
				codeModalTMCE.offset(tinyPosition);
			//	console.log('code windows resize!');
			//	console.log(codeModalTMCE.offset());
			}
		});

	});
	
	  observer.observe(document.body, {childList: true});

	//
	// //END event observer
	//
	/**
	 * 2018-05-24: Greg:
	 * Fixed a bug here: all modal from tinyMCE were triggering that function.
	 * Was able to limit to the specified classes of the "code source" window
	 *
	 */
	// automatically update the TinyMCE iframe with the content of the code view
	$(document.body).on('keyup', codeInputID, function(e) {
		//must be sure the event is launched by the code window!!!
		var evSource = $(e.target);
		//beware of any tinyMCE plugin wich may display an item with that classes!
		if( evSource.is(codeModalTMCEClasses)){
			var newHtml = evSource.val();
			if(newHtml !=''){
				$('#visualContents_ifr').contents().find('body').html(newHtml);
			}
		}
	});
	
  }
 

 
//initialize tinyMCE
function initTinyMCE()
{
	var uploadUrl = $('#formUpload').attr('action') ;
	var curUrl = window.location.href;
	var cssUrl =curUrl.match('create') ?  '../../css/front/main.css':  '../../../css/front/main.css' ;
	
	tinymce.init({
	//	selector:'textarea[name=page_content]',
		selector:'textarea',
		//URL must be set according to server url
		content_css : cssUrl,
		plugins: "code advlist autolink link image lists charmap ",

		toolbar: "undo redo | styleselect | bold italic | link image | alignleft aligncenter alignright | code | charmap |",
		menubar : false,
		branding: false,
		force_br_newlines : false,
		force_p_newlines : false,
		images_upload_url: uploadUrl,
		height: 560,
		language: 'ja',
		 relative_urls: false,
		
		 file_picker_types: 'image',
		  // we override default upload handler to simulate successful upload
		  images_upload_handler: function (blobInfo, success, failure) {
			var xhr, formData;
			xhr = new XMLHttpRequest();
			xhr.withCredentials = false;
			xhr.open('POST', uploadUrl);

			xhr.onload = function() {
			  var json;

			  if (xhr.status != 200) {
				failure('HTTP Error: ' + xhr.status);
				return;
			  }

			  json = JSON.parse(xhr.responseText);

			  if (!json || typeof json.location != 'string') {
				failure('Invalid JSON: ' + xhr.responseText);
				return;
			  }
			  		
			  success(json.location);
			};
			formData = new FormData();
			if( typeof(blobInfo.blob().name) !== undefined )
				fileName = blobInfo.blob().name;
			else
				fileName = blobInfo.filename();
			
			if (typeof (blobInfo.blob().name) !== undefined){
				// Start image pre process
				var file = blobInfo.blob();

				var img = document.createElement("img");
				var reader = new FileReader();
			
				img.onload = function () {
				
					var canvas = document.createElement('canvas');
					var ctx = canvas.getContext("2d");
					ctx.drawImage(img, 0, 0);

					var MAX_WIDTH = 1280;
					var MAX_HEIGHT = 720;
					var width = img.width;
					var height = img.height;

					if (width > height) {
						if (width > MAX_WIDTH) {
							height *= MAX_WIDTH / width;
							width = MAX_WIDTH;
						}
					} else {
						if (height > MAX_HEIGHT) {
							width *= MAX_HEIGHT / height;
							height = MAX_HEIGHT;
						}
					}
					canvas.width = width;
					canvas.height = height;
					var ctx = canvas.getContext("2d");
					ctx.drawImage(img, 0, 0, width, height);
					
					var dataurl = canvas.toDataURL("image/png");
					var imgBlob = dataURItoBlob(dataurl);

					formData.append('image', imgBlob, fileName);
					var laravelToken = $("input[name=_token]").val();
					formData.append('_token', laravelToken);
					xhr.send(formData);
				}
				
				reader.onload = function (e) { img.src = e.target.result }
				reader.readAsDataURL(file);
			}else{
				formData.append('image', blobInfo.blob(), fileName);
				var laravelToken = $("input[name=_token]").val();
				formData.append('_token', laravelToken);
				xhr.send(formData);
			}
			
		  }
  /*
  init_instance_callback: function (ed) {
    ed.execCommand('mceImage');
  }*/
		/*
        file_browser_callback: function(field_name, url, type, win) {
            // trigger file upload form
            if (type == 'image'){
				$('#formUpload input').click();
				console.log('upload');
			}
        }*/
	});
}

function dataURItoBlob(dataURI) {
	// convert base64/URLEncoded data component to raw binary data held in a string
	var byteString;
	if (dataURI.split(',')[0].indexOf('base64') >= 0)
		byteString = atob(dataURI.split(',')[1]);
	else
		byteString = unescape(dataURI.split(',')[1]);

	// separate out the mime component
	var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

	// write the bytes of the string to a typed array
	var ia = new Uint8Array(byteString.length);
	for (var i = 0; i < byteString.length; i++) {
		ia[i] = byteString.charCodeAt(i);
	}

	return new Blob([ia], { type: mimeString });
}

	initTinyMCE();
	integrateTinyMCE();