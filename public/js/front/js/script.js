
function device() {
	if ( _ua.Mobile ) {
		console.log('mobile');
	} else if ( _ua.Tablet ) {
		console.log('tablet');
		document.getElementById('viewport').setAttribute('content','width=1200');
	} else {
		console.log('others');
	}
}


function scroll() {
    $('a[href^="#"]').click(function(){
        var href= $(this).attr("href");
        var target = $(href == "#" || href == "" ? 'html' : href);
        var position = target.offset().top;
        $('html, body').stop().animate({ scrollTop: position }, 500, 'swing');
        return false;
    });
}

function showSubnavOnMobile() {
	if ($('.sp').css('display') == 'block') {
		$('nav li a').on('click', function(e) {
			e.preventDefault();

			if ($(this).attr('data-target')) {
				var target = '#' + $(this).attr('data-target');
					$(target).slideToggle('slow');

			} else {
				var target = $(this).attr('href');
				window.location.href = target;
			}

			if (!($(this).closest('li').hasClass('subnav_container'))) {
				if ($('.main_nav').css('display') == 'block') {
					if ($('body').hasClass('lock')) {
						$('body').removeClass('lock');
						$(this).closest('li').css({'z-index' : '1'})
					} else {
						$('body').addClass('lock');
						$(this).closest('li').css({'z-index' : '4'})
					}
				} 
			}
		})
	}
}

function showSubnav() {

	if ($('.pc').css('display') == 'block') {
		$('.main_nav').addClass('pc_layout');
		$('.main_nav').removeClass('sp_layout');

		if ($('.pc_layout').is(':visible')) {
			$('.pc_layout li a').css({'transition' : 'all 0.25s ease '});
		}

		$('nav li a').on('mouseover', function(e) {
			e.preventDefault();

			if ($(this).hasClass('hoversub')) {
				if ($(this).attr('data-target')) {
					var target = '#' + $(this).attr('data-target');
					if (!($(target).hasClass('open'))) {
						$('.subnav').removeClass('open');
						$(target).addClass('open');
					} else {
						$('.subnav').removeClass('open');
					}
				}
			} else {
				if (!($(this).hasClass('subnav_item'))) {
					$('.subnav').removeClass('open');
				}

			}
		})

		$('.subnav').mouseleave(function(e) {
			e.preventDefault();
			$(this).removeClass('open');
		})
	} else {
		$('.main_nav').addClass('sp_layout');
		$('.main_nav').removeClass('pc_layout');

	}

	// show search
	$('a[data-target="searchform"]').on('click', function() {
		if ($('.searchform').css('display') == 'none') {
			$('.searchform').css({'display' : 'block'});
		} else {
			$('.searchform').css({'display' : 'none'});
		}
	})
}


function initSliders() {
	var topSwiper = new Swiper('.top__kv_swiper', {
      	navigation: {
        	nextEl: '.swiper__next',
        	prevEl: '.swiper__prev',
      	},
      	pagination: {
	        el: '.swiper-pagination',
	    },
	    autoplay: {
		    delay: 6000,
		},
		loop: true
    });
}

function checkTabChange(aTab)
{
	
	if (!aTab.classList.contains('active')){
		if (!$('#' + $(aTab).attr('data-target')).hasClass('open')) {
			$('#info-tab_changed').val(1);
		}
	}
	
}

function showPanel() {

	$('a.tab').on('click', function(e) {
		e.preventDefault();

		if ($(this).attr('data-target')) {
			
			checkTabChange(this); // 07/06/2018 Louie

			var target 		= '.panel[data-id="'+ $(this).attr('data-target') +'"]';
				ctr 		= 0;
				itemsToShow = 20;
				display 	= '';

			var targetPanel = $(this).attr('data-target');

			if ($('.pc').css('display') == 'block') {
				itemsToShow = $(this).attr('data-items');
				display 	= 'table-row';
			} else {
				itemsToShow = 6;
				display		= 'flex';
			}
			
			$(this).closest('section').find('.panel').removeClass('open');
			$('.panel').removeClass('open');
			$(this).closest('section').find('a.tab').removeClass('active');
			$(this).addClass('active');
			
			// Hide all tabs paginations first
			// $('.pagination__container').each(function() {
			// 	$(this).hide();
			// })

			if ($(this).attr('data-target') == 'top_all') {
				$(this).closest('section').find('.panel').addClass('open');

				$('.pagination__container').each(function() {
					$(this).hide();
				});

				$('.panel .js-div-current_pagination .panel__table tr').each(function() {
					ctr++;
					if (ctr > itemsToShow) {
						$(this).css({'display' : 'none'});
					} else {
						$(this).css({'display' : display});
					}
				})
			} else {
				
				$('.pagination__container').each(function () {
					$(this).show();
				});

				if ($(target).hasClass('open')) {
					$('.panel').removeClass('open');
				} else {
					$(target).addClass('open');
				}

				// Tab is changed and paginated, revert back to base pagination when clicked
				if ($('#info-tab_changed').val() == 1 && $('#info-current_page').val() >= 2) {

					// if there is a base paginaton, proceed
					if ($('#' + targetPanel +' .js-div-base_pagination').length){
						$('#' + targetPanel + ' .js-div-current_pagination').hide();
						$('#' + targetPanel + ' .js-div-base_pagination').show();

						window.history.pushState('', '', '/info'); // rewrite url										
					}

				} else {
					// Change tab normally
					
					// Display pagination for non-all tab 
					// if ($('#' + targetPanel).find('table tr').length > 0){
					// 	$('#' + targetPanel + ' .pagination__container').first().show();
					// }
					
					$(target + ' .js-div-current_pagination .panel__table tr').each(function() {
						if (ctr > itemsToShow) {
							$(this).css({'display' : 'none'});
						} else {
							$(this).css({'display' : display});
						}
						ctr++;
					})
				}
			}

		}
	})
	
}

// function responsiveShowPanel() {

// 	// show only 6 panels on sp
// 	if ($('.panel.open').length) {
// 		ctr = 0;
// 		if ($('.sp').css('display') == 'block') {

// 			$('.panel.open .panel__table tr').each(function() {
// 				ctr++;
// 				if (ctr > 6) {
// 					$(this).css({'display' : 'none'});
// 				} else {
// 					$(this).css({'display' : 'flex'});
// 				}
// 			})
// 		} else {
// 				itemsToShow = $('.info__tabs .tab.active').attr('data-items');
// 				$('.panel.open .panel__table tr').each(function() {
// 					ctr++;
// 					if (ctr > itemsToShow) {
// 						$(this).css({'display' : 'none'});
// 					} else {
// 						$(this).css({'display' : 'table-row'});
// 					}
// 				})
// 		}
// 	}

// }

// Non open pages active pag should be set to 1
function setPanelActivePage() {
	$panelPaginationContainer = $('.info__panel.panel').not('.open').find('.pagination__container');
	$panelPaginationContainer.each(function () {
		$panel = $(this)
		$panel.find('li').each(function() {
			$li = $(this);
			if($li.find('a').first().text() == "1"){
				if (!$li.find('a').first().hasClass('active')){
					$li.addClass('active');
				}
			}else {
				$li.removeClass('active');
			}
		
		});		
	})
	
	// // Make sure the 1st page is the active for non current pagination tabs
	// $('#settlement .pagination__container').find('li:contains("1")').addClass('active');
}


function responsiveShowPanel() {

	// show only 6 panels on sp
	if ($('.panel.open').length) {
		ctr = 0;
		if ($('.sp').css('display') == 'block') {

			$('.panel.open .js-div-current_pagination .panel__table tr').each(function () {
				ctr++;
				if (ctr > 6) {
					$(this).css({ 'display': 'none' });
				} else {
					$(this).css({ 'display': 'flex' });
				}
			})
		} else {
			var targetPanel = $('.info__tabs .tab.active').attr('data-target');
			var itemsToShow = $('.info__tabs .tab.active').attr('data-items');

			if (targetPanel != 'all') {
				$('.info__panel.panel').each(function () {
					$(this).removeClass('open');
				})
			}
			
			// Toggle click for default settings
			$('.info__tabs .tab.active').click();

			ctr = 0;

			$('#' + targetPanel + '.panel.open .js-div-current_pagination .panel__table tr').each(function () {
				if (ctr > itemsToShow) {
					$(this).css({ 'display': 'none' });
				} else {
					$(this).css({ 'display': 'table-row' });
				}
				ctr++;
			})
		}
	}

}


function initMatchHeight() {
	if ($('.template__btnlist').length) {
		$('.template__btnlist a').matchHeight();
	}

	if ($('.transport__container01').length) {
		$('.linkblock__item_title').matchHeight();
	}

	if ($('.vein__container02').length) {
		$('.img__item').matchHeight();
	}

	if ($('.template__btnlist .btn-primary-fill').length) {
		$('.template__btnlist .btn-primary-fill').matchHeight();
	}

	if ($('.matchheight'.length)) {
		$('.matchheight').matchHeight();
	}
}

function dropdown() {
	function setSelectedText() {
		var selected = $('.dropdown__content div[selected]');
		$('.dropbtn').html($(selected).text());
	}

	if ($('.dropbtn').length) {
		setSelectedText();
	}
	$('.dropbtn').on('click', function(e) {
		e.preventDefault();
		
		$(this).closest('.dropdown__container').find('.dropdown').toggleClass('open');
		$(this).closest('.dropdown__container').find('.dropdown__content').slideToggle();
	})
	$('.dropdown__content div').on('click', function() {
		$('.dropdown__content div').removeAttr('selected');
		$(this).attr('selected', '');
		$(this).closest('.dropdown__container').find('.dropdown').toggleClass('open');
		setSelectedText();
		$('.dropdown__content').slideToggle();

		// add code to show years only in opened panel yay!
	})
}


function accordionJS() {
	$('.accordion__header').on('click', function (){
		$(this).toggleClass('js--accordion');
		$(this).next().slideToggle();
	});
}


function enableInputs() {
	// terms agree
	$(".calculate__form :input").attr("disabled", true);
	$('.checkmark').click(function(e) {
		e.preventDefault();

		if ($('#acceptTerms').hasClass('checked')) {
			$('#acceptTerms').removeClass('checked').prop('checked', false);
			$(".calculate__form :input").attr("disabled", true);
			$(".calculate__form select").attr("disabled", true);
			$('.form__overlay').show();

		}else {
			$('#acceptTerms').addClass('checked').click().attr('checked');
			$(".calculate__form :input").attr("disabled", false);
			$(".calculate__form select").attr("disabled", false);
			$('.calculate__form :radio').closest('.form__field').find(':input').not(':radio').attr('disabled', true);
			$('.form__overlay').hide();

		}

	})

	//select-range
	$('.range__btn').each(function(){
		if ($(this).closest('.range_input__container').find(':input').attr('disabled') == 'disabled') {
			$(this).css("pointer-events", "none");
		} else {
			$(this).css("pointer-events", "auto");
		}
	})
	//select-option
	$('.option__btn').each(function(){
		if ($(this).closest('.dropdown_container').find('select').attr('disabled') == 'disabled') {
			$(this).css("pointer-events", "none");
		} else {
			$(this).css("pointer-events", "auto");
		}
	})
	

	// checkbox
	$('.form__field input:radio').on('click', function() {
		if ($(this).prop('checked', true)) {
			$(this).closest('td').find(':input').not(':radio').attr('disabled', true);
			$(this).closest('.form__field').find(':input').attr('disabled', false);
			$(this).closest('.form__field').find('select').attr('disabled', false);
			$(this).closest('.form__field').find('.option__btn, .range__btn').css("pointer-events", "auto");
		} else {
			$(this).closest('td').find(':input').not(':radio').attr('disabled', true);
			$(this).closest('.form__field').find(':input').attr('disabled', true);
			$(this).closest('.form__field').find('select').attr('disabled', true);
			$(this).closest('.form__field').find('.option__btn, .range__btn').css("pointer-events", "none");
		}
	})
}

function inputInrementDecrement() {
	$('.range__btn--up').on('click', function(e) {
		e.preventDefault();

		enabled 	= $(this).closest('.range_input__container').find(':input').attr('disabled') != 'disabled';
		if (enabled) {
			current		= $(this).closest('.range_input__container').find('input').val();
			newCurrent 	= parseFloat(current) + 1;
			$(this).closest('.range_input__container').find('input').val(newCurrent + '%');
		}
	});

	$('.range__btn--down').on('click', function(e) {
		e.preventDefault();

		enabled 	= $(this).closest('.range_input__container').find(':input').attr('disabled') != 'disabled';
		if (enabled) {
			current		= $(this).closest('.range_input__container').find('input').val();
			newCurrent 	= parseFloat(current) - 1;
			$(this).closest('.range_input__container').find('input').val(newCurrent + '%');
		}
	});


	// options
	$('.option__btn--up').on('click', function(e) {
		e.preventDefault();

		enabled 	= $(this).closest('.dropdown_container').find('select').attr('disabled') != 'disabled';
		if (enabled) {
			container = parseFloat($('.dropdown_container').index($(this).closest('.dropdown_container')));
			nextElement = $('.dropdown_container:eq('+ container + ') select > option:selected').prev('option');

		  	if (nextElement.length > 0) {
			    $('.dropdown_container:eq('+ container + ') select > option:selected').removeAttr('selected').prev('option').attr('selected', 'selected');
		  	}
		}
	});

	$('.option__btn--down').on('click', function(e) {
		e.preventDefault();

		enabled 	= $(this).closest('.dropdown_container').find('select').attr('disabled') != 'disabled';
		if (enabled) {
			container = parseFloat($('.dropdown_container').index($(this).closest('.dropdown_container')));
			nextElement = $('.dropdown_container:eq('+ container + ') select > option:selected').next('option');

		  	if (nextElement.length > 0) {
			    $('.dropdown_container:eq('+ container + ') select > option:selected').removeAttr('selected').next('option').attr('selected', 'selected');
		  	}
		}
	});
}


function accordion() {
	$('.accordion__item .accordion__panel.show').css({display: 'block'});
	$('.accordion__btn').click(function(e) {
	  	e.preventDefault();

		    var $this = $(this);
		    	$attr = $(this).attr('href');

		    if (typeof $attr !== typeof undefined && $attr !== false) {
		    	window.location.href = $attr;
		    } else {

			    if ($this.parent().parent().find('.accordion__item .accordion__panel').hasClass('show')) {
			    	$('.accordion__btn').removeClass('collapsed');
			    } else {
			    	$this.addClass('collapsed');
			    }

			    if ($this.next().hasClass('show')) {
			        $this.next().removeClass('show');
			        $this.next().slideUp(350);
			    } else {
	    		  	// if (!($this.parent().parent().find('type2'))) {
			        	$this.parent().parent().find('.accordion__item .accordion__panel').removeClass('show');
				        $this.parent().parent().find('.accordion__item .accordion__panel').slideUp(350);
				        $this.next().toggleClass('show');
				        $this.next().slideToggle(350);
				        $('.accordion__item .accordion__panel.type2').addClass('show');
	  				// }
			    }
		    	
		    }
	});
}

function accordionArrowClick() {
	$('.question__container .accordion__header span').on('click', function() {
		new_location = $(this).closest('.accordion__container').find('.btn-primary-fill').attr('href');
		window.location.href = new_location;
	});
}

function panelTableLinks() {
	txtcolor 	= '';
	bgcolor 	= '';

	// $('.panel__table a:not(.btn)').on('mouseover', function(e) {
	// 	var $this 		= $(this).closest('td');
	// 		txtcolor	= $this.css('color');
	// 		bordercolor = $this.find('.btn').css('border-color');
	// 		newcolor 	= bordercolor.replace(')', ', 0.1)');
	// 		bgcolor 	= $(this).css('background-color');

	// 	$this.find('.btn').addClass('active');
	// 	$this.css({'background-color' : newcolor});
	// 	$(this).css({'color' : bordercolor})
	// })
	// $('.panel__table a:not(.btn)').on('mouseleave', function(e) {
	// 	$this = $(this).closest('td');
	// 	$this.find('.btn').removeClass('active');
	// 	$this.css({'background-color' : bgcolor});
	// 	$(this).css({'color' : txtcolor})
	// })
}

function inputNumberFields() {
	$('.js-txt-num').on('keypress', function (e) {
		return e.metaKey || // cmd/ctrl
			e.which <= 0 || // arrow keys
			e.which == 8 || // delete key
			/[0-9]/.test(String.fromCharCode(e.which)); // numbers
	});
}

function printSuccess() {
	// if ($('.print_btn').length) {
	// 	$('.print_btn').on('click', function(e) {
	// 		e.preventDefault();
	// 		console.log('hey');
	// 	})
	// }
			window.print();
}

function topPageLoad() {
	if ($('.kv_play').length) {
		$('.header__content').on('load', function() {
			console.log('loadid')
			$('.preloader--full').fadeOut('slow');
			scroll();
		})
	}
}

function onYouTubeIframeAPIReady() {
	if ($('.kv_play').length) {
	    player = new YT.Player('video-placeholder', {
	        width: 600,
	        videoId: 'yjYEo9Sf0P0',
	        playerVars: {
	            color: 'white',
	            // playlist: 'taJ60kskkns,FG0fTKAqZ5g'
	        },
	        events: {
	            onReady: initialize
	        }
	    });
	}
}

function initialize(){
	if ($('.kv_play').length) {

	    // Update the controls on load
	    // updateTimerDisplay();
	    // updateProgressBar();

	    // Clear any old interval.
	    // clearInterval(time_update_interval);

	    // Start interval to update elapsed time display and
	    // the elapsed part of the progress bar every second.
	    // time_update_interval = setInterval(function () {
	        // updateTimerDisplay();
	        // updateProgressBar();
	    // }, 1000)
	}

}
function topVideo() {
	if ($('.kv_play').length) {

		$('.kv_play').on('click', function () {

		    player.playVideo();
		    $(this).fadeOut('slow')

		});
	}
}


function tblSameHeight() {
	if ($('.financial__container').length) {
		newTblHeight = $('.financial__container').outerHeight();

		$('.financial__container .en-table02').css({'height' : newTblHeight});
	}
}

$(function(){
	// device();
	panelTableLinks();
	initSliders();
	showPanel();
	scroll();
	showSubnavOnMobile();
	dropdown();
	accordionJS();
	enableInputs();
	inputInrementDecrement();
	accordion();
	inputNumberFields();
	topPageLoad();
	topVideo();
	tblSameHeight();
}); // ready




$(window).on('load', function(){
	$('.preloader--full').fadeOut('slow');
	scroll();
}); // load





$(window).on('resize', function(){
}); // resize





$(window).on('load resize', function(){
	showSubnav();
	initMatchHeight();
	responsiveShowPanel();
}); // load resize





$(window).scroll(function(){
}); // scroll





$(window).on('load scroll', function(){
}); // scroll load