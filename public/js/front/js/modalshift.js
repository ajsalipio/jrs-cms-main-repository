
var stations = {
    "0": { "0": "駅名" },
    "1": {
        "0": "駅名",
        "1012": "帯広貨物", "1021": "音別", "1025": "釧路貨物", "1120": "中斜里ＯＲＳ", "1302": "北旭川", "1310": "名寄", "1514": "小樽築港ＯＲＳ", "1525": "札幌（タ）", "1543": "滝川", "1608": "東室蘭", "1617": "苫小牧貨物", "1803": "函館貨物", "142000": "北見(鉄道)", "142001": "北見(代行)", "170800": "富良野（鉄道）", "170801": "富良野（代行）"
    }, "3": {
        "0": "駅名",
        "2007": "水沢", "2010": "六原ＯＲＳ", "2018": "盛岡（タ）"
    }, "2": {   
        "0": "駅名",
        "2034": "八戸貨物", "2043": "東青森", "2259": "弘前", "203501": "北沼"
    }, "6": {
        "0": "駅名",
        "2207": "山形ＯＲＳ", "3061": "酒田港"
    }, "5": {
        "0": "駅名",
        "2227": "横手ＯＲＳ", "2241": "秋田貨物", "2254": "大館", "2314": "羽後本荘ＯＲＳ", "224402": "向浜"
    }, "7": {
        "0": "駅名",
        "2405": "郡山（タ）", "2415": "東福島ＯＲＳ", "2475": "会津若松ＯＲＳ", "422902": "小名浜"
    }, "4": {
        "0": "駅名",
        "2424": "岩沼", "2428": "仙台（タ）", "2540": "古川ＯＲＳ", "2551": "石巻港", "243201": "仙台港", "243204": "仙台西港"
    }, "15": {
        "0": "駅名",
        "3010": "黒井", "3014": "柏崎ＯＲＳ", "3019": "南長岡", "3040": "新潟（タ）", "3052": "中条ＯＲＳ", "5744": "青海ＯＲＳ"
    }, "11": {
        "0": "駅名",
        "4105": "熊谷（タ）", "4418": "羽生ＯＲＳ", "4475": "越谷（タ）", "4674": "新座（タ）", "410619": "三ケ尻"
    }, "10": {
        "0": "駅名",
        "4113": "倉賀野"
    }, "8": {
        "0": "駅名",
        "4203": "土浦", "4213": "水戸ＯＲＳ", "4220": "日立", "436302": "神栖", "436304": "奥野谷浜"
    }, "9": {
        "0": "駅名",
        "4425": "宇都宮（タ）", "4439": "矢板ＯＲＳ"
    }, "13": {
        "0": "駅名",
        "4450": "隅田川", "4505": "東京（タ）", "4610": "八王子ＯＲＳ"
    }, "14": {
        "0": "駅名",
        "4506": "川崎貨物", "4538": "横浜羽沢", "4548": "相模貨物", "4670": "梶ケ谷（タ）", "450702": "千鳥町", "450703": "末広町", "450704": "浮島町", "453310": "横浜本牧", "453312": "本牧埠頭"
    }, "19": {
        "0": "駅名",
        "4620": "竜王"
    }, "20": {
        "0": "駅名",
        "5016": "北長野", "5106": "岡谷ＯＲＳ", "5141": "南松本"
    }, "22": {
        "0": "駅名",
        "5203": "沼津", "5209": "富士", "5213": "静岡貨物", "5229": "西浜松"
    }, "23": {
        "0": "駅名",
        "5233": "豊橋ＯＲＳ", "5404": "刈谷ＯＲＳ", "5410": "名古屋（タ）", "5605": "春日井(専用線)", "540802": "名南貨", "540806": "昭和町", "546602": "半田埠頭"
    }, "21": {
        "0": "駅名",
        "5423": "岐阜（タ）", "5608": "多治見"
    }, "24": {
        "0": "駅名",
        "5510": "四日市", "5513": "南四日市(専用線)"
    }, "18": {
        "0": "駅名",
        "5710": "南福井", "5760": "敦賀港新営業所"
    }, "17": {
        "0": "駅名",
        "5725": "金沢（タ）"
    }, "16": {
        "0": "駅名",
        "5737": "富山貨物", "5823": "高岡貨物", "5843": "速星(専用線)"
    }, "26": {
        "0": "駅名",
        "6011": "京都貨物", "6505": "福知山ＯＲＳ"
    }, "27": {
        "0": "駅名",
        "6017": "吹田（タ）", "6020": "大阪（タ）", "6131": "安治川口", "6215": "百済（タ）"
    }, "28": {
        "0": "駅名",
        "6032": "神戸（タ）", "6049": "姫路貨物"
    }, "30": {
        "0": "駅名",
        "6444": "和歌山ＯＲＳ"
    }, "31": {
        "0": "駅名",
        "6609": "湖山ＯＲＳ", "6616": "伯耆大山(専用線)"
    }, "32": {
        "0": "駅名",
        "6624": "東松江新営業所"
    }, "33": {
        "0": "駅名",
        "6708": "岡山（タ）", "671006": "東水島"
    }, "34": {
        "0": "駅名",
        "6713": "東福山", "6717": "糸崎ＯＲＳ", "8010": "広島（タ）", "8016": "大竹"
    }, "37": {
        "0": "駅名",
        "7001": "高松（タ）"
    }, "38": {
        "0": "駅名",
        "7011": "伊予三島(専用線)", "7012": "新居浜", "7019": "松山"
    }, "39": {
        "0": "駅名",
        "7045": "高知ＯＲＳ"
    }, "36": {
        "0": "駅名",
        "7080": "徳島ＯＲＳ"
    }, "35": {
        "0": "駅名",
        "8020": "岩国(専用線)", "8030": "新南陽", "8032": "防府貨物ＯＲＳ", "8041": "宇部", "8051": "下関"
    }, "40": {
        "0": "駅名",
        "9005": "北九州（タ）", "9012": "黒崎(専用線)", "9019": "福岡（タ）", "9508": "大牟田ＯＲＳ"
    }, "41": {
        "0": "駅名",
        "9032": "鳥栖タ", "9103": "鍋島", "9144": "有田ＯＲＳ"
    }, "42": {
        "0": "駅名",
        "9118": "長崎ＯＲＳ"
    }, "44": {
        "0": "駅名",
        "9410": "西大分"
    }, "46": {
        "0": "駅名",
        "9420": "延岡", "9423": "南延岡(専用線)", "9650": "佐土原ＯＲＳ", "9654": "都城ＯＲＳ"
    }, "43": {
        "0": "駅名",
        "9516": "熊本", "9530": "八代"
    }, "45": {
        "0": "駅名",
        "9603": "川内", "9606": "鹿児島（タ）"
    }, "12": {
        "0": "駅名",
        "434101": "千葉貨物", "434109": "京葉久保田"
    }
};

function setMenuItem(value, target) {
    //alert(value);

    var select = document.getElementsByName(target)[0];
    //alert(select);
    //for(i=select.options.length-1;i>0;i-- ) {
    //select.options[i] = null;
    //}


    select.options.length = 0;
    //alert(select.options.length);
    var items = stations[value];

    var i = 0;
    for (var item in items) {
        //alert(items[item] + ":" + item);
        select.options[i] = new Option(items[item], item);

        // Make 1st option not selectable
        if(item == 0){
            select.options[i].hidden = true;
            select.options[i].disabled = true;
        }

        i++;
    }
}

function toggle2(value, target1, target2) {
    for (i = 0; i < target1.length; i++) {
        var element = document.getElementsByName(target1[i])[0];
        if (value.checked = true) {
            element.disabled = false;
        }
    }
    for (i = 0; i < target2.length; i++) {
        var element = document.getElementsByName(target2[i])[0];
        if (value.checked = true) {
            element.disabled = true;
        }
    }
}
function init() {
    toggle2(document.getElementsByName('collectionTypeArr[collection_type]')[0], ['collection_max_load', 'collection_loading_ratio'], ['collection_container_max_load']);
    toggle2(document.getElementsByName('collectionTypeArr[collection_type]')[1], ['collection_container_max_load'], ['collection_max_load', 'collection_loading_ratio']);
    toggle2(document.getElementsByName('deliveryTypeArr[delivery_type]')[0], ['delivery_max_load', 'delivery_loading_ratio'], ['delivery_container_max_load']);
    toggle2(document.getElementsByName('deliveryTypeArr[delivery_type]')[1], ['delivery_container_max_load'], ['delivery_max_load', 'delivery_loading_ratio']);
}

// Boot loader

$(function() {
    
    $('#form-calculate').submit(function () {

        // Set text labels
        $('#start_station_name').val($('#dd-start_station :selected').text());
        $('#dest_station_name').val($('#dd-dest_station :selected').text());

    });

    $('#dd-start_prefecture').change(function () {
        setMenuItem(this.value, 'start_station');
    });

    $('#dd-dest_prefecture').change(function () {
        setMenuItem(this.value, 'dest_station');
    });
    
    $('.js-rb-required').change(function() {
        var $this = $(this);
        var $maxLoad = $this.parent().find('.js-txt-num');
        var $ddloadingRatio = $this.closest('.form__field').find('select');
        
        if($ddloadingRatio.length)
        {
            $maxLoad.prop('required', true);
            $ddloadingRatio.prop('required', true);
        }
        
    });

    $('.js-dd-required').change(function() {
        var $this = $(this);
        var $subRequried = $this.closest('td').find('.js-dd-sub_required');
        
        $subRequried[0].options[0].value = ""; // Make sure default value is blank to trigger required

        if($this.val()){
            $subRequried.prop('required', true);
        }else{
            $subRequried.prop('required', false);
        }
    });

    $('.js-rb-delivery').change(function() {
       $this = $(this);
       
        $('input[name=delivery_distance]').prop('required', true);
    });

    $('#btn-restart').click(function() {
        setMenuItem("0", 'start_station');
        setMenuItem("0", 'dest_station');
    });

    $('input[name=delivery_distance]').change(function() {
        $('.js-rb-delivery').prop('required', true);
    });

});