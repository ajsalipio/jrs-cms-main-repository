//Main js:
// init all scripts

function loadValidationExtensions()
{
    $.fn.hasExtension = function (exts) {
        return (new RegExp('(' + exts.join('|').replace(/\./g, '\\.') + ')$', "i")).test($(this).val());
    }
}

$(document).ready(function () {
    /**
     * Add checked attr to class="list_chk"
     **/
    $('#check_all').change(function() {
	let status = $(this).is(":checked") ? true : false;
	$('input[class="list_chk"]').prop("checked", status);
    });

    $(document).on('click', '.a-toggle', function (e) {
        e.preventDefault();
    });

    
    loadValidationExtensions();

}); // ready

