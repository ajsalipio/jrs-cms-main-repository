
/**
 * Event: Downlaod request csv data
 *
 * @author Martin Louie Dela Serna 
 * @since  2018/05/28
 *
 * @param  button src
 * @fires  void
 * 
 * @return void
 */
function validateDownloadCsv(btnDownloadCsv) {

    var passed = true;
    var tableCount = btnDownloadCsv.dataset.tableCount;
    var emptyMessage = btnDownloadCsv.dataset.emptyMessage;

    if (tableCount < 1) {
        passed = false;
        alert(emptyMessage);
    }

    return passed;
}
 
function addMailEvent() {

    var $btnAddMail = $('#btn-add_mail');

    $('.js--actionpopup').click(function () {
        $('#txt-mail_id').val('');
        $('.popup__container').slideToggle(); 
        document.getElementById("form-delete").reset();
       
    });

    $btnAddMail.click(function() {
        var $this = $(this);
        var form = document.getElementById('form-delete');
        var isValidForm = form.checkValidity()
        
        if(!isValidForm){
            triggerFormValidation(form);
            return;
        }

        $this.prop('disabled', true); // Prevent multiple submit

        var divMessage = $('#div-message');
        var form = document.getElementById('form-delete');
        var url = $this.data('href');;
        var data = new FormData(form);

        $.ajax({
            url: url,
            data: data,
            method: 'POST',
            processData: false,
            contentType: false,
            // dataType: 'json', // Make use of view make so avoid json return
            success: function (response) {
                $('#div-tbl_content').html(response); // Update div of table results
                $('.js--actionpopup').click(); // FOR NOW, trigger collapse of modal
                $this.prop('disabled', false); // Prevent multiple submit
            },
            error: function (jqXhr, textStatus, errorThrown) {
                console.log(jqXhr);
                var errors = JSON.parse(jqXhr.responseText)['errors'];
                var errorDisplay = "";
                
                if (errors['mail_address']){
                    errorDisplay = errors['mail_address'][0];
                }

                if (errors['options']){
                    errorDisplay += (errorDisplay)
                                 ? "\n"
                                 : '';
                    errorDisplay += errors['options'][0];
                                 
                }
                
                $this.prop('disabled', false); // Prevent multiple submit
                alert(errorDisplay);
            }
        });

    });

    $('.close').click(function () {
        var divModal = document.getElementById('div-modal_mail');
        divModal.style.display = "none";
    });

    $('#div-tbl_content').on('click', '.js-edit_mail', function (){
        var $this = $(this);
        var txtMailId = document.getElementById('txt-mail_id');
        var txtMailAddress = document.getElementById('txt-mail_address');
        var ddOptions = document.getElementById('dd-options');
        
        var mail = JSON.parse(this.dataset.mail);
        
        if($(".popup__container").is(":hidden")){
            $('.js--actionpopup').click(); // FOR NOW, trigger open of modal
        }
        
        txtMailId.value = mail.mail_id;
        txtMailAddress.value = mail.mail_address;
        ddOptions.value = mail.options;

    });
    
    // For validation
    $('#btn-download_csv').click(function() {
        
        var $this = $(this);
        var form = document.getElementById('form-delete');
        // var action = $this.data('href');
        
        // form.action = action;
        // form.method = "POST";

        // if (validateDownloadCsv($this[0])) {
        //     form.submit();
        // }

    });

}

// Hack to trigger web html native ui validation
function triggerFormValidation(form){
    // Create the temporary button, click and remove it
    const tmpSubmit = document.createElement('button')
    form.appendChild(tmpSubmit)
    tmpSubmit.click()
    form.removeChild(tmpSubmit)

    return;
}

function deleteMailEvent()
{
    $('#btn-delete').click(function() {
        if ($('.list_chk:checked').length <= 0) {
            alert(errorMessages['DELETE_CHECKBOX_EMPTY']);
            return false;
        }
    });

    $('#div-tbl_content').on('click', '#cb-check_all', function () {
        let status = $(this).is(":checked") ? true : false;
        $('input[class="list_chk"]').prop("checked", status);
    });
}

function exportCsvEvent() {
    $('#btn-download_csv').click(function () {
        var tblRowCount = $('.list__template').find('tr').length;

        // 1st row is the header
        if (tblRowCount < 2) {
            alert(errorMessages['DELETE_CHECKBOX_EMPTY']);
            return false;
        }
    });
}


// Main loader
$(function () {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $('#form-delete').on('submit', function(e) {
        // 
    });

    addMailEvent();
    deleteMailEvent();
    exportCsvEvent();

});