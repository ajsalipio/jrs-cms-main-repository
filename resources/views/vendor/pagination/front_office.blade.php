@if ($paginator->hasPages())
    <div class="pagination__container">
        <div class="pagination">
            <ul>
                @if (!$paginator->onFirstPage())
                    {{-- Previous Page Link --}}
                    <li>
                        <a href="{{ $paginator->previousPageUrl() }}" class=""><</a>
                    </li>
                @endif
                @for ($i = 1; $i <= $paginator->lastPage(); $i++)
                    <li class="{{ ($paginator->currentPage() == $i) ? 'active' : '' }}">
                        <a href="{{ $paginator->url($i) }}">{{ $i }}</a>
                    </li>
                @endfor
            
                @if ($paginator->hasMorePages())
                    <li>
                        <a href="{{ $paginator->nextPageUrl() }}" class="">></a>
                    </li>
                @endif
            </ul>
        </div>
    </div> <!-- .pagination__container -->
@endif
