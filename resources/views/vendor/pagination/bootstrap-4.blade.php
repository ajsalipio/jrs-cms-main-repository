@if ($paginator->hasPages())
    <div class="pagination__container">
        <div class="column__container">
            <div class="column__item">
                Page
                <span class="current">
                    <a href="{{ $paginator->url(1) }}"> 
                        {{ $paginator->currentPage() }}
                    </a>
                </span>
                / 
                <span class="next_page">
                    <a href="{{ $paginator->url($paginator->lastPage()) }}">
                        {{ $paginator->lastPage() }} 
                    </a>
                </span>
                
            </div>
            <div class="column__item">
                {{-- Previous Page Link --}}
                 @if ($paginator->onFirstPage())
                     <a href="#" class="btn-primary-fill nav__btn a-toggle"><</a>
                @else
                     <a href="{{ $paginator->previousPageUrl() }}" class="btn-primary-fill nav__btn"><</a>
                @endif
                
                {{-- Next Page Link --}}
            
                @if ($paginator->hasMorePages())
                    <a href="{{ $paginator->nextPageUrl() }}" class="btn-primary-fill nav__btn">></a>
                @else
                    <a href="#" class="btn-primary-fill nav__btn a-toggle">></a>
                @endif
            </div>
        </div>
    </div> <!-- .pagination__container -->
@endif
