<html>
    <head>
		 <meta charset="utf-8">
        <style>
			br {
				margin-top: 2px;
			}
        </style>
    </head>
    <body>
        <div>

            ＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
			<br/>
            
			【 ご意見 】 {{ $inquiry['ご意見'] }}
			<br/>
			【 お問い合わせ内容 】 {!! nl2br($inquiry['通報内容']) !!}
			<br/>
			【 郵便番号 】 {{ $inquiry['郵便番号'] }}
			<br/>
			【 都道府県 】 {{ $inquiry['pref_id'] }}
			<br/>
			【 住所 】 {{ $inquiry['住所'] }}
			<br/>
			【 職業 】 {{ $inquiry['職業'] }}
			<br/>
			【 お名前 】 {{ $inquiry['お名前'] }}
			<br/>
			【 電話番号 】 {{ $inquiry['電話番号'] }}
			<br/>
			【 FAX番号 】 {{ $inquiry['FAX番号'] }}
			<br/>
			【 E-mail 】 {{ $inquiry['email'] }}
			
            <br/>
			＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝ 
			<br/>
			
			<!-- 送信された日時：{{ date( "Y/m/d (D) H:i:s", time() ) }}
			<br/>
			送信者のIPアドレス：119.93.177.116
			<br/>
			送信者のホスト名：119.93.177.116
			<br/>
			問い合わせのページURL：{{ url('/inquiry/complete') }} -->


        </div>
    </body>
</html>
