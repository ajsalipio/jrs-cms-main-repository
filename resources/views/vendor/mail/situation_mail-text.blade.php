 {{ \App\Utilities\JapaneseCalendarUtility::convertGreToJpn(date("Y"), 2, 1) . date("m月d日") }} 

お客様各位

日本貨物鉄道株式会社
輸送情報統括責任者 

輸送情報

({{ date("H時") }}現在）

いつもＪＲ貨物をご利用いただき、誠にありがとうございます。
現在、下記の内容により貨物列車に遅れ・その他運行の支障が生じております。
ご迷惑をおかけして誠に申し訳ございません。

１．{{ $situationTitle }}

各地区での輸送障害に伴う列車遅延について

２．発生時刻、概要 

 @if($situation['situation_content'])
    {{  str_replace("<br />", "\r\n\r\n", $situation['situation_content']) }}
@endif

 @if($situation['situation_details'])
    {{  str_replace("<br />", "\r\n\r\n", $situation['situation_details']) }}
@endif

※このメールには返信できません。 
