<html>
    <head>
		 <meta charset="utf-8">
        <style>
			br {
				margin-top: 2px;
			}
        </style>
    </head>
    <body>
        <div>

            ＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
			<br/>

			[ お名前 ] {{ $compliance['お名前'] }}<br> 
			[ お取引会社名部署名 ] {{ $compliance['お取引会社名部署名'] }}<br>
			[ 電話番号 ] {{ $compliance['電話番号'] }}<br>
			[ 電子メールアドレス ] {{ $compliance['電子メールアドレス'] }}<br>
			[ 通報内容 ] {!! nl2br($compliance['通報内容']) !!} <br>
	
			＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝ 
			<br/>
			
			<!-- 送信された日時：{{ date( "Y/m/d (D) H:i:s", time() ) }}
			<br/>
			送信者のIPアドレス：119.93.177.116
			<br/>
			送信者のホスト名：119.93.177.116
			<br/>
			問い合わせのページURL ：{{ url('/compliance/complete') }} -->

        </div>
    </body>
</html>
