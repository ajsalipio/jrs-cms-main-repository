
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- saved from url=(0045)http://www.jrfreight.co.jp/i_daiya/index.html -->
<html lang="ja">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=ISO-2022-JP">
      <meta name="description" content="7時から24時までの間に、長距離貨物列車におおむね１時間以上の遅れが発生または見込まれる場合にお知らせしています。">
      <meta http-equiv="Pragma" content="no-cache">
      <meta http-equiv="Cache-Control" content="no-cache">
      <style>
         @CHARSET "ISO-2022-JP";
         body {
         margin: 0;
         padding: 0;
         font-family: 'Lucida Grande','Hiragino Kaku Gothic Pro','ヒラギノ角ゴ Pro W3','ＭＳ Ｐゴシック', sans-serif;
         background: white;
         }
         .excel a{
         text-indent: 60px;
         text-decoration: none;
         height: 49px;
         background: url(../img/xls.png) no-repeat;
         display: block;
         }
         .word a{
         text-indent: 60px;
         text-decoration: none;
         height: 49px;
         background: url(../img/word.png) no-repeat;
         display: block;
         }
         .image a{
         text-indent: 60px;
         text-decoration: none;
         height: 49px;
         background: url(../img/img.png) no-repeat;
         display: block;
         }
         .required{
         color: #f00;   
         }
      @media only screen and (min-device-width: 601px) {
         ul {
            padding: 0;
         }
      }
      </style>
      <title>貨物列車輸送情報 | JR貨物 日本貨物鉄道株式会社</title>
   </head>
   <body>
      <center>
         <table border="0" width="85%">
            <tbody>
               <tr>
                  <td>
                     <!-- <div align="right">平成30年06月28日</div> -->
                     <div align="right">  {{ \App\Utilities\JapaneseCalendarUtility::convertGreToJpn(date("Y"), 2, 1) . date("m月d日") }}  </div>
                     <br>
                     <div align="left">お客様各位</div>
                     <br>
                     <div align="right">
                        日本貨物鉄道株式会社<br>
                        輸送情報統括責任者
                     </div>
                     <br>
                     <br>
                     <div align="center">
                        <font size="6" style="letter-spacing: 12px; "><b>輸送情報</b></font><br>
                        <br>
                        ({{ date("H時") }}現在） <br>
                        <br>
                        <br>
                        <br>
                     </div>
                     <div align="left">
                        <p>
                           いつもＪＲ貨物をご利用いただき、誠にありがとうございます。<br>
                           現在、下記の内容により貨物列車に遅れ・その他運行の支障が生じております。<br>
                           ご迷惑をおかけして誠に申し訳ございません。<br>
                        </p>
                        <br>
                        <br>
                        <p>
                           １．{{ $situationTitle }}
                           <br>
                           各地区での輸送障害に伴う列車遅延について<br>
                           <br>
                           ２．発生時刻、概要
                           <br>
                            @if($situation['situation_content'])
                                {!! nl2br($situation['situation_content']) !!}
                            @endif
                           <br>
                            @if($situation['situation_details'])
                                {!! nl2br($situation['situation_details']) !!}
                            @endif
                           <br>
                           <br>
                           <br>
                            ※このメールには返信できません。
                        </p>
                        <br>
                     </div>
                  </td>
               </tr>
            </tbody>
         </table>
      </center>
   </body>
</html>