@extends('layouts.app')
@section('content')
<style>
.thumbnail{
	width: 100px;
}
.thumbnail img{
	width: 100%;
}
</style>
<!-- <?PHP //print_r($data);?> -->
			<div class="breadcrumbs__container clearfix">
				<div class="breadcrumbs">
					<ul>
						<li>メディア一覧</li>
						<li class="active"><a href="#">一覧</a></li>
					</ul>
				</div>
	<!--			<div class="breadcrumbs__btnContainer">
					<a href="{{ url('admin/page_new') }}" class="bc__button"><span class="add"></span>新規作成</a>
				</div>-->
			</div> <!-- .breadcrumbs__container -->
			<form method="post" action="{{ url('admin/media_delete') }}">
				{{ csrf_field() }} 
				<div class="section nosidepad">
					<!--<div class="section__row">
						<div class="pages">
							<div class="thead all-pages">
								<div class="tr">
									<div class="td">
										<div class="custom__checkbox">
											<input type="checkbox" name="page_all" value="1" id="page_all">
											<label for="page_all"></label>
										</div>
										<p class="checkbox__label">一括削除</p>
									</div>
									<div class="td"><p>&nbsp;</p></div>
									<div class="td"><p>&nbsp;</p></div>
									<div class="td trash"><button><img src="{{ asset('images/icons/icon_trash.png')}}" alt=""></button></div>
								</div>
							</div>
						</div>
					</div>
				</div> -->
				<div class="section nosidepad page-list">
					<div class="section__row">
						<div class="pages">
							<div class="thead">
								<div class="tr">
									<div class="td">ページタイトル</div>
									<div class="td"></div>
									<div class="td">作成日</div>
							<!--		<div class="td">&nbsp;</div>-->
								</div>
							</div>
							<div class="tbody">
							@if(!empty($data))
								@foreach($data as $row)
								<div class="tr">
									<div class="td">
										<div class="custom__checkbox">
											<input type="checkbox" name="media_id[]" value="{{ $row->media_id}}" id="page{{ $row->media_id}}"><label for="page{{ $row->page_id}}"></label>
										</div>
													{{ $row->original}}
									</div>
									<div class="td">
										<div class="thumbnail">
											<img src="{{ url( $row->path)}}">
											
										</div>
									</div>

									<div class="td">{{date( "Y年m月d日", strtotime( $row->recorded))}}</div>

		<!--							<div class="td"><div class="pages__Btn"><a href="{{ url('/') }}/{{ $row->media_id}}" target= "_blank"><p>プレビュー</p></a></div></div>
		-->
								</div>
								@endforeach
							@endif
							</div>
						</div>
					</div>
				</div>
				<button class="btn__container">削除</button> <!-- .btn__container -->
			</form>

@stop