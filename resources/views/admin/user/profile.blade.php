@extends('layouts.dashboard')

@section('bodyClass', 'transpo user__profile')
@section('title', 'パスワードを変更する')

@section('content')
    <form id="form-save" action="{{ url('dashboard/user/profile/') . '/' . $user['id'] }}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="id" value="{{ $user['id'] }}" />

        <div class="content__inner">
            <div class="inner__container">
                <h1> パスワードを変更する </h1>
            </div>

            <div class="inner__container">
                <div class="list__template">
                     <table>
                        <tr>
                            <th>パスワード</th>
                             <td>
                                <input type="password" id="txt-password" name="password" maxlength="190" required>
                                <p class="note--red">
                                    @if ($errors->has('password'))
                                        {{ $errors->first('password') }}
                                    @endif
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <th>パスワードを認証する</th>
                             <td>
                                <input type="password" id="txt-password_confirmation" name="password_confirmation" maxlength="190" required>
                                
                                <p class="note--red">
                                    @if ($errors->has('password_confirmation'))
                                        {{ $errors->first('password_confirmation') }}
                                    @endif
                                </p>
                            </td>
                        </tr>
                    </table>
                </div>

                <input type="submit" class="btn-user__save js-btn-save" value="更新">   
          
            </div> <!-- .inner__container -->
        </div> <!-- .content__inner -->
    </form>
@stop
@section('scripts')
    <script src="{{ asset('js/user.js') }}" ></script>
@stop
