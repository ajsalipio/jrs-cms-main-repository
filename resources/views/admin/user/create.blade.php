@extends('layouts.dashboard')

@section('bodyClass', 'transpo user__create')
@section('title', 'ユーザー 作成')

@section('content')
    <form id="form-save" action="{{ url('dashboard/user/add') }}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
        @csrf
        
        <div class="content__inner">
            <div class="inner__container">
                <h1>ユーザー 作成 </h1>
            </div>

            <div class="inner__container">
                <div class="list__template">
                     <table>
                        <tr>
                            <th>メールアドレス</th>
                             <td>
                                <input type="email" id="txt-name" name="email" value="{{ old('email') }}" maxlength="190" required>
                                @if ($errors->has('email'))
                                    <p class="note--red">{{ $errors->first('email') }}</p>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>パスワード</th>
                             <td>
                                <input type="password" id="txt-password" name="password" maxlength="190" required>
                                @if ($errors->has('password'))
                                    <p class="note--red">{{ $errors->first('password') }}</p>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>グループ</th>
                             <td>
                                <div class="select__container">
                                <select id="dd-group" name="group" required value="{{ old('group') }}" class="js-dd-group">
                                        <option value=""> 選択  </option>
                                        @foreach(config('constants.USER_GROUPS' ) as $key => $value)
                                            @if(old('group'))
                                                <option value="{{ $key }}" {{ (old('group') == $key )? 'selected' : ''  }}>
                                                    {{ $value }}
                                                </option>
                                            @else
                                                <option value="{{ $key }}">
                                                    {{ $value }}
                                                </option>
                                            @endif
                                        @endforeach
                                    </select>
                                    @if ($errors->has('group'))
                                        <p class="note--red">{{ $errors->first('group') }}</p>
                                    @endif
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>

                <input type="submit" class="btn-user__save js-btn-save" value="更新">   
          
            </div> <!-- .inner__container -->
        </div> <!-- .content__inner -->
    </form>
@stop
@section('scripts')
    <script src="{{ asset('js/user.js') }}" ></script>
@stop
