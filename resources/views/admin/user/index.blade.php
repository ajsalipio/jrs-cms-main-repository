@extends('layouts.dashboard')

@section('title', 'ユーザー　一覧')
@section('bodyClass', 'transpo user__list')

@section('content')
    <div class="content__inner">
        <div class="inner__container">
            <h1>ユーザー 一覧・削除</h1>
            <form id="form-delete" action ="{{ url('/dashboard/user/delete') }}" method="POST">
                @csrf
                <div class="action__container">
                    <div class="action__column">
                        <input type="submit"  id="btn-delete" class="btn-red-invert" value="一括削除">
                    </div>
                    <div class="action__column">
                        <a href="{{url('dashboard/user/add')}}" class="btn-primary-fill">
                            新規作成
                        </a>
                    </div>
                </div> <!-- .action__container -->

                <table id="tbl-save" class="list__template">
                    <tr>
                        <th>
                            <div class="custom__checkbox">
                                <label class="container">
                                    <input type="checkbox" id="cb-check_all">
                                    <span class="checkmark"></span>
                                </label>
                            </div>	
                        </th>
                        <th>配信先メールアドレス</th>
                        <th>作成日</th>
                        <th>グループ</th>
                    </tr>
                    @foreach($users as $user)
                        <tr>
                            <td>
                                <div class="custom__checkbox">
                                    <label class="container">
                                        <input type="checkbox" id="cb-archive_items" name="archived_items[]" class="list_chk" value="{{ $user['id'] }}">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>	
                            </td>
                            <td>
                                <a href="{{ url('/dashboard/user/edit/') . '/' . $user['id'] }}">
                                    {{ $user['email'] }}
                                </a>
                            </td>
                            <td> 
                                {{  date("Y.m.d", strtotime($user['created_at'])) }} 
                            </td>
                            <td>
                                {{ config('constants.USER_GROUPS')[$user['group']] }}
                            </td>
                        </tr>
                    @endforeach
                </table> <!-- .list__template -->

                {{ $users->links() }}

            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/user.js') }}" defer></script>
@stop
