@extends('layouts.dashboard')

@section('bodyClass',$bodyClass)
@section('content')

		<div class="content__inner">

			<div class="inner__container">
				<h1>{{$headAdminTitle}}　編集</h1>
			</div>

			<form method="POST" action="{{ url('dashboard/page/update') }}/{{ $page->id }}" id="mainform">
			{{ csrf_field() }} 
			<input type="hidden" id="preview_flag" />

			<div class="inner__container">
				<div class="column__item left__column">
					<div class="toggle_rsidebar"></div>
					<div class="container__inner">
						<h3>タイトル</h3>
						<input type="text" name="page_title" value="{{ $page->page_title }}" required>
						<h3>記事</h3>
						<div class="texteditor__container">
							<textarea name="page_content" id="visualContents">{{ $page->page_content }}</textarea>
						</div>
					</div>
				</div>
					
		
				<div class="column__item right__column">
					<div class="container__inner">
						<h2>公開情報</h2>
						<h2>公開ステータス</h2>
						<div class="select__container">
							<select name="page_status"  id="page_status" class="js-dd-post_status" required>
								<option value=""> 公開ステータスを選択 </option>
								@foreach(config('constants.POST_STATUS' ) as $key => $value)
									<option value="{{ $key }}" {{ ($key == $page->page_status ? 'selected=selected' : '' )}} >
										{{ $value }}
									</option>
								@endforeach
							</select>
						</div>
						<div class="btn__container">
							<a href="{{ url('preview/page') }}/{{ $page->id }}" target="blank" class="preview btn-primary-invert js-btn-preview">プレビュー</a>
							<button class="btn-primary-fill js-btn-save">更新</button>
						</div>
					</div> <!-- .container__inner  -->
				</div>
			</div> <!-- .inner__container -->
		</div> <!-- .content__inner -->
	</form>

			<!-- tinyMCE form -->
{{--  
	-- In order to upload and insert pictures inside tinyMCE, we have to import an upload form.
	-- The upload form URL can be set with the variable 'upload_url'
	--}}
	@include('layouts.upload_form', ['upload_url' => url('dashboard/media/upload')])
		
	<script>
	/**
	 * Approach:
	 * Clicking on preview send the data to the "preview" function of the controller.
	 * The controller return the preview url (url forget with the preview id)
	 * then open a new tab/window 
	 */
		$('.preview').click(function(evt){
			evt.preventDefault();
			//console.log('preview click');
			var form_data = new FormData();
			$('#mainform *').filter(':input').each(function(){
				if(this.name){
					//console.log(this);
					//trick for tinyMCE: since the content is in an iFrame, we need to get the content manually
					if(this.name == 'page_content'){
						form_data.append(this.name,$('#visualContents_ifr').contents().find("body").html());
					} else {
						form_data.append(this.name,this.value);
					}
				}
			});

			//console.log(form_data);
			
			request = $.ajax({
				url: $('.preview').attr("href" ),
				dataType: "json",
				cache: false,
				contentType: false, // "multipart/form-data",
				data: form_data,
				processData: false,
				type: 'post',				
				success: function (data, status)
				{
					console.log('success');
					console.log(data);
					console.log(status);
					//open the preview screen:
					//window.open(data.url,'_blank');
					return data;
				},
				error: function (xhr, desc, err)
				{
				//	console.log('error');
				//	console.log(xhr);
				//	console.log(desc);
				//	console.log(err);
				}
			});
			newWindow = window.open("", "_blank");
			request.done((function(_this) {
				return function(data, textStatus, jqXHR) {
				  return newWindow.location = data.url;
				};
			  })(this));
		});
	</script>
@endsection

@section('scripts')
<script src="{{ asset('js/tinyInit.js') }}" defer></script>
@endsection
