@extends('layouts.front')
@section('mainClass', $mainClass)
@section('content')

@if( isset($page) )
{!! $page->page_content !!}
@else
No default page!
@endif

@endsection
