@extends('layouts.dashboard')

@section('bodyClass', 'transpo transpo__list')

@section('content')

    <div class="content__inner">
        <div class="inner__container">
            <h1>現在の輸送状況　一覧</h1>
            <form id="form-delete" action ="{{ url('/dashboard/situation/delete') }}" method="POST">
                @csrf
                <div class="action__container">
                    <div class="action__column">
                        <input type="submit"  id="btn-delete" class="btn-red-invert" value="一括削除">
                    </div>
                    <div class="action__column">
                        <a href="{{url('dashboard/situation/add')}}" class="btn-primary-fill">
                            新規作成
                        </a>
                    </div>
                </div> <!-- .action__container -->

                <table id="tbl-save" class="list__template">
                    <tr>
                        <th>
                            <div class="custom__checkbox">
                                <label class="container">
                                    <input type="checkbox" id="cb-check_all">
                                    <span class="checkmark"></span>
                                </label>
                            </div>	
                        </th>
                        <th>公開ステータス</th>
                        <th>作成日</th>
                        <th>標題</th>
                        <th>メール配信</th>
                    </tr>
                    @foreach($situations as $situation)
                        <tr>
                            <td>
                                <div class="custom__checkbox">
                                    <label class="container">
                                        <input type="checkbox" id="cb-archive_items" name="archived_items[]" class="list_chk" value="{{ $situation['id'] }}">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>	
                            </td>
                            <td>
                                <div class="{{ ($situation['publishing_status'] == 0) ? 'status btn-primary-invert': 'status btn-primary-fill' }}">
                                    {{ config('constants.SITUATION_POST_PUBLISH_STATUS')[$situation['publishing_status']] }}
                                </div>
                            </td>
                            <td> 
                                {{  date("Y.m.d", strtotime($situation->created_at)) }} 
                            </td>
                            <td>
                                <a href="{{ url('/dashboard/situation/edit/' . $situation['id']) }}" >
                                    {{ ($situation['situation_title']) ? $situation['situation_title'] : config('constants.SITUATION_STATUS')[$situation['situation_status']]  }}
                                </a>
                            </td>
                            <td>
                                {{ config('constants.SITUATION_MAIL_STATUS')[$situation['publishing_status']] }}
                            </td>
                        </tr>
                    @endforeach
                </table> <!-- .list__template -->

                {{ $situations->links() }}

            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/situation.js') }}" defer></script>
@stop
