@extends('layouts.dashboard')

@section('bodyClass', 'transpo transpo__create')

@section('content')

    <form id="form-save" action="{{ url('dashboard/situation/edit/' . $situation['id']) }}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
        @csrf
        <!-- <input type='hidden' name='have_time_publising'  value='0'> -->
        <input type="hidden" id="preview_flag" />
        <input type="hidden" id="file_xls_archived" name="file_xls_archived" value="0"/>
        <input type="hidden" id="file_doc_archived" name="file_doc_archived" value="0"/>
        <input type="hidden" id="file_pic_archived" name="file_pic_archived"/>

        <div class="content__inner">
            <div class="inner__container">
                <h1>現在の輸送状況　作成</h1>
            </div>

            <div class="inner__container">
                <div class="column__item left__column">
                    <div class="toggle_rsidebar"></div>
                    <div class="container__inner">
                        <h2>現在の輸送状況</h2>
                        @if($timeVar)
                            <p class="note--red">
                                {{ $timeVar }}
                            </p>
                        @endif
                        <div class="custom__radio">
                            @foreach($situationStatuses as $key => $value)
                                <label class="container">
                                    <p>{{ $value }} </p>
                                    @if(old('situation_status'))
                                        <input type="radio" class="js-rb-situation_status" name="situation_status" value="{{ $key }}"  {{ old('situation_status') == $key ? 'checked' : '' }} required>
                                    @else
                                        <input type="radio" class="js-rb-situation_status" name="situation_status" value="{{ $key }}"  {{ $situation['situation_status'] == $key ? 'checked' : '' }} required>
                                    @endif
                                    <span class="checkmark"></span>
                                </label>
                            @endforeach
                        </div>
                        @if ($errors->has('situation_status'))
                            <p class="note--red">{{ $errors->first('situation_status') }}</p>
                        @endif
                    </div> <!-- .container__inner [01] -->

                    <p class="note--light">現在の輸送状況を更新します。下記に内容を入力してください。</p>

                    <div class="container__inner">
                        <table>
                            <tr>
                                <th>標題</th>
                            </tr>
                            <tr>
                                <td>
                                    @if(old('situation_title'))
                                        <input type="text" id="txt-situation_title"
										name="situation_title" value="{{ old('situation_title') }}"
										maxlength="255" {{ old('situation_status') == 3 ? 'required' : '' }}>
                                    @else
                                        <input type="text" id="txt-situation_title"
										name="situation_title" value="{{
											$situation['situation_title'] }}" maxlength="255" {{ $situation['situation_status'] == 3 ? 'required' : '' }}>
                                    @endif
                                    @if ($errors->has('situation_title'))
                                        <p class="note--red">{{ $errors->first('situation_title') }}</p>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>概要</th>
                            </tr>
                            <tr>
                                <td>
                                    <textarea id="txt-situation_content" name="situation_content">@if(old('situation_content')){{ old('situation_content') }}@else{{ $situation['situation_content'] }}@endif</textarea>
                                </td>
                            </tr>
                            <tr>
                                <th>詳細</th>
                            </tr>
                            <tr>
                                <td>
                                    <textarea id="txt-situation_details" name="situation_details">@if(old('situation_content')){{ old('situation_details') }}@else{{ $situation['situation_details'] }}@endif</textarea>
                                </td>
                            </tr>
                            <tr>
                                <th>貨物列車遅延情報(.xlsx)</th>
                            </tr>
                            <tr>
                                <td>
                                    <p>貨物列車が大幅に遅れる場合はこちらからエクセル表をアップしてください。</p>
                                    <label for="file_xls" id="lbl-file_xls" class="lbl-file"> </label>
                                    <div class="custom__upload">
                                        <button type="button" class="btn__file {{ ($situation->fileXls) ? 'filled' : '' }}">ファイルをアップロードする</button>
                                        <input type="file" id="file_xls" name="file_xls" accept=".xlsx"/>
                                    </div>
                                     <br/>
                                     <div class="">
                                        @if( $situation->fileXls)
                                            <a href="{{ asset($situation->fileXls['path']) }}" id="link-file_xls" download="{{ $situation->fileXls['original'] }}"> 
                                                {{ $situation->fileXls['original'] }}
                                            </a>
                                            <a href="#" id="btn-remove-file_xls" style="margin-left: 10px;" class="a-toggle">
                                                Remove
                                            </a>
                                        @endif
                                        <input type="hidden" name="file_xls_existing" value="{{ json_encode($situation->fileXls) }}" />
                                    </div>
                                    @if ($errors->has('file_xls'))
                                        <p class="note--red">{{ $errors->first('file_xls') }}</p>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>列車運転計画(.xlsx)</th>
                            </tr>
                            <tr>
                                <td>
                                    <p>貨物列車がの運転計画をアップしてください。</p>
                                    <label for="file_doc" id="lbl-file_doc" class="lbl-file"> </label>
                                    <div class="custom__upload">
                                        <button type="button" class="btn__file {{ ($situation->fileDoc) ? 'filled' : '' }}">ファイルをアップロードする</button>
                                        <input type="file" id="file_doc" name="file_doc" accept=".xlsx"/>
                                    </div>
                                    <br/>
                                    <div class="">
                                        @if( $situation->fileDoc)
                                             <a href="{{ asset($situation->fileDoc['path']) }}" id="link-file_doc" download="{{ $situation->fileDoc['original'] }}"> 
                                                {{ $situation->fileDoc['original'] }}
                                            </a>
                                            <a href="#" id="btn-remove-file_doc" style="margin-left: 10px;" class="a-toggle">
                                                Remove
                                            </a>
                                        @endif
                                        <input type="hidden" name="file_doc_existing" value="{{ json_encode($situation->fileDoc) }}" />
                                    </div>
                                    @if ($errors->has('file_doc'))
                                        <p class="note--red">{{ $errors->first('file_doc') }}</p>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>画像ファイル(.jpg/.png/.jpeg)</th>
                            </tr>
                            <tr>
                                <td>                                    
                                    <div class="js-div-pic_container" id="div-pic_container">

                                        @if(!$filePicExist)
                                            <div class="js-div-pic">
                                                 <div class="custom__upload">
                                                    <button type="button" class="btn__file">ファイルをアップロードする</button>
                                                    <input type="file" id="file_pic1" name="file_pic[]"  class="js-file_pic" accept=".jpg,.png,.jpeg" />
                                    </div>
                                    <br/>
                                                <div class="pic__preview hidden"></div>
                                                <a href="#" class="js-btn-remove-file_pic a-toggle hidden btn-delete" ></a>
                                            </div>
                                        @endif

                                        @foreach($situation->filePics() as $filePic)                                           
                                            <div class="js-div-pic">
                                                 <div class="custom__upload">
                                                    <button type="button" class="btn__file {{ ($filePic) ? 'filled' : '' }}">ファイルをアップロードする</button>
                                                    <input type="file" id="file_pic1" name="file_pic[]"  class="js-file_pic" accept=".jpg,.png,.jpeg" data-id="{{ $filePic['media_id'] }}" />
                                                </div>
                                                <br/>
                                                <div class="pic__preview">
                                                   <img src="{{ asset($filePic['path']) }}" class="js-link-file_pic img-width_auto" alt="{{ $filePic['original'] }}" />
                                                </div>                                                
                                                <a href="#" class="js-btn-remove-file_pic a-toggle btn-delete" data-id="{{ $filePic['media_id'] }}"></a>
                                            </div>
                                        @endforeach
                                        <input type="hidden" name="file_pic_existing" value="{{ json_encode($situation->filePics()) }}" />
                                    </div>
                                     <button type="button" id="btn-add_image"class="btn btn-add-sm"></button>
                                    @if ($errors->has('file_pic'))
                                        <p class="note--red">{{ $errors->first('file_pic') }}</p>
                                    @endif
                                </td>
                            </tr>
                        </table>
                    </div> <!-- .container__inner [02] --> 
                </div>
                <div class="column__item right__column">
                    <div class="container__inner">
                        <h2>公開情報</h2>
                        <p class="date__header">開始時間</p>
                            <input type="text" class="datetimepicker" id="datetimepicker" name="publish_from" value="{{ (old('publish_from') ?? $situation['publish_from']) ? date('Y/m/d H:i', strtotime((old('publish_from') ?? $situation['publish_from']))) : '' }}">
                        @if ($errors->has('publish_from'))
                            <p class="note--red">{{ $errors->first('publish_from') }}</p>
                        @endif
                        <p class="date__header">終了時間</p>
                            <input type="text" class="datetimepicker" id="datetimepicker02" name="publish_end" value="{{ (old('publish_end') ?? $situation['publish_end']) ? date('Y/m/d H:i', strtotime((old('publish_end') ?? $situation['publish_end']))) : ''}}">
                         @if ($errors->has('publish_end'))
                            <p class="note--red">{{ $errors->first('publish_end') }}</p>
                        @endif
                        @if ($errors->has('have_time_publising'))
                            <p class="note--red">{{ $errors->first('have_time_publising') }}</p>
                        @endif
                        <div class="agree__container">
                            <div class="column__item">
                                <div class="custom__checkbox">
                                    <label class="container">予約投稿
                                        <input type="checkbox" id="cb-have_time_publising" name="have_time_publising" value="1" {{ (old('have_time_publising') ?? $situation['have_time_publising']) ? 'checked' : '' }}>
                                        <span class="checkmark"></span>
                                    </label>
                                </div>	
                            </div>
                            <div class="column__item">
                                <input type="submit" class="btn-primary-fill js-btn-save_scheduled" value="予約する">
                            </div>
                        </div>
                    </div> <!-- .container__inner [01] -->
                    <div class="container__inner">
                        <h2>公開ステータス</h2>
                        <div class="select__container">
                           <select id="dd-publishing_status" name="publishing_status" required class="js-dd-post_status">
                                <option value=""> 公開ステータスを選択 </option>
                                @foreach(config('constants.SITUATION_POST_STATUS' ) as $key => $value)                                   
                                        <option value="{{ $key }}" {{ ($key == (old('publishing_status') ?? $situation['publishing_status']) ? 'selected=selected' : '' )}} >
                                            {{ $value }}
                                        </option>
                                @endforeach
                            </select>
                            @if ($errors->has('publishing_status'))
                                <p class="note--red">{{ $errors->first('publishing_status') }}</p>
                            @endif
                        </div>
                        <div class="btn__container">
                            <button type="submit" id="btn-preview" formaction="{{ url('dashboard/situation/preview') }}" class="btn-primary-invert js-btn-preview" formnovalidate formtarget="_blank"/>
                                プレビュー
                            </button>
                            <button type="submit" id="btn-save" formaction="{{ url('dashboard/situation/edit/' . $situation['id']) }}" class="btn-primary-fill js-btn-save"  />
                                更新
                            </button>
                        </div>
                    </div> <!-- .container__inner [02] -->
                </div>
            </div> <!-- .inner__container -->
        </div> <!-- .content__inner -->
    </form>
@endsection

@section('scripts')
    <script src="{{ asset('js/situation.js') }}" defer></script>
@stop
