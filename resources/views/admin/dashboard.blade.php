@extends('layouts.dashboard')
@section('content')

				<div class="content__inner">
					@if(in_array('SITUATION_ROUTES', config('constants.USER_GROUP_PAGES')[Auth::user()['group']] ))
						<div class="inner__container">
							<h1>{{ $headAdminTitle}}</h1>
							<div class="container__inner">
								<h2>現在の輸送状況</h2>
								<div class="btn__status">{{config('constants.SITUATION_POST_PUBLISH_STATUS' )[ $situationInfo['publishing_status'] ] }}</div>
								<h3>{{ $situationInfo['situation_title'] }} </h3>
								<p>作成日: {{ date("Y.m.d",strtotime($situationInfo['created_at'])) }}</p>
								<h3>メール配信アドレス 登録件数</h3>
								<p>PC添付あり: {{ $totalMailsAttachementPC }}件　　PC添付なし:  {{ $totalMailsPC }}件　　携帯添付なし:  {{ $totalMailsPhone }}件</p>
								<div class="btn__container btn__container--btnThree">
									<a href="{{url('dashboard/situation/add')}}" class="btn-primary-invert">輸送状況新規作成</a>
									<a href="{{url('dashboard/mail/list')}}" class="btn-primary-invert">登録アドレス一覧・CSVエクスポート</a>
									<a href="{{url('dashboard/mail/add')}}" class="btn-primary-invert">CSVインポート</a>
								</div>
							</div> <!-- .container__inner -->
						</div> <!-- .inner__container [01] -->
					@endif

					@if(!array_diff(['NEWS_ROUTES', 'CUSTOMER_VOICES_ROUTES', 'USED_CONTAINER_ROUTES', 'PAGE_ROUTES'], 
						config('constants.USER_GROUP_PAGES')[Auth::user()['group']] ))
						<div class="inner__container">
							<div class="column__item">
								<div class="container__inner">
									<h2>最新情報</h2>
									<div class="status__container">
										<p class="desc">最新情報 登録件数</p>
										<p class="stats">{{ $totalNews }}件</p>
									</div>
									<div class="btn__container btn__container--btnTwo">
										<a href="{{url('dashboard/news/create')}}" class="btn-primary-invert">新規作成</a>
										<a href="{{url('dashboard/news/list')}}" class="btn-primary-invert">一覧・編集・削除</a>
									</div>
								</div> <!-- .container__inner -->
							</div>
							<div class="column__item">
								<div class="container__inner">
									<h2>お客様の声</h2>
									<div class="status__container">
										<p class="desc">お客様の声 登録件数</p>
										<p class="stats">{{ $totalCustomerVoice }}件</p>
									</div>
									<div class="btn__container btn__container--btnTwo">
										<a href="{{url('dashboard/customer-voices/create')}}" class="btn-primary-invert">新規作成</a>
										<a href="{{url('dashboard/customer-voices/list')}}" class="btn-primary-invert">一覧・編集・削除</a>
									</div>
								</div> <!-- .container__inner -->
							</div>
						</div> <!-- .inner__container [02] -->
					@endif

					<div class="inner__container">
						<div class="container__inner">
							<h2>システム環境</h2>
							<span>{{ $appver }}</span>
							<ul>
								<li>{{ $apache }}</li>
								<li>{{$phpver }}</li>
								<li>{{ $mysqlver }}</li>
							</ul>
						</div> <!-- .container__inner -->
					</div> <!-- .inner__container [03] -->
				</div> <!-- .content__inner -->
@endsection
