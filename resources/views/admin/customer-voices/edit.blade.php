@extends('layouts.dashboard')

@section('title', 'お客様の声　作成')
@section('bodyClass', 'voice voice__create')
@section('content')
<form action="{{ action('CustomerVoicesController@update', $data['id']) }}" method="POST" enctype="multipart/form-data">
@csrf
<input type="hidden" id="preview_flag" />

	@if($errors->any()) 
		@foreach($errors->all() as $error)
			<h3>{{ $error }}</h3>
		@endforeach
	@endif
    <div class="content__inner">
	<div class="inner__container">
	    <h1>お客様の声　作成</h1>
	</div>

	    <div class="inner__container">
		    <div class="column__item left__column">
		    	<div class="toggle_rsidebar"></div>
				<div class="container__inner">
					<input type="hidden" value="{{ $data['id'] }}" name="id">
					<h3>タイトル</h3>

					<input type="text" id="cv_title" name="cv_title"  value="{{
						old('cv_title', $data['cv_title']) }}" maxlength="45" required><br><br>
					@if($errors->any())
						@foreach($errors->get('cv_title') as $message)
							<p class="note">{{ $message }}</p>
						@endforeach
					@endif
					<h3>会社名</h3><br>
					<input type="text" id="cv_company" name="cv_company" value="{{
						old('cv_company', $data['cv_company']) }}" maxlength="45" required><br><br>
					@if($errors->any()) 
						@foreach($errors->get('cv_company') as $message)
							<p class="note">{{ $message }}</p>
						@endforeach
					@endif
					<h3>記事</h3>
					<div class="texteditor__container">
					<textarea id="visualContents" name="cv_content">{{ old('cv_content', $data['cv_content']) }}</textarea><br><br><br>
					</div>
				</div>
		    </div>

		    <div class="column__item right__column">	
				<div class="container__inner">
					<h2>公開情報</h2>
					<h2>公開ステータス</h2>
					<div class="select__container">
					<select id="post_status" name="cv_status" class="js-dd-post_status">
						@foreach(config('constants.POST_STATUS') as $key => $value)
						<option value="{{ $key }}" {{ old('cv_status', $data['cv_status']) == $key ? 'selected=selected' : '' }}>{{ $value }}</option>
						@endforeach
					</select>
					</div>
					
					<div class="btn__container">
					<button formnovalidate="formnovalidate" formtarget="_blank" id="preview" class="btn-primary-invert js-btn-preview" type="Submit" formmethod="post" formaction="{{
						url('/dashboard/customer-voices/preview') }}">プレビュー</button>
					<button id="save" class="btn-primary-fill js-btn-save" type="Submit" formaction="{{
						action('CustomerVoicesController@update', $data['id']) }}"
						formmethod="POST" onclick="$('form').attr('target','')">更新</button>
					</div> 
				</div><!-- .container__inner [01] -->
			
				<div class="container__inner">
					<h2>アイキャッチ画像</h2>
					<h2>アイキャッチ画像設定</h2>
					<div id="previewpic">
						@if($data->cv_picture)
							<img src="{{ asset($data->cv_picture  ? $data->media->path : '') }}">
						@endif
					</div>
					<div class="custom__upload">
						<button class="btn__file">ファイルをアップロードする</button>
						<input id="cv_picture" type="file" name="cv_picture" onchange="uploadPreview(this,'previewpic')">
						@if($errors->any()) 
							@foreach($errors->get('cv_picture') as $message)
								<p class="note">{{ $message }}</p>
							@endforeach
						@endif
					</div>
				</div>
		    </div>
	    </div><!-- .inner__container -->
    </div> <!-- .content__inner -->
</form>
    <!-- tinyMCE form -->
    {{--  
	-- In order to upload and insert pictures inside tinyMCE, we have to import an upload form.
	-- The upload form URL can be set with the variable 'upload_url'
	--}}
	@include('layouts.upload_form', ['upload_url' => url('dashboard/media/upload')])
@endsection
@section('scripts')
    <script src="{{ asset('js/customer_voice.js') }}" defer></script>
	<script src="{{ asset('js/tinyInit.js') }}" defer></script>
@endsection
