@extends('layouts.dashboard')
@section('title', $headAdminTitle)
@section('bodyClass', 'voice voice__user')
@section('content')
    <div class="content__inner">
	<div class="inner__container">
	    <h1>お客様の声　一覧</h1>
	    <form action="{{ url('dashboard/customer-voices/delete') }}" method="POST">
	    @csrf
		<div class="action__container">
			<div class="action__column">
			    <input id="delete" type="submit"  class="btn-red-invert"  value="一括削除">
			</div>
			<div class="action__column">
			    <input type="button" onclick="location.href='create'" class="btn-primary-fill" value="新規作成">
			</div>
		</div> <!-- .action__container -->
	    
		<table class="list__template list__template03">
		    <thead>
			<th> 
			    <div class="custom__checkbox">
				<label class="container">
				    <input type="checkbox" id="check_all">
				    <span class="checkmark"></span>
				</label>
			    </div>	
			</th>
			<th>作成日 </th>
			<th>タイトル</th>
			<th>会社名</th>
		    </thead>
		    <tbody>
			@foreach($data as $feedback)
			<tr>
			    <td>
				<div class="custom__checkbox">
				    <label class="container">
					<input type="checkbox" class="list_chk" id="feedback_check" name="id[]" value="{{
					    $feedback['id'] }}">
					<span class="checkmark"></span>
				    </label>
				</div>	 
			    </td>
			    <td>{{ $feedback['created_at']->format('Y-m-d') }}</td>
			    <td><a href="{{ url('dashboard/customer-voices/edit/' . $feedback['id'])
			    }}">{{ $feedback['cv_title'] }}</a></td>
			    <td>{{ $feedback['cv_company'] }}</td>
			</tr> 
			@endforeach 
		    </tbody>
		</table> <!-- .list__template -->
	    </form>
	    {{ $data->links() }}
	</div>
    </div> <!-- .content__inner -->
@endsection
@section('scripts')
    <script src="{{ asset('js/main.js') }}" defer></script>
    <script src="{{ asset('js/customer_voices.js') }}" defer></script>
	<script>
		$(document).ready(function() {
			$('#delete').click(function() {
				if ($('.list_chk:checked').length <= 0) {
					alert(errorMessages['DELETE_CHECKBOX_EMPTY'])
					return false;
				}	
			})		
		})
	</script>
@endsection
