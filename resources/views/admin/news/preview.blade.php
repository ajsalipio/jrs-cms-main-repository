@extends('layouts.preview')
@section('title', $headAdminTitle)
@section('mainClass', 'child info')
@section('content')
<section class="child__kv template__content info_detail__content">
	<div class="container">
		<div class="breadcrumbs__container">
			<ul>
				<li><a href="../">TOP</a></li>
				<li class="active"><a href="../">最新情報</a></li>
				<li class="active"><a href="./">Detail01</a></li>
			</ul>
		</div>
		<div class="kv__btn">
			@foreach(config('constants.NEWS_CATEGORIES') as $key => $value)
				@foreach($data['category'] as $category)
					@if ($category == $key)
						<div href="./" class="btn {{$catClasses[$key]}} active">{{ $value }}</div>
					@endif
				@endforeach
			@endforeach
		</div>
		<div class="header__title">
			@if ($data['regional_category']) 
				[ {{ $data['regional_category'] }} ] 
			@endif
			
			{{ $data['news_title'] }}
		</div>
		<div class="content__container">
			<div class="info_detail__writeup">
				<p>{!! $data['news_content'] !!}</p>
				@if (empty($data['is_link']))
					<a href="{{ asset($file['path']) }}" rel="noopener noreferrer" target="_blank">{{ $file['original_name'] }}</a>
			
				@endif
			</div>
			<a href="javascript:window.open('', '_self').close()" class="btn btn-blue">< 一覧に戻る</a>
		</div>
	</div>
</section> <!-- .info_detail__content -->
@endsection
@section('scripts')
@endsection()

