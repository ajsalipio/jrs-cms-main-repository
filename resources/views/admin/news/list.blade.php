@extends('layouts.dashboard')
@section('title', $headAdminTitle)
@section('bodyClass','info info__list')
@section('content')
<div class="content__inner">
    <div class="inner__container">	
	<form id="form-list" action="{{ url('dashboard/news/delete') }}" method="POST">
	    @csrf
		<input type="hidden" id="archived_sticky" name="archived_sticky" />
	    <h1>最新情報　一覧</h1>
	    <div class="action__container">
		    <div class="action__column">
			    <input id="delete" type="submit" class="btn-red-invert" value="一括削除">
				<button type="submit" id="btn-update_sticky" formaction="{{ url('dashboard/news/sticky/edit') }}" class="btn-primary-fill js-btn-sticky" formnovalidate/>
					スティッキーの更新
				</button>
		    </div>
		    <div class="action__column">
			    <input type="button" onclick="location.href='create'" class="btn-primary-fill" value="新規作成">
		    </div>
	    </div> <!-- .action__container -->
	    
	    <table class="list__template list__template03">
		<thead>
		    <th>
				<div class="custom__checkbox">
					<label class="container">
					<input type="checkbox" id="check_all">
					<span class="checkmark"></span>
					</label>
				</div>	
		    </th>
			<th>
				<div class="custom__checkbox">
					<label class="container">
					<input type="checkbox" id="cb-sticky_all">
					<span class="checkmark"></span>
					</label>
				</div>	
		    </th>
		    <th>作成日 </th>
		    <th>タイトル</th>
		    <th>カテゴリー</th>
		</thead>
		<tbody>
		    @foreach($data as $news)
		    <tr>
			<td>
				<div class="custom__checkbox">
					<label class="container">
					<input type="checkbox" class="list_chk" id="news_check" name="id[]" value="{{ $news['id'] }}">
					<span class="checkmark"></span>
					</label>
				</div>	
			</td>
			<td>
				<div class="custom__checkbox">
					<label class="container">
					<input type="checkbox" class="cb-sticky_news" name="sticky_items[]" value="{{ $news['id'] }}" data-is-sticky="{{ $news['is_sticky'] }}" {{ ($news->is_sticky) ? 'checked' : '' }}>
					<span class="checkmark"></span>
					</label>
				</div>	
			</td>
			<td>{{ $news['created_at']->format('Y-m-d') }}</td>
			<td><a href="{{ url('dashboard/news/edit/' . $news['id']) }}">{{ $news['news_title'] }}</a></td>
			<td>	
				{{ $news->categoryUnion }}
			</td>
		    </tr> 
		    @endforeach 
		</tbody>
	    </table> <!-- .list__template -->
	</form>
	{{ $data->links() }}
    </div>
</div
@endsection
@section('scripts')
    <script src="{{ asset('js/main.js') }}" defer></script>
	<script src="{{ asset('js/news.js') }}"></script>

	<script>
		$(document).ready(function() {
			$('#delete').click(function() {
				if ($('.list_chk:checked').length <= 0) {
					alert(errorMessages['DELETE_CHECKBOX_EMPTY']);
					return false;
				}	
			})		
		})
	</script>
@endsection
