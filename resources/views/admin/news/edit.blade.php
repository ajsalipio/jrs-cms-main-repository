@extends('layouts.dashboard')
@section('title', $headAdminTitle)
@section('bodyClass', 'info info__create')
@section('content')
<form action="{{ action('NewsController@update', $data['id']) }}" method="POST" enctype="multipart/form-data">
@csrf
<input type="hidden" id="preview_flag" />
<input type="hidden" name="id" value="{{ $data['id'] }}"/>

	<div class="content__inner">
		<div class="inner__container">
	    	<h1>ニュース編集</h1>
		</div>
	
		<div class="inner__container">
			<div class="column__item left__column">
				<div class="toggle_rsidebar"></div>
		    	<div class="container__inner">
					<h3>タイトル</h3>
					<input type="text" id="news_title" name="news_title" maxlength="255" value="{{
						old('news_title', $data['news_title']) }}">  
					@if($errors->any())
						@foreach($errors->get('news_title') as $message)
							<p class="note">{{ $message }}</p>
						@endforeach
					@endif
					<h3>記事</h3>
					<div class="texteditor__container">
			    		<textarea id="visualContents" name="news_content">{{
							old('news_content', $data['news_content']) }}</textarea><br><br><br>
					</div>
			
					<div class="custom__checkbox">
			    		<label class="container">PDFのみの場合はこちらにチェックを入れてください。
				    	<input type="checkbox"  id="is_link" value="{{ old('is_link', $data['is_link']) }}"
				    		{{ old('is_link', $data['is_link']) == 1 ? 'checked' : '' }} name="is_link">
				    	<span class="checkmark"></span>
			    		</label>
					</div>

		    <div class="custom__upload">
			<button class="btn__file">ファイルをアップロードする</button>
					<input type="file" id="file_pdf" name="file_pdf"
					accept="application/pdf"><br><br>
					@if($errors->any())
						@foreach($errors->get('file_pdf') as $message)
							<p class="note">{{ $message }}</p>
						@endforeach
					@endif
		    </div>
				<div>
					<label id="file_name">
						@if ($data->file_pdf)
							<a href="{{ asset($data->media->path) }}" download="{{ $data->media->original }}">{{ $data->file_pdf ? $data->media->original : '' }}</a>
						@endif
					</label><br>
				</div>
			<div class="timer__container" {{ $data['validity'] ? '': 'hidden'}}>
			    <h3>タイマー</h3>
			    <p>タイマーをセットしてください。<br>時間になると自動的に「開催終了」表示が出ます。</p>
			    <input type="text" class="datetime__input datetimepicker" value="{{
						old('validity', $data['validity']) }}" name="validity">
				</div>
		    </div>
		</div>
		
	    <div class="column__item right__column">	
		<div class="container__inner">
		    <h2>公開情報</h2>
		    <h2>公開ステータス</h2>
		    <div class="select__container">
			    	<select id="post_status" name="status" class="js-dd-post_status">
			    @foreach(config('constants.POST_STATUS') as $key => $value)
				    		<option value="{{ $key }}" {{ old('status', $data['status']) == $key ?
				    			'selected=selected' : '' }}>{{ $value }}</option>
						@endforeach
			    	</select>
				</div>
			
				<div class="btn__container">
					<button id="preview" type="submit" formmethod="post" formaction="{{
				    	url('/dashboard/news/preview') }}" class="btn-primary-invert js-btn-preview"
				    	formnovalidate="formnovalidate" formtarget="_blank">プレビュー</button>
					<button id="save" type="submit" formmethod="post"  formaction="{{
				    	action('NewsController@update', $data['id']) }}"class="btn-primary-fill js-btn-save">更新</button>
				
				</div>
		    </div> <!-- .container__inner [01] -->
		    
		    <div class="container__inner">
				<h2>カテゴリー</h2>
				<div class="listtemplate__container">
			    	<ul>
					@foreach(config('constants.NEWS_CATEGORIES') as $key => $value)
				    	<li> 
							@if ($key == '4')
					    		@foreach(config('constants.REGIONAL_NEWS_SUB_CAT') as $index => $cat)
					    		<ul>
									<li>
						    			<div class="custom__checkbox">
											<label class="container">{{ $cat }}
							    				<input type="checkbox" value="{{ $index }}" class="js-cb-sub_category" 
												name="regional_category[]" id="rn_{{ $index }}" {{
												in_array($index, old('regional_category', $regional_categories)) ? 'checked' :
												''}}>
							    				<span class="checkmark"></span>
											</label>
						    			</div>
									</li>
					    		</ul>
					    		@endforeach
							<br>
							@endif
							
							<div class="custom__checkbox">
								<label class="container">{{ $value }}
									<input type="checkbox" value="{{ $key }}" class="{{ $key == '3' ? 'js-cb-regional_category' : '' }}" name="category[]"  {{
										in_array($key, old('category', $categories)) ? 'checked' : '' }}>
									<span id="nc_{{ $key }}" class="checkmark {{ $key == '3' ? 'js-sp-regional_category' : '' }}"></span>
								</label>
							</div>	 
				    	</li>
					@endforeach
			    	</ul>
				</div>
		    </div>
		</div>		
	</div>
</form>	
    <!-- tinyMCE form -->
    {{--  
	-- In order to upload and insert pictures inside tinyMCE, we have to import an upload form.
	-- The upload form URL can be set with the variable 'upload_url'
	--}}
	@include('layouts.upload_form', ['upload_url' => url('dashboard/media/upload')])
@endsection

@section('scripts')
<script src="{{ asset('js/news.js') }}" defer></script>
<script src="{{ asset('js/tinyInit.js') }}" defer></script>
<script>
	$(document).ready(function() {
		$('#is_link').change(function() {
			let status = $(this).is(":checked") ? true : false;
			$(this).prop('checked', status);
			$(this).val( status ? '1' : '0');
		})
		
		$('#file_pdf').change(function() {
			let filename = $(this).val().replace(/.*(\/|\\)/, '');
			$('#file_name').text(filename);
		})
	})
</script>
@endsection
