@extends('layouts.dashboard')

@section('bodyClass', 'transpo transpo__csv')

@section('content')
    <div class="content__inner">

        <div class="inner__container">
            <h1>メールリストのCSVインポート</h1>

            <table class="list__template list__template02">
                <tr>
                    <th>配信先登録種別</th>
                    <th>件数</th>
                </tr>
                <tr>
                    <td>{{ config('constants.MAIL_STATUS' )[0] }}</td>
                    <td>{{ $withAttachmentCount }}</td>
                </tr>
                <tr>
                    <td>{{ config('constants.MAIL_STATUS' )[1] }}</td>
                    <td>{{ $noPcAttachmentCount }}</td>
                </tr>
                <tr>
                    <td>{{ config('constants.MAIL_STATUS' )[2] }}</td>
                    <td>{{ $noMobileAttachmentCount }}</td>
                </tr>
            </table> <!-- .list__template -->

            <div class="container__inner container__inner02">
                <form id="form-input" action ="{{ url('/dashboard/mail/add') }}" method="POST">
                    @csrf

                    <h2><span>一件ずつ登録する場合は下記にメールアドレスを記入の上、登録してください。</span></h2>
                    <div class="column__container">
                        <div class="column__item">
                            <input type="email" id="txt-mail_address" name="mail_address" maxlength="100" value="{{ old('mail_address') }}" required placeholder="メールアドレスを記入してください。">
                        </div>
                        <div class="column__item">
                            <div class="select__container">
                                <select id="dd-options" name="options" required>
                                    <option value="" disabled selected hidden>種別を選択</option>
                                    @foreach(config('constants.MAIL_STATUS' ) as $key => $value)
                                        <option value="{{ $key }}" {{ (old('mail_address')) ? 'selected=selected' : '' }} >
                                            {{ $value }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <input type="submit" class="btn-primary-fill" value="登録">
                        </div>

                         @if($errors->any() && !$errors->has('file_csv'))
                            <p class="note">
                                @foreach($errors->all() as $error)
                                    {{ $error }} <br/>
                                @endforeach
                            </p>
                        @endif
                    </div>
                </form>
            </div> <!-- .container__inner02 -->

            <form id="form-import" action ="{{ url('/dashboard/mail/import') }}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
                @csrf

                <div class="container__inner container__inner02">
                    <h2><span>CSVで一括登録する場合は下記のCSVインポートボタンからCSVを登録してください。</span></h2>
                    <div class="column__container">
                        <div class="file__upload">
                            <button class="btn__file btn-primary-fill btn__csv">CSVインポート</button>
                            <!-- Custom width set to trigger file validation at right position -->
                            <input type="file" id="file-csv" name="file_csv" accept=".csv" required style="width: 300px;">
                        </div>
                        @if(Session::has('result'))
                            <p class="note">
                                 {!! Session::get('result') !!}
                            </p>
                        @endif
                        @if($errors->any() && $errors->has('file_csv'))
                            <p class="note">
                                @foreach($errors->all() as $error)
                                    {!! $error !!} <br/>
                                @endforeach
                            </p>
                        @endif
                    </div>
                </div> <!-- .container__inner02 -->
    
                <input type="submit" id="btn-upload_csv" class="btn__submit btn-primary-fill" value="更新">
            </form>
        </div>
    </div> <!-- .content__inner -->
    
@endsection
