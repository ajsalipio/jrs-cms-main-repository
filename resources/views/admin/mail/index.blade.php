@extends('layouts.dashboard')

@section('bodyClass', 'transpo transpo__mail')

@section('content')

    <div class="content__inner">
        
        <form id="form-delete" action ="{{ url('/dashboard/mail/delete') }}" method="POST">
            @csrf
            <input type="hidden" id="txt-mail_id" name="mail_id" value="" />
            
            <div class="inner__container">
                <h1>メールの配信先　一覧</h1>
                <div class="action__container">
                    <div class="action__column">
                        <button type="submit" type="submit" formaction ="{{ url('/dashboard/mail/delete') }}" formnovalidate id="btn-delete" class="btn-red-invert">
                            一括削除
                        </button>
                    </div>
                    <div class="action__column">
                        <a href="javascript:void(0);" class="btn-primary-invert js--actionpopup">新規作成 </a>
                        <button type="submit" id="btn-download_csv" class="btn-primary-invert" formaction="{{ url('/dashboard/mail/download') }}" formnovalidate>
                            メールリストのCSVエクスポート
                        </button>
                        <div class="popup__container">
                            <p>配信先メールアドレス</p>
                            <input type="email" placeholder="メールアドレスを記入してください。 " id="txt-mail_address" name="mail_address" maxlength="100" required>
                            <p>配信種別</p>
                            <div class="select__container">
                                <select id="dd-options" name="options" required>
                                    <option value=""> 配信種別を選択 </option>
                                    @foreach(config('constants.MAIL_STATUS' ) as $key => $value)
                                        <option value="{{ $key }}" >
                                            {{ $value }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <button type="button" id="btn-add_mail" data-href ="{{ url('/dashboard/mail/save') }}" class="btn-primary-fill">
                                種別を選択
                            </button>
                        </div>
                    </div>
                </div> <!-- .action__container -->

                <div id="div-tbl_content">
                    @include('partials.mail_row')
                </div>
            </div>
        </form>
    </div> <!-- .content__inner -->
@endsection

@section('scripts')
    <script src="{{ asset('js/mail.js') }}" defer></script>
@stop