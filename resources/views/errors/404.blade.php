@extends(Auth::check() && str_contains(url()->getRequest()->getRequestUri(), 'dashboard') ? 'layouts.dashboard': 'layouts.front')

@section('title', 'エラーページ | JR貨物')

@if(!str_contains(url()->getRequest()->getRequestUri(), 'dashboard'))
    @section('mainClass', 'child')
@endif

@section('content')

     @if(str_contains(url()->getRequest()->getRequestUri(), 'dashboard'))
        <div class="content__inner">
            <div class="inner__container">
                <h1>このページは存在しません </h1>
            </div>
        </div>
    @else
		<section class="template__content voice__content">
			<div class="container">
				<div class="content__container">
					<div>
						<p>このページは存在しません</p>
					</div>					
				</div>
			</div>
		</section><!-- voice__content -->
    @endif
        
@endsection