<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>
	@if(!empty($headAdminTitle))
	    {{ $headAdminTitle }}
	@else
	    @yield('title')
	@endif
    </title>
   <!-- <title>{{ !empty($headAdminTitle) ?$headAdminTitle :'' }}</title>-->
<!-- FAVICON -->
	<link rel="shortcut icon" href="{{ asset('img/common/favicon.png') }}" type="image/vnd.microsoft.icon">
	<link rel="icon" href="{{ asset('img/common/favicon.png') }}" type="image/vnd.microsoft.icon">
	<link rel="stylesheet" href="{{ asset('css/normalize.css') }}">
	<link rel="stylesheet" href="{{ asset('css/jquery.datetimepicker.css') }}">
	<link rel="stylesheet" href="{{ asset('css/main-admin.css') }}">
	<!-- <link rel="stylesheet" href="{{ asset('css/style.css') }}"> -->
	
    @yield('styles')
	 <!--<script language="JavaScript"  src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.0/jquery.min.js"></script>-->
	 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
</head>
	<body class="login" id="top">
	<main>
			<section class="access__container">
				<div class="input__container">
					<div class="inner__container">
						<h1 class="logo">
							<img src="{{asset('img/common/logo_01.png')}}" alt="">
						</h1>
					<div class="login__container">
						@yield('content')
	</main>
	<footer>@include('layouts.dashboardFooter')</footer>
	<script src="{{ asset('js/vendor/modernizr-2.8.3.min.js') }}"></script>

	<script>window.jQuery || document.write('<script src="{{asset('js/vendor/jquery-3.1.1.min.js')}}"><\/script>')</script>

		<script src="{{asset('js/vendor/jquery.datetimepicker.full.min.js')}}"></script>
	
    @yield('scripts')
</body>
</html>
