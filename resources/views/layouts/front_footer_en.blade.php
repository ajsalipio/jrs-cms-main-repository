		<div class="footer__row">
			<div class="footer_column">
				<a href="{{ url('en/')}}"><b>TOP</b></a>
			</div>
			<div class="footer_column">
				<a href="{{ url('en/philosophy')}}"><b>CORPORATE DATA</b></a>
				<ul>
					<li><a href="{{ url('en/philosophy')}}">Philosophy</a></li>
					<li><a href="{{ url('en/corporate-overview')}}">Corporate Overview</a></li>
					<li><a href="{{ url('en/service')}}">Service</a></li>
					<li><a href="{{ url('en/main-data')}}">Main Data</a></li>
					<li><a href="{{ url('en/financials')}}">Financials</a></li>
					<li><a href="{{ url('en/material-procurement-info')}}">Material Procurement Information</a></li>
				</ul>
			</div>
			<div class="footer_column">
				<a href="{{ url('en/on-time')}}"><b>RAILWAY BUSINESS</b></a>
				<ul>
					<li><a href="{{ url('en/on-time')}}">On Time</a></li>
					<li><a href="{{ url('en/ecology')}}">Ecology</a></li>
					<li><a href="{{ url('en/logistics')}}">Logistics</a></li>
					<li><a href="{{ url('en/lifeline')}}">Lifeline</a></li>
					<li><a href="{{ url('en/innovation')}}">Innovation</a></li>
				</ul>
			</div>
			<div class="footer_column">
				<a href="{{ url('en/new-ventures')}}"><b>NEW VENTURES</b></a>
			</div>
		</div>

		<div class="footer__row">
			<div class="footer__row_inner">
				<div class="footer_column">
					<a href="{{ url('en')}}" class="logo__footer">
						<img src="{{ asset('front_images/images/logo_en.png') }}" alt="" class="pc">
						<img src="{{ asset('front_images/images/logo_en.png') }}" alt="" class="sp">
					</a>
				</div>
				<div class="footer_column">
					©Copyright Japan Freight Railway Company.All rights reserved.
				</div>
			</div>
		</div>

