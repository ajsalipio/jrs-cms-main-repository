	<div class="header__content">
		<h1 class="logo">
			<a href="../../">
				<img src="{{ asset('preview_images/images/logo.png') }}"  alt="JR Kamotsu" class="pc">
				<img src="{{ asset('preview_images/images/logo_sp.png') }}"  alt="JR Kamotsu" class="sp">
			</a>
		</h1>
		<nav>
			<ul class="main_nav subnav" id="main_nav">
				<li class="sp">
					<div>
						<input type="text" name="search">
					</div>
				</li>
				<li class="active">
					<a href="../../">ホーム</a>
				</li>
				<li>
					<a href="javascript:void(0)" data-target="service-info" class="hoversub">サービスご案内</a>
					<div class="subnav main_subnav" id="service-info">
						<div class="container">
							<div class="container__inner">
								<div class="subnav__inner">
									<a class="subnav_item" href="../../service">
										<div class="subnav_item__img">
											<img src="{{ asset('preview_images/images/common/img_01.jpg') }}" alt="">
										</div>
										仕組みと特徴
									</a>
									<a class="subnav_item" href="../../service/container">
										<div class="subnav_item__img">
											<img src="{{ asset('preview_images/images/common/img_02.jpg') }}" alt="">
										</div>
										コンテナのサイズ・種類
									</a>
									<a class="subnav_item" href="../../service/transport">
										<div class="subnav_item__img">
											<img src="{{ asset('preview_images/images/common/img_03.jpg') }}" alt="">
										</div>
										様々な輸送サービス
									</a>
									<a class="subnav_item" href="../../service/improvement">
										<div class="subnav_item__img">
											<img src="{{ asset('preview_images/images/common/img_04.jpg') }}"  alt="">
										</div>
										サービス向上に向けた取り組み
									</a>
									<a class="subnav_item" href="./">
										<div class="subnav_item__img">
											<img src="{{ asset('preview_images/images/common/img_temp.jpg') }}"  alt="">
										</div>
										値段の仕組み
									</a>
									<a class="subnav_item" href="./">
										<div class="subnav_item__img">
											<img src="{{ asset('preview_images/images/common/img_temp.jpg')
											}}"  alt="">
										</div>
										「お問い合わせ」から「ご利用まで」
									</a>
								</div>
							</div>
						</div>
					</div>
				</li>
				<li>
					<a href="../../modalshift">鉄道貨物の特徴</a>
				</li>
				<li>
					<a href="../../voice">ご利用者の声</a>
				</li>
				<li>
					<a href="../../question">よくあるご質問</a>
				</li>
				<li>
					<a href="../../recruit">採用情報</a>
				</li>
				<li>
					<a href="../../about"  data-target="about-info" class="hoversub">私たちについて</a>
					<div class="subnav main_subnav" id="about-info">
						<div class="container">
							<div class="container__inner">
								<div class="subnav__inner">
									<a class="subnav_item" href="../../about/">
										<div class="subnav_item__img">
											<img src="{{ asset('preview_images/images/common/img_temp.jpg')
											}}"  alt="">
										</div>
										企業情報
									</a>
									<a class="subnav_item" href="../../about/railgate">
										<div class="subnav_item__img">
											<img src="{{
												asset('preview_images/images/common/img_temp.jpg') }}"  alt="">
										</div>
										レールゲート
									</a>
									<a class="subnav_item" href="../../about/relation">
										<div class="subnav_item__img">
											<img src="{{ asset('preview_images/images/common/img_temp.jpg') }}"  alt="">
										</div>
										関連事業(一覧)
									</a>
									<a class="subnav_item" href="../../about/group">
										<div class="subnav_item__img">
											<img src="{{ asset('preview_images/images/common/img_temp.jpg') }}"  alt="">
										</div>
										JR貨物グループ
									</a>
									<a class="subnav_item" href="../../about/csr">
										<div class="subnav_item__img">
											<img src="{{ asset('preview_images/images/common/img_temp.jpg') }}"  alt="">
										</div>
										CSR報告書
									</a>
									<a class="subnav_item" href="../../about/other">
										<div class="subnav_item__img">
											<img src="{{ asset('preview_images/images/common/img_temp.jpg') }}"  alt="">
										</div>
										その他
									</a>
								</div>
							</div>
						</div>
					</div>
				</li>
			</ul>
			<div class="menu-tabs">
				<ul>
					<li class="pc">
						<a href="javascript:void(0)" data-target="searchform">
							<i></i>
							<p>SEARCH</p>
						</a>
						<form role="search" method="get" id="searchform" class="searchform subnav" action="#">
							<div class="searchboxwrapper">
								<input class="searchbox" type="text" value="" name="s" placeholder="" id="s">
								<button class="searchsubmit" type="submit" id="searchsubmit">検索</button>
							</div>
						</form>
					</li>
					<li>
						<a href="javascript:void(0)">
							<i></i>
							<p>ENGLISH</p>
						</a>
					</li>
					<li>
						<a href="../../inquiry/">
							<i></i>
							<p>お問い合わせ</p>
						</a>
					</li>
					<li class="sp">
						<a href="javascript:void(0)" data-target="main_nav">
							<div class="menu-icon">
								<div></div>
								<div></div>
								<div></div>
							</div>
						</a>
					</li>
				</ul>
			</div>
		</nav>
	</div>

