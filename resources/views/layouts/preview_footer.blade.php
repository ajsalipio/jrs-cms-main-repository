
		<div class="footer__row">
			<div class="footer_column pc">
				<a href="javascript:void(0)"><b>目指すところ</b></a>
				<ul>
					<li><a href="../../modalshift">モ－ダルシフト</a></li>
					<li><a href="../../modalshift/calculate"> エネルギー使用量・CO2排出量計算シート</a></li>
				</ul>
				<a href="../../#top__firsttime"><b>初めてご利用の方へ</b></a>
				<a href="../../question"><b>よくあるご質問</b></a>
				<a href="../../#top__video"><b>動画でご紹介</b></a>
			</div>
			<div class="footer_column pc">
				<a href="javascript:void(0)"><b>私たちについて</b></a>
				<ul>
					<li><a href="../../about">企業情報</a></li>
					<li><a href="../../about/csr/plan">事業計画</a></li>
					<li><a href="../../about/csr/financial">財務情報</a></li>
					<li><a href="../../about/relation">関連事業</a></li>
					<li><a href="../../about/group">JR貨物グループ</a></li>
					<li><a href="../../about/csr">CSR・IR情報</a></li>
					<li><a href="../../about/csr/safety">安全報告書</a></li>
					<li><a href="../../about/other/">その他</a></li>
				</ul>
			</div>
			<div class="footer_column pc">
				<a href="javascript:void(0)"><b>サービスご案内</b></a>
				<ul>
					<li><a href="../../service">仕組みと特長</a></li>
					<li><a href="../../service/price">値段の仕組み</a></li>
					<li><a href="../../service/container">コンテナのサイズ・種類</a></li>
					<li><a href="../../">様々な郵送サービス</a></li>
					<li><a href="../../">サービス向上に向けた取り組み</a></li>
				</ul>
				<a href="../../i_daiya"><b>現在の輸送状況</b></a>
				<ul>
					<li><a href="../../modalshift">モ－ダルシフト</a></li>
					<!-- <li><a href="../../modalshift/calculate/">エネルギー使用量・CO2排出量計算シート</a></li> -->
				</ul>
			</div>
			<div class="footer_column">
				<a href="../../inquiry/" class="pc"><b>お問い合わせ</b></a>
				<a href="../../compliance/"><b>外部通報窓口</b></a>
				<a href="../../individual"><b>プライバシーポリシー</b></a>
				<a href="../../sitepolicy"><b>サイトのご利用にあたって</b></a>
				<a href="../../sitemap"><b>サイトマップ</b></a>
				<a href="../../link"><b>リンク</b></a>
			</div>
		</div>
		<div class="footer__row">
			<div class="footer_column">
				<a href="../../">
					<img src="{{ asset('preview_images/images/common/img_05.jpg') }}" alt="">
				</a>
			</div>
			<div class="footer_column pc">
				<img src="{{ asset('preview_images/images/common/img_06.jpg') }}" alt="">
				<div>
					<a href="../../">JR貨物モバイル</a>
					<p>ケータイからもご利用いただけます。<br>
					ケータイで左のバーコードを<br>
					読み取ってください。</p>
				</div>
			</div>
		</div>
		<div class="footer__row">
			<div class="footer__row_inner">
				<div class="footer_column">
					<a href="../../" class="logo__footer">
						<img src="{{ asset('preview_images/images/logo_footer.png') }}" alt="" class="pc">
						<img src="{{ asset('preview_images/images/logo_footer_sp.png') }}" alt="" class="sp">
					</a>
				</div>
				<div class="footer_column">
					©Copyright Japan Freight Railway Company.All rights reserved.
				</div>
			</div>
		</div>
