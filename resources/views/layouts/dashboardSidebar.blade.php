<div class="sidebar__nav">
	<a href="javascript:void(0)" class="sidebar__action"></a>
</div>
<div class="sidebar__container">
	<ul>
		<li>
			<a href="{{url('/dashboard/')}}" class="dashboard__container">
				<figure>
					<img src="{{asset('img/common/ico_dashboard.png')}}" alt="">
				</figure>
				<p>ダッシュボード</p>
			</a>
		</li>
		@if(in_array('SITUATION_ROUTES', config('constants.USER_GROUP_PAGES')[Auth::user()['group']] ))
			<li>
				<a href="javascript:void(0)" class="transpo__container js--navmenu">
					<figure>
						<img src="{{asset('img/common/ico_transportation.png')}}" alt="">
					</figure>
					<p>現在の輸送状況</p>
				</a>
				<ul class="submenu__list">
					<li>
						<a href="{{url('dashboard/situation/add')}}">
							<p>現在の輸送状況　作成</p>
						</a>
					</li>
					<li>
						<a href="{{url('dashboard/situation/list')}}">
							<p>現在の輸送状況一覧</p>
						</a>
					</li>
					@if(in_array('MAIL_ROUTES', config('constants.USER_GROUP_PAGES')[Auth::user()['group']] ))
						<li>
							<a href="{{url('dashboard/mail/list')}}">
								<p>配信先一覧</p>
							</a>
						</li>
						<li>
							<a href="{{url('dashboard/mail/add')}}" class="two--p">
								<p>CSVエクスポート</p>
							</a>
						</li>
					@endif
				</ul>			
			</li>
		@endif
		@if(!array_diff(['NEWS_ROUTES', 'CUSTOMER_VOICES_ROUTES', 'USED_CONTAINER_ROUTES', 'PAGE_ROUTES'], 
			config('constants.USER_GROUP_PAGES')[Auth::user()['group']] ))
			<li>
				<a href="javascript:void(0)" class="info__container js--navmenu">
					<figure>
						<img src="{{asset('img/common/ico_information.png')}}" alt="">
					</figure>
					<p>最新情報</p>
				</a>
				<ul class="submenu__list">
					<li>
						<a href="{{url('dashboard/news/create')}}">
							<p>最新情報　作成</p>
						</a>
					</li>
					<li>
						<a href="{{url('dashboard/news/list')}}">
							<p>最新情報　一覧</p>
						</a>
					</li>
				</ul>
			</li>
			<li>
				<a href="javascript:void(0)" class="voice__container js--navmenu">
					<figure>
						<img src="{{asset('img/common/ico_voice.png')}}" alt="">
					</figure>
					<p>お客様の声</p>
				</a>
				<ul class="submenu__list">
					<li>
						<a href="{{url('dashboard/customer-voices/create')}}">
							<p>お客様の声　作成</p>
						</a>
					</li>
					<li>
						<a href="{{url('dashboard/customer-voices/list')}}">
							<p>お客様の声　一覧</p>
						</a>
					</li>
				</ul>
			</li>
			<li>
				<a href="{{url('dashboard/page/edit/used_container')}}" class="cont__container">
					<figure>
						<img src="{{asset('img/common/ico_container.png')}}" alt="">
					</figure>
					<p>中古コンテナ</p>
				</a>
			</li>
			<li>
				<a href="{{url('dashboard/page/edit/parking')}}" class="parking__container">
					<figure>
						<img src="{{asset('img/common/ico_parking.png')}}" alt="">
					</figure>
					<p>駐車場のご案内</p>
				</a>
			</li>
		@endif
		@if(in_array('USER_ROUTES', config('constants.USER_GROUP_PAGES')[Auth::user()['group']] ))
			<li>
				<a href="javascript:void(0)" class="user__container js--navmenu">
					<figure>
						<img src="{{asset('img/common/ico_user.png')}}" alt="">
					</figure>
					<p>ユーザー</p>
				</a>
				<ul class="submenu__list">
					<li>
						<a href="{{url('dashboard/user/add')}}">
							<p>ユーザー 作成</p>
						</a>
					</li>
					<li>
						<a href="{{url('dashboard/user/list')}}">
							<p>ユーザー 一覧・削除</p>
						</a>
					</li>
				</ul>
			</li>
		@endif
	</ul>
</div>
