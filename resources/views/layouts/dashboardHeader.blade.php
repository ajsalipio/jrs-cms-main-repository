		<div class="header__container">
			<a href="{{url('/')}}" class="logo">
				<img src="{{asset('img/common/header_logo.png')}}" alt="">
			</a>
			<span class="txt__banner">Webサイト CMS</span>
			<a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="logout__container">
				<img src="{{asset('img/common/ico_logout.png')}}" alt="">
				<span>ログアウト</span>
			</a>
			<a href="{{ url('/dashboard/user/profile') }}" class="logout__container">
				<img src="{{asset('img/common/header_ico_user.png')}}" alt="">
				<span>プロフィール</span>
			</a>
			<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
			{{ csrf_field() }}
			</form>
		</div>