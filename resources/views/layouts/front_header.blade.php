
	<div class="header__content">
			<h1 class="logo">
				<a href="{{ url('/') }}">
					<img src="{{ asset('front_images/images/logo.png') }}" alt="JR Kamotsu" class="pc">
					<img src="{{ asset('front_images/images/logo_sp.png') }}" alt="JR Kamotsu" class="sp">
				</a>
			</h1>
			<nav>
				<ul class="main_nav subnav" id="main_nav">

					<li class="active">
						<a href="./">ホーム</a>
					</li>
					<li>
						<a href="javascript:void(0)" data-target="service-info" class="hoversub">サービスご案内</a>
						<div class="subnav main_subnav" id="service-info">
							<div class="container">
								<div class="container__inner">
									<div class="subnav__inner">
										<a class="subnav_item" href="{{ url('service')}}">
											<div class="subnav_item__img">
												<img src="{{ asset('front_images/images/common/img_01.jpg') }}" alt="">
											</div>
											仕組みと特長
										</a>
										<a class="subnav_item" href="{{ url('service/price')}}" alt="">
											<div class="subnav_item__img">
												<img src="{{ asset('front_images/images/common/img_01a.jpg') }}" alt="">
											</div>
											値段の仕組み
										</a>
										<a class="subnav_item" href="{{ url('service/container')}}">
											<div class="subnav_item__img">
												<img src="{{ asset('front_images/images/common/img_02.jpg') }}" alt="">
											</div>
											コンテナのサイズ・種類
										</a>
										<a class="subnav_item" href="{{ url('service/transport')}}">
											<div class="subnav_item__img">
												<img src="{{ asset('front_images/images/common/img_03.jpg') }}" alt="">
											</div>
											様々な輸送サービス
										</a>
										<a class="subnav_item" href="{{ url('service/area')}}">
											<div class="subnav_item__img">
												<img src="{{ asset('front_images/images/common/img_03a.jpg') }}" alt="">
											</div>
											エリア別サービス案内
										</a>
										<a class="subnav_item" href="{{ url('service/improvement')}}">
											<div class="subnav_item__img">
												<img src="{{ asset('front_images/images/common/img_04.jpg') }}" alt="">
											</div>
											サービス向上に向けた取組み
										</a>
									</div>
								</div>
							</div>
						</div>
					</li>
					<li class="subnav_container">
						<a href="{{ url('modalshift')}}" data-target="railcargo" class="hoversub">鉄道貨物の特長</a>
						<div class="subnav main_subnav type2" id="railcargo">
							<div class="container">
								<div class="container__inner">
									<div class="subnav__inner">
										<a class="subnav_item" href="{{ url('modalshift')}}">
											モーダルシフト
										</a>
										<!-- <a class="subnav_item" href="{{ url('modalshift/calculate')}}">
											エネルギー使用量・CO2排出計算シート
										</a> -->
									</div>
								</div>
							</div>
						</div>
					</li>
					<li>
						<a href="{{ url('/voice')}}">お客様の声</a>
					</li>
					<li>
						<a href="{{ url('question')}}">よくあるご質問</a>
					</li>
					<li>
						<a href="{{ url('recruit')}}">採用情報</a>
					</li>
					<li class="subnav_container">
						<a href="{{ url('about')}}"  data-target="about-info" class="hoversub">私たちについて</a>
						<div class="subnav main_subnav type2" id="about-info">
							<div class="container">
								<div class="container__inner">
									<div class="subnav__inner">
										<a class="subnav_item" href="{{ url('about/')}}">
											企業情報
										</a>
										<a class="subnav_item" href="{{ url('about/railgate')}}">
											レールゲート
										</a>
										<a class="subnav_item" href="{{ url('about/relation')}}">
											関連事業(一覧)
										</a>
										<a class="subnav_item" href="{{ url('about/relation/globalwork')}}">
											海外事業
										</a>
										<a class="subnav_item" href="{{ url('about/group')}}">
											JR貨物グループ
										</a>
										<a class="subnav_item" href="{{ url('about/csr')}}">
											CSR・IR情報
										</a>
										<a class="subnav_item" href="{{ url('about/other')}}">
											その他
										</a>
									</div>
								</div>
							</div>
						</div>
					</li>
				</ul>
				<div class="menu-tabs">
					<ul>
						<li class="pc">
							<a href="javascript:void(0)" data-target="searchform">
								<i></i>
								<p>SEARCH</p>
							</a>
							<div class="searchform">
								<form action="http://custom.search.yahoo.co.jp/search" method="get" id="srch">
									<div class="searchboxwrapper">
										<p id="srchForm">
											<input class="searchbox" type="text" value="" name="p" placeholder="" id="srchInput">
											<input type="hidden" id="fr" name="fr" value="cse">
											<input type="hidden" id="ei" name="ei" value="UTF-8">
											<input type="hidden" id="csid" name="csid" value="x.1LLYUMD465k1OkUZc6SaFB8b2TjPYjtlpjh9Xiaw--">
											<input type="radio" name="vs" value="www.jrfreight.co.jp" id="yjInsite" checked="checked">
											<input class="searchsubmit" value="検索" type="submit" id="srchBtn" >
											<!-- <input type="submit" value="検索" id="srchBtn"> -->
										</p>
									</div>
								</form>
							</div>
						</li>
						<li class="coming-soon">
							<a href="javascript:void(0)">
								<i></i>
								<p>ENGLISH</p>
							</a>
						</li>
						<li>
							<a href="{{ url('inquiry') }}">
								<i></i>
								<p>お問い合わせ</p>
							</a>
						</li>
						<li class="sp">
							<a href="javascript:void(0)" data-target="main_nav">
								<div class="menu-icon">
									<span></span>
								</div>
							</a>
						</li>
					</ul>
				</div>
			</nav>
		</div>