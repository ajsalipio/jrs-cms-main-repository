<div class="footer__row">
			<div class="footer_column pc">
				<a href="{{ url('/')}}"><b>トップ</b></a>
				<a href="{{ url('info') }}"><b>最新情報</b></a>
				<a href="javascript:void(0)"><b>サービスご案内</b></a>
				<ul>
					<li><a href="{{ url('service')}}">仕組みと特長</a></li>
					<li><a href="{{ url('service/price')}}">値段の仕組み</a></li>
					<li><a href="{{ url('service/container')}}">コンテナのサイズ・種類</a></li>
					<li><a href="{{ url('service/transport')}}">様々な輸送サービス</a></li>
					<li><a href="{{ url('service/area')}}">エリア別サービス案内</a></li>
					<li><a href="{{ url('service/improvement')}}">サービス向上に向けた取組み</a></li>
				</ul> <!-- services -->
			</div>
			<div class="footer_column pc">
				<!-- <a href="./i_daiya/index.html" target="_blank"><b>現在の輸送状況</b></a> -->
				<a href="javascript:void(0)"><b>鉄道貨物の特長</b></a>
				<ul>
					<li><a href="{{ url('modalshift')}}">モ－ダルシフト</a></li>
					<!-- <li><a href="{{ url('modalshift/calculate')}}">エネルギー使用量・CO2排出量計算シート</a></li> -->
				</ul><!-- modalshift -->
				<a href="{{ url('voice')}}"><b>お客様の声</b></a>
				<a href="{{ url('recruit')}}"><b>採用情報</b></a>
			</div>
			<div class="footer_column pc">
				<a href="javascript:void(0)"><b>私たちについて</b></a>
				<ul>
					<li><a href="{{ url('about')}}">企業情報</a></li>
					<li><a href="{{ url('about/railgate')}}">レールゲート</a></li>
					<li><a href="{{ url('about/relation')}}">関連事業</a></li>
					<li><a href=".{{ url('about/relation/globalwork')}}">海外事業</a></li>
					<li><a href="{{ url('about/group')}}">JR貨物グループ</a></li>
					<li><a href="{{ url('about/csr')}}">CSR・IR情報</a></li>
					<!-- <li><a href="./about/csr/plan">事業計画</a></li>
					<li><a href="./about/csr/financial">財務情報</a></li>
					<li><a href="./about/csr/safety">安全報告書</a></li> -->
					<li><a href="{{ url('about/other')}}">その他</a></li>
				</ul>
				<!-- <a href="./#top__firsttime"><b>初めてご利用の方へ</b></a>
				<a href="./question"><b>よくあるご質問</b></a>
				<a href="./#top__video"><b>動画でご紹介</b></a> -->
			</div>
			<div class="footer_column">
				<a href="{{ url('question')}}"><b>よくあるご質問</b></a>
				<a href="{{ url('inquiry')}}" class="pc"><b>お問い合わせ</b></a>
				<a href="{{ url('compliance')}}"><b>外部通報窓口</b></a>
				<a href="{{ url('individual')}}"><b>プライバシーポリシー</b></a>
				<a href="{{ url('sitepolicy')}}"><b>サイトのご利用にあたって</b></a>
				<a href="{{ url('sitemap')}}"><b>サイトマップ</b></a>
				<a href="{{ url('link')}}"><b>リンク</b></a>
			</div>
		</div>
		<div class="footer__row">
			<div class="footer__row_inner">
				<div class="footer_column">
					<a href="{{ url('/') }}" class="logo__footer">
						<img src="{{ asset('front_images/images/logo_footer.png') }} " alt="" class="pc">
						<img src="{{ asset('front_images/images/logo_footer_sp.png') }} " alt="" class="sp">
					</a>
				</div>
				<div class="footer_column">
					©Copyright Japan Freight Railway Company.All rights reserved.
				</div>
			</div>
		</div>