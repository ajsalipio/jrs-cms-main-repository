		<div class="header__content">
			<h1 class="logo">
				<a href="{{ url('en')}}">
					<img src="{{ asset('front_images/images/logo_en.png') }}" alt="JR Kamotsu" class="pc">
					<img src="{{ asset('front_images/images/logo_en.png') }}" alt="JR Kamotsu" class="sp">
				</a>
			</h1>
			<nav>
				<ul class="main_nav subnav" id="main_nav">
					<li class="subnav_container">
						<a href="{{ url('en/philosophy')}}"  data-target="corporate" class="hoversub">CORPORATE DATA</a>
						<div class="subnav main_subnav type3" id="corporate">
							<div class="container">
								<div class="container__inner">
									<div class="subnav__inner">
										<a class="subnav_item" href="{{ url('en/philosophy')}}">
											Philosophy
										</a>
										<a class="subnav_item" href="{{ url('en/corporate-overview')}}">
											Corporate Overview
										</a>
										<a class="subnav_item" href="{{ url('en/service')}}">
											Service
										</a>
										<a class="subnav_item" href="{{ url('en/main-data')}}">
											Main Data
										</a>
										<a class="subnav_item" href="{{ url('en/financials')}}">
											Financials
										</a>
										<a class="subnav_item text--f12" href="{{ url('en/material-procurement-info')}}">
											Material Procurement Information
										</a>
									</div>
								</div>
							</div>
						</div>
					</li>
					<li class="subnav_container">
						<a href="{{ url('en/on-time')}}"  data-target="railway" class="hoversub">RAILWAY BUSINESS</a>
						<div class="subnav main_subnav type2" id="railway">
							<div class="container">
								<div class="container__inner">
									<div class="subnav__inner">
										<a class="subnav_item" href="{{ url('en/on-time')}}">
											On Time
										</a>
										<a class="subnav_item" href="{{ url('en/ecology')}}">
											Ecology
										</a>
										<a class="subnav_item" href="{{ url('en/logistics')}}">
											Logistics
										</a>
										<a class="subnav_item" href="{{ url('en/lifeline')}}">
											Lifeline
										</a>
										<a class="subnav_item" href="{{ url('en/innovation')}}">
											Innovation
										</a>
									</div>
								</div>
							</div>
						</div>
					</li>
					<li>
						<a href="{{ url('en/new-ventures')}}">NEW VENTURES</a>
					</li>
				</ul>

				<div class="menu-tabs">
					<ul>
						<li class="pc">
							<a href="javascript:void(0)" data-target="searchform">
								<i></i>
								<p>SEARCH</p>
							</a>
							<div class="searchform">
								<form action="http://custom.search.yahoo.co.jp/search" method="get" id="srch">
									<div class="searchboxwrapper">
										<p id="srchForm">
											<input class="searchbox" type="text" value="" name="p" placeholder="" id="srchInput">
											<input type="hidden" id="fr" name="fr" value="cse">
											<input type="hidden" id="ei" name="ei" value="UTF-8">
											<input type="hidden" id="csid" name="csid" value="x.1LLYUMD465k1OkUZc6SaFB8b2TjPYjtlpjh9Xiaw--">
											<input type="radio" name="vs" value="www.jrfreight.co.jp" id="yjInsite" checked="checked">
											<input class="searchsubmit" value="検索" type="submit" id="srchBtn" >
											<!-- <input type="submit" value="検索" id="srchBtn"> -->
										</p>
									</div>
								</form>
							</div>
						</li>
						<li>
							<a href="{{ url('/')}}">
								<i></i>
								<p>JAPANESE</p>
							</a>
						</li>
						<li>
							<a href="{{ url('/inquiry/')}}">
								<i></i>
								<p>CONTACT</p>
							</a>
						</li>
						<li class="sp">
							<a href="javascript:void(0)" data-target="main_nav">
								<div class="menu-icon">
									<span></span>
								</div>
							</a>
						</li>
					</ul>
				</div>
			</nav>
		</div>