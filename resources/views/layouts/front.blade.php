<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="format-detection" content="telephone=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
	@yield('meta')
	
    <title>@if(!empty($headAdminTitle))
	    {{ $headAdminTitle }} | JR貨物
	@else
	    @yield('title')
	@endif </title>
	<link rel="stylesheet" href="{{url('/css/front/main.css')}}">
	<link rel="stylesheet" href="{{url('css/front/vendor/swiper.min.css')}}">
	<link rel="shortcut icon" href="{{url('front_images/images/favicon.ico')}}">
	<link rel="apple-touch-icon" sizes="57x57" href="{{url('front_images/images/common/ico/apple-icon-57x57.png')}}">
	<link rel="apple-touch-icon" sizes="60x60" href="{{url('front_images/images/common/ico/apple-icon-60x60.png')}}">
	<link rel="apple-touch-icon" sizes="72x72" href="{{url('front_images/images/common/ico/apple-icon-72x72.png')}}">
	<link rel="apple-touch-icon" sizes="76x76" href="{{url('front_images/images/common/ico/apple-icon-76x76.png')}}">
	<link rel="apple-touch-icon" sizes="114x114" href="{{url('front_images/images/common/ico/apple-icon-114x114.png')}}">
	<link rel="apple-touch-icon" sizes="120x120" href="{{url('front_images/images/common/ico/apple-icon-120x120.png')}}">
	<link rel="apple-touch-icon" sizes="144x144" href="{{url('front_images/images/common/ico/apple-icon-144x144.png')}}">
	<link rel="apple-touch-icon" sizes="152x152" href="{{url('front_images/images/common/ico/apple-icon-152x152.png')}}">
	<link rel="apple-touch-icon" sizes="180x180" href="{{url('front_images/images/common/ico/apple-icon-180x180.png')}}">
	<link rel="icon" type="image/png" sizes="192x192"  href="{{url('front_images/images/common/ico/android-icon-192x192.png')}}">
	<link rel="icon" type="image/png" sizes="32x32" href="{{url('front_images/images/common/ico/favicon-32x32.png')}}">
	<link rel="icon" type="image/png" sizes="96x96" href="{{url('front_images/images/common/ico/favicon-96x96.png')}}">
	<link rel="icon" type="image/png" sizes="16x16" href="{{url('front_images/images/common/ico/favicon-16x16.png')}}">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://cdn.jsdelivr.net/css3-mediaqueries/0.1/css3-mediaqueries.min.js"></script>
	<![endif]-->
</head>
<body>
	<div class="preloader preloader--full">
		<div class="preloader__content">
			<div class="frame">
			  	<div class="center">
					<svg width="75px" height="75px" viewBox="0 0 100 100">
						<circle class="bg" cx="50" cy="50" r="46"/>
						<circle class="loader" cx="50" cy="50" r="46"/>
					</svg>
			  	</div>
			</div>
		</div>
	</div>

	<div class="overlay"></div>
	
	<header>@include('layouts.front_header')</header>
	<main class="@yield('mainClass')">
            @yield('content')
        </main>
	<footer>@include('layouts.front_footer')
	</footer>
	<script src="{{ asset('js/vendor/jquery-3.1.1.min.js') }}"></script>
	<script src="{{ asset('js/front/js/vendor/swiper.min.js') }}"></script>
	<script src="{{ asset('js/front/js/vendor/jquery.matchHeight-min.js') }}"></script>
	<script src="https://www.youtube.com/iframe_api"></script>
	<script src="{{ asset('js/front/js/script.js') }}"></script>

	@yield('scripts')
</body>
</html>