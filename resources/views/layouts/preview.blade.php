<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<!-- <meta name="robots" content="noindex,nofollow"> -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="format-detection" content="telephone=no">
    
    <!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<!--
	<meta name="description" content="このWebサイトの説明が入ります。">
	<meta name="keywords" content="キーワード,キーワード">
	<meta property="og:title" content="このWebサイトのタイトルが入ります。">
	<meta property="og:type" content="website">
	<meta property="og:description" content="このWebサイトの説明が入ります。">
	<meta property="og:url" content="http://XXXXXXXXXX.com">
	<meta property="og:image" content="http://XXXXXXXXXX.com/images/og.png">
	<meta property="fb:app_id" content="XXXXXXXXXXXXXXXX">
	<meta name="twitter:card" content="summary">
	<meta name="twitter:site" content="@XXXXX">
	<meta name="twitter:creator" content="@XXXXX">
	<meta name="twitter:image:src" content="http://XXXXXXXXXX.com/images/og.png">
	-->
	<title>@if(!empty($headAdminTitle))
	    {{ $headAdminTitle }} | JR貨物
	@else
	    @yield('title')
	@endif </title>
	
	<link rel="stylesheet" href="{{asset('css/front/main.css')}}">
	<link rel="stylesheet" href="{{asset('css/front/vendor/swiper.min.css')}}">
	<link rel="shortcut icon" href="{{asset('front_images/images/favicon.ico')}}" type="image/vnd.microsoft.icon">
	<link rel="icon" href="{{ asset('img/common/favicon.png') }}" type="image/vnd.microsoft.icon">
	
	    @yield('styles')
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://cdn.jsdelivr.net/css3-mediaqueries/0.1/css3-mediaqueries.min.js"></script>
	<![endif]-->
</head>
	<body>
	
	<div class="preloader preloader--full">
		<div class="preloader__content">
			<div class="frame">
			  	<div class="center">
					<svg width="75px" height="75px" viewBox="0 0 100 100">
						<circle class="bg" cx="50" cy="50" r="46"/>
						<circle class="loader" cx="50" cy="50" r="46"/>
					</svg>
			  	</div>
			</div>
		</div>
	</div>

	<div class="overlay"></div>
	
	<header>@include('layouts.preview_header')</header>
	<main class="@yield('mainClass')">
		@yield('content')	
	</main> 
	<footer>@include('layouts.preview_footer')
	</footer>
	<script src="{{ asset('js/preview/js/vendor/jquery-3.1.1.min.js') }}"></script>
	<script src="{{ asset('js/preview/js/vendor/swiper.min.js') }}"></script>
	<script src="{{ asset('js/preview/js/vendor/jquery.matchHeight-min.js') }}"></script>
	<script src="{{ asset('js/preview/js/script.js') }}"></script>
</body>
</html>
