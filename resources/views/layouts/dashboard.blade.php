<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@if(!empty($headAdminTitle))
	    {{ $headAdminTitle }}
	@else
	    @yield('title')
	@endif
    </title>
   <!-- <title>{{ !empty($headAdminTitle) ?$headAdminTitle :'' }}</title>-->
<!-- FAVICON -->
	<link rel="shortcut icon" href="{{ asset('img/common/favicon.png') }}" type="image/vnd.microsoft.icon">
	<link rel="icon" href="{{ asset('img/common/favicon.png') }}" type="image/vnd.microsoft.icon">
	<link rel="stylesheet" href="{{ asset('css/normalize.css') }}">
	<link rel="stylesheet" href="{{ asset('css/jquery.datetimepicker.css') }}">
	<link rel="stylesheet" href="{{ asset('css/main-admin.css') }}">
	<link rel="stylesheet" href="{{ asset('css/style.css') }}"> <!-- Custom -->
    @yield('styles')
	<!--<script language="JavaScript"  src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.0/jquery.min.js"></script>-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
</head>
	<body class="@yield('bodyClass', 'dashboard')" id="top">
	<header id="global__header">@include('layouts.dashboardHeader')</header>
			<div class="main__container">
		<aside id="global__sidebar">@include('layouts.dashboardSidebar')</aside>
		<main class="content__main">
		@yield('content')
		
			<footer>@include('layouts.dashboardFooter')</footer>
		</main> 
		<div class="modal_dialog">
			<div class="modal_dialogbox" id="modalDialog">
				<div class="modal_dialogbox__content">
					<p>本当によろしいでしょうか？</p>
				</div>
				<div class="modal_dialogbox__buttons">
					<a href="javascript:void(0)" class="close_modal">戻る</a>
					<button id="btn-modal-confirm" type="submit">更新する</button>
				</div>
			</div>
		</div>
	</div> <!-- .main__container -->
	
	<input type="hidden" id="messages_validations" value="{{ json_encode(__('messages')) }}" />

	<!-- <script src="{{ asset('js/jquery.min.js') }}" defer type="text/javascript"></script>-->
   <!-- <script src="{{ asset('js/app.js') }}" defer></script>-->
	<script src="{{ asset('js/tinyMCE/tinymce.min.js') }}"></script>
	<script src="{{ asset('js/vendor/modernizr-2.8.3.min.js') }}"></script>
	<script>window.jQuery || document.write('<script src="{{asset('js/vendor/jquery-3.1.1.min.js')}}"><\/script>')</script>
	<script src="{{ asset('js/main.js') }}"></script>
	<script src="{{asset('js/vendor/datepicker-jquery.js')}}"></script>
	<script src="{{asset('js/vendor/jquery.datetimepicker.full.min.js')}}"></script>
	<script src="{{asset('js/exif.js')}}"></script>
	<script src="{{asset('js/main-admin.js')}}"></script>
    @yield('scripts')
</body>
</html>
