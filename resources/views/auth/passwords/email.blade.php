@extends('layouts.login')

@section('content')

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf
					<table>
						<tr>
							<td>
								<div class="forgotpw__div">
									<h2>パスワードの再設定</h2>
									<p>パスワードを再設定するためのリンクをあなたのメールアドレスにお送りします。</p>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<span>メール</span>
							</td>
						</tr>
						<tr>
							<td>
								<div class="email__container">
									<input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
								</div>
							</td>
						</tr>
						<tr>
							<td><input type="submit" value="送信する" class="forgotpw__submit"></td>
						</tr>
						<tr>
							<td>
								@if ($errors->has('email'))
								<p class="txt__error">{!! $errors->first('email') !!}</p>
								@endif
								
								
								@if (session('status'))
									<p class="txt__error">{{ session('status') }}</p>
								@endif
							</td>
						</tr>
					</table>
                    </form>
			</div> <!-- .login__container -->
		</div> <!-- .inner__container -->
	</div> <!-- .input__container -->
</section> <!-- .access__container -->
@endsection
