@extends('layouts.login')

@section('content')

                    <form method="POST" action="{{ route('password.request') }}">
                        @csrf
                        <input type="hidden" name="token" value="{{ $token }}">
					<table>
						<tr>
							<td>
								<div class="forgotpw__div">
									<h2>パスワードの再設定</h2>
									<p>新しいパスワードを登録してください。</p>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<span>メール</span>
							</td>
						</tr>
						<tr>
							<td>
								<div class="email__container">
									<input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email or old('email') }}" required autofocus>
								</div>
								@if ($errors->has('email'))
									<span class="invalid-feedback">
										<strong>{{ $errors->first('email') }}</strong>
									</span>
								@endif								
							</td>
						</tr>
						<tr>
							<td>
								<span>パスワード</span>
							</td>
						</tr>
						<tr>
							<td>
								<div class="password__container">
									<input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<span>確認用パスワード</span>
							</td>
						</tr>
						<tr>
							<td>
								<div class="password__container">
									<input id="password-confirm" type="password" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" name="password_confirmation" required>
								</div>
							</td>
						</tr>
						<tr>
							<td><input type="submit" value="登録する" class="forgotpw__submit"></td>
						</tr>
						<tr>
							<td>
							@if ($errors->has('password'))
									<p class="txt__error">{{ $errors->first('password') }}</p>
								@endif

								@if ($errors->has('password_confirmation'))
									<p class="txt__error"> {{ $errors->first('password_confirmation') }}</p>
								@endif
							</td>
						</tr>
					</table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
