@extends('layouts.login')

@section('title', 'Login | JR貨物')

@section('content')

                    <form method="POST" action="{{ route('login') }}" autocomplete="off">
                        @csrf
					<table>
						<tr>
							<td>
								<span>メール</span>
							</td>
						</tr>
						<tr>
							<td>
								<div class="user__container">
									<input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email', (($user) ? $user['email'] : '')) }}" required autofocus autocomplete="off" placeholder="JR Kamotsu">
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<span>パスワード</span>
							</td>
						</tr>
						<tr>
							<td>
								<div class="password__container">
									<input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="パスワード" autocomplete="new-password" {{ ($user && empty(old('email'))) ? 'value=' . $user['id'] : ""}} >
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="custom__checkbox">
									<label class="container">ログイン情報を保存する
										<input type="checkbox" name="remember_me" {{ ($user && empty(old('email'))) || !empty(old('email'))  ? 'checked=checked' : '' }} />
										<span class="checkmark"></span>
									</label>
								</div>		
							</td>
						</tr>
						<tr>
							<td><input type="submit" value="ログイン" class="login__submit"></td>
						</tr>
						<tr>
							<td>
								@if($errors->any())
									<p class="txt__error" style="visibility:visible">
										@if($errors->has('email'))
											{!! $errors->first('email') !!}
										@endif
										@if($errors->has('password'))
											{!! $errors->first('password') !!}
										@endif										
									</p>
								@endif
								<!-- @if ($errors->has('email'))
									<span class="invalid-feedback">
										<strong>{{ $errors->first('email') }}</strong>
									</span>
								@endif
								@if ($errors->has('password'))
									<span class="invalid-feedback">
										<strong>{{ $errors->first('password') }}</strong>
									</span>
								@endif -->

							</td>
						</tr>
					</table>
                   </form>
						</div> <!-- .login__container -->
					</div> <!-- .inner__container -->
				</div> <!-- .input__container -->
				<div class="bottom__container">
					<a href="{{ route('password.request') }}" class="forgot__pw">パスワード忘れた方はこちら ></a>
				</div>
			</section> <!-- .access__container -->
		</main>
@endsection
