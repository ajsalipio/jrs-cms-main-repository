<section class="child__kv">
    <div class="container">
        <div class="kv__container">
            <img src="{{ asset('front_images/images/child/kvisual/kv_default_02.jpg')}}" alt="">
            <h1 class="kv__title">お問い合わせ</h1>
        </div>
        <div class="breadcrumbs__container">
            <ul>
                <li><a href="../">TOP</a></li>
                <li class="active"><a href="./">お問い合わせ</a></li>
            </ul>
        </div>
    </div>
</section> <!-- .child__kv -->

<section class="compliance__container">
    <div class="container">
        <p class="complete--p">メールは正常に送信されました。<br>お問い合せ内容を確認させて頂き、後日ご連絡を差し上げますので、今しばらくお待ちくださいますよう、よろしくお願い申し上げます。</p>
    </div>
</section><!-- .compliance__container [01] -->

<a class="back-to-top"  href="#top">
    <span>TOPに戻る</span>
</a>