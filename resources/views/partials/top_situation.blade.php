<section class="top__transport">
    <div class="container">
        <div class="container__inner">
            <div class="transport__inner">
                <div class="transport_column">
                    <p><b>現在の輸送状況</b></p>
                </div>
                <div class="transport_column">
                @if(!empty($situation))
                    
                    @if($situation->situation_status != 0  && $isOffHours)
                        <p> {{ config('constants.PUBLISH_TIME_OUT_OF_SERVICE.TEXT') }} </p>
                    @else
                        @if(!empty($situation->have_time_publising))
                            <p>{{ date("Y年m月d日 H時", strtotime($situation->publish_from))}}現在</p>
                            @else
                            <p>{{ date("Y年m月d日 H時", strtotime($situation->created_at))}}現在</p>
                            @endif
                        @if( $situation->situation_title)
                            <p>{{ $situation->situation_title }}</p>
                        @else
                            <p> {{ config('constants.SITUATION_STATUS')[$situation['situation_status']] }} </p>
                        @endif
                        
                        @if($situation->situation_status == 3)
                            <a href="{{url('/situation')}}/detail{{$situation->id}}" class="btn-primary-fill btn-learnmore">詳しく見る</a>
                        @endif
                    @endif
                @endif
                </div>
            </div>
        </div>
    </div>
</section><!-- top__transport -->