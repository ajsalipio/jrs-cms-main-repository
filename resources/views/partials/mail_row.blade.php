<table class="list__template">
    <tr>
        <th>
            <div class="custom__checkbox">
                <label class="container">
                    <input type="checkbox" id="cb-check_all">
                    <span class="checkmark"></span>
                </label>
            </div>	
        </th>
        <th>配信先メールアドレス</th>
        <th>作成日</th>
        <th>配信種別</th>
    </tr>
    @foreach($mails as $mail)
        <tr>
            <td>
                <div class="custom__checkbox">
                    <label class="container">
                        <input type="checkbox" id="cb-archive_items" name="archived_items[]" value="{{ $mail['mail_id'] }}" class="list_chk">
                        <span class="checkmark"></span>
                    </label>
                </div>	
            </td>
            <td>
                <a href="#"  class="js-edit_mail" data-mail="{{ json_encode($mail) }}">
                    {{ $mail['mail_address'] }}
                </a>
            </td>
            <td> 
                {{  date("Y.m.d", strtotime($mail['created_at'])) }}
            </td>
            <td> {{ config('constants.MAIL_STATUS')[$mail['options']] }} </td>
        </tr>
    @endforeach
</table> <!-- .list__template -->

{{ $mails->links() }}