<table class="panel__table js-base_pagination">
	@foreach($currentPaginatedData as $item)
		<tr style="display: block;">
			<td>
				{{  date("Y.m.d", strtotime($item->created_at)) }}
			</td>
			<td>	
				<div class="btn {{ $tab_class }} disabled">ニュースリリース</div>
			</td>
			<td>
				{{--  
					-- Here we have possibility to display a link
				--}}
				@if($item->is_link)
					@if($item->media)
						<a href="{{url('/')}}/{{ $item->media->path }}" target="_blank">{{ $item->newsLabel}}</a>
					@else
						<a href="#" target="_blank">{{ $item->newsLabel}}</a>
					@endif
				@else
				<a href="{{url('/info')}}/detail{{ $item->id}}">{{ $item->newsLabel}}</a>
				@endif
			</td>
		</tr>
	@endforeach
</table>
{{ $currentPaginatedData->appends(['tab' => $pageDefaults['tab'] ])
			->links('vendor.pagination.front_office') }}