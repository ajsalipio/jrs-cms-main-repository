@extends('layouts.front')
@section('title', $headAdminTitle)
@section('mainClass', 'child voice')
@section('content')

		<section class="child__kv">
			<div class="container">
				<div class="kv__container" style="background-image: url({{asset('front_images/images/child/kvisual/voice.jpg')}})">
					<!-- <img src="{{asset('front_images/images/child/kvisual/voice.jpg')}}" alt=""> -->
					<h1 class="kv__title">お客様の声</h1>
				</div>
				<div class="breadcrumbs__container">
					<ul>
						<li><a href="{{url('/')}}">TOP</a></li>
						<li class="active"><a href="{{url('voice')}}">お客様の声</a></li>
					</ul>
				</div>
			</div>
		</section> <!-- .child__kv -->

		<section class="template__content voice__content">
			<div class="container">
				<div class="content__container">
					<div>
						<p>実際にご利用いただいている方の生の声をお届けします。JR貨物輸送の特徴やメリットなどぜひ皆様の参考にしてくださいませ。</p>
					</div>
					<div class="container__inner">
						<div class="yourvoice__items">
						@foreach($customerVoices as $voice)
							<div class="yourvoice_item" style="background-image: url('{{ asset($voice->cv_picture  ? $voice->media->path : '') }}')">
								<div class="yourvoice_item__intro">
									<p>{{ $voice->cv_company}}</p>
									<h4>{{ $voice->cvTitleLabel}}</h4>
									<a href="{{url('/voice/')}}/detail{{$voice->id}}" class="btn-primary-fill">この記事を読む</a>
							</div>
							</div>
						@endforeach
						</div>
					</div>
				</div>
			</div>
		</section><!-- voice__content -->

		<a class="back-to-top"  href="#top">
			<span>TOP<br class="sp">に戻る</span>
		</a>

@endsection
@section('scripts')
@endsection()

