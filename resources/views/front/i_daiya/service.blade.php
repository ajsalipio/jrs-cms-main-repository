@extends('layouts.front')
@section('mainClass', 'child i_daiya service')
@section('title', '現在の輸送状況 | JR貨物')
@section('content')

    <section class="child__kv">
        <div class="container">
            <div class="breadcrumbs__container">
                <ul>
                    <li><a href="{{ url('i_daiya') }}">TOP</a></li>
                    <li class="active"><a href="./">現在の輸送状況</a></li>
                </ul>
            </div>
            <div class="kv__container type2">
                <!-- <img src="../front_images/images/child/kvisual/i_daiya.jpg" alt=""> -->
                <h1 class="kv__title">現在の輸送状況</h1>
            </div>
        </div>
    </section> <!-- .child__kv -->

    <section class="template__content i_daiya__content">
        <div class="container">
            <div class="content__container">
                <hr>
                <ul>
                    <li>
                        ７：００から２４：００までの間（ただし、年末年始、GW期、盆休期等の長期連休期間を除く）、長距離貨物列車に概ね１時間以上の遅れが発生、または見込まれるときに、輸送情報をご案内します。
                    </li>
                    <li>
                        本サービスの情報は、実際の列車運転状況と異なる場合があります。輸送情報は目安としてご利用ください
                    </li>
                    <li>
                        本サービスの情報に基きお客様の判断により発生した損害につきましては、いかなる責任も負いかねますのでご容赦願います。
                    </li>
                    <li>
                        最新情報の確認は、ブラウザの更新ボタンを押して下さい。
                    </li>
                    <li>
                        多数の貨物列車に遅れ、運休が発生する場合、「Microsoft Excel」等で作成した輸送情報を添付します。アイコンをクリックし、当該ファイルをダウンロードしてご覧下さい。
                    </li>
                    <li>
                        掲載されている遅延時分は、以下の基準により記載しております。
                        <ol>
                            <li>
                                遅延時分は、駅の発車時分もしくは通過時分を表します。ただし、「着」と記載されている場合は駅に到着する遅延時分、また「終着」については列車が終着駅に到着する遅延時分のことをそれぞれ表します。
                            </li>
                            <li>
                                遅延時分は、各駅で基準とされた地点を対象となる列車が通過する際に発生する所定の時刻との差異を表しており、コンテナの引き渡し可能な時分とは異なります。
                            </li>
                        </ol>
                    </li>
                </ul>
            </div>
        </div>
    </section><!-- i_daiya__content -->

    <a class="back-to-top"  href="#top">
        <span>TOPに戻る</span>
    </a>
    
@endsection
