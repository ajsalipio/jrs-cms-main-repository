@extends('layouts.front')

@section('content')
    <div>
		<h2> {{ config('constants.SITUATION_STATUS')[$situation['situation_status']] }} <h2>
		
		<h2> {{ $situation['situation_title'] }} <h2>
		<h2> {{ $situation['situation_content'] }} <h2>
		<p> {!! $situation['situation_details'] !!} </p>
		<h2>
		</h2>

		<div class="div-files">
			@if( $situation->fileXls)
				<a href="{{ asset( $fileXls['path']) }}" id="link-file_xls" download="{{ $fileXls['originalName'] }}"> 
                    {{ $fileXls['originalName'] }}
                </a>
			@endif
		</div>
    </div>
@endsection

