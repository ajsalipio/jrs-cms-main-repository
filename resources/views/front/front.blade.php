@extends('layouts.front')
@section('mainClass', 'top')
@section('title', 'JR貨物 日本貨物鉄道株式会社')
@section('content')

		<section class="top__kv">
			<div class="top__box">
				<h2><span>価値を運ぶ</span><br>ネットワーク</h2>
			</div>
			<!-- <div class="top__kvtitle">
				<h2><img src="{{asset('front_images/images/top/img_kv_title.png')}}" alt=""></h2>
			</div> -->
			<div class="swiper-container top__kv_swiper">
				<div class="swiper-wrapper">
					<div class="swiper-slide" style="background-image: url('{{ asset('/front_images')}}/images/top/img_kv_01.jpg')"></div>
					<div class="swiper-slide" style="background-image: url('{{ asset('/front_images')}}/images/top/img_kv_03.jpg')"></div>
					<div class="swiper-slide" style="background-image: url('{{ asset('/front_images')}}/images/top/img_kv_04.jpg')"></div>
					<div class="swiper-slide" style="background-image: url('{{ asset('/front_images')}}/images/top/img_kv_02.jpg'); position: relative;;">
						<div class="kv__button_container">
							<h2>JR貨物紹介動画を見る</h2>
							<a href="#top_video" class="btn-primary-fill">click here</a>
						</div>
					</div>
				</div>
				<div class="swiper__next pc"></div>
				<div class="swiper__prev pc"></div>
				<div class="swiper-pagination"></div>
			</div>
		</section><!-- top__kv -->

		<!-- <section class="top__transport"> -->
		<div id="div-top_situation">
			@include('partials.top_situation')
		</div>
		<!-- </section> -->

		<section class="top__latestinfo">
			<div class="container">
				<div class="container__inner">
					<div class="latestinfo__inner">
						<h3>最新情報</h3>
						<div class="latestinfo__tabs">
							<a href="{{url('/')}}" data-target="all" data-items="6" class="btn btn-primary active tab">最新情報全て</a>
							<a href="{{url('/')}}" data-target="newrelease" data-items="6" class="btn btn-blue tab">ニュースリリース</a>
							<a href="{{url('/')}}" data-target="diamondrevision" data-items="6" class="btn btn-cyan tab">ダイヤ改正</a>
							<a href="{{url('/')}}" data-target="settlement" data-items="6" class="btn btn-orange tab">決算・中間決算</a>
							<a href="{{url('/')}}" data-target="regionalnews" data-items="6" class="btn btn-purple tab">地域ニュース</a>
							<a href="{{url('/')}}" data-target="event" data-items="6" class="btn btn-pink tab">イベント</a>
						</div>
						<div class="latestinfo__panel_container">
							<div class="latestinfo__panel panel open" data-id="all">
								<table class="panel__table">
									@foreach($all as $item)
										<tr style="display: table-row;">
											<td>
												{{  date("Y.m.d", strtotime($item['created_at'])) }}
											</td>
											<td>
												<div class="btn {{ config('constants.NEWS_CATEGORIES_CSS')[$item['category']] }} disabled">{{ $item['category'] }}</div>
											</td>
											<td>
												<a href="{{ $item['url'] ?? '#' }}" target="{{ ($item['is_link'] == true) ? '_blank' : '' }}">
													{{ $item['news_title'] }}
												</a>
											</td>
											@if($item['category'] == 'イベント' && !empty($item['invalid']))
												<td>
													<div class="btn btn-red-fill">開催終了</div>
												</td>
											@endif
										</tr>
									@endforeach
								</table>
							</div>
							<div class="latestinfo__panel panel" data-id="newrelease">
								<table class="panel__table">
								@foreach($news as $item)
									<tr style="display: table-row;">
										<td>
											{{  date("Y.m.d", strtotime($item->created_at)) }}
										</td>
										<td>
											<div class="btn btn-blue-fill disabled">ニュースリリース</div>
										</td>
										<td>
											{{--  
												-- Here we have possibility to display a link
											--}}
											@if($item->is_link)
											<a href="{{url('/')}}/{{ $item->media->path }}" target="_blank">	{{ $item->newsLabel }}</a>
											@else
											<a href="{{url('/info')}}/detail{{ $item->id}}">	{{ $item->newsLabel }}</a>
											@endif
										
										</td>
									</tr>
								@endforeach
								</table>
							</div>
							<div class="latestinfo__panel panel" data-id="diamondrevision">
								<table class="panel__table">
								@foreach($daiya as $item)
									<tr>
										<td>
											{{ date("Y.m.d", strtotime($item->created_at)) }}
										</td>
										<td>
											<div class="btn btn-cyan-fill disabled">ダイヤ改正</div>
										</td>
										<td>
																						@if($item->is_link)
											<a href="{{url('/')}}/{{ $item->media->path }}" target="_blank">	{{ $item->newsLabel }}</a>
											@else
											<a href="{{url('/info')}}/detail{{ $item->id}}">	{{ $item->newsLabel }}</a>
											@endif
										
										</td>
									</tr>
								@endforeach
								</table>
							</div>
							<div class="latestinfo__panel panel" data-id="settlement">
								<table class="panel__table">
								@foreach($settlements as $item)
									<tr>
										<td>
											{{ date("Y.m.d", strtotime($item->created_at))}}
										</td>
										<td>
											<div class="btn btn-orange-fill disabled">決算・中間決算</div>
										</td>
										<td>
																						@if($item->is_link)
											<a href="{{url('/')}}/{{ $item->media->path }}" target="_blank">	{{ $item->newsLabel }}</a>
											@else
											<a href="{{url('/info')}}/detail{{ $item->id}}">	{{ $item->newsLabel }}</a>
											@endif
										
										</td>
									</tr>
								@endforeach
								</table>
							</div>
							<div class="latestinfo__panel panel" data-id="regionalnews">
								<table class="panel__table">
								@foreach($locals as $item)
									<tr>
										<td>
											{{  date("Y.m.d", strtotime($item->created_at))}}
										</td>
										<td>
											<div class="btn btn-purple-fill disabled">地域ニュース</div>
										</td>
										<td>
																						@if($item->is_link)
												<a href="{{url('/')}}/{{ $item->media->path }}" target="_blank">
													<!-- Move to accessor -->
													<!-- @if(isset($item->regional_category))
													[{{ $item->regionalCategoryUnion }}]
												@endif -->
												{!! nl2br($item->regionalNewsLabel) !!}</a>
											@else
												<a href="{{url('/info')}}/detail{{ $item->id}}">
													<!-- @if(isset($item->regional_category))
													[{{ $item->regionalCategoryUnion }}]
												@endif -->
												{!! nl2br($item->regionalNewsLabel) !!}</a>
											@endif
											
										</td>
									</tr>
								@endforeach
								</table>
							</div>
							<div class="latestinfo__panel panel" data-id="event">
								<table class="panel__table">
								@foreach($events as $item)
									<tr>
										<td>
											{{ date("Y.m.d", strtotime($item->created_at))}}
										</td>
										<td>
											<div class="btn btn-pink-fill disabled">イベント</div>
										</td>
										<td>
																						@if($item->is_link)
											<a href="{{url('/')}}/{{ $item->media->path }}" target="_blank">{{ $item->newsLabel }}</a>
											@else
											<a href="{{url('/info')}}/detail{{ $item->id}}">{{ $item->newsLabel }}</a>
											@endif
											
										</td>
										<td>
										@if(  $item->validity && strtotime( $item->validity) < strtotime('NOW'))
											<div class="btn btn-red-fill">開催終了</div>
											@endif
									</td>
									</tr>
								@endforeach
								</table>
							</div>
						</div>
						<a href="{{url('/info')}}" class="btn-primary-fill btn-learnmore center">もっと見る</a>
					</div>
				</div>
			</div>
		</section><!-- top__latestinfo -->

		<section class="top__firsttime">
			<div class="container">
				<div class="container__inner">
					<div class="firsttime__inner">
						<h2>初めてご利用の方へ</h2>
						<div class="firsttime__writeup">
							<h3>簡単・便利な鉄道貨物輸送</h3>
							<p>コンテナで運ぶ「コンテナ輸送」と、専用貨車で運ぶ「車扱輸送」の2つの輸送方式で、さまざまな 輸送ニーズに幅広くお応えしています。</p>
						</div>
						<div class="firsttime__img">
							<img src="{{asset('front_images/images/top/img_01.png')}}" alt="">
						</div>
						<div class="firsttime__steps">
							<div class="firsttime__steps_item">
								<div>
									<p>STEP1</p>
									<p>集荷</p>
								</div>
							</div>
							<div class="firsttime__steps_item">
								<div>
									<p>STEP2</p>
									<p>出発</p>
								</div>
							</div>
							<div class="firsttime__steps_item">
								<div>
									<p>STEP3</p>
									<p>到着</p>
								</div>
							</div>
							<div class="firsttime__steps_item">
								<div>
									<p>STEP4</p>
									<p>配達</p>
								</div>
							</div>
						</div>
						<a href="{{url('/service')}}" class="btn-primary-fill btn-learnmore center">もっと見る</a>
					</div>
				</div>
			</div>
		</section><!-- top__firsttime -->

		<section class="top__railcargo">
			<div class="railcargo__inner">
				<div class="container">
					<div class="container__inner">
						<h2>鉄道貨物の特長</h2>
						<div class="railcargo__writeup">
							<p>私たちJR貨物は今日本のライフラインを支える重要な位置付けを担っております。<br>昨今のトラック運送業への過剰な負荷、後継者問題による海運輸送の人員不足は深刻です。<br>私たちJR貨物は運送業の一員として、皆様の生活に貢献いたします。</p>
						</div>
						<div class="railcargo__characteristics">
							<span>01. 遠くまで一度に大量に運べる</span>
							<p>貨物列車26両分は、10tトラック65台分</p>
							<span>02. 時間通りに運べる</span>
							<p>時刻通りに発車して、走行速度も決まっている</p>
							<span>03. 日本全国にあるネットワーク</span>
							<p>全国各地に約150か所の貨物駅。1日の本数は約500本。</p>
						</div>
						<div class="railcargo__modalshift">
							モーダルシフト
						</div>
						<a href="{{url('/modalshift')}}" class="btn-primary-fill btn-learnmore center">もっと見る</a>
					</div>
				</div>
			</div>
		</section><!-- top__railcargo -->

		<section class="top__video">
			<div class="container" id="top_video">
				<div class="container__inner">
					<h2>JR貨物紹介動画<span>(約3分)</span></h2>
					<div class="top__video__inner">
						<div class="video_container">
							<div class="kv_play"></div>
							<div id="video-placeholder"></div>
						</div>
					</div>
				</div>
			</div>
		</section><!-- top__video -->

		<section class="top__yourvoice">
			<div class="container">
				<div class="container__inner">
					<h2>お客様の声</h2>
					<div class="yourvoice__items">
					@foreach($customerVoices as $voice)
						<div class="yourvoice_item matchheight" style="background-image: url('{{ asset($voice->cv_picture  ? $voice->media->path : '') }}')">
							<div class="yourvoice_item__intro">
								<p>{{ $voice->cv_company}}</p>
								<h4>{{ $voice->cvTitleLabel}}</h4>
								<a href="{{url('/voice/')}}/detail{{$voice->id}}" class="btn-primary-fill">この記事を読む</a>
							</div>
						</div>
					@endforeach
				</div>
					<div class="yourvoice__readmore">
						<a href="{{url('/voice')}}" class="btn-shadow-fill btn-learnmore center"><span>お客様の声をもっと見る</span></a>
					</div>
				</div>
			</div>
		</section><!-- top__yourvoice -->

		<section class="top__safety">
			<h2>安全への取り組み</h2>
			<div class="safety__inner">
				<div class="safety__inner_img">
					<img src="{{asset('front_images/images/top/img_06.jpg')}}" alt="">
				</div>
				<div class="safety__writeup_container">
					<div class="safety__writeup">
						<p>鉄道事業者にとって最大の使命である「安全の確立」に向けて、安全基本方針を定め、各種の安全対策を進めるとともに、安全最優先の職場風土の確立に取り組んでいます。</p>
						<a href="{{url('/about/csr/safety')}}" class="btn-primary-fill btn-learnmore center">もっと見る</a>
					</div>
				</div>
			</div>
		</section><!-- top__safety -->

		<section class="top__careers">
			<h2>採用情報</h2>
			<div class="careers__inner">
				<a href="{{url('/recruit')}}">
					<img src="{{asset('front_images/images/top/img_careers_pc.jpg')}}" alt="" class="pc">
					<img src="{{asset('front_images/images/top/img_careers_sp.jpg')}}" alt="" class="sp">
				</a>
			</div>
		</section><!-- top__careers -->

		<section class="top__relatedinfo">
			<div class="container">
				<div class="container__inner">
					<h2>関連情報</h2>
					<div class="relatedinfo__inner">
						<div class="relatedinfo_item">
							<a href="http://home.entetsu.co.jp/bt/bt-iwata/" target="_blank" class="relatedinfo_item__inner">
								<img src="{{asset('front_images/images/top/img_related_01.png')}}" alt="">
							</a>
						</div>
						<div class="relatedinfo_item">
							<a href="http://www.g-chigasaki-f.jp/" target="_blank" class="relatedinfo_item__inner type2">
								<img src="{{asset('front_images/images/top/img_related_02.jpg')}}" alt="">
							</a>
						</div>
						<div class="relatedinfo_item">
							<a href="http://www.sumitomo-rd-mansion.jp/shuto/hachioji/" target="_blank" class="relatedinfo_item__inner">
								<img src="{{asset('front_images/images/top/img_related_03.png')}}" alt="">
							</a>
						</div>
						<div class="relatedinfo_item">
							<a href="http://crascodesignstudio.jp/fresia-kanazawa/" target="_blank" class="relatedinfo_item__inner">
								<img src="{{asset('front_images/images/top/img_related_04.jpg')}}" alt="">
							</a>
						</div>
						<div class="relatedinfo_item">
							<div class="relatedinfo_item__inner">
								<a href="http://www.jrf-syouji.co.jp/" target="_blank">
									<img src="{{asset('front_images/images/top/img_related_05.jpg')}}" alt="">
								</a>
								<a href="{{url('about/relation/used_container')}}" target="_blank">
									<img src="{{asset('front_images/images/top/img_related_06.jpg')}}" alt="">
								</a>
								<a href="{{url('about/relation/parking')}}" target="_blank">
									<img src="{{asset('front_images/images/top/img_related_07.jpg')}}" alt="">
								</a>
							</div>
						</div>
						<div class="relatedinfo_item">
							<div class="relatedinfo_item__inner">
								<a href="https://www.rfa.or.jp/" class="" target="_blank">
									<img src="{{asset('front_images/images/top/img_related_08.jpg')}}" alt="">
								</a>
								<a href="http://www.t-renmei.or.jp/movie/school.html" target="_blank" >
									<img src="{{asset('front_images/images/top/img_related_09.jpg')}}" alt="">
								</a>
								<a href="http://www.greenpartnership.jp/" target="_blank" >
									<img src="{{asset('front_images/images/top/img_related_10.jpg')}}" alt="">
								</a>
							</div>
						</div>
						<div class="relatedinfo_item">
							<a href="http://www.mlit.go.jp/tetudo/tetudo_tk1_000007.html" target="_blank" class="relatedinfo_item__inner type2">
								<img src="{{asset('front_images/images/top/img_related_11.jpg')}}" alt="">
							</a>
						</div>
						<div class="relatedinfo_item type2">
							<a href="{{url('/campaign/shourei.html')}}" class="relatedinfo_item__inner">
								<img src="{{asset('front_images/images/top/img_related_12.jpg')}}" alt="">
							</a>
						</div>
					</div>
				</div>
			</div>
		</section><!-- top__relatedinfo -->
								
		<a class="back-to-top"  href="#top">
			<span>TOP<br class="sp">に戻る</span>
		</a>


@endsection

@section('scripts')
<script src="{{ asset('js/pusher/pusher.min.js') }}"></script>
<script>
	var pusher = new Pusher("{{ env('PUSHER_APP_KEY') }}", {
      cluster: 'ap1',
      encrypted: true
    });

    var channel = pusher.subscribe("{{ config('constants.CHANNELS.situation') }}");
    channel.bind("{{ config('constants.EVENTS.situation_save') }}", function(data) {
		$('#div-top_situation').html(data)
    });
</script>

@endsection