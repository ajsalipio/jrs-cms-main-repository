@extends('layouts.front')
@section('mainClass', 'child info')
@section('title', $headAdminTitle)
@section('headerID', 'top')
@section('content')

<input type="hidden" id="info-tab_changed" />
<input type="hidden" id="info-current_page" value="{{ Request::get('page') }}" />

		<section class="child__kv">
			<div class="container">
				<div class="kv__container" style="background-image: url('{{ asset('front_images/images/child/kvisual/kv_default_05.jpg')}}');">
					<!-- <img src="{{ asset('front_images/images/child/kvisual/kv_default_05.jpg')}}" alt=""> -->
					<h1 class="kv__title">最新情報</h1>
				</div>
				<div class="breadcrumbs__container">
					<ul>
						<li><a href="{{url('/')}}">TOP</a></li>
						<li class="active"><a href="{{url('/info')}}">最新情報</a></li>
					</ul>
				</div>
			</div>
		</section> <!-- .child__kv -->

		<section class="template__content info__content">
			<div class="container">
				<div class="content__container">
					<div class="dropdown__container">
						<div class="info_dropdown dropdown">
						  <button class="dropbtn"></button>
						  	<div class="dropdown__content">
							    @foreach($yearsList as $year)
									@foreach($year as $y)

										@if(!empty($yearSearched))
											<a href="{{ url('info/')}}/list/{{ $y}}" >
												<div {{ ($yearSearched == $y) ? 'selected' : ''}}>
													{{$y}}
												</div>
											</a>
										@else
											<a href="{{ url('info/')}}/list/{{ $y}}" >
												<div {{ (date("Y") == $y) ? 'selected' : ''}}>
													{{$y}}
												</div>
											</a>
										@endif
										
									@endforeach
								@endforeach
						  	</div>
						</div>
					</div>
					<div class="info__inner">
						<div class="info__tabs">
							<a href="./" data-target="all" data-items="20" class="btn btn-primary tab {{ $pageDefaults['tab'] == '最新情報全て' ? 'active' : '' }}">最新情報全て</a>
							<a href="./" data-target="newrelease" data-items="20" class="btn btn-blue tab {{ $pageDefaults['tab'] == 'ニュースリリース' ? 'active' : '' }}">ニュースリリース</a>
							<a href="./" data-target="diamondrevision" data-items="20" class="btn btn-cyan tab {{ $pageDefaults['tab'] == 'ダイヤ改正' ? 'active' : '' }}">ダイヤ改正</a>
							<a href="./" data-target="settlement" data-items="20" class="btn btn-orange tab {{ $pageDefaults['tab'] == '決算・中間決算' ? 'active' : '' }}">決算・中間決算</a>
							<a href="./" data-target="regionalnews" data-items="20" class="btn btn-purple tab {{ $pageDefaults['tab'] == '地域ニュース' ? 'active' : '' }}">地域ニュース</a>
							<a href="./" data-target="event" data-items="20" class="btn btn-pink tab {{ $pageDefaults['tab'] == 'イベント' ? 'active' : '' }}">イベント</a>
						</div>
						<div class="info__panel_container">
							<div id="all" class="info__panel panel open" data-id="all">
								<div class="js-div-current_pagination">
									<table class="panel__table">
										@foreach($all as $item)
											<tr style="display: block;">
												<td>
													{{  date("Y.m.d", strtotime($item['created_at'])) }}
												</td>
												<td>
													<div class="btn {{ config('constants.NEWS_CATEGORIES_CSS')[$item['category']] }} disabled">{{ $item['category'] }}</div>
												</td>
												<td>
													<a href="{{ $item['url'] ?? '#' }}" target="{{ ($item['is_link'] == true) ? '_blank' : '' }}">
														{{ $item['news_title'] }}
													</a>
												</td>
												@if($item['category'] == 'イベント' && !empty($item['invalid']))
													<td>
														<div class="btn btn-red-fill">開催終了</div>
													</td>
												@endif
											</tr>
										@endforeach
									</table>
									{{ $all->appends(['tab' => '最新情報全て'])
											->links('vendor.pagination.front_office') }}
								</div>
								@if($pageDefaults['tab'] == '最新情報全て' && Request::get('page') >= 2)
									<div class="js-div-base_pagination" style="display:none;">
										<table class="panel__table js-base_pagination">
											@foreach($currentPaginatedData as $item)
											<tr style="display: block;">
												<td>
													{{  date("Y.m.d", strtotime($item['created_at'])) }}
												</td>
												<td>
													<div class="btn {{ config('constants.NEWS_CATEGORIES_CSS')[$item['category']] }} disabled">{{ $item['category'] }}</div>
												</td>
												<td>
													<a href="{{ $item['url'] ?? '#' }}" target="{{ ($item['is_link'] == true) ? '_blank' : '' }}">
														{{ $item['news_title'] }}
													</a>
												</td>
												@if($item['category'] == 'イベント' && !empty($item['invalid']))
													<td>
														<div class="btn btn-red-fill">開催終了</div>
													</td>
												@else
													<td><div class="btn"></div></td>
												@endif
											</tr>
										@endforeach
										</table>
										{{ $currentPaginatedData->appends(['tab' => $pageDefaults['tab'] ])
													->links('vendor.pagination.front_office') }}
									</div>
								@endif
							</div>
							<div id="newrelease" class="info__panel panel open" data-id="newrelease">
								<div class="js-div-current_pagination">
									<table class="panel__table">
										@foreach($news as $item)
											<tr style="display: block;">
												<td>
													{{  date("Y.m.d", strtotime($item->created_at)) }}
												</td>
												<td>
													<div class="btn btn-blue-fill disabled">ニュースリリース</div>
												</td>
												<td>
													{{--  
														-- Here we have possibility to display a link
													--}}
													@if($item->is_link)
														@if($item->media)
															<a href="{{url('/')}}/{{ $item->media->path }}" target="_blank">{{ $item->newsLabel}}</a>
														@else
															<a href="#" target="_blank">{{ $item->newsLabel}}</a>
														@endif
													@else
													<a href="{{url('/info')}}/detail{{ $item->id}}">{{ $item->newsLabel}}</a>
													@endif
												</td>
											</tr>
										@endforeach
									</table>
									{{ $news->appends(['tab' => 'ニュースリリース'])
											->links('vendor.pagination.front_office') }}
								</div>
								@if($pageDefaults['tab'] == 'ニュースリリース' && Request::get('page') >= 2)
									<div class="js-div-base_pagination" style="display:none;">
										@include('partials.news_table', ['tab_class' => 'btn-blue-fill'])
									</div>
								@endif
							</div>
							<div id="diamondrevision" class="info__panel panel open" data-id="diamondrevision">
								<div class="js-div-current_pagination">
									<table class="panel__table">
										@foreach($daiya as $item)
											<tr style="display: block;">
												<td>
													{{ date("Y.m.d", strtotime($item->created_at)) }}
												</td>
												<td>
													<div class="btn btn-cyan-fill disabled">ダイヤ改正</div>
												</td>
												<td>
													@if($item->is_link)
														<a href="{{url('/')}}/{{ $item->media->path }}" target="_blank">{{ $item->newsLabel}}</a>
													@else
														<a href="{{url('/info')}}/detail{{ $item->id}}">{{ $item->newsLabel}}</a>
													@endif
													
												</td>
											</tr>
										@endforeach
									</table>
									{{ $daiya->appends(['tab' => 'ダイヤ改正'])
											->links('vendor.pagination.front_office') }}
								</div>
								@if($pageDefaults['tab'] == 'ダイヤ改正' && Request::get('page') >= 2)
									<div class="js-div-base_pagination" style="display:none;">
										@include('partials.news_table', ['tab_class' => 'btn-cyan-fill'])
									</div>
								@endif
							</div>
							@if($settlements)							
								<div id="settlement" class="info__panel panel open" data-id="settlement">
									<div class="js-div-current_pagination">
										<table class="panel__table">
											@foreach($settlements as $item)
												<tr style="display: block;">
													<td>
														{{ date("Y.m.d", strtotime($item->created_at))}}
													</td>
													<td>
														<div class="btn btn-orange-fill disabled">決算・中間決算</div>
													</td>
													<td>
																									@if($item->is_link)
														<a href="{{url('/')}}/{{ $item->media->path }}" target="_blank">{{ $item->newsLabel}}</a>
														@else
														<a href="{{url('/info')}}/detail{{ $item->id}}">{{ $item->newsLabel}}</a>
														@endif
														
													</td>
												</tr>
											@endforeach
										</table>
										{{ $settlements->appends(['tab' => '決算・中間決算'])
												->links('vendor.pagination.front_office') }}
									</div>
									@if($pageDefaults['tab'] == '決算・中間決算' && Request::get('page') >= 2)
										<div class="js-div-base_pagination" style="display:none;">
											@include('partials.news_table',  ['tab_class' => 'btn-orange-fill'])
										</div>
									@endif
								</div>
							@endif
							<div id="regionalnews" class="info__panel panel open" data-id="regionalnews">
								<div class="js-div-current_pagination">
									<table class="panel__table">
										@foreach($locals as $item)
											<tr style="display: block;">
												<td>
													{{  date("Y.m.d", strtotime($item->created_at))}}
												</td>
												<td>
													<div class="btn btn-purple-fill disabled">地域ニュース</div>
												</td>
												<td>
													@if($item->is_link)
														<a href="{{url('/')}}/{{ $item->media->path }}" target="_blank">
														@if(isset($item->regional_category))
															[
																{{ $item->regionalCategoryUnion }}
															]
														@endif
														{{ $item->newsLabel}}</a>
													@else
														<a href="{{url('/info')}}/detail{{ $item->id}}">
														@if(isset($item->regional_category))
															[
																{{ $item->regionalCategoryUnion }}
															]
														@endif
														{{ $item->newsLabel }}</a>
													@endif
												</td>
											</tr>
										@endforeach
									</table>
									{{ $locals->appends(['tab' => '地域ニュース'])
											->links('vendor.pagination.front_office') }}
								</div>
								@if($pageDefaults['tab'] == '地域ニュース' && Request::get('page') >= 2)
									<div class="js-div-base_pagination" style="display:none;">
										@include('partials.news_table',  ['tab_class' => 'btn-purple-fill'])
									</div>
								@endif
							</div>
							<div id="event" class="info__panel panel open" data-id="event">
								<div class="js-div-current_pagination">
									<table class="panel__table">
										@foreach($events as $item)
											<tr style="display: block;">
												<td>
													{{ date("Y.m.d", strtotime($item->created_at))}}
												</td>
												<td>
													<div class="btn btn-pink-fill disabled">イベント</div>
												</td>
												<td>
																								@if($item->is_link)
													<a href="{{url('/')}}/{{ $item->media->path }}" target="_blank">{{ $item->newsLabel}}</a>
													@else
													<a href="{{url('/info')}}/detail{{ $item->id}}">{{ $item->newsLabel}}</a>
													@endif
													
												</td>
												<td>
												@if(  $item->validity && strtotime( $item->validity) < strtotime('NOW'))
													<div class="btn btn-red-fill">開催終了</div>
												@endif
											</td>
											</tr>
										@endforeach
									</table>
									{{ $events->appends(['tab' => 'イベント'])
											->links('vendor.pagination.front_office') }}
								</div>
								@if($pageDefaults['tab'] == 'イベント' && Request::get('page') >= 2)
									<div class="js-div-base_pagination" style="display:none;">
										@include('partials.news_table',  ['tab_class' => 'btn-pink-fill'])
									</div>
								@endif
							</div>
						</div>
					</div>

				</div>
			</div>
		</section><!-- info__content -->

@endsection
