@extends('layouts.front')
@section('title', '外部通報窓口')
@section('mainClass', 'child compliance')
@section('content')
	
	@include('partials.compliance_mail_sent')
	
@endsection
