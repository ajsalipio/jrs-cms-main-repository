@extends('layouts.front')
@section('title', '外部通報窓口')
@section('mainClass', 'child compliance')
@section('content')
	<section class="child__kv">
		<div class="container">
			<div class="kv__container">
				<img src="{{ asset('front_images/images/child/kvisual/kv_default_02.jpg')}}" alt="">
				<h1 class="kv__title">外部通報窓口</h1>
			</div>
			<div class="breadcrumbs__container">
				<ul>
					<li><a href="../">TOP</a></li>
					<li class="active"><a href="./">外部通報窓口</a></li>
				</ul>
			</div>
		</div>
	</section> <!-- .child__kv -->

	<section class="compliance__container">
			<div class="container">
				<p>弊社では、コンプライアンス推進を目的として、公益通報者保護法その他関係法令の趣旨に則り、外部通報制度を導入しております。<br>　弊社とお取引関係のある法人・企業・団体等にお勤めの方が、弊社役員又は社員の企業コンプライアンスに反する行為や、そのおそれのある行為を認められた場合は、下記窓口（ＪＲ貨物外部通報窓口）までご連絡を賜りますようお願い申し上げます。</p>
				<p>■ 外部通報窓口をご利用いただける方<br>
					　弊社とお取引関係のある法人・企業・団体等にお勤めの方<br>　（過去に在籍されていた方を含む）</p>
				<p>■ご通報いただける内容<br>弊社の役員又は社員の企業コンプライアンスに反する行為、又はそのおそれのある行為（原則として、公益通報者保護法第2条第3項に規定する「通報対象事実」に限ります。<br>外部通報窓口では、これ以外のコンプライアンスと直接関係のない事柄（弊社に対するご意見、お問い合わせ、ご苦情など）についてはお受けいたしかねます。これらにつきましては、「ご意見・お問い合わせ」フォームからご連絡ください。）</p>
				<p>■ご通報方法<br>　このホームページ上の専用フォームに通報内容及び必要事項をご記入のうえご送信いただくか、これらを所定の様式に記載のうえ下記窓口へご郵送ください。<br>　※匿名でも承りますが、お名前、お取引会社名及びご連絡先をお伝えいただけない場合は、調査等の対応に限界がありますことを予めご了承ください。</p>
				<p>ＪＲ貨物外部通報窓口<br>　　 〒151-0051 <br>　　 　東京都渋谷区千駄ヶ谷5-33-8 サウスゲート新宿<br>　　 　日本貨物鉄道株式会社 コンプライアンス・法務室 内<br>　　 　ＪＲ貨物外部通報窓口</p>
				<p>○ご郵送いただく場合の書式<br><a href="./assets/files/compliance_pdf_01.pdf" target="_blank">外部通報記入用紙 PDF</a>　　 　(PDF/104KB)</p>
				<p>■ ご通報後の弊社の対応<br>ご通報いただきました事柄は、事実関係等をできる限り調査したうえで、コンプライアンス違反の事実が認められた場合は必要な是正措置を行います（調査等に際し、弊社担当部署（コンプライアンス・法務室）から通報者ご本人にご連絡を差し上げる場合があります）。<br>なお、通報者が希望される場合は、通報者ご本人に対し調査結果をご連絡いたしますが、お名前、お取引会社名及びご連絡先をお伝えいただけないときは、調査結果をお伝えできかねます。また、その他の場合でも、調査結果をお伝え出来ないことがありますので、予めご了承ください。</p>
				<p>■ その他<br>・ 通報者に関する情報や通報いただいた内容は、弊社の規程に則り厳正に管理いたします。弊社におけるコンプライアンス違反の調査、対応以外の用途に使用したり、通報者の同意を得ずに他に開示したりすることはありません（法令等により開示を求められる場合を除く）。<br>・ 通報したことにより、通報者が弊社から不利益な取扱いを受けることはありません。</p>
				<a href="{{ url('compliance/form') }}" class="btn-primary-fill">外部通報の専用フォームへ　＞</a>
			</div>
	</section> <!-- .compliance__container [02] -->
	<a class="back-to-top"  href="#top">
		<span>TOPに戻る</span>
	</a>
@endsection
