@extends('layouts.front')
@section('title', '外部通報窓口')
@section('mainClass', 'child compliance')
@section('content')
	<section class="child__kv">
		<div class="container">
			<div class="kv__container">
				<img src="{{ asset('front_images/images/child/kvisual/kv_default_02.jpg')}}" alt="">
				<h1 class="kv__title">外部通報窓口</h1>
			</div>
			<div class="breadcrumbs__container">
				<ul>
					<li><a href="../">TOP</a></li>
					<li class="active"><a href="./">外部通報窓口</a></li>
				</ul>
			</div>
		</div>
	</section> <!-- .child__kv -->

	<section class="compliance__container">
		<section class="compliance__container">
			<div class="container">
			<h2>[お願い]</h2>
				<p>以下、半角カタカナは使用しないで下さい。<br>
				※は必ずご記入下さい(ご記入いただきませんとメッセージが届きません)。</p>
			</div>
		</section><!-- .compliance__container [01] -->

		<section class="compliance__container">
			<div class="container">
				<form action="{{ url('compliance/send') }}" method="POST">
					@csrf
					<div class="btn__container">
						<table>
							<tbody>
								<tr><th>お名前</th>
									<td style="text-align:left;">
										{{ $compliance['お名前'] }} 
										<input type="hidden" name="お名前" value="{{ $compliance['お名前'] }} ">
									</td>
								</tr>
								<tr>
									<th>お取引会社名部署名</th>
									<td style="text-align:left;">
										{{ $compliance['お取引会社名部署名'] }}
										<input type="hidden" name="お取引会社名部署名" value="{{ $compliance['お取引会社名部署名'] }} ">
									</td>
								</tr>
								<tr>
									<th>電話番号</th>
									<td style="text-align:left;">
										{{ $compliance['電話番号'] }}
										<input type="hidden" name="電話番号" value="{{ $compliance['電話番号'] }} ">
									</td>
								</tr>
								<tr>
									<th>電子メールアドレス</th>
									<td style="text-align:left;">
										{{ $compliance['電子メールアドレス'] }}
										<input type="hidden" name="電子メールアドレス" value="{{ $compliance['電子メールアドレス'] }} ">
									</td>
								</tr>
								<tr>
									<th>お問い合わせ内容</th>
									<td style="text-align:left;">
										{!! nl2br($inquiry['通報内容']) !!}
										<input type="hidden" name="通報内容" value="{{ $compliance['通報内容'] }} ">
									</td>
								</tr>
							</tbody>
						</table>
						<br><br>
						<div class="btn__content">
							<input type="submit" class="btn-primary-fill" value="　送信する　">
							<input type="button" class="btn-primary-fill" value="前画面に戻る" onClick="history.back()">
						</div>
					</div>
				</form>
			</div>
		</section> <!-- .compliance__container [02] -->
	
	</section> <!-- .compliance__container [02] -->
	<a class="back-to-top"  href="#top">
		<span>TOPに戻る</span>
	</a>
@endsection
