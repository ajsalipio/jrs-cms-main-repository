@extends('layouts.front')
@section('title', '外部通報窓口')
@section('mainClass', 'child compliance')
@section('content')
	<section class="child__kv">
		<div class="container">
			<div class="kv__container">
				<img src="{{ asset('front_images/images/child/kvisual/kv_default_02.jpg')}}" alt="">
				<h1 class="kv__title">外部通報窓口</h1>
			</div>
			<div class="breadcrumbs__container">
				<ul>
					<li><a href="../">TOP</a></li>
					<li class="active"><a href="./">外部通報窓口</a></li>
				</ul>
			</div>
		</div>
	</section> <!-- .child__kv -->
	
	<section class="compliance__container">
		<div class="container">
			<h2>[お願い]</h2>
				<p>以下、半角カタカナは使用しないで下さい。<br>
				※は必ずご記入下さい(ご記入いただきませんとメッセージが届きません)。</p>
		</div>
	</section><!-- .compliance__container [01] -->


	<section class="compliance__container">
			<div class="container">
				<form action="{{ url('./finish') }}" method="post" accept-charset="utf-8">
					@csrf
					<table>
						<tbody>
							<tr>
								<th>※ お名前</th>
								<td>
									<input type="text" name="お名前" value="" required>
									<p>※ 匿名をご希望の場合は、「匿名」とご記入ください。</p>
								</td>
							</tr>
							<tr>
								<th>※ お取引会社名・部署名</th>
								<td><input type="text" name="お取引会社名部署名" required></td>
							</tr>
							<tr>
								<th>※ 電話番号</th>
								<td><input type="text" class="js-pos_code" name="電話番号" value="" placeholder=""
								required></td>
							</tr>
							<tr>
								<th>※ 電子メールアドレス</th>
								<td><input type="email" name="電子メールアドレス" value=""
								placeholder="" required></td>
							</tr>
							<tr>
								<th>※ 通報内容</th>
								<td>
									<textarea name="通報内容" id="" cols="30" rows="10" required></textarea>
									<p>※ 調査結果の連絡をご希望の場合は、あわせてその旨をご記入ください。<br>（お名前、お取引会社名及びご連絡先（電話番号又は電子メールアドレス）<br>をお伝えいただけない場合は、調査結果をお伝えできかねます）</p>
								</td>
							</tr>
						</tbody>
					</table>
					<div class="btn__container">
						<div class="btn__content">
							<input class="confirm btn-primary-fill" type="submit" name="" value="確認する">
						</div>
					</div>
				</form>
			</div>
		</section> <!-- .compliance__container [02] -->
	</section> <!-- .compliance__container [02] -->
	<a class="back-to-top"  href="#top">
		<span>TOPに戻る</span>
	</a>
@endsection
@section('scripts')
	<script>
		$('.js-pos_code').on('keypress', function(e){
			return e.metaKey || // cmd/ctrl
				e.which <= 0 || // arrow keys
				e.which == 8 || // delete key
				/[0-9]/.test(String.fromCharCode(e.which)); // numbers
		})	
	</script>
@stop
