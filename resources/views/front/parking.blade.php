@extends('layouts.front')
@section('title', $headTitle)
@section('mainClass', 'child about relation')
@section('content')

	<section class="child__kv" style="margin-bottom:56px">
		<div class="container">
			<div class="kv__container">
				<img src="{{url('front_images/images/child/kvisual/kv_default_03.jpg')}}" alt="visual"/>
				<h1 class="kv__title">駐車場のご案内</h1>
			</div>
			<div class="breadcrumbs__container">
				<ul>
					<li><a href="{{ url('/')}}">TOP</a></li>
					<li><a href="{{url('/relation')}}">関連事業一覧 </a></li>
					<li class="active"><a href="#">駐車場のご案内</a></li>
				</ul>
			</div>
		</div>
	</section>
<!-- .child__kv -->

	@if( isset($page) )
	{!! $page->page_content !!}
	@endif
@endsection
@section('scripts')
@endsection()

