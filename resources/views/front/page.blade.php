@extends('layouts.front')

@section('content')
page preview /front
@if( isset($page) )
{!! $page->page_content !!}
@else
<div>No default page!
</div>
@endif
@endsection
