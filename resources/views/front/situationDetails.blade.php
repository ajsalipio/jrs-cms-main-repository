@extends('layouts.front')
@section('title', $headAdminTitle)
@section('mainClass', 'child i_daiya')
@section('content')
	<section class="child__kv">
		<div class="container">
			<div class="breadcrumbs__container">
				<ul>
					<li><a href="{{url('/')}}">TOP</a></li>
					<li class="active"><a href="#">現在の輸送状況</a></li>
				</ul>
				<br>
				<p><a href="{{url('/i_daiya/service')}}">輸送情報の掲示について</a><br> {{ \App\Utilities\JapaneseCalendarUtility::convertGreToJpn(date("Y"), 2, 1) . date("m月d日") }}</p>
			</div>
			<div class="kv__container type2">
				<!-- <img src="{{asset('front_images/images/child/kvisual/i_daiya.jpg')}}" alt=""> -->
				<span class="kv__text--fl">お客様各位</span>
				<span class="kv__text--fr">日本貨物鉄道株式会社<br>輸送状況統括責任者</span>
				<h1 class="kv__title">現在の輸送状況</h1>
			</div>
		</div>
	</section> <!-- .child__kv -->

	<section class="template__content i_daiya__content">
		
		<div class="container">
			<div class="content__container">
				<h3>いつもＪＲ貨物をご利用いただき、誠にありがとうございます。<br>現在、下記の内容により貨物列車に遅れ・その他運行の支障が生じております。<br>ご迷惑をおかけして誠に申し訳ございません。</h3>
			</div>
			
			<div class="header__title">
				標題
			</div>
			<div class="content__container">
				北海道、中国、九州地区での輸送障害に伴う列車遅延について
			</div>

			@if($situation['situation_status'] != 0 && $isOffHours)
				<div class="header__title">
					標　　題

				</div>
				<div class="content__container">
					<!-- {{ $situation['situation_title'] }} -->
					{{ config('constants.PUBLISH_TIME_OUT_OF_SERVICE.TEXT') }}
				</div>
			@endif
		</div>

	</section><!-- i_daiya__content -->

	@if($situation['situation_status'] == 0 || !$isOffHours)
		<section class="template__content i_daiya__content">
			<div class="container">
				<div class="header__title">
					発生時刻・概要
				</div>
				<div class="content__container">
					{!! nl2br($situation['situation_content'] ?? '') !!} 
				</div>
			</div>
		</section><!-- i_daiya__content -->

		<section class="template__content i_daiya__content">
			<div class="container">
				<div class="header__title">
					線　　区
				</div>
				<div class="content__container">
					{!! nl2br($situation['situation_details'] ?? '') !!} 
				</div>
			</div>
		</section><!-- i_daiya__content -->
		<section class="template__content i_daiya__content">
		<div class="div-files">
			<br/>
			@if(isset($situation['id']))
				@if($situation->fileXls)
					<a href="{{ asset($situation->fileXls['path']) }}" id="link-file_xls" download="{{ $situation->fileXls['original'] }}"> 
						{{ $situation->fileXls['original'] }}
					</a>
				<br/>
				@endif
				@if( $situation->fileDoc)
						 <a href="{{ asset($situation->fileDoc['path']) }}" id="link-file_doc" download="{{ $situation->fileDoc['original'] }}"> 
							{{ $situation->fileDoc['original'] }}
						</a>
					<br/>
				@endif

				@if($filePics)
					@foreach($filePics as $filePic)                                           
						<div class="">
					<img src="{{ asset( $filePic['path']) }}" alt="{{ $filePic['original'] }}" />
						</div>
					@endforeach
				@endif
			@endif
		</div>
		</section><!-- i_daiya__content -->
	@else
		<section class="template__content i_daiya__content">
				 {{ config('constants.SITUATION_STATUS')[$situation['situation_status']] }} 
		</section>
	@endif
			<a class="back-to-top"  href="#top">
			<span>TOP<br class="sp">に戻る</span>
		</a>
@endsection
@section('scripts')
@endsection()

