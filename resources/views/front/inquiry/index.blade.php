@extends('layouts.front')
@section('title', 'お問い合わせ')
@section('mainClass', 'child compliance')
@section('content')
	<section class="child__kv">
		<div class="container">
			<div class="kv__container">
				<img src="{{ asset('front_images/images/child/kvisual/kv_default_02.jpg')}}" alt="">
				<h1 class="kv__title">お問い合わせ</h1>
			</div>
			<div class="breadcrumbs__container">
				<ul>
					<li><a href="../">TOP</a></li>
					<li class="active"><a href="./">お問い合わせ</a></li>
				</ul>
			</div>
		</div>
	</section> <!-- .child__kv -->

	<section class="compliance__container">
		<div class="container">
			<h2>[ご注意]</h2>
			<ul>
				<li>いただいたご意見等の内容によりましては、調査等により回答までお時間をいただく場合がございます。</li>
				<li>いただいたご意見等、すべてにお返事ができない場合がございます。</li>
				<li>弊社へのご意見以外（商品セールス等）の内容を送信することは、固くお断りいたします。</li>
				<li>弊社より回答させていただいた内容は、お客さまからの特定のご質問・ご意見にお答えするものでございますので、回答内容の一部または全ての内容を転用したり、二次利用することはご遠慮ください。</li>
				<li>いただいたご意見等の内容によりましては、必要に応じて関係するＪＲ貨物関係会社よりご回答させていただく場合がございます。</li>
			</ul>
		</div>
	</section><!-- .compliance__container [01] -->

	<section class="compliance__container">
		<div class="container">
			<p>以下、半角カタカナは使用しないで下さい。<br>※は必ずご記入下さい(ご記入いただきませんとメッセージが届きません)。</p>
			<form action="{{ url('inquiry/confirm') }}" method="post" accept-charset="utf-8">
				@csrf
				<table>
					<tbody>
						<tr>
							<th>※ ご意見・お問い合わせの内容について<br> (以下から選択ください)</th>
							<td>
								<div><input type="radio" name="ご意見" checked value="鉄道貨物輸送のお問い合わせ・お見積り" required>鉄道貨物輸送のお問い合わせ・お見積り</div>
								<div><input type="radio" name="ご意見" value="関連事業" required>関連事業 </div>
								<div><input type="radio" name="ご意見" value=" 安全について " required>安全について</div>
								<div><input type="radio" name="ご意見" value=" その他 " required>その他</div>
							</td>
						</tr>
						<tr>
							<th>※ JR貨物についてのご意見・お問い合わせ内容をご記入ください。</th>
							<td>
								<textarea name="通報内容" id="" cols="30" rows="10" required></textarea>
							</td>
						</tr>
						<tr>
							<th>※ ご住所</th>
							<td>
								<div>郵便番号 <input type="text" class="input--small js-pos_code" name="郵便番号_01" required maxlength="3"> - <input type="text" class="input--small js-pos_code" name="郵便番号_02" required maxlength="4"></div>
								<div>
									<select name="pref_id" required>
										<option value="" selected>都道府県</option>
										@foreach(config('constants.PREFECTURES') as $key => $value)
											<option value="{{ $value }}"> {{ $value }} </option>										
										@endforeach
									</select>
								</div>
								<input type="text" class="margin--top" name="郵便番号_03" required>
							</td>
						</tr>
						<tr>
							<th>職業(会社名・部署名)</th>
							<td><input type="text" name="職業" value="" placeholder=""></td>
						</tr>
						<tr>
							<th>※ お名前</th>
							<td><input type="text" name="お名前" required></td>
						</tr>
						<tr>
							<th>※ 電話番号</th>
							<td><input type="text" name="電話番号" required></td>
						</tr>
						<tr>
							<th>FAX番号</th>
							<td><input type="text" name="FAX番号"></td>
						</tr>
						<tr>
							<th>※ E-mail</th>
							<td><input type="email" required name="email"></td>
						</tr>
					</tbody>
				</table>
				<div class="btn__container">
					<div class="btn__content">
						<input class="confirm btn-primary-fill" type="submit" name="" value="確認する">
					</div>
				</div>
			</form>
		</div>
	</section> <!-- .compliance__container [02] -->
	<a class="back-to-top"  href="#top">
		<span>TOPに戻る</span>
	</a>
@endsection

@section('scripts')
	<script>
		$('.js-pos_code').on('keypress', function(e){
			return e.metaKey || // cmd/ctrl
				e.which <= 0 || // arrow keys
				e.which == 8 || // delete key
				/[0-9]/.test(String.fromCharCode(e.which)); // numbers
		})	
	</script>
@stop
