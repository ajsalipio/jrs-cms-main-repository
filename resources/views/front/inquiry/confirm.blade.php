@extends('layouts.front')
@section('title', 'お問い合わせ')
@section('mainClass', 'child compliance')
@section('content')
	<section class="child__kv">
		<div class="container">
			<div class="kv__container">
				<img src="{{ asset('front_images/images/child/kvisual/kv_default_02.jpg')}}" alt="">
				<h1 class="kv__title">お問い合わせ</h1>
			</div>
			<div class="breadcrumbs__container">
				<ul>
					<li><a href="../">TOP</a></li>
					<li class="active"><a href="./">お問い合わせ</a></li>
				</ul>
			</div>
		</div>
	</section> <!-- .child__kv -->

	<section class="compliance__container">
			<div class="container">
				<h2>[お願い]</h2>
				<p>以下、半角カタカナは使用しないで下さい。<br>※は必ずご記入下さい(ご記入いただきませんとメッセージが届きません)。</p>
			</div>
		</section><!-- .compliance__container [01] -->

		<section class="compliance__container">
			<div class="container">
				<form action="{{ url('inquiry/send') }}" method="POST">
					@csrf
					<table>
						<tbody>
							<tr>
								<th>ご意見</th>
								<td>
									{{ $inquiry['ご意見'] }}
									<input type="hidden" name="ご意見" value="{{ $inquiry['ご意見'] }}">
								</td>
							</tr>
							<tr>
								<th>お問い合わせ内容</th>
								<td>
									{!! nl2br($inquiry['通報内容']) !!}
									<input type="hidden" name="通報内容" value="{{ $inquiry['通報内容'] }}">
								</td>
							</tr>
							<tr>
								<th>郵便番号</th>
								<td>
									{{ $inquiry['郵便番号_01'] }}
									-
									{{ $inquiry['郵便番号_02'] }}
									<input type="hidden" name="郵便番号" value="{{ $inquiry['郵便番号_01'] . '-' . $inquiry['郵便番号_02'] }}">
								</td>
							</tr>
							<tr>
								<th>住所</th>
								<td>
									{{ $inquiry['pref_id'] }}
									<input type="hidden" name="pref_id" value="{{ $inquiry['pref_id'] }}"><br>
									{{ $inquiry['郵便番号_03'] }}
									<input type="hidden" name="住所" value="{{ $inquiry['郵便番号_03'] }}">
								</td>
							</tr>
							<tr>
								<th>職業</th>
								<td>
									{{ $inquiry['職業'] }}
									<input type="hidden" name="職業" value="{{ $inquiry['職業'] }}">
								</td>
							</tr>
							<tr>
								<th>お名前</th>
								<td>
									{{ $inquiry['お名前'] }}
									<input type="hidden" name="お名前" value="{{ $inquiry['お名前'] }}">
								</td>
							</tr>
							<tr>
								<th>電話番号</th>
								<td>
									{{ $inquiry['電話番号'] }}
									<input type="hidden" name="電話番号" value="{{ $inquiry['電話番号'] }}">
								</td>
							</tr>
							<tr>
								<th>FAX番号</th>
								<td>
									{{ $inquiry['FAX番号'] }}
									<input type="hidden" name="FAX番号" value="{{ $inquiry['FAX番号'] }}">
								</td>
							</tr>
							<tr>
								<th>E-mail</th>
								<td>
									{{ $inquiry['email'] }}
									<input type="hidden" name="email" value="{{ $inquiry['email'] }}">
								</td>
							</tr>
						</tbody>
					</table>
					<div class="btn__container">
						<div class="btn__content">
							<input type="submit" class="btn-primary-fill" value="　送信する　">
							<input type="button" class="btn-primary-fill" value="前画面に戻る" onClick="history.back()">
						</div>
					</div>
				</form>
			</div>
		</section> <!-- .compliance__container [02] -->
	
	<a class="back-to-top"  href="#top">
		<span>TOPに戻る</span>
	</a>
@endsection