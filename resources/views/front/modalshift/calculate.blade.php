@extends('layouts.front')
@section('title', 'エネルギー使用量・CO2排出計算シート | JR貨物')
@section('mainClass', 'child compliance modalshift')
@section('content')
	<section class="child__kv">
        <div class="container">
            <div class="kv__container">
                <img src="{{ asset('front_images/images/child/kvisual/kv_default_05.jpg') }}" alt="">
                <h1 class="kv__title">エネルギー使用量・CO2排出計算シート</h1>
            </div>
            <div class="breadcrumbs__container">
                <ul>
                    <li><a href="../">TOP</a></li>
                    <li class="active"><a href="./">エネルギー使用量・CO2排出シート</a></li>
                </ul>
            </div>
        </div>
    </section> <!-- .child__kv -->
    
    <form id="form-calculate" action="{{ url('modalshift/calculate/finish') }}" method="post" accept-charset="utf-8" class="">
        @csrf
        <input type="hidden" id="start_station_name" name="start_station_name" />
        <input type="hidden" id="dest_station_name"  name="dest_station_name" />
        
        <section class="compliance__container">
            <div class="container">
                <div class="terms__container">
                    <div class="terms__content">
                        <h2>ご利用上の注意</h2>
                        <ul>
                            <li>輸送区間及び貨物の重量を入力することにより、「エネルギーの使用の合理化に関する法律（以下「省エネ法」と表記）」上の「トンキロ法」に基づく、エネルギー使用量、CO2排出量及び原油換算量を算出します。 
                            </li>
                            <li>このホームページでご案内する輸送距離は、省エネ法に基づくエネルギー使用量、CO2排出量計算のための参考数値として表示しています。 </li>
                            <li>このホームページでご案内する、「集配区間（トラック）」のエネルギー使用量、CO2排出量等は、鉄道利用の際の参考数値として表示しており、法令・告示に基づく計算結果と一致しない場合があります。算定の前提やその導出過程の詳細については、経済産業省資源エネルギー庁ホームページ（http://www.enecho.meti.go.jp/category/saving_and_new/saving/ninushi/index.html「省エネ法（荷主に係る措置）について」）等をご参照下さい。 </li>
                            <li>詳しくは、弊社営業窓口等にお問い合わせください。</li>
                        </ul>
                        <p>※エネルギー使用量・CO2排出量計算ページをご利用いただく場合は、Internet Explorer6 SP1 以上でご覧いただくことを推奨いたします。 (他のブラウザでご覧になった場合、一部コンテンツの表示に不具合が生じることがあります。)</p>
                    </div>
                    <div class="terms__btn">
                        <label class="cb_container">個人情報の取扱いについて同意する
                            <input type="checkbox" id="acceptTerms" required>
                            <span class="checkmark"></span>
                        </label>
                    </div>
                </div>

            </div>
        </section><!-- .compliance__container [01] -->

        <section class="compliance__container ">
            <div class="container">                
                <div class="calculate__form">
                    <div class="form__overlay"></div>
                    <table>
                        <tbody>
                            <tr>
                                <th>貨物の重量</th>
                                <td>
                                    <input type="text" name="weight" maxlength="10" class="js-txt-num" required>
                                    <label>トン (必須入力・最大10桁)</label>
                                    <p>※参考 JR12ftコンテナ1個当りの積載重量は5tです。</p>
                                </td>
                            </tr>
                            <tr>
                                <th colspan="2">【集貨】</th>
                            </tr>
                            <tr>
                                <th>集貨距離</th>
                                <td>
                                    <input type="text" name="collection_distance" maxlength="4" placeholder="" class="js-txt-num" required>
                                    <label>km</label>
                                    <p>集貨場所から貨物駅までの距離を入力します。</p>
                                </td>
                            </tr>
                            <tr>
                                <th>着駅</th>
                                <td>
                                    <div class="form__field">
                                        <div class="form__field__item">
                                            <input type="radio" name="collection_type" value="truck" class="js-rb-required" required>
                                            <label>普通トラック 最大積載量</label>
                                            <input type="text" name="collection_max_load" maxlength="4" class="js-txt-num"  placeholder="">
                                        </div>
                                        <div class="form__field__item">
                                            <label>トン 積載率</label>
                                            <!-- <div class="range_input__container">
                                                <input type="text" name="collection_rail" value="0%">
                                                <div class="range__btn">
                                                    <div class="range__btn--up"></div>
                                                    <div class="range__btn--down"></div>
                                                </div>
                                            </div> -->
                                            <div class="dropdown_container">
                                                <select name="collection_loading_ratio">
                                                    <option selected="selected" hidden disabled></option>  
                                                    @foreach(config('calculator.TRUCK_LOADING_RATES') as $key => $value)
                                                        <option value="{{ $key }}">{{ $value }}</option>
                                                    @endforeach
                                                </select>
                                                <div class="option__btn">
                                                    <div class="option__btn--up"></div>
                                                    <div class="option__btn--down"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form__field">
                                        <div class="form__field__item">
                                            <input type="radio" name="collection_type" value="rail" class="js-rb-required" required>
                                            <label>鉄道コンテナ用トラック</label>
                                                <div class="dropdown_container">
                                                <select name="collection_container_max_load">
                                                    <!-- <option selected="selected" hidden disabled></option>   -->
                                                    @foreach(config('calculator.RAIL_TYPES') as $key => $value)
                                                        <option value="{{ $key }}">{{ $value }}</option>
                                                    @endforeach
                                                </select>
                                                <div class="option__btn">
                                                    <div class="option__btn--up"></div>
                                                    <div class="option__btn--down"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <p>「普通トラック」又は「鉄道コンテナ用トラック」を選択します。「普通トラック」を選択した場合、「最大積載量」及び「積載率」を入力します。「鉄道コンテナ用トラック」を選択した場合、『1個積み（8t）』又は『2個積み以上（15t以上）』のいずれかを選択します。</p>
                                </td>
                            </tr>
                            <tr>
                                <th colspan="2">【鉄道輸送】</th>
                            </tr>
                            <tr>
                                <th>発駅</th>
                                <td>
                                    <div class="row__container">
                                        <div class="col">
                                            <div class="form__field">
                                                <div class="form__field__item">
                                                    <!-- <input type="radio" name="rad2" value=""> -->
                                                    <label>都道府県</label>
                                                    <div class="dropdown_container">
                                                        <select id="dd-start_prefecture" name="start_prefecture" class="js-dd-required">
                                                            <option selected="selected" hidden disabled>都道府県</option>
                                                            @foreach(config('calculator.PREFECTURES') as $key => $value))
                                                                <option value="{{ $key+1 }}"> {{ $value }} </option>
                                                            @endforeach
                                                        </select>
                                                        <div class="option__btn">
                                                            <div class="option__btn--up"></div>
                                                            <div class="option__btn--down"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form__field">
                                                <div class="form__field__item">
                                                    <!-- <input type="radio" name="rad2" value=""> -->
                                                    <label>駅名</label>
                                                    <div class="dropdown_container">
                                                        <select id="dd-start_station" name="start_station" class="js-dd-sub_required">
                                                            <option selected="selected" hidden disabled>駅名</option>
                                                        </select>
                                                        <div class="option__btn">
                                                            <div class="option__btn--up"></div>
                                                            <div class="option__btn--down"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <p>集貨場所から貨物駅までの距離を入力します。</p>
                                </td>
                            </tr>
                            <tr>
                                <th>発駅</th>
                                <td>
                                    <div class="row__container">
                                        <div class="col">
                                            <div class="form__field">
                                                <div class="form__field__item">
                                                    <!-- <input type="radio" name="rad3" value=""> -->
                                                    <label>都道府県</label>
                                                    <div class="dropdown_container">
                                                        <select id="dd-dest_prefecture" name="dest_prefecture" class="js-dd-required">
                                                            <option selected="selected" hidden disabled>都道府県</option>
                                                            @foreach(config('calculator.PREFECTURES') as $key => $value))
                                                                <option value="{{ $key+1 }}"> {{ $value }} </option>
                                                            @endforeach
                                                        </select>
                                                        <div class="option__btn">
                                                            <div class="option__btn--up"></div>
                                                            <div class="option__btn--down"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form__field">
                                                <div class="form__field__item">
                                                    <!-- <input type="radio" name="rad3" value=""> -->
                                                    <label>駅名</label>
                                                    <div class="dropdown_container">
                                                        <select id="dd-dest_station" name="dest_station" class="js-dd-sub_required">
                                                            <option selected="selected" hidden disabled>駅名</option>
                                                        </select>
                                                        <div class="option__btn">
                                                            <div class="option__btn--up"></div>
                                                            <div class="option__btn--down"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <p>集貨場所から貨物駅までの距離を入力します。</p>
                                </td>
                            </tr>
                            <tr>
                                <th colspan="2">【配達】</th>
                            </tr>
                            <tr>
                                <th>集貨距離</th>
                                <td>
                                    <input type="text" name="delivery_distance" maxlength="4" class="js-txt-num" placeholder="">
                                    <label>km</label>
                                </td>
                            </tr>
                            <tr>
                                <th>利用トラック</th>
                                <td>
                                    <div class="form__field">
                                        <div class="form__field__item">
                                            <input type="radio" name="delivery_type" value="truck" class="js-rb-required js-rb-delivery">
                                            <label>普通トラック 最大積載量</label>
                                            <input type="text" name="delivery_max_load" maxlength="2" class="js-txt-num" placeholder="">
                                        </div>
                                        <div class="form__field__item">
                                            <label>トン 積載率</label>
                                            <!-- <div class="range_input__container">
                                                <input type="text" name="" value="0">
                                                <div class="range__btn">
                                                    <div class="range__btn--up"></div>
                                                    <div class="range__btn--down"></div>
                                                </div>
                                            </div> -->
                                            <div class="dropdown_container">
                                                <select name="delivery_loading_ratio">
                                                    <option selected="selected" hidden disabled></option>  
                                                    @foreach(config('calculator.TRUCK_LOADING_RATES') as $key => $value)
                                                        <option value="{{ $key }}">{{ $value }}</option>
                                                    @endforeach
                                                </select>
                                                <div class="option__btn">
                                                    <div class="option__btn--up"></div>
                                                    <div class="option__btn--down"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form__field">
                                        <div class="form__field__item">
                                            <input type="radio" name="delivery_type" value="rail" class="js-rb-required js-rb-delivery">
                                            <label>鉄道コンテナ用トラック</label>
                                            <div class="dropdown_container">
                                                <select name="delivery_container_max_load">
                                                    <!-- <option selected="selected" hidden disabled></option> -->
                                                    @foreach(config('calculator.RAIL_TYPES') as $key => $value)
                                                        <option value="{{ $key }}">{{ $value }}</option>
                                                    @endforeach
                                                </select>
                                                <div class="option__btn">
                                                    <div class="option__btn--up"></div>
                                                    <div class="option__btn--down"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <p>「集貨」の例により入力します。</p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="btn__container">
                    <div class="btn__content">
                        <button id="btn-restart" type="reset"  class="btn-primary">リセット</button>
                        <button type="submit" class="btn-primary">送信</button>
                    </div>
                </div>
            </div>
        </section> <!-- .compliance__container [02] -->
    </form>

    <a class="back-to-top"  href="#top">
        <span>TOPに戻る</span>
    </a>
@endsection

@section('scripts')
    <script src="{{ asset('js/front/js/modalshift.js') }}"></script>
@endsection()