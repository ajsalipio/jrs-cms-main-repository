@extends('layouts.front')
@section('title', 'モーダルシフト | JR貨物')
@section('mainClass', 'child modalshift')
@section('content')
    <section class="child__kv">
        <div class="container">
            <div class="breadcrumbs__container">
                <ul>
                    <li><a href="{{ url('') }}">TOP</a></li>
                    <li class="active"><a href="{{ url('modalshift') }}">鉄道貨物の特長</a></li>
                </ul>
            </div>
            <div class="kv__container type2">
                <!-- <img src="{{ asset('front_images/images/child/kvisual/modalshift.jpg') }}" alt=""> -->
                <h1 class="kv__title">鉄道貨物の特長</h1>
            </div>
        </div>
    </section> <!-- .child__kv -->

    <section class="template__content modalshift__content">
        <div class="container">
            <div class="content__container">
                <div class="modalshift__writeup">
                    <h2>モーダルシフトとは</h2>
                    <p>モーダルシフトとは、輸送手段の転換のこと。<br>JR貨物は、大量性、定時性、環境性といった鉄道特性を生かし、鉄道へのモーダルシフトを推進しています。</p>
                </div>
                <div class="img_container">
                    <img src="{{ asset('front_images/images/child/modalshift/modalshift_pic_01.png') }}" alt="">
                </div>
            </div>
        </div>
    </section><!-- modalshift__content [01] -->

    <section class="template__content modalshift__content">
        <div class="container">

            <div class="header__title">
                鉄道貨物の特徴
            </div>

            <div class="content__container">
                <div class="header__content">
                    <div class="number__container">
                        <img src="{{ asset('front_images/images/common/txt_number01.png') }}" alt="">
                    </div>
                    <div class="title__container">
                        遠くまで一度に大量に運べる
                    </div>
                </div>
                <div class="details__content">
                    <p>貨物列車26両分は、10tトラック65台分。輸送効率が高いので、長距離になるほど輸送コストが低減できます。<br>また、一度に大量輸送が可能なので、長距離ドライバー不足の不安も解消します。</p>
                    <div class="img_wrap">
                        <img src="{{ asset('front_images/images/child/modalshift/modalshift_pic_02.png') }}" alt="" class="pc">
                        <img src="{{ asset('front_images/images/child/modalshift/modalshift_pic_02_sp.png') }}" alt="" class="sp">
                    </div>
                </div>
            </div><!-- content__container [01] -->

            <div class="content__container">
                <div class="header__content">
                    <div class="number__container">
                        <img src="{{ asset('front_images/images/common/txt_number02.png') }}" alt="">
                    </div>
                    <div class="title__container">
                        時間通りに運べる
                    </div>
                </div>
                <div class="details__content">
                    <p>貨物列車は、決められた走行速度と時刻表通りに正確な運行を行っています。<br>また、線路での輸送は渋滞等のトラブルもなく、計画的な出荷に最適な輸送手段と言えます。</p>
                    <div class="img_container">
                        <img src="{{ asset('front_images/images/child/modalshift/modalshift_pic_03.jpg') }}" alt="">
                    </div>
                </div>
            </div><!-- content__container [02] -->

            <div class="content__container">
                <div class="header__content">
                    <div class="number__container">
                        <img src="{{ asset('front_images/images/common/txt_number03.png') }}" alt="">
                    </div>
                    <div class="title__container">
                        日本全国にあるネットワーク
                    </div>
                </div>
                <div class="details__content">
                    <p>貨物駅は全国に約150ヶ所。1日約500本の貨物列車が、全国各地へ荷物を運んでいます。<br>走行距離はおよそ19万km、これは地球約５周分です。</p>
                    <div class="img_container">
                        <img src="{{ asset('front_images/images/child/modalshift/modalshift_pic_03.png') }}" alt="">
                    </div>
                </div>
            </div><!-- content__container [03] -->

            <div class="content__container">
                <div class="header__content">
                    <div class="number__container">
                        <img src="{{ asset('front_images/images/common/txt_number04.png') }}" alt="">
                    </div>
                    <div class="title__container">
                        ニーズに合わせて便利に使える
                    </div>
                </div>
                <div class="details__content">
                    <p>コンテナは１つからでもご利用可能。引っ越しなど個人でのご利用も増えています。<br>コンテナ輸送のほか、専用貨車で運ぶ「車扱輸送」もあり、どんな荷物にも対応します。<br>また片道輸送もできますから、無駄なコストがかかりません。</p>
                    <div class="img_container">
                        <img src="{{ asset('front_images/images/child/modalshift/modalshift_pic_04.jpg') }}" alt="">
                    </div>
                </div>
            </div><!-- content__container [04] -->

            <div class="content__container">
                <div class="header__content">
                    <div class="number__container">
                        <img src="{{ asset('front_images/images/common/txt_number05.png') }}" alt="">
                    </div>
                    <div class="title__container">
                        環境に優しい
                    </div>
                </div>
                <div class="details__content">
                    <div class="row__container">
                        <div class="col">
                            <p>地球温暖化の一因とされるCO2(二酸化炭素)。<br>
                            鉄道は、輸送単位当たりのCO2排出量がトラックの約11分の1と、様々な輸送機関の中で環境負荷が最も少ない輸送手段です。</p>
                        </div>
                        <div class="col">
                            <img src="{{ asset('front_images/images/child/modalshift/modalshift_pic_04.png') }}" alt="">
                        </div>
                    </div>
                    <div class="row__container">
                        <p>改正省エネ法が施行され、鉄道貨物輸送の活用がさらに注目されるようになりました。<br>改正省エネ法(荷主に係る措置)について詳しく<a href="./">はこちら</a> </p>
                    </div>
                    <div class="row__container">
                        <div class="col">
                            <img src="{{ asset('front_images/images/child/modalshift/modalshift_pic_05.png') }}" alt="">
                        </div>
                        <div class="col">
                            <p>エコレールマークは地球環境に優しい鉄道貨物輸送を積極的に利用している商品や企業がわかるマークです。</p>
                            <br><br>
                            <span>詳しくは</span><a href="https://www.rfa.or.jp/index.html" target="_blank">こちら</a>
                        </div>
                    </div>
                    <div class="row__container">
                        <div class="col">
                            <img src="{{ asset('front_images/images/child/modalshift/modalshift_pic_06.png') }}" alt="">
                        </div>
                        <div class="col">
                            <p>グリーン物流パートナーシップ会議は、CO2排出削減に向けた取り組みの拡大に向けて、荷主企業（発荷主・着荷主）と物流事業者が広く連携していくことを目的として設置されました。</p>
                            <br><br>
                            <span>詳しくは</span><a href="http://www.greenpartnership.jp/" target="_blank">こちら</a>
                        </div>
                    </div>
                    <!-- <a href="{{ url('modalshift/calculate') }}" class="btn btn-primary-fill">エネルギー使用量・CO2排出計算シート(フォーム)</a> -->
                </div>
            </div><!-- content__container [05] -->

        </div>
    </section><!-- modalshift__content [02] -->

    <a class="back-to-top"  href="#top">
        <span>TOP<br class="sp">に戻る</span>
    </a>
@endsection

@section('scripts')
    <script src="{{ asset('js/front/js/modalshift.js') }}"></script>
@endsection()