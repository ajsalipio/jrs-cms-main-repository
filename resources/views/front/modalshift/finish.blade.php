@extends('layouts.front')
@section('title', 'エネルギー使用量・CO2排出計算シート | JR貨物')
@section('mainClass', 'child compliance modalshift')
@section('content')
	<section class="child__kv">
        <div class="container">
            <div class="kv__container">
                <img src="../../front_images/images/child/kvisual/kv_default_05.jpg" alt="">
                <h1 class="kv__title">エネルギー使用量・CO2排出量計算シート</h1>
            </div>
            <div class="breadcrumbs__container">
                <ul>
                    <li><a href="../">TOP</a></li>
                    <li class="active"><a href="./">エネルギー使用量・CO2排出量計算シート</a></li>
                </ul>
            </div>
        </div>
    </section> <!-- .child__kv -->

    <section class="template__content success__content">
        <div class="container">
            <div class="content__container">
                <div class="table__template01">
                    <table>
                        <tbody>
                            <tr>
                                <td class="property_name">貨物の重量 </td>
                                <td  class="property_value">
                                    {{ $weight }}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="table__template01">
                    <table width="100%" border="0" cellpadding="5" cellspacing="1">
                        <tbody>
                            <tr class="form">
                                <th colspan="2">【集貨】</th>
                            </tr>
                            <tr>
                                <td class="property_name">集貨距離</td>
                                <td class="property_value"> {{ $collectionResults['distance'] }} km</td>
                            </tr>
                            <tr>
                                <td class="property_name"> エネルギー使用量</td>
                                <td class="property_value"> {{ $collectionResults['energy'] }} MJ（メガジュール）</td>
                            </tr>
                            <tr>
                                <td class="property_name">利用トラック</td>
                                <td class="property_value"> {{ $collectionResults['truck'] }}  </td>
                            </tr>
                            <tr>
                                <td class="property_name">CO<span class="co2_cap">2</span>排出量</td>
                                <td class="property_value"> {{ $collectionResults['co2'] }}t-CO<span class="co2_cap">2</span></td>
                            </tr>
                            <tr>
                                <td class="property_name">積載率</td>
                                <td class="property_value"> {{ $collectionResults['loading_ratio'] }} </td>
                            </tr>
                            <tr>
                                <td class="property_name">原油換算量</td>
                                <td class="property_value"> {{ $collectionResults['equivalent_oil'] }}リットル</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                @if($trainResults)
                     <div class="table__template01">
                        <table width="100%" border="0" cellpadding="5" cellspacing="1">
                            <tbody>
                                <tr class="form">
                                    <th colspan="2">【鉄道輸送】</th>
                                </tr>
                                 <tr>
                                    <td class="property_name"> 鉄道輸送区間 </td>
                                    <td class="property_value"> {{ $trainResults['transport'] }}</td>
                                </tr>
                                <tr>
                                    <td class="property_name"> エネルギー使用量 </td>
                                    <td class="property_value"> {{ $trainResults['energy'] }} MJ（メガジュール）</td>
                                </tr>
                                <tr>
                                    <td class="property_name"> 輸送距離 </td>
                                    <td class="property_value"> {{ $trainResults['distance'] }} km</td>
                                </tr>
                                <tr>
                                    <td class="property_name">CO<span class="co2_cap">2</span>排出量</td>
                                    <td class="property_value"> {{ $trainResults['co2'] }}t-CO<span class="co2_cap">2</span></td>
                                </tr>
                                <tr>
                                    <td class="property_name">原油換算量 </td>
                                    <td class="property_value"> {{ $trainResults['equivalent_oil'] }}リットル</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                @endif

                @if($deliveryResults)
                    <div class="table__template01">
                        <table width="100%" border="0" cellpadding="5" cellspacing="1">
                            <tbody>
                                <tr class="form">
                                    <th colspan="2">【配達】</th>
                                </tr>
                                <tr>
                                    <td class="property_name">配達距離</td>
                                    <td class="property_value"> {{ $deliveryResults['distance'] }} km</td>
                                </tr>
                                <tr>
                                    <td class="property_name"> エネルギー使用量</td>
                                    <td class="property_value"> {{ $deliveryResults['energy'] }} MJ（メガジュール）</td>
                                </tr>
                                <tr>
                                    <td class="property_name">利用トラック</td>
                                    <td class="property_value"> {{ $deliveryResults['truck'] }}  </td>
                                </tr>
                                <tr>
                                    <td class="property_name">CO<span class="co2_cap">2</span>排出量</td>
                                    <td class="property_value"> {{ $deliveryResults['co2'] }}t-CO<span class="co2_cap">2</span></td>
                                </tr>
                                <tr>
                                    <td class="property_name">積載率</td>
                                    <td class="property_value"> {{ $deliveryResults['loading_ratio'] }} </td>
                                </tr>
                                <tr>
                                    <td class="property_name">原油換算量</td>
                                    <td class="property_value"> {{ $deliveryResults['equivalent_oil'] }}リットル</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                @endif

                @if($totalResults)
                    <div class="table__template01">
                        <table width="100%" border="0" cellpadding="5" cellspacing="1">
                            <tbody>
                                <tr class="form">
                                    <th colspan="2">【鉄道輸送及び集配区間の合計】</th>
                                </tr>
                                <tr>
                                    <td class="property_name">合計輸送距離</td>
                                    <td class="property_value"> {{ $totalResults['distance'] }} km</td>
                                </tr>
                                <tr>
                                    <td class="property_name"> エネルギー使用量合計</td>
                                    <td class="property_value"> {{ $totalResults['energy'] }} MJ（メガジュール）</td>
                                </tr>
                                <tr>
                                    <td class="property_name">CO<span class="co2_cap">2</span>排出量</td>
                                    <td class="property_value"> {{ $totalResults['co2'] }}t-CO<span class="co2_cap">2</span></td>
                                </tr>
                                <tr>
                                    <td class="property_name">原油換算量合計</td>
                                    <td class="property_value"> {{ $totalResults['equivalent_oil'] }}リットル</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                @endif
            </div>
            <div>
                <button class="btn-primary-fill center print_btn" onclick="printSuccess()">印刷</button>
            </div>
        </div>
    </section><!-- modalshift__content [01] -->
    
    <a class="back-to-top"  href="#top">
        <span>TOPに戻る</span>
    </a>
@endsection
