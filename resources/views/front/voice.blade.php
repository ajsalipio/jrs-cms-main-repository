
@extends('layouts.front')
@section('title', $headAdminTitle)
@section('mainClass', 'child voice')
@section('content')
		<section class="child__kv">
			<div class="container">
				<div class="breadcrumbs__container">
					<ul>
						<li><a href="{{url('/')}}">TOP</a></li>
						<li class="active"><a href="{{url('voice')}}">お客様の声</a></li>
						<li class="active"><a href="#">Detail{{$voice->id}}</a></li>
					</ul>
				</div>
			</div>
		</section><!-- child__kv -->

		<section class="template__content voice_detail__content">
			<div class="container">
				<div class="content__container">
					<div class="voice_detail__writeup">
						<h3>{{$voice->cv_title}}</h3>
						<p>{{$voice->cv_company}}</p>
						<br>
						<div class="row__container img_row" id="eyecatch" @if($voice->cv_picture )
							style ="background-image: url('{{asset($voice->media->path)}}')"
							@endif  ></div>
						<br>
						{!!$voice->cv_content!!}
					</div>
					<div class="close_btn__container">
						<a href="{{url('voice')}}" class="btn btn-primary">× CLOSE</a>
					</div>
				</div>
			</div>
		</section> <!-- .voice_detail__content -->

		<a class="back-to-top"  href="#top">
			<span>TOP<br class="sp">に戻る</span>
		</a>
@endsection
@section('scripts')
@endsection()

