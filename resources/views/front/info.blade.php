@extends('layouts.front')
@section('title', $headAdminTitle)
@section('mainClass', 'child info')
@section('content')
<section class="child__kv template__content info_detail__content">
	<div class="container">
		<div class="breadcrumbs__container">
	<ul> 
				<li><a href="{{url('/')}}">TOP</a></li>
				<li class="active"><a href="{{url('/info')}}">最新情報</a></li>
				<li class="active"><a href="#">Detail{{ $data['id'] }}</a></li>
			</ul>
		</div>
		<div class="kv__btn">
	@foreach(config('constants.NEWS_CATEGORIES') as $key => $value)
		@if($categories)
			@foreach($categories as $category)
				@if ($category == $key)
							<div class="btn {{$catClasses[$key]}} active">{{ $value }}</div>
				@endif
			@endforeach
		@endif
	@endforeach
	</div>
		<div class="header__title">
			@if ($data->regional_category)
				[{{ $data->regional_category }}]
			@endif
			{{ $data->news_title }}
		</div>
		<div class="content__container">

			<div class="info_detail__writeup">
				{!! $data->news_content !!}
				@if (empty($data->is_link) && $data->file_pdf)
					<a href="{{ asset($data->media->path) }}" target="_blank"> {{
						$data->media->original }}</a>
				@endif
			</div>
			<a href="{{url('/info')}}" class="btn btn-blue">< 一覧に戻る</a>
		</div>
	</div>
</section> <!-- .info_detail__content -->
@endsection
@section('scripts')
@endsection()

