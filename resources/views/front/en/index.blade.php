@extends('layouts.front_en')
@section('title', 'Homepage')
@section('mainClass', 'child top-en')
@section('content')

		<section class="child__kv">
			<div class="container">
				<div class="kv__container" style="background-image: url('{{ asset('front_images/images/en/kv_00.png')}}">
					<h1 class="kv__title">YES WE DO</h1>
				</div>
				<div class="breadcrumbs__container">
					<ul>
						<li><a href="{{ url('en/')}}">TOP</a></li>
						<li class="active"><a href="{{ url('en/')}}">English</a></li>
					</ul>
				</div>
			</div>
		</section> <!-- .child__kv -->

		<section class="template__content">
			<div class="container">
				<h2 class="header__title">
					Serving People and the Community
				</h2>
				<div class="content__container">
					<div class="top-en__writeup">
						<p>Giving and receiving, a source of satisfaction and shared understanding. </p>
						<p>Through rail transportation, JR Freight delivers to its customers the value of material goods, new benefit from assured service. We are committed to benefiting people and supporting their activities nationwide. </p>
						<p>At the dawn of the 21st century, the world faces an uncertain future. Environmental and energy problems, traffic congestion, and labor costs. Logistics will become increasingly complex. At JR Freight, we can meet the challenges. Offering clean, reliable and efficient mass transportation services, we will continue to build new distribution systems geared to a new era.</p>
					</div>
				</div>
			</div>
		</section>

@endsection