@extends('layouts.front_en')
@section('title', 'Lifeline')
@section('mainClass', 'child en')
@section('content')

		<section class="child__kv">
			<div class="container">
				<div class="kv__container" style="background-image: url('{{ asset('front_images/images/en/kv_02.jpg')}}">
					<h1 class="kv__title">Lifeline</h1>
				</div>
				<div class="breadcrumbs__container">
					<ul>
						<li><a href="{{ url('en/')}}">TOP</a></li>
						<li class="active"><a href="">Lifeline (for RAILWAY BUSINESS)</a></li>
					</ul>
				</div>
			</div>
		</section> <!-- .child__kv -->

		<section class="template__content lifeline__content">
			<div class="container">
				<h2 class="header__title">
					Committed to safe, dependable rail services
				</h2>
				<div class="content__container">
					<p>With up-to-date systems to handle any disruption caused by natural disaster or failure, JR Freight keeps strict round-theclock watch on all vital freight services.</p>
					<div class="row__container type2">
						<div class="col">
							<div class="img__container">
								<img src="{{ asset('front_images/images/en/lifeline_pic_01.jpg') }}" alt="">
							</div>
							<p>The disassembled chassis and bogie undergo thorough checks</p>
						</div>
						<div class="col">
							<div class="img__container">
								<img src="{{ asset('front_images/images/en/lifeline_pic_02.jpg') }}" alt="">
							</div>
							<p>We use the latest maintenance equipment to inspect the tracks</p>
						</div>
					</div>
				</div>
				<div class="content__container">
					<div class="img__container">
						<img src="{{ asset('front_images/images/en/lifeline_pic_03.png') }}" alt="">
					</div>
					<br>
					<div class="img__container">
						<img src="{{ asset('front_images/images/en/lifeline_pic_04.png') }}" alt="">
					</div>
				</div>
			</div>
		</section> <!-- section01 -->

@endsection