@extends('layouts.front_en')
@section('title', 'Corporate Overview')
@section('mainClass', 'child corporate_overview')
@section('content')


		<section class="child__kv">
			<div class="container">
				<div class="kv__container" style="background-image: url('{{ asset('front_images/images/en/kv_01.jpg')}}">
					<h1 class="kv__title">Corporate Overview</h1>
				</div>
				<div class="breadcrumbs__container">
					<ul>
						<li><a href="{{ url('en/')}}">TOP</a></li>
						<li class="active"><a href="">Corporate Overview (in CORPORATE DATA)</a></li>
					</ul>
				</div>
			</div>
		</section> <!-- .child__kv -->

		<section class="template__content">
			<div class="container">
				<h2 class="header__title">
					Corporate Overview
				</h2>
				<div class="content__container">
					<div class="table__container">
						<table class="en-table01 corpo__table">
							<tbody>
								<tr>
									<td>Company Name</td>
									<td>Japan Freight Railway Company</td>
								</tr>
								<tr>
									<td>Establishment</td>
									<td>April 1, 1987</td>
								</tr>
								<tr>
									<td>Head Office</td>
									<td>5-33-8 Sendagaya, Shibuya-ku, Tokyo</td>
								</tr>
								<tr>
									<td>Capital</td>
									<td>19,000,000,000 yen (380,000 stocks)</td>
								</tr>
								<tr>
									<td>Stockholder</td>
									<td>Japan Railway Construction, Transport and Technology</td>
								</tr>
								<tr>
									<td>Representative</td>
									<td>Shuji Tamura, president</td>
								</tr>
								<tr>
									<td>Number of employee</td>
									<td>5,725persons (as of April 1, 2015)</td>
								</tr>
								<tr>
									<td>Business</td>
									<td>
										<ol>
											<li>Rail freight services</li>
											<li>Warehousing</li>
											<li>Car park operation</li>
											<li>Advertising</li>
											<li>Indemnity and other non-life insurance agency services</li>
											<li>Vehicle services</li>
											<li>General civil engineering and construction desin, project execution and management</li>
											<li>Incidital and related business operations</li>
										</ol>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</section> <!-- section01 -->

		<section class="template__content">
			<div class="container">
				<h2 class="header__title">
					Specification Management
				</h2>
				<div class="content__container">
					<div class="table__container">
						<p class="text--right">as of April 1, 2015</p>
						<table class="en-table01">
							<tbody>
								<tr>
									<td colspan="3" bgcolor="#d9e3ed">Number of Lines</td>
									<td>78</td>
								</tr>
								<tr>
									<td colspan="3" bgcolor="#d9e3ed">Operation Length (km)</td>
									<td>8,166.8</td>
								</tr>
								<tr>
									<td colspan="3" bgcolor="#d9e3ed">Number of Stations</td>
									<td>243</td>
								</tr>
								<tr>
									<td colspan="3" bgcolor="#d9e3ed">Number of Trains</td>
									<td>479 / day</td>
								</tr>
								<tr>
									<td colspan="3" bgcolor="#d9e3ed">Train-kilometers</td>
									<td>about 193,000 km / day</td>
								</tr>
								<tr>
									<td rowspan="2" colspan="3" bgcolor="#d9e3ed">
										Volume of Traffic
										<ul>
											<li>(Tonnage: thousands of ton / Ton-km: millions of ton-km)</li>
										</ul>
									</td>
									<td>Total Tonnage: 30,310</td>
								</tr>
								<tr>
									<td>Total Ton-km: 20,700</td>
								</tr>
								<tr>
									<td rowspan="6" bgcolor="#d9e3ed">Cars</td>
									<td bgcolor="#f2f9ff" rowspan="2">Locomotives</td>
									<td bgcolor="#f2f9ff">Electric</td>
									<td>442</td>
								</tr>
								<tr>
									<td bgcolor="#f2f9ff">Diesel</td>
									<td>175</td>
								</tr>
								<tr>
									<td bgcolor="#f2f9ff" colspan="2"> Electric Multipul Units</td>
									<td>42</td>
								</tr>
								<tr>
									<td rowspan="3" bgcolor="#f2f9ff">Freight Cars</td>
									<td>JR for Containers</td>
									<td>7,472</td>
								</tr>
								<tr>
									<td bgcolor="#f2f9ff">JR for Others</td>
									<td>79</td>
								</tr>
								<tr>
									<td bgcolor="#f2f9ff">Private</td>
									<td>1,869</td>
								</tr>
								<tr>
									<td bgcolor="#d9e3ed" rowspan="2">Containers</td>
									<td bgcolor="#f2f9ff" colspan="2">JR</td>
									<td>66,900</td>
								</tr>
								<tr>
									<td bgcolor="#f2f9ff" colspan="2">Private</td>
									<td>17,751</td>
								</tr>
								<tr>
									<td rowspan="2" colspan="2" bgcolor="d9e3ed">Loading Equipment</td>
									<td bgcolor="#f2f9ff">Top Lifters</td>
									<td>82</td>
								</tr>
								<tr>
									<td bgcolor="#f2f9ff">Forklifts</td>
									<td>446</td>
								</tr>
							</tbody>
						</table>
						<p>*For the year</p>
					</div>
				</div>
			</div>
		</section> <!-- section02 -->

@endsection