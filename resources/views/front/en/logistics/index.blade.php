@extends('layouts.front_en')
@section('title', 'Logistics')
@section('mainClass', 'child en')
@section('content')

	<main class="child en">

		<section class="child__kv">
			<div class="container">
				<div class="kv__container" style="background-image: url('{{ asset('front_images/images/en/kv_02.jpg')}}">
					<h1 class="kv__title">Logistics</h1>
				</div>
				<div class="breadcrumbs__container">
					<ul>
						<li><a href="{{ url('en/')}}">TOP</a></li>
						<li class="active"><a href="">Logistics (for RAILWAY BUSINESS)</a></li>
					</ul>
				</div>
			</div>
		</section> <!-- .child__kv -->

		<section class="template__content">
			<div class="container">
				<h2 class="header__title">
					Environmentally friendly rail transportation
				</h2>
				<div class="content__container">
					<p>We can meet a wide range of customer needs using our two major transportation systems: Container transportation on flatbed trains, and our freight car service for specialized cargo.</p>
					<div class="img__container--single">
						<img src="{{ asset('front_images/images/en/logistics_pic_01.jpg') }}" alt="">
						<p class="caption">The ECO-POWER hauling container cars.</p>
					</div>
					<p>Containers are our primary mode of transportation, linking 140 container rail terminals nationwide with road, sea, and air routes. Taking advantage of the JR Freight nationwide network, we handle everything -- from household necessities to frozen, fresh, and processed foods, from consumer durables to automobiles and waste products.</p>
					<div class="img__container--single">
						<img src="{{ asset('front_images/images/en/logistics_pic_02.jpg') }}" alt="">
						<p class="caption">Freight car transportation of bulky, large-volume cargo</p>
					</div>
					<p>Our powerful freight trains are specialized for oil or cement, limestone, chemicals, bulky machinery and other materials, Maximizing the advantages of  rail transport, JR Freight makes a significant contribution to business and industry in Japan.</p>
				</div>
			</div>
		</section> <!-- section01 -->

@endsection