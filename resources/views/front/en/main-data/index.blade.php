@extends('layouts.front_en')
@section('title', 'Main Data')
@section('mainClass', 'child en main-data')
@section('content')

		<section class="child__kv">
			<div class="container">
				<div class="kv__container" style="background-image: url('{{ asset('front_images/images/en/kv_01.jpg')}}">
					<h1 class="kv__title">Main Data</h1>
				</div>
				<div class="breadcrumbs__container">
					<ul>
						<li><a href="{{ url('en/')}}">TOP</a></li>
						<li class="active"><a href="">Main Data (in CORPORATE DATA)</a></li>
					</ul>
				</div>
			</div>
		</section> <!-- .child__kv -->

		<section class="template__content">
			<div class="container">
				<div class="content__container">
					<div class="img__container">
						<h3>Income</h3>
						<img src="{{ asset('front_images/images/en/main-data_pic_01.png') }}" alt="">
					</div>
				</div>
				<div class="content__container">
					<div class="img__container">
						<h3>Freight volume</h3>
						<img src="{{ asset('front_images/images/en/main-data_pic_02.png') }}" alt="">
					</div>
				</div>
				<div class="content__container">
					<div class="row__container type2">
						<div class="col">
							<div class="img__container">
								<h3>Container freight volume <br>by category <span>(2014)</span></h3>
								<img src="{{ asset('front_images/images/en/main-data_pic_03.png') }}" alt="">
							</div>
							<br class="sp">
						</div>
						<div class="col">
							<div class="img__container">
								<h3>Freight car freight volume<br>by category <span>(2014)</span></h3>
								<img src="{{ asset('front_images/images/en/main-data_pic_04.png') }}" alt="">
							</div>
						</div>
					</div>
				</div>
			</div>
		</section> <!-- section01 -->

@endsection