@extends('layouts.front_en')
@section('title', 'On Time')
@section('mainClass', 'child en')
@section('content')

		<section class="child__kv">
			<div class="container">
				<div class="kv__container" style="background-image: url('{{ asset('front_images/images/en/kv_02.jpg')}}">
					<h1 class="kv__title">On Time</h1>
				</div>
				<div class="breadcrumbs__container">
					<ul>
						<li><a href="{{ url('en/')}}">TOP</a></li>
						<li class="active"><a href="">On Time (for RAILWAY BUSINESS)</a></li>
					</ul>
				</div>
			</div>
		</section> <!-- .child__kv -->

		<section class="template__content on-time__content">
			<div class="container">
				<h2 class="header__title">
					Punctual and reliable
				</h2>
				<div class="content__container">
					<p>Forget congestion on the roads. With rail transportation, the time of arrival is assured. We are constantly working to make our freight services faster, to deliver reliable, risk-free transportation using our up-to-date freight management system.</p>
					<div class="row__container type2">
						<div class="col">
							<div class="img__container">
								<img src="{{ asset('front_images/images/en/on-time_pic_01.jpg') }}" alt="">
							</div>
							<p>The Super Liner container express service links all Japan's main cities</p>
						</div>
						<div class="col">
							<div class="img__container">
								<img src="{{ asset('front_images/images/en/on-time_pic_02.jpg') }}" alt="">
							</div>
							<p>The Tokyo Freight Terminal, JR Freight's largest container hub, linking the Tokyo region with western Japan</p>
						</div>
					</div>
					<br class="pc">
					<div class="row__container type2">
						<div class="col">
							<div class="img__container">
								<img src="{{ asset('front_images/images/en/on-time_pic_03.jpg') }}" alt="">
							</div>
							<p>Following signaled instructions, trains are kept precisely on schedule</p>
						</div>
						<div class="col">
							<div class="img__container">
								<img src="{{ asset('front_images/images/en/on-time_pic_04.jpg') }}" alt="">
							</div>
							<p>Using the E&S system, containers are loaded and unloaded directly on the tracks</p>
						</div>
					</div>
				</div>
				<div class="content__container">
					<div class="img__container">
						<img src="{{ asset('front_images/images/en/on-time_pic_05.png') }}" alt="">
					</div>
				</div>
			</div>
		</section> <!-- section01 -->

@endsection