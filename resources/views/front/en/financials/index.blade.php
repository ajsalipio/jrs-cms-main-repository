@extends('layouts.front_en')
@section('title', 'Financials')
@section('mainClass', 'child en')
@section('content')

		<section class="child__kv">
			<div class="container">
				<div class="kv__container" style="background-image: url('{{ asset('front_images/images/en/kv_01.jpg')}}">
					<h1 class="kv__title">Financials</h1>
				</div>
				<div class="breadcrumbs__container">
					<ul>
						<li><a href="{{ url('en/')}}">TOP</a></li>
						<li class="active"><a href="">Financials (in CORPORATE DATA)</a></li>
					</ul>
				</div>
			</div>
		</section> <!-- .child__kv -->

		<section class="template__content financial">
			<div class="container">
				<div class="table__container">
					<div class="content__container financial__container">
						<div class="financial__tbl--balance">
							<p>Balance Sheet</p>
							<div class="table__container--balance">
								<table class="en-table02">
									<tbody>
										<tr>
											<td colspan="2">(as of March 31,2010)</td>
										</tr>
										<tr>
											<th class="bordered">Item</th>
											<th class="bordered">Amount</th>
										</tr>
										<tr>
											<td><b>Assets</b></td>
											<td></td>
										</tr>
										<tr>
											<td><b>Current Assets</b></td>
											<td><b>38,848</b></td>
										</tr>
										<tr>
											<td><span>Cash and equivalents</span></td>
											<td>15,078</td>
										</tr>
										<tr>
											<td><span>Accounts receivable-trade</span></td>
											<td>12,385</td>
										</tr>
										<tr>
											<td><span>Accounts receivable</span></td>
											<td>2,709</td>
										</tr>
										<tr>
											<td><span>Consumption tax receivable  etc.</span></td>
											<td>251</td>
										</tr>
										<tr>
											<td><span>Accounts income</span></td>
											<td>511</td>
										</tr>
										<tr>
											<td><span>Stores</span></td>
											<td>2,051</td>
										</tr>
										<tr>
											<td><span>Advance</span></td>
											<td>3,647</td>
										</tr>
										<tr>
											<td><span>Deferred income taxes</span></td>
											<td>1,575</td>
										</tr>
										<tr>
											<td><span>Other</span></td>
											<td>636</td>
										</tr>
										<tr>
											<td><b>Fixed Assets</b></td>
											<td><b>327,691</b></td>
										</tr>
										<tr>
											<td><span>Fixtures and other structures exclusively for Railway division</span></td>
											<td>230,262</td>
										</tr>
										<tr>
											<td><span>Fixed assets of affiliated business</span></td>
											<td>46,015</td>
										</tr>
										<tr>
											<td><span>Fixtures and other equipment for all divisions</span></td>
											<td>14,316</td>
										</tr>
										<tr>
											<td><span>Other fixed property assets</span></td>
											<td>13</td>
										</tr>
										<tr>
											<td><span>Construction in progress</span></td>
											<td>10,994</td>
										</tr>
										<tr>
											<td><span>Investment and other assets</span></td>
											<td>26,087</td>
										</tr>
										<tr>
											<td><span>Investments in subsidiaries and affiliates</span></td>
											<td>9,050</td>
										</tr>
										<tr>
											<td><span>Other marketable securities</span></td>
											<td>709</td>
										</tr>
										<tr>
											<td><span>Long-term prepaid expenses</span></td>
											<td>3,567</td>
										</tr>
										<tr>
											<td><span>Long-term deferred income taxes</span></td>
											<td>11,937</td>
										</tr>
										<tr>
											<td><span>Other</span></td>
											<td>865</td>
										</tr>
										<tr>
											<td><span>(Allowance of doubtful accounts)</span></td>
											<td>△42</td>
										</tr>
										<tr>
											<td class="bordered"><b>Total Assets</b></td>
											<td class="bordered"><b>366,539</b></td>
										</tr>
									</tbody>
								</table>
							
								<table class="en-table02">
									<tbody>
										<tr>
											<td colspan="2" class="text--right">(unit: million yen)</td>
										</tr>
										<tr>
											<th class="bordered">Item</th>
											<th class="bordered">Amount</th>
										</tr>
										<tr>
											<td><b>Liabilities</b></td>
											<td></td>
										</tr>
										<tr>
											<td><b>Current Liabilities</b></td>
											<td><b>51,179</b></td>
										</tr>
										<tr>
											<td><span>Long-term debt to be Redeemed within 1 year</span></td>
											<td>15,918</td>
										</tr>
										<tr>
											<td><span>Accounts payable</span></td>
											<td>19,885</td>
										</tr>
										<tr>
											<td><span>Accrued expenses</span></td>
											<td>930</td>
										</tr>
										<tr>
											<td><span>Accrued income taxes</span></td>
											<td>287</td>
										</tr>
										<tr>
											<td><span>Advance received</span></td>
											<td>4,491</td>
										</tr>
										<tr>
											<td><span>Deferred revenue</span></td>
											<td>1,873</td>
										</tr>
										<tr>
											<td><span>Bonus reserves</span></td>
											<td>3,482</td>
										</tr>
										<tr>
											<td><span>Reserve for environmental expenditures</span></td>
											<td>33</td>
										</tr>
										<tr>
											<td><span>Other</span></td>
											<td>4,276</td>
										</tr>
										<tr>
											<td><b>Long-term Liabilities</b></td>
											<td><b>273,607</b></td>
										</tr>
										<tr>
											<td><span>Long-term debt</span></td>
											<td>145,101</td>
										</tr>
										<tr>
											<td><span>Long-term loan from controlling shareholders</span></td>
											<td>31,954</td>
										</tr>
										<tr>
											<td><span>Accrued severance and retirement allowances</span></td>
											<td>48,721</td>
										</tr>
										<tr>
											<td><span>Reserve for environmental expenditures</span></td>
											<td>356</td>
										</tr>
										<tr>
											<td><span>Guarantee deposits received</span></td>
											<td>35,117</td>
										</tr>
										<tr>
											<td><span>Other</span></td>
											<td>12,355</td>
										</tr>
										<tr>
											<td class="bordered"><b>Total liabilities</b></td>
											<td class="bordered"><b>324,786</b></td>
										</tr>
										<tr>
											<td><b>Section of Total Net Assets Capital stock</b></td>
											<td><b>42,189</b></td>
										</tr>
										<tr>
											<td><span>Capital fund</span></td>
											<td>19,000</td>
										</tr>
										<tr>
											<td><span>Additional paid-in capital</span></td>
											<td>15,300</td>
										</tr>
										<tr>
											<td><span>capital appropriation</span></td>
											<td>15,300</td>
										</tr>
										<tr>
											<td><span>Accumulated income</span></td>
											<td>7,889</td>
										</tr>
										<tr>
											<td><span class="ind--2">Other accumulated income</span></td>
											<td>7,889</td>
										</tr>
										<tr>
											<td><span class="ind--3">Compressed accumulated fund</span></td>
											<td>12,950</td>
										</tr>
										<tr>
											<td><span class="ind--3">Compressed special account accumulated fund</span></td>
											<td>45</td>
										</tr>
										<tr>
											<td><span class="ind--3">Special amortization reserves</span></td>
											<td>△5,107</td>
										</tr>
										<tr>
											<td><b>Assessment/ exchange difference etc</b></td>
											<td><b>△436</b></td>
										</tr>
										<tr>
											<td><span>Exchange difference of other appraisal of securities</span></td>
											<td>6</td>
										</tr>
										<tr>
											<td><span>Carry-over Hedge gain and loss</span></td>
											<td>△442</td>
										</tr>
										<tr>
											<td class="bordered"><b>Total net assets</b></td>
											<td class="bordered"><b>41,753</b></td>
										</tr>
										<tr>
											<td class="bordered"><b>Debt and total net assets</b></td>
											<td class="bordered"><b>366,539</b></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="financial__tbl--statement">
							<p>Statements of Income</p>
							<table class="en-table02">
								<tbody>
									<tr>
										<td colspan="2">(for the period of April 2009-March 31 2010)</td>
									</tr>
									<tr>
										<th class="bordered">Item</th>
										<th class="bordered">Amount</th>
									</tr>
									<tr>
										<td><b>Railroading</b></td>
										<td></td>
									</tr>
									<tr>
										<td><span>Operating revenues</span></td>
										<td>137,010</td>
									</tr>
									<tr>
										<td><span>Operating expenses</span></td>
										<td>147,146</td>
									</tr>
									<tr>
										<td><span>Operating loss</span></td>
										<td>10,136</td>
									</tr>
									<tr>
										<td><b>Rail-related business</b></td>
										<td></td>
									</tr>
									<tr>
										<td><span>Operating revenues</span></td>
										<td>15,222</td>
									</tr>
									<tr>
										<td><span>Operating expenses</span></td>
										<td>6,603</td>
									</tr>
									<tr>
										<td><span>Operating income</span></td>
										<td class="border--bottom">8,619</td>
									</tr>
									<tr>
										<td><b>Entire business operating loss</b></td>
										<td><b>1,517</b></td>
									</tr>
									<tr>
										<td><b>Non-operating revenues</b></td>
										<td><b>595</b></td>
									</tr>
									<tr>
										<td><b>Non-operating expenses</b></td>
										<td><b>3,713</b></td>
									</tr>
									<tr>
										<td><b>Ordinary loss</b></td>
										<td><b>4,635</b></td>
									</tr>
									<tr>
										<td><b>Extraordinary profit</b></td>
										<td class="border--bottom"><b>3,176</b></td>
									</tr>
									<tr>
										<td><b>Extraordinary loss</b></td>
										<td class="border--bottom"><b>2,547</b></td>
									</tr>
									<tr>
										<td><b>Loss before tax</b></td>
										<td><b>4,006</b></td>
									</tr>
									<tr>
										<td><b>Income taxes</b></td>
										<td><b>175</b></td>
									</tr>
									<tr>
										<td><b>Adjustment for income taxes</b></td>
										<td><b>△1,422</b></td>
									</tr>
									<tr>
										<td class="border--bottom"><b>Net loss</b></td>
										<td class="bordered"><b>2,759</b></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<table class="en-table0">
					<tr>
						<td colspan="3">Note:Amounts worth less than 1 million yen have been dropped.</td>
						<td colspan="1" class="text--right">(unit:million yen)</td>
					</tr>
				</table>
			</div>
		</section> <!-- section01 -->

@endsection