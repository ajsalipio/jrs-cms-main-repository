@extends('layouts.front_en')
@section('title', 'Service')
@section('mainClass', 'child en service')
@section('content')

		<section class="child__kv">
			<div class="container">
					<div class="kv__container" style="background-image: url('{{ asset('front_images/images/en/kv_01.jpg')}}">
					<h1 class="kv__title">Service</h1>
				</div>
				<div class="breadcrumbs__container">
					<ul>
						<li><a href="{{ url('en/')}}">TOP</a></li>
						<li class="active"><a href="">Service (in CORPORATE DATA)</a></li>
					</ul>
				</div>
			</div>
		</section> <!-- .child__kv -->

		<section class="template__content">
			<div class="container">
				<div class="content__container">
					<div class="img__container">
						<img src="{{ asset('front_images/images/en/service_pic_01.png') }}" alt="">
					</div>
				</div>
			</div>
		</section> <!-- section01 -->

@endsection