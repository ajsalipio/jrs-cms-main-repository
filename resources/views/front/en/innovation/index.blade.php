@extends('layouts.front_en')
@section('title', 'Innovation')
@section('mainClass', 'child en')
@section('content')

		<section class="child__kv">
			<div class="container">
				<div class="kv__container" style="background-image: url('{{ asset('front_images/images/en/kv_02.jpg')}}">
					<h1 class="kv__title">Innovation</h1>
				</div>
				<div class="breadcrumbs__container">
					<ul>
						<li><a href="{{ url('en/')}}">TOP</a></li>
						<li class="active"><a href="">Innovation (for RAILWAY BUSINESS)</a></li>
					</ul>
				</div>
			</div>
		</section> <!-- .child__kv -->

		<section class="template__content lifeline__content">
			<div class="container">
				<h2 class="header__title">
					Responding to next-generation needs
				</h2>
				<div class="content__container">
					<p>JR Freight embraces the latest technology in developing innovative locomotives, freight trains, and containers to meet Japan's evolving transportation needs.</p>
					<div class="img__container--single">
						<img src="{{ asset('front_images/images/en/innovation_pic_01.jpg') }}" alt="">
					</div>
					<p>M250 express container train, 'Super Rail Cargo' (The firstly developed power-diffused express container train for the purpose of highspeed transportation)</p>
					<div class="row__container type2">
						<div class="col">
							<div class="img__container">
								<img src="{{ asset('front_images/images/en/innovation_pic_02.jpg') }}" alt="">
							</div>
							<p>EF510 AC/DC electric locomotive (in service on AC/DC electrified portions of Hokuriu Line, Shinetsu Line, Kosei Line, Tokaido Line, etc.)</p>
						</div>
						<div class="col">
							<div class="img__container">
								<img src="{{ asset('front_images/images/en/innovation_pic_03.jpg') }}" alt="">
							</div>
							<p>EH200 DC electric locomotive (in service on DC electrified portions of Chuo and Shinonoi Lines)</p>
						</div>
					</div>
					<br class="pc">
					<div class="row__container type2">
						<div class="col">
							<div class="img__container">
								<img src="{{ asset('front_images/images/en/innovation_pic_04.jpg') }}" alt="">
							</div>
							<p>The ECO-POWER 'Kintaro', an EH500-model AC/DC electric locomotive (main routes: Tohoku Line and Tsugaru Kaikyo Line)</p>
						</div>
						<div class="col">
							<div class="img__container">
								<img src="{{ asset('front_images/images/en/innovation_pic_05.jpg') }}" alt="">
							</div>
							<p>The ECO-POWER 'Momotaro', an EF210-model DC electric locomotive (main routes: Tokaido and Sanyo Lines)</p>
						</div>
					</div>
					<br class="pc">
					<div class="row__container type2">
						<div class="col">
							<div class="img__container">
								<img src="{{ asset('front_images/images/en/innovation_pic_06.jpg') }}" alt="">
							</div>
							<p>New tank trains are capable of speeds of up to 95 km/h, as fast as standard container trains</p>
						</div>
						<div class="col">
						</div>
					</div>
				</div>
			</div>
		</section> <!-- section01 -->

@endsection