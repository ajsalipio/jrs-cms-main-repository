@extends('layouts.front_en')
@section('title', 'Material Procurement Information')
@section('mainClass', 'child en')
@section('content')

		<section class="child__kv">
			<div class="container">
				<div class="kv__container" style="background-image: url('{{ asset('front_images/images/en/kv_01.jpg')}}">
					<h1 class="kv__title">Material Procurement Information</h1>
				</div>
				<div class="breadcrumbs__container">
					<ul>
						<li><a href="{{ url('en/')}}">TOP</a></li>
						<li class="active"><a href="">Material Procurement Information (in CORPORATE DATA)</a></li>
					</ul>
				</div>
			</div>
		</section> <!-- .child__kv -->

		<section class="template__content">
			<div class="container">
				<h2 class="header__title">
					Procurement Items
				</h2>
				<div class="content__container">
					<p>JR Freight procures materials, components, and supplies in the following categories: </p>
					<br>
					<ul class="list--square">
						<li>Rails and other track material </li>
						<li>Locomotive and Freight cars (and these parts) </li>
						<li>Signal and communication systems </li>
						<li>Electronic products</li>
						<li>Metal Industrial Products </li>
						<li>Fuel</li>
						<li>Machinery and Work Goods</li>
						<li>Miscellaneous</li>
					</ul>
					<p>With regard to the most recent achievement data, please refer to the page in Japanese. </p>
					<br>
					<p>If you are interested in beginning bussiness transactions with us, please contact us by e-mail (procurement@jrfreight.co.jp) and provide us with the following information: </p>
					<br>
					<ol>
						<li>Company name</li>
						<li>Address of head office </li>
						<li>Items you wish to supply</li>
						<li>Contact personnel (section, title, name, phone number, and e-mail address) </li>
					</ol>
					<br>
					<p>The information you provide will be used only for considering new transactions and handled in accordance with our privacy policy.</p>
				</div>
			</div>
		</section> <!-- section01 -->

@endsection