@extends('layouts.front_en')
@section('title', 'Philosophy')
@section('mainClass', 'child en')
@section('content')

		<section class="child__kv">
			<div class="container">
				<div class="kv__container" style="background-image: url('{{ asset('front_images/images/en/kv_01.jpg')}}">
					<h1 class="kv__title">Philosophy</h1>
				</div>
				<div class="breadcrumbs__container">
					<ul>
						<li><a href="{{ url('en/')}}">TOP</a></li>
						<li class="active"><a href="">Philosophy(in CORPORATE DATA)</a></li>
					</ul>
				</div>
			</div>
		</section> <!-- .child__kv -->

		<section class="template__content">
			<div class="container">
				<h2 class="header__title">
					Corporate Purpose
				</h2>
				<div class="content__container">
					<div class="top-en__writeup">
						<p>We respond to the trust placed in us by customers and contribute to the realization of an abundant society by creating comprehensive exchanges of goods, value, and information in the context of rail freight transport and with an eye to the future role of that transport. </p>
					</div>
				</div>
			</div>
			<div class="container">
				<h2 class="header__title">
					Corporate Guidelines
				</h2>
				<div class="content__container">
					<div class="top-en__writeup">
						<ol>
							<li>Propagate distribution that generates new value.</li>
							<li>Undertake work that opens a new generation. </li>
							<li>Build a company rich in humanity.</li>
						</ol>
					</div>
				</div>
			</div>
			<div class="container">
				<h2 class="header__title">
					Our Five Actions
				</h2>
				<div class="content__container">
					<div class="top-en__writeup">
						<ol>
							<li>Always act from the perspective of the customer</li>
							<li>Express opinions, listen to opinions, and take the initiative</li>
							<li>Tackle problems head-on, even in difficult times. </li>
							<li>Maintain a broad, outward-looking perspective. </li>
							<li>Always have a dream.</li>
						</ol>
					</div>
				</div>
			</div>
		</section>

@endsection