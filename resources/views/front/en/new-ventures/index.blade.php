@extends('layouts.front_en')
@section('title', 'NEW VENTURES')
@section('mainClass', 'child en')
@section('content')

		<section class="child__kv">
			<div class="container">
				<div class="kv__container" style="background-image: url('{{ asset('front_images/images/en/kv_03.jpg')}}">
					<h1 class="kv__title">NEW VENTURES</h1>
				</div>
				<div class="breadcrumbs__container">
					<ul>
						<li><a href="{{ url('en/')}}">TOP</a></li>
						<li class="active"><a href="">NEW VENTURES</a></li>
					</ul>
				</div>
			</div>
		</section> <!-- .child__kv -->

		<section class="template__content">
			<div class="container">
				<h2 class="header__title">
					Developing new businesses
				</h2>
				<div class="content__container">
					<p>JR Freight is engaged in a variety of new business operations, based on our core distribution business and utilizing our human resources and property assets.</p>
					<div class="img__container--single">
						<img src="{{ asset('front_images/images/en/new-ventures_pic_01.jpg') }}" alt="">
					</div>
					<p>I-GARDEN AIR, a modern and enjoyable high-rise urban complex that is transforming the former Iidamachi freight station Committed development of a high-rise office building complex </p>
					<br>
					<h3><b>I-GARDEN AIR </b></h3>
					<p>Iidabashi 3-chome in Chiyoda-ku (former site of the Iidamachi freight station) is home to a high-rise development that will invigorate the whole area. Utilizing the economies of scale on this prime JR site, we are fully committed to this leading project which will be pivotal to future business expansion.</p>
					<div class="img__container--full">
						<img src="{{ asset('front_images/images/en/new-ventures_pic_02.jpg') }}" alt="">
					</div>
					<p>Ario Kitasuna, a large and modern shopping mall that is transforming the former Onagigawa freight station</p>
					<br>
					<h3><b>Ario Kitasuna</b></h3>
					<p>JR Freight built the large commercial complex in Kohtoh-ku to lend the major retail dealer. For example, emergency information TV system for guests,these buildings have many kinds of facilities for environment and disaster prevention.</p>
					<br>
					<div class="img__container--full">
						<img src="{{ asset('front_images/images/en/new-ventures_pic_03.jpg') }}" alt="">
					</div>
					<p>Large-scale compound distribution facilities in freight terminal</p>
					<br>
					<h3><b>F-Plaza</b></h3>
					<p>Large-scale compound facilities that provide overall distribution functions such as disposal of goods, storage, distribution working, transshipment, etc. We will satisfy any types of transporting needs from customers by utilizing conveniences in combing with container transportation.</p>

				</div>
			</div>
		</section> <!-- section01 -->

@endsection