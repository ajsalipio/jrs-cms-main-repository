@extends('layouts.front_en')
@section('title', 'Ecology')
@section('mainClass', 'child en')
@section('content')

		<section class="child__kv">
			<div class="container">
				<div class="kv__container" style="background-image: url('{{ asset('front_images/images/en/kv_04.jpg')}}">
					<h1 class="kv__title">Ecology</h1>
				</div>
				<div class="breadcrumbs__container">
					<ul>
						<li><a href="{{ url('en/')}}">TOP</a></li>
						<li class="active"><a href="">Ecology (for RAILWAY BUSINESS)</a></li>
					</ul>
				</div>
			</div>
		</section> <!-- .child__kv -->

		<section class="template__content">
			<div class="container">
				<h2 class="header__title">
					Environmentally friendly rail transportation
				</h2>
				<div class="content__container">
					<p>Compared with trucks and other vehicles, rail transportation is cleaner, more energy-efficient, and able to carry mass goods and materials. These advantages make it an excellent option for solving our grave environmental and energy problems.</p>
					<div class="img__container--single">
						<img src="{{ asset('front_images/images/en/ecology_pic_01.jpg') }}" alt="">
					</div>
					<p>Rail freight can considerably reduce CO2,while helping mitigate traffic congestion and minimize energy consumption</p>
				</div><!-- content__container[01] -->
				<div class="content__container">
					<p>&#9632; CO2emissions per unit volume of transport (as of 2005)</p>
					<div class="img__container">
						<img src="{{ asset('front_images/images/en/ecology_pic_02.png') }}" alt="">
					</div>
					<p>&#9632; Rail transportation plays a key role in 'modal shift'</p><br>
					<p>Modal shift is the conversion of the method of internal main-line freight transportation from truck transport to rail and marine transport, a mass transport system. Specifically, It refers to the shift of ameans of transport to the one using railway service or others, thereby contributing to the solution of energy problems and medium-to-long term labor problems caused by aging populace with low birthrates as well as the reduction of environmental load.</p>
					<div class="img__container">
						<img src="{{ asset('front_images/images/en/ecology_pic_03.png') }}" alt="">
					</div>
				</div><!-- content__container[02] -->
			</div>
		</section> <!-- section01 -->

@endsection