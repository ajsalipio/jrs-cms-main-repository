<?php

	return [
		'REQUIRED'	=> 'The :attribute is required.',
		'STRING' 	=> 'The :attribute must be string.',
		'MIMES'		=> 'The :attribute must be :mimes',
		'MAX'		=> 'The :attribute must not be greater than :max',
		'FILE_SIZE'	=> 'The :attribute must not be greater than :max kilobytes'
	];

