<?php

/*
    |--------------------------------------------------------------------------
    | Messages
    |--------------------------------------------------------------------------
    |
    | These messages are used in javascript (.js) files for alert messages - grouped per module.
    | Note that NOT ALL messages are here and this is only for simple alert validations. 
    | Most validations are specified in App\Http\Requests directory via FormRequests.
    | It utilizes the dynamic japanese validation speciied in resources/lang/ja/validation.php
    |
    */

return [

    
    'DELETE_CHECKBOX_EMPTY'             => 'チェックボックスは、必ず指定してください。',
    'FILE_XLS_INVALID'                  => 'チェックボックスは、必ず指定してください。',
    'FILE_DOC_INVALID'                  => '列車運転計画には、xlsxタイプのファイルを指定してください。',
    'FILE_PIC_INVALID'                  => '画像ファイルには、jpg,png,jpegタイプのファイルを指定してください。',
    'FILE_PDF_INVALID'                  => 'PDFタイプのファイルを指定してください。',

    'PASSWORD_CONFIRM_INVALID'          => '入力されたパスワードと確認パスワードが一致してません',
];

