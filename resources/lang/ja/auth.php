<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => "登録されているメールアドレス、またはパスワードが無効です。<br>再度ご入力ください。",
    'throttle' => 'エラーは多い過ぎる.  :seconds 秒を待ってください。.',

];
